package threads.thor;


import android.content.Context;

import androidx.annotation.NonNull;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReentrantLock;

import tech.lp2p.Lite;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Server;
import threads.thor.data.blocks.BLOCKS;
import threads.thor.data.pages.PAGES;
import threads.thor.data.peers.PEERS;
import threads.thor.model.API;

final class TestEnv {
    public static final String AGENT = "lite/1.0.0/";
    private static final String TAG = TestEnv.class.getSimpleName();

    @NonNull
    private static final AtomicReference<Server> SERVER = new AtomicReference<>();
    @NonNull
    private static final ReentrantLock reserve = new ReentrantLock();
    private static Lite.Settings SETTINGS;
    private static volatile Lite INSTANCE = null;

    static {
        try {
            SETTINGS = Lite.createSettings(Lite.generateKeys(), API.bootstrap(), AGENT);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }


    public static Lite getTestInstance(@NonNull Context context) throws Exception {
        reserve.lock();
        try {

            Objects.requireNonNull(SETTINGS);
            Lite lite = TestEnv.getInstance(SETTINGS);


            PAGES.getInstance(context).clear(); // clear the page store

            if (SERVER.get() == null) {

                Server server = lite.startServer(
                        Lite.createServerSettings(5001),
                        BLOCKS.getInstance(context),
                        PEERS.getInstance(context),
                        peeraddr -> LogUtils.error(TAG, "Incoming connection : "
                                + peeraddr.toString()),
                        peeraddr -> LogUtils.error(TAG, "Closing connection : "
                                + peeraddr.toString()),
                        reservationGain -> LogUtils.error(TAG, "Reservation gain " +
                                reservationGain.toString()),
                        reservationLost -> LogUtils.error(TAG, "Reservation lost " +
                                reservationLost.toString()),
                        peerId -> {
                            LogUtils.error(TAG, "Peer Gated : " + peerId.toString());
                            return false;
                        },
                        request -> null,
                        envelope -> {
                        });

                // just to get coverage of shutdown method
                Runtime.getRuntime().addShutdownHook(new Thread(server::shutdown));
                SERVER.set(server);

                if (Lite.reservationFeaturePossible()) {
                    Lite.hopReserve(server, 25, 120);

                    Peeraddrs set = Lite.reservationPeeraddrs(server);
                    for (Peeraddr peeraddr : set) {
                        LogUtils.info(TAG, "Dialable Address " + peeraddr.toString());
                    }
                }
            }

            return lite;
        } finally {
            reserve.unlock();
        }
    }

    @NonNull
    public static Lite getInstance(@NonNull Lite.Settings settings) throws Exception {
        if (INSTANCE == null) {
            synchronized (Lite.class) {
                if (INSTANCE == null) {
                    INSTANCE = new Lite(settings);
                }
            }
        }
        return INSTANCE;
    }


}

package threads.thor;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;

import junit.framework.TestCase;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import tech.lp2p.Lite;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Raw;
import tech.lp2p.core.Session;
import threads.thor.data.blocks.BLOCKS;
import threads.thor.data.pages.PAGES;
import threads.thor.data.pages.Page;
import threads.thor.data.peers.PEERS;


public class PageTest {

    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void test_page() throws Exception {

        Lite lite = TestEnv.getTestInstance(context);

        Session session = lite.createSession(
                BLOCKS.getInstance(context), PEERS.getInstance(context));
        String content = "Hallo dfsadf";
        Raw text = Lite.storeText(session, content);
        TestCase.assertNotNull(text);
        PAGES pageStore = PAGES.getInstance(context);

        Page page = pageStore.page(lite.self());
        Assert.assertNull(page);


        page = new Page(lite.self(), text.cid());

        TestCase.assertNotNull(page);

        pageStore.store(page);

        page = pageStore.page(lite.self());
        TestCase.assertNotNull(page);

        Cid cmp = page.cid();
        TestCase.assertEquals(text.cid(), cmp);

        cmp = pageStore.cid(lite.self());
        TestCase.assertEquals(text.cid(), cmp);


    }

}
package threads.thor.ui;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.Objects;

import threads.thor.R;
import threads.thor.model.API;

public class InfoFragment extends DialogFragment {

    public static final String TAG = InfoFragment.class.getSimpleName();
    public static final String URL = "url";
    public static final String TITLE = "title";
    public static final String ICON = "icon";

    public static InfoFragment newInstance(String url, String title, @Nullable Bitmap bitmap) {
        InfoFragment fragment = new InfoFragment();
        Bundle bundle = new Bundle();
        bundle.putString(URL, url);
        bundle.putString(TITLE, title);
        if (bitmap != null) {
            bundle.putParcelable(ICON, bitmap);
        }
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_info, container, false);

        Bundle args = getArguments();
        Objects.requireNonNull(args);
        String url = args.getString(URL);
        String title = args.getString(TITLE);
        Bitmap icon = args.getParcelable(ICON, Bitmap.class);

        ImageView info_icon = view.findViewById(R.id.info_icon);
        ImageView imageView = view.findViewById(R.id.info_code);
        TextView titleView = view.findViewById(R.id.info_title);

        Uri uri = Uri.parse(url);
        if (title == null || title.isEmpty()) {
            title = API.getUriTitle(uri);
        }
        titleView.setText(title);


        if (Objects.equals(uri.getScheme(), API.FILE_SCHEME)) {
            info_icon.setVisibility(View.VISIBLE);
            info_icon.setImageResource(R.drawable.bookmark_mht);
        } else if (Objects.equals(uri.getScheme(), API.PNS_SCHEME)) {
            info_icon.setVisibility(View.VISIBLE);
            info_icon.setImageResource(R.drawable.bookmark_pns);
        } else {
            if (icon != null) {
                info_icon.setVisibility(View.VISIBLE);
                info_icon.setImageBitmap(icon);
            } else {
                info_icon.setVisibility(View.GONE);
            }
        }
        Objects.requireNonNull(url);
        TextView page = view.findViewById(R.id.info_page);
        if (url.isEmpty()) {
            page.setVisibility(View.GONE);
        } else {
            page.setText(url);
        }

        boolean isHomepage = Objects.equals(API.getHomepage(requireContext()), url);

        CheckBox homepage = view.findViewById(R.id.info_homepage);
        homepage.setEnabled(!Objects.equals(uri.getScheme(), API.FILE_SCHEME));
        homepage.setChecked(isHomepage);
        homepage.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                API.setHomepage(requireContext(), url);
            } else {
                API.setHomepage(requireContext(), null);
            }
        });

        Bitmap bitmap = API.getBitmap(url);
        imageView.setImageBitmap(bitmap);
        return view;
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        BottomSheetDialog dialog = new BottomSheetDialog(requireContext());
        BottomSheetBehavior<FrameLayout> behavior = dialog.getBehavior();
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        behavior.setPeekHeight(0);
        return dialog;
    }

}

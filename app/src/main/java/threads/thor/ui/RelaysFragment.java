package threads.thor.ui;

import android.app.Dialog;
import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.Objects;

import tech.lp2p.core.PeerId;
import threads.thor.LogUtils;
import threads.thor.R;
import threads.thor.state.StateModel;
import threads.thor.utils.RelaysAdapter;


public class RelaysFragment extends BottomSheetDialogFragment {

    public static final String TAG = RelaysFragment.class.getSimpleName();

    public static final String PID = "pid";

    public static RelaysFragment newInstance(@NonNull PeerId peerId) {
        RelaysFragment fragment = new RelaysFragment();
        Bundle bundle = new Bundle();
        bundle.putString(PID, peerId.toBase58());
        fragment.setArguments(bundle);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Bundle args = getArguments();
        Objects.requireNonNull(args);
        String peerId = args.getString(PID);
        Objects.requireNonNull(peerId);

        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        BottomSheetBehavior<FrameLayout> behavior = dialog.getBehavior();
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        behavior.setPeekHeight(0);

        dialog.setContentView(R.layout.fragment_relays);

        StateModel stateModel =
                new ViewModelProvider(requireActivity()).get(StateModel.class);

        TextView textConnectTo = dialog.findViewById(R.id.connect_to);
        Objects.requireNonNull(textConnectTo);
        textConnectTo.setText(peerId);


        MaterialToolbar relaysToolbar = dialog.findViewById(R.id.relays_toolbar);
        Objects.requireNonNull(relaysToolbar);
        relaysToolbar.getMenu().findItem(R.id.connect_stop).setOnMenuItemClickListener(item -> {
            if (item.getItemId() == R.id.connect_stop) {
                stateModel.abortConnection(peerId);
                dismiss();
                return true;
            }
            return false;
        });


        RecyclerView recyclerView = dialog.findViewById(R.id.relays);
        Objects.requireNonNull(recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));


        RelaysAdapter relaysAdapter = new RelaysAdapter();
        recyclerView.setAdapter(relaysAdapter);


        stateModel.liveDataRelays().observe(this, (relays -> {
            try {
                if (relays != null) {
                    relaysAdapter.updateData(relays);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }));
        return dialog;
    }
}

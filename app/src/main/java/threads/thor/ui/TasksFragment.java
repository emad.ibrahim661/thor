package threads.thor.ui;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.Objects;

import threads.thor.LogUtils;
import threads.thor.R;
import threads.thor.model.API;
import threads.thor.state.StateModel;
import threads.thor.utils.TasksAdapter;
import threads.thor.work.DownloadContentWorker;
import threads.thor.work.DownloadFileWorker;
import threads.thor.work.DownloadMagnetWorker;
import threads.thor.work.DownloadMhtWorker;


public class TasksFragment extends BottomSheetDialogFragment {

    public static final String TAG = TasksFragment.class.getSimpleName();


    private static TasksFragment newInstance(long tabItem) {
        TasksFragment fragment = new TasksFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(API.TAB, tabItem);
        fragment.setArguments(bundle);
        return fragment;
    }

    public static void show(long tabItem, FragmentManager supportFragmentManager) {
        TasksFragment tasksFragment = TasksFragment.newInstance(tabItem);
        tasksFragment.show(supportFragmentManager, TasksFragment.TAG);
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Bundle args = getArguments();
        Objects.requireNonNull(args);
        long tabItem = args.getLong(API.TAB);

        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        BottomSheetBehavior<FrameLayout> behavior = dialog.getBehavior();
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        behavior.setPeekHeight(BottomSheetBehavior.PEEK_HEIGHT_AUTO, true);

        dialog.setContentView(R.layout.fragment_tasks);

        StateModel stateModel = new ViewModelProvider(requireActivity()).get(StateModel.class);

        MaterialToolbar tasksToolbar = dialog.findViewById(R.id.tasks_toolbar);
        Objects.requireNonNull(tasksToolbar);
        tasksToolbar.getMenu().findItem(R.id.tasks_clear).setOnMenuItemClickListener(item -> {
            if (item.getItemId() == R.id.tasks_clear) {
                WorkManager.getInstance(requireContext()).pruneWork();
                return true;
            }
            return false;
        });

        RecyclerView recyclerView = dialog.findViewById(R.id.tasks);
        Objects.requireNonNull(recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(requireContext());
        linearLayoutManager.setReverseLayout(true);
        recyclerView.setLayoutManager(linearLayoutManager);


        TasksAdapter tasksAdapter = new TasksAdapter(new TasksAdapter.TaskCallback() {
            @Override
            public void cancel(@NonNull WorkInfo workInfo) {
                try {
                    WorkManager.getInstance(requireContext()).cancelWorkById(workInfo.getId());
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            }

            @Override
            public void start(@NonNull WorkInfo workInfo) {
                try {
                    Uri uri = API.extractUri(workInfo);
                    if (uri != null) {
                        String scheme = uri.getScheme();
                        if (Objects.equals(scheme, API.MAGNET_SCHEME)) {
                            DownloadMagnetWorker.download(requireContext(), uri);
                        } else if (Objects.equals(scheme, API.PNS_SCHEME)) {
                            DownloadContentWorker.download(requireContext(), uri);
                        } else if (Objects.equals(scheme, API.HTTP_SCHEME)) {
                            API.Tags tags = API.extractTags(workInfo);
                            if (tags != null) {
                                DownloadFileWorker.download(requireContext(), uri, tags);
                            }
                        } else if (Objects.equals(scheme, API.HTTPS_SCHEME)) {
                            API.Tags tags = API.extractTags(workInfo);
                            if (tags != null) {
                                DownloadFileWorker.download(requireContext(), uri, tags);
                            }
                        } else if (Objects.equals(scheme, API.FILE_SCHEME)) {
                            API.Tags tags = API.extractTags(workInfo);
                            if (tags != null) {
                                DownloadMhtWorker.download(requireContext(), uri, tags);
                            }
                        }
                    }
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            }

            @Override
            public void downloads() {
                try {
                    stateModel.showDownloads(true);
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                } finally {
                    dismiss();
                }
            }

            @Override
            public void invoke(@NonNull WorkInfo workInfo) {
                try {
                    Uri uri = API.extractUri(workInfo);
                    if (uri != null) {

                        if (Objects.equals(uri.getScheme(), API.FILE_SCHEME)) {
                            // todo virtual thread
                            new Thread(() -> stateModel.updateTab(tabItem, uri)).start();
                        } else {
                            Intent viewIntent = new Intent(Intent.ACTION_VIEW, uri);
                            viewIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            startActivity(viewIntent);
                        }
                    }
                } catch (Throwable throwable) {
                    stateModel.warning(getString(R.string.no_activity_found_to_handle_uri));
                } finally {
                    dismiss();
                }
            }
        });
        recyclerView.setAdapter(tasksAdapter);


        WorkManager.getInstance(requireContext())
                .getWorkInfosByTagLiveData(API.WORK_TAG)
                .observe(this, workInfos -> {
                    if (workInfos != null) {
                        tasksAdapter.updateData(workInfos);
                    }
                });
        return dialog;
    }

}

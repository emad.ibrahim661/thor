package threads.thor.ui;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.Settings;

import androidx.activity.OnBackPressedCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.sidesheet.SideSheetDialog;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import tech.lp2p.Lite;
import threads.thor.LogUtils;
import threads.thor.R;
import threads.thor.data.tabs.Tab;
import threads.thor.model.API;
import threads.thor.model.MDNS;
import threads.thor.state.StateModel;
import threads.thor.utils.LongDiffCallback;
import threads.thor.utils.MimeType;
import threads.thor.utils.TabsAdapter;


public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();

    private StateModel stateModel;
    private final ActivityResultLauncher<Intent> mFolderRequestForResult = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    Intent intent = result.getData();

                    try {
                        Objects.requireNonNull(intent);
                        Uri uri = intent.getData();
                        Objects.requireNonNull(uri);

                        Intent viewIntent = new Intent(Intent.ACTION_VIEW, uri);
                        viewIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        startActivity(viewIntent);
                    } catch (Throwable throwable) {
                        stateModel.warning(getString(R.string.no_activity_found_to_handle_uri));
                    }
                }
            });
    private ViewPager2 browserPager;
    private ViewPagerFragmentAdapter browserAdapter;
    private Dialog tabsDialog;
    private RelaysFragment relaysFragment;
    private MDNS mdns;

    void openTabsDialog() {

        tabsDialog = new SideSheetDialog(this);
        tabsDialog.setContentView(R.layout.fragment_tabs);

        RecyclerView recyclerView = tabsDialog.findViewById(R.id.tabs_recycler_view);
        Objects.requireNonNull(recyclerView);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);

        recyclerView.setLayoutManager(linearLayoutManager);

        TabsAdapter tabsAdapter = new TabsAdapter(new TabsAdapter.ViewHolderListener() {
            @Override
            public void onItemClicked(int position) {
                try {
                    browserPager.setCurrentItem(position, true);
                    closeTabsDialog();
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            }

            @Override
            public void onItemDismiss(Tab tab) {
                // todo virtual thread
                new Thread(() -> stateModel.removeTab(tab)).start();
            }

            @Override
            public boolean isSelected(int position) {
                return browserPager.getCurrentItem() == position;
            }
        });

        recyclerView.setAdapter(tabsAdapter);


        stateModel.tabs().observe(this, tabs -> {
            if (tabs != null) {
                tabsAdapter.updateData(tabs);
                int position = browserPager.getCurrentItem();
                linearLayoutManager.scrollToPosition(position);
            }
        });


        MaterialToolbar tabsToolbar = tabsDialog.findViewById(R.id.tabs_toolbar);
        Objects.requireNonNull(tabsToolbar);


        tabsToolbar.getMenu().findItem(R.id.add_tab).setOnMenuItemClickListener(item -> {
            if (item.getItemId() == R.id.add_tab) {
                // todo virtual thread
                new Thread(() -> stateModel.createDefaultTab()).start();
                closeTabsDialog();
                return true;
            }
            return false;
        });

        tabsToolbar.getMenu().findItem(R.id.close_tabs).setOnMenuItemClickListener(item -> {
            if (item.getItemId() == R.id.close_tabs) {
                // todo virtual thread
                new Thread(() -> stateModel.clearTabs()).start();
                return true;
            }
            return false;
        });

        tabsDialog.setOnDismissListener(dialog -> {
            // todo virtual thread
            new Thread(() -> stateModel.ensureHasTab()).start();
        });

        tabsDialog.show();
    }

    private void closeTabsDialog() {
        if (tabsDialog != null) {
            tabsDialog.dismiss();
            tabsDialog = null;
        }
    }

    @Nullable
    private BrowserFragment getBrowserFragment() {
        long tabItem = browserAdapter.getItemId(browserPager.getCurrentItem());
        if (tabItem > 0L) {
            Optional<Fragment> result = getSupportFragmentManager().getFragments()
                    .stream()
                    .filter(it -> it instanceof BrowserFragment && it.getArguments() != null
                            && it.getArguments().getLong(API.TAB) == tabItem)
                    .findFirst();
            return (BrowserFragment) result.orElse(null);
        }
        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        stateModel = new ViewModelProvider(this).get(StateModel.class);

        getOnBackPressedDispatcher().addCallback(this, new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                BrowserFragment browserFragment = getBrowserFragment();
                if (browserFragment != null) {
                    boolean result = browserFragment.onBackPressedCheck();
                    if (result) {
                        return;
                    }
                }
                finish();
            }
        });


        browserPager = findViewById(R.id.browser_pager);


        browserAdapter = new ViewPagerFragmentAdapter(
                getSupportFragmentManager(), getLifecycle());
        browserPager.setAdapter(browserAdapter);
        browserPager.setUserInputEnabled(false);
        browserPager.setOffscreenPageLimit(20);
        browserPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);

                stateModel.tabIdx(browserAdapter.getItemId(position));
            }
        });

        stateModel.showDownloads().observe(this, value -> {
            if (value != null) {
                if (value) {
                    showDownloads();
                }
                stateModel.showDownloads(false);
            }
        });

        stateModel.idxs().observe(this, idxs -> {
            if (idxs != null) {
                int count = browserAdapter.getItemCount();
                browserAdapter.updateData(idxs);

                if (idxs.size() > count) {
                    browserPager.setCurrentItem(idxs.size(), true);
                }

            }
        });

        stateModel.error().observe(this, (content) -> {
            try {
                if (content != null) {
                    if (!content.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(browserPager, content, Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }
                    stateModel.error(null);

                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        stateModel.connectTo().observe(this, (peerId) -> {
            try {
                if (peerId != null) {
                    LogUtils.error(TAG, "Connect to " + peerId);
                    relaysFragment = RelaysFragment.newInstance(peerId);
                    relaysFragment.setCancelable(false);
                    relaysFragment.show(getSupportFragmentManager(),
                            RelaysFragment.TAG);
                } else {
                    LogUtils.error(TAG, "Connect to done");
                    if (relaysFragment != null) {
                        relaysFragment.dismiss();
                        relaysFragment = null;
                    }
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        stateModel.warning().observe(this, (content) -> {
            try {
                if (content != null) {
                    if (!content.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(browserPager, content,
                                Snackbar.LENGTH_SHORT);
                        snackbar.show();
                    }
                    stateModel.warning(null);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });


        stateModel.permission().observe(this, (content) -> {
            try {
                if (content != null) {
                    if (!content.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(browserPager, content,
                                Snackbar.LENGTH_INDEFINITE);
                        snackbar.setAction(R.string.settings, view -> {
                            final Intent intent = new Intent();
                            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            intent.addCategory(Intent.CATEGORY_DEFAULT);
                            intent.setData(Uri.parse("package:" +
                                    view.getContext().getPackageName()));
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                            startActivity(intent);
                        });
                        snackbar.show();

                    }
                    stateModel.permission(null);
                }

            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        stateModel.removed().observe(this, (bookmark) -> {
            try {
                if (bookmark != null) {
                    Snackbar snackbar = Snackbar.make(browserPager,
                            getString(R.string.bookmark_removed, bookmark.title()),
                            Snackbar.LENGTH_LONG);
                    snackbar.setAction(getString(R.string.revert), (view) -> {
                        try {
                            // todo virtual thread
                            new Thread(() -> stateModel.restoreBookmark(bookmark)).start();
                        } catch (Throwable throwable) {
                            LogUtils.error(TAG, throwable);
                        } finally {
                            snackbar.dismiss();
                        }

                    });
                    snackbar.show();
                    stateModel.removed(null);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });


        Intent intent = getIntent();

        if (!handleIntents(intent)) {
            // todo virtual thread
            new Thread(() -> stateModel.ensureHasTab()).start();
        }
        stateModel.online(stateModel.isNetworkConnected());
    }


    @Override
    public void onStart() {
        super.onStart();
        mdns = MDNS.mdns(MainActivity.this);
        mdns.startDiscovery(peeraddr -> stateModel.local(peeraddr));
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mdns != null) {
            mdns.stop();
            mdns = null;
        }
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        handleIntents(intent);
    }

    private boolean handleIntents(Intent intent) {

        final String action = intent.getAction();

        if (Intent.ACTION_VIEW.equals(action)) {
            Uri uri = intent.getData();
            if (uri != null) {
                // todo virtual thread
                new Thread(() -> stateModel.updateTab(uri)).start();
                closeTabsDialog();

                return true;
            }
        }

        if (Intent.ACTION_SEND.equals(action)) {
            if (Objects.equals(intent.getType(), MimeType.PLAIN_MIME_TYPE)) {
                String text = intent.getStringExtra(Intent.EXTRA_TEXT);
                return doSearch(text);
            }
        }

        return false;
    }

    private boolean doSearch(@Nullable String query) {
        try {
            if (query != null && !query.isEmpty()) {
                Uri uri = Uri.parse(query);
                String scheme = uri.getScheme();
                if (Objects.equals(scheme, API.PNS_SCHEME) ||
                        Objects.equals(scheme, API.HTTP_SCHEME) ||
                        Objects.equals(scheme, API.HTTPS_SCHEME)) {

                    // todo virtual thread
                    new Thread(() -> stateModel.updateTab(uri)).start();

                    closeTabsDialog();
                } else {
                    try {
                        // in case the search is an pns string
                        Uri test = Uri.parse(API.PNS_SCHEME + "://" +
                                Lite.decodePeerId(query).toBase36());

                        // todo virtual thread
                        new Thread(() -> stateModel.updateTab(test)).start();

                        closeTabsDialog();

                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                }
                return true;
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        networkCallback();
    }

    private void networkCallback() {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager)
                    getSystemService(Context.CONNECTIVITY_SERVICE);

            ConnectivityManager.NetworkCallback networkCallback =
                    new ConnectivityManager.NetworkCallback() {
                        @Override
                        public void onAvailable(@NonNull Network network) {
                            super.onAvailable(network);
                            stateModel.online(true);
                        }

                        @Override
                        public void onLost(@NonNull Network network) {
                            super.onLost(network);
                            stateModel.online(false);
                        }

                    };

            NetworkRequest networkRequest = new NetworkRequest.Builder()
                    .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
                    .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
                    .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
                    .build();

            connectivityManager.registerNetworkCallback(networkRequest, networkCallback);


        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void showDownloads() {
        try {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("*/*");
            intent.putExtra(DocumentsContract.EXTRA_EXCLUDE_SELF, false);
            intent.putExtra(DocumentsContract.EXTRA_INITIAL_URI, Uri.parse(
                    Environment.getExternalStorageDirectory().getPath()));

            mFolderRequestForResult.launch(intent);
        } catch (Throwable throwable) {
            stateModel.warning(
                    getString(R.string.no_activity_found_to_handle_uri));
        }
    }

    static class ViewPagerFragmentAdapter extends FragmentStateAdapter {
        private final List<Long> tabs = new ArrayList<>();


        ViewPagerFragmentAdapter(@NonNull FragmentManager fragmentManager,
                                 @NonNull Lifecycle lifecycle) {
            super(fragmentManager, lifecycle);
        }

        @Override
        public long getItemId(int position) {
            if (position < 0 || position >= getItemCount()) {
                return 0;
            }
            return tabs.get(position);
        }

        @Override
        public boolean containsItem(long tabIdx) {
            for (Long tab : tabs) {
                if (tab == tabIdx) {
                    return true;
                }
            }
            return false;
        }


        void updateData(@NonNull List<Long> tabs) {
            final LongDiffCallback diffCallback = new LongDiffCallback(this.tabs, tabs);
            final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);

            this.tabs.clear();
            this.tabs.addAll(tabs);
            diffResult.dispatchUpdatesTo(this);
        }

        @NonNull
        @Override
        public Fragment createFragment(int position) {
            long tab = tabs.get(position);
            BrowserFragment browserFragment = new BrowserFragment();
            Bundle bundle = new Bundle();
            bundle.putLong(API.TAB, tab);
            browserFragment.setArguments(bundle);
            return browserFragment;
        }

        @Override
        public int getItemCount() {
            return tabs.size();
        }

    }
}
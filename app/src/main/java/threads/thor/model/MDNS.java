package threads.thor.model;

import android.content.Context;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

import tech.lp2p.Lite;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import threads.thor.LogUtils;


public class MDNS {
    public static final String MDNS_SERVICE = "_p2p._udp."; // default libp2p [or kubo] service name

    @RequiresApi(Build.VERSION_CODES.UPSIDE_DOWN_CAKE)
    private static final ExecutorService EXECUTOR = Executors.newSingleThreadExecutor();

    private static final String TAG = MDNS.class.getName();
    private final NsdManager nsdManager;

    private volatile DiscoveryService discoveryService;

    private MDNS(NsdManager nsdManager) {
        this.nsdManager = nsdManager;
    }

    public static MDNS mdns(Context context) {
        NsdManager nsdManager = (NsdManager) context.getSystemService(Context.NSD_SERVICE);
        Objects.requireNonNull(nsdManager);
        return new MDNS(nsdManager);
    }

    public void startDiscovery(Consumer<Peeraddr> consumer) {
        try {
            discoveryService = new DiscoveryService(nsdManager, consumer);

            nsdManager.discoverServices(MDNS.MDNS_SERVICE, NsdManager.PROTOCOL_DNS_SD,
                    discoveryService);

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    public void stop() {
        try {
            if (discoveryService != null) {
                nsdManager.stopServiceDiscovery(discoveryService);
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable); // not ok when fails
        }
    }


    private record DiscoveryService(@NonNull NsdManager nsdManager,
                                    @NonNull Consumer<Peeraddr> consumer)
            implements NsdManager.DiscoveryListener {
        private static final String TAG = DiscoveryService.class.getSimpleName();

        @Override
        public void onStartDiscoveryFailed(String serviceType, int errorCode) {
            LogUtils.error(TAG, "onStartDiscoveryFailed " + serviceType);
        }

        @Override
        public void onStopDiscoveryFailed(String serviceType, int errorCode) {
            LogUtils.error(TAG, "onStopDiscoveryFailed " + serviceType);
        }

        @Override
        public void onDiscoveryStarted(String serviceType) {
            LogUtils.error(TAG, "onDiscoveryStarted " + serviceType);
        }

        @Override
        public void onDiscoveryStopped(String serviceType) {
            LogUtils.error(TAG, "onDiscoveryStopped " + serviceType);
        }

        @Override
        public void onServiceFound(NsdServiceInfo serviceInfo) {
            LogUtils.error(TAG, "onServiceFound " + serviceInfo);

            if (Build.VERSION.SDK_INT >= 34) {
                //noinspection AnonymousInnerClass
                nsdManager.registerServiceInfoCallback(serviceInfo, EXECUTOR,
                        new NsdManager.ServiceInfoCallback() {
                            @Override
                            public void onServiceInfoCallbackRegistrationFailed(int i) {
                                LogUtils.error(TAG, "onServiceInfoCallbackRegistrationFailed " + i);
                            }

                            @Override
                            public void onServiceUpdated(@NonNull NsdServiceInfo nsdServiceInfo) {
                                evaluateHosts(nsdServiceInfo);
                            }

                            @Override
                            public void onServiceLost() {
                                LogUtils.error(TAG, "onServiceLost");
                            }

                            @Override
                            public void onServiceInfoCallbackUnregistered() {
                                LogUtils.error(TAG, "onServiceInfoCallbackUnregistered");
                            }
                        });
            } else {
                //noinspection AnonymousInnerClass
                nsdManager.resolveService(serviceInfo, new NsdManager.ResolveListener() {

                    @Override
                    public void onResolveFailed(NsdServiceInfo nsdServiceInfo, int i) {
                        LogUtils.error(TAG, "onResolveFailed " + nsdServiceInfo.toString());
                    }

                    @Override
                    public void onServiceResolved(NsdServiceInfo nsdServiceInfo) {
                        evaluateHost(nsdServiceInfo);
                    }
                });
            }
        }

        @Override
        public void onServiceLost(NsdServiceInfo serviceInfo) {
            LogUtils.info(TAG, "onServiceLost " + serviceInfo.toString());
        }

        private void evaluateHost(@NonNull NsdServiceInfo serviceInfo) {
            try {
                PeerId peerId = Lite.decodePeerId(serviceInfo.getServiceName());
                InetAddress inetAddress = serviceInfo.getHost();

                Peeraddr peeraddr = Peeraddr.create(peerId,
                        new InetSocketAddress(inetAddress, serviceInfo.getPort()));
                consumer.accept(peeraddr);

            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable.getMessage()); // is ok, fails for kubo
            }
        }

        @RequiresApi(Build.VERSION_CODES.UPSIDE_DOWN_CAKE)
        private void evaluateHosts(@NonNull NsdServiceInfo serviceInfo) {
            try {
                PeerId peerId = Lite.decodePeerId(serviceInfo.getServiceName());

                Peeraddrs peeraddrs = new Peeraddrs();
                for (InetAddress inetAddress : serviceInfo.getHostAddresses()) {
                    LogUtils.error(TAG, inetAddress.toString());
                    peeraddrs.add(Peeraddr.create(peerId,
                            new InetSocketAddress(inetAddress, serviceInfo.getPort())));
                }

                if (!peeraddrs.isEmpty()) {
                    Peeraddr peeraddr = Peeraddrs.best(peeraddrs);
                    Objects.requireNonNull(peeraddr);
                    consumer.accept(peeraddr);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable.getMessage()); // is ok, fails for kubo
            }
        }

    }

}

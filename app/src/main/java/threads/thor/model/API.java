package threads.thor.model;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.ArrayMap;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.TypeConverter;
import androidx.work.WorkInfo;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import tech.lp2p.Lite;
import tech.lp2p.core.ALPN;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Dir;
import tech.lp2p.core.Envelope;
import tech.lp2p.core.HopConnect;
import tech.lp2p.core.Info;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.Payload;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Session;
import tech.lp2p.utils.Utils;
import threads.magnet.magnet.MagnetUri;
import threads.magnet.magnet.MagnetUriParser;
import threads.thor.LogUtils;
import threads.thor.R;
import threads.thor.data.blocks.BLOCKS;
import threads.thor.data.pages.PAGES;
import threads.thor.data.pages.Page;
import threads.thor.data.peers.PEERS;
import threads.thor.utils.MimeType;
import threads.thor.utils.MimeTypeService;

public class API {

    public static final String WORK_TAG = "Thor Works";
    public static final String WORK_FAILURE = "Failure";
    public static final String FILE_INFO = "FI:";
    public static final String APP_KEY = "AppKey";
    public static final String INDEX_HTML = "index.html";
    public static final int CLICK_OFFSET = 500;
    public static final String MAGNET_SCHEME = "magnet";
    public static final String PNS_SCHEME = "pns";
    public static final String HTTPS_SCHEME = "https";
    public static final String HTTP_SCHEME = "http";
    public static final String FILE_SCHEME = "file";
    public static final String TAB = "tab";
    public static final Payload PNS_RECORD = new Payload(20);
    private static final String NO_NAME = "download-file.bin";
    private static final String JAVASCRIPT_KEY = "javascriptKey";
    private static final String HOMEPAGE_KEY = "homepageKey";
    private static final String SEARCH_ENGINE_KEY = "searchEngineKey";

    private static final Engine DUCKDUCKGO = new Engine("DuckDuckGo",
            Uri.parse("https://start.duckduckgo.com/"), "https://duckduckgo.com/?q=");
    private static final Engine GOOGLE = new Engine("Google",
            Uri.parse("https://www.google.com/"), "https://www.google.com/search?q=");
    private static final Engine ECOSIA = new Engine("Ecosia",
            Uri.parse("https://www.ecosia.org/"), "https://www.ecosia.org/search?q=");
    private static final Engine BING = new Engine("Bing",
            Uri.parse("https://www.bing.com/"), "https://www.bing.com/search?q=");

    @NonNull
    private static final Map<String, Engine> SEARCH_ENGINES = Map.of(
            DUCKDUCKGO.name, DUCKDUCKGO,
            GOOGLE.name, GOOGLE,
            ECOSIA.name, ECOSIA,
            BING.name, BING);

    private static final String TAG = API.class.getSimpleName();
    private static final int QR_CODE_SIZE = 250;
    @NonNull
    private static final ConcurrentHashMap<PeerId, Cid> resolves = new ConcurrentHashMap<>();
    private static final int WHITE = 0xFFFFFFFF;
    private static final int BLACK = 0xFF000000;
    private static final Pattern CONTENT_DISPOSITION_PATTERN =
            Pattern.compile("attachment;\\s*filename\\s*=\\s*(\"?)([^\"]*)\\1(\"?);",
                    Pattern.CASE_INSENSITIVE);
    private static volatile API INSTANCE = null;

    @NonNull
    private final ConcurrentHashMap<PeerId, Connection> swarm = new ConcurrentHashMap<>();
    @NonNull
    private final ConcurrentHashMap<PeerId, ExecutorService> services = new ConcurrentHashMap<>();
    @NonNull
    private final PAGES pages;
    @NonNull
    private final Session session;

    private API(@NonNull Context context) throws Exception {

        Lite.Settings settings = Lite.createSettings(
                Lite.generateKeys(), bootstrap(), "thor/1.0.0/");
        Lite lite = new Lite(settings);

        pages = PAGES.getInstance(context);
        BLOCKS blocks = BLOCKS.getInstance(context);
        PEERS peers = PEERS.getInstance(context);
        session = lite.createSession(blocks, peers); // default global session
    }

    public static void setHomepage(Context context, @Nullable String uri) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(HOMEPAGE_KEY, uri);
        editor.apply();
    }

    @Nullable
    public static String getHomepage(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        return sharedPref.getString(HOMEPAGE_KEY, null);
    }

    public static void setJavascriptEnabled(Context context, boolean auto) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(JAVASCRIPT_KEY, auto);
        editor.apply();
    }

    public static boolean isJavascriptEnabled(@NonNull Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        return sharedPref.getBoolean(JAVASCRIPT_KEY, true);

    }

    public static String getSearchEngine(@NonNull Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        return sharedPref.getString(SEARCH_ENGINE_KEY, "DuckDuckGo");
    }

    public static void setSearchEngine(@NonNull Context context, @NonNull String searchEngine) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(SEARCH_ENGINE_KEY, searchEngine);
        editor.apply();
    }

    @SuppressLint("SetJavaScriptEnabled")
    public static void setWebSettings(@NonNull WebView webView, boolean enableJavascript) {


        WebSettings settings = webView.getSettings();
        settings.setUserAgentString("Mozilla/5.0 (Linux; Android " + Build.VERSION.RELEASE + ")");

        settings.setJavaScriptEnabled(enableJavascript);
        settings.setJavaScriptCanOpenWindowsAutomatically(false);


        settings.setSafeBrowsingEnabled(true);
        settings.setAllowContentAccess(false);
        settings.setAllowFileAccess(true);    // set to true for mht files
        settings.setLoadsImagesAutomatically(true);
        settings.setBlockNetworkLoads(false);
        settings.setBlockNetworkImage(false);
        settings.setDomStorageEnabled(true);
        settings.setCacheMode(WebSettings.LOAD_DEFAULT);
        settings.setDatabaseEnabled(true);
        settings.setSupportZoom(true);
        settings.setBuiltInZoomControls(true);
        settings.setDisplayZoomControls(false);
        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
        settings.setMixedContentMode(WebSettings.MIXED_CONTENT_NEVER_ALLOW);
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        settings.setMediaPlaybackRequiresUserGesture(false); // set to false, required for camera permission
        settings.setSupportMultipleWindows(false);
        settings.setGeolocationEnabled(false);
    }

    @NonNull
    public static ArrayMap<String, Engine> searchEngines() {
        ArrayMap<String, Engine> map = new ArrayMap<>();
        map.putAll(SEARCH_ENGINES);
        return map;
    }

    @NonNull
    public static Engine getEngine(@NonNull Context context) {
        String searchEngine = getSearchEngine(context);
        Engine engine = SEARCH_ENGINES.get(searchEngine);
        if (engine == null) {
            return DUCKDUCKGO;
        }
        return engine;
    }

    public static byte[] bytes(@Nullable Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        Bitmap copy = bitmap.copy(bitmap.getConfig(), true);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        copy.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        copy.recycle();
        return byteArray;
    }

    public static API getInstance(@NonNull Context context) throws Exception {

        if (INSTANCE == null) {
            synchronized (API.class) {
                if (INSTANCE == null) {
                    INSTANCE = new API(context);
                }
            }
        }
        return INSTANCE;
    }

    @NonNull
    public static String getFileName(@NonNull Uri uri) {
        List<String> paths = uri.getPathSegments();
        if (!paths.isEmpty()) {
            return paths.get(paths.size() - 1);
        } else {
            return NO_NAME;
        }
    }

    @NonNull
    public static String getUriTitle(@NonNull Uri uri) {
        String scheme = uri.getScheme();
        if (Objects.equals(scheme, MAGNET_SCHEME)) {
            MagnetUri magnetUri = MagnetUriParser.parse(uri.toString());

            String name = uri.toString();
            if (magnetUri.getDisplayName().isPresent()) {
                name = magnetUri.getDisplayName().get();
            }
            return name;
        } else if (Objects.equals(scheme, PNS_SCHEME)) {
            List<String> paths = uri.getPathSegments();
            if (!paths.isEmpty()) {
                return paths.get(paths.size() - 1);
            } else {
                String host = uri.getHost();
                if (host != null && !host.isBlank()) {
                    return host;
                } else {
                    return "";
                }
            }
        } else {
            String host = uri.getHost();
            if (host != null && !host.isBlank()) {
                return host;
            } else {
                List<String> paths = uri.getPathSegments();
                if (!paths.isEmpty()) {
                    return paths.get(paths.size() - 1);
                } else {
                    return "";
                }
            }
        }
    }

    @DrawableRes
    public static int getImageResource(@NonNull Uri uri) {
        if (Objects.equals(uri.getScheme(), API.FILE_SCHEME)) {
            return R.drawable.bookmark_mht; // it can only be mht right now
        } else if (Objects.equals(uri.getScheme(), API.PNS_SCHEME)) {
            return R.drawable.bookmark_pns;
        } else {
            return R.drawable.bookmark;
        }
    }

    public static Bitmap getBitmap(@NonNull String content) {
        try {
            QRCodeWriter qrCodeWriter = new QRCodeWriter();
            BitMatrix bitMatrix = qrCodeWriter.encode(content, BarcodeFormat.QR_CODE,
                    QR_CODE_SIZE, QR_CODE_SIZE);
            return createBitmap(bitMatrix);
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    public static Bitmap createBitmap(BitMatrix matrix) {
        int width = matrix.getWidth();
        int height = matrix.getHeight();
        int[] pixels = new int[width * height];
        for (int y = 0; y < height; y++) {
            int offset = y * width;
            for (int x = 0; x < width; x++) {
                pixels[offset + x] = matrix.get(x, y) ? BLACK : WHITE;
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return bitmap;
    }

    public static WebResourceResponse createEmptyResource() {
        return new WebResourceResponse(MimeType.PLAIN_MIME_TYPE,
                StandardCharsets.UTF_8.name(), new ByteArrayInputStream("".getBytes()));
    }

    public static WebResourceResponse createErrorMessage(@NonNull Throwable throwable) {
        LogUtils.error(TAG, throwable);
        String message = throwable.getMessage();
        if (message == null || message.isEmpty()) {
            message = throwable.getClass().getSimpleName();
        }
        String report = generateErrorHtml(message);
        return new WebResourceResponse(MimeType.HTML_MIME_TYPE,
                StandardCharsets.UTF_8.name(), new ByteArrayInputStream(report.getBytes()));
    }

    public static WebResourceResponse createErrorMessage(@NonNull String message) {
        String report = generateErrorHtml(message);
        return new WebResourceResponse(MimeType.HTML_MIME_TYPE,
                StandardCharsets.UTF_8.name(), new ByteArrayInputStream(report.getBytes()));
    }

    private static String generateErrorHtml(@NonNull String message) {
        return "<html>" + "<head>" + MimeTypeService.META +
                "<title>" + "Error" + "</title>" +
                "</head>\n" + MimeTypeService.STYLE +
                "<body><div <div>" + message + "</div></body></html>";
    }

    @NonNull
    private static String getMimeType(@NonNull Info info) {
        if (info.isDir()) {
            return MimeType.DIR_MIME_TYPE;
        }
        return MimeType.OCTET_MIME_TYPE;
    }

    @Nullable
    private static String getHost(@NonNull Uri uri) {
        try {
            if (Objects.equals(uri.getScheme(), PNS_SCHEME)) {
                return uri.getHost();
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return null;
    }

    @NonNull
    private static WebResourceResponse getContentResponse(@NonNull Connection connection,
                                                          @NonNull Cid cid,
                                                          @NonNull String mimeType)
            throws Exception {

        try (InputStream in = Lite.getInputStream(connection, cid)) {

            Map<String, String> responseHeaders = new HashMap<>();

            return new WebResourceResponse(mimeType, StandardCharsets.UTF_8.name(), 200,
                    "OK", responseHeaders, new BufferedInputStream(in));
        }
    }

    @NonNull
    public static String getMimeType(@NonNull Uri uri, @NonNull Info info) {

        List<String> paths = uri.getPathSegments();
        if (!paths.isEmpty()) {
            String name = paths.get(paths.size() - 1);
            String mimeType = MimeTypeService.getMimeType(name);
            if (!mimeType.equals(MimeType.OCTET_MIME_TYPE)) {
                return mimeType;
            } else {
                return getMimeType(info);
            }
        } else {
            return getMimeType(info);
        }

    }

    private static void addResolves(@NonNull PeerId peerId, @NonNull Cid root) {
        resolves.put(peerId, root);
    }

    public static WebResourceResponse createRedirectMessage(@NonNull Uri uri) {
        return new WebResourceResponse(MimeType.HTML_MIME_TYPE,
                StandardCharsets.UTF_8.name(), new ByteArrayInputStream(("<!DOCTYPE HTML>\n" +
                "<html lang=\"en-US\">\n" +
                "    <head>\n" + MimeTypeService.META +
                "        <meta http-equiv=\"refresh\" content=\"0; url=" + uri + "\">\n" +
                "        <title>Page Redirection</title>\n" +
                "    </head>\n" + MimeTypeService.STYLE +
                "    <body>\n" +
                "        Automatically redirected to the <a style=\"word-wrap: break-word;\" href='" + uri + "'>" + uri + "</a> location\n" +
                "</html>").getBytes()));
    }

    private static String generateDirectoryHtml(@NonNull Session session,
                                                @NonNull Cid root,
                                                @NonNull Uri uri,
                                                @NonNull List<String> paths,
                                                @Nullable List<Info> infos) {
        String title = "";

        try {
            title = Lite.info(session, root).name();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

        if (paths.isEmpty()) {
            if (title == null || title.isBlank()) {
                title = uri.getHost();
            }
        } else {
            title = paths.get(paths.size() - 1); // get last
        }


        StringBuilder answer = new StringBuilder("<html>" + "<head>" + MimeTypeService.META +
                "<title>" + title + "</title>");

        answer.append("</head>");
        answer.append(MimeTypeService.STYLE);
        answer.append("<body>");

        if (infos != null) {
            if (!infos.isEmpty()) {
                answer.append("<form><table  width=\"100%\" style=\"border-spacing: 8px;\">");

                // folder up
                if (!paths.isEmpty()) {
                    Uri.Builder builder = new Uri.Builder();
                    builder.scheme(uri.getScheme()).authority(uri.getAuthority());
                    for (int i = 0; i < paths.size() - 1; i++) {
                        String path = paths.get(i);
                        builder.appendPath(path);
                    }

                    Uri linkUri = builder.build();
                    answer.append("<tr>");
                    answer.append("<td align=\"center\">");
                    answer.append("<a href=\"");
                    answer.append(linkUri.toString());
                    answer.append("\">");
                    answer.append(MimeTypeService.FOLDER_UP);
                    answer.append("</a>");

                    answer.append("<td width=\"100%\" style=\"word-break:break-word\">");
                    answer.append("..");
                    answer.append("</td>");
                    answer.append("<td/>");
                    answer.append("<td/>");
                    answer.append("</td>");
                    answer.append("</tr>");
                }


                for (Info info : infos) {

                    Uri.Builder builder = new Uri.Builder();
                    builder.scheme(uri.getScheme()).authority(uri.getAuthority());
                    for (String path : paths) {
                        builder.appendPath(path);
                    }
                    builder.appendPath(info.name());
                    builder.appendQueryParameter("download", "0");
                    Uri linkUri = builder.build();
                    answer.append("<tr>");

                    answer.append("<td>");
                    answer.append(MimeTypeService.getSvgResource(info));
                    answer.append("</td>");

                    answer.append("<td width=\"100%\" style=\"word-break:break-word\">");
                    answer.append("<a href=\"");
                    answer.append(linkUri.toString());
                    answer.append("\">");
                    answer.append(info.name());
                    answer.append("</a>");
                    answer.append("</td>");

                    answer.append("<td>");
                    answer.append(getFileSize(info.size()));
                    answer.append("</td>");

                    answer.append("<td align=\"center\">");
                    String text = "<button style=\"float:none!important;display:inline;\" " +
                            "name=\"download\" value=\"1\" formenctype=\"text/plain\" " +
                            "formmethod=\"get\" type=\"submit\" formaction=\"" +
                            linkUri + "\">" + MimeTypeService.getSvgDownload() + "</button>";
                    answer.append(text);
                    answer.append("</td>");
                    answer.append("</tr>");
                }
                answer.append("</table></form>");
            }

        }

        ZonedDateTime zoned = ZonedDateTime.now();
        DateTimeFormatter pattern = DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL);

        answer.append("</body><div class=\"footer\">")
                .append("<p>")
                .append(zoned.format(pattern))
                .append("</p>")
                .append("</div></html>");


        return answer.toString();
    }

    private static String getFileSize(long size) {

        String fileSize;

        if (size < 1000) {
            fileSize = String.valueOf(size);
            return fileSize.concat(" B");
        } else if (size < 1000 * 1000) {
            fileSize = String.valueOf((double) (size / 1000));
            return fileSize.concat(" KB");
        } else {
            fileSize = String.valueOf((double) (size / (1000 * 1000)));
            return fileSize.concat(" MB");
        }
    }

    @NonNull
    private static Uri redirect(@NonNull Connection connection,
                                @NonNull Uri uri, @NonNull Cid root,
                                @NonNull List<String> paths)
            throws Exception {

        Info info = Lite.resolvePath(connection, root, paths);
        if (info instanceof Dir dir) {
            boolean exists = Lite.hasChild(connection, dir, INDEX_HTML);

            if (exists) {
                Uri.Builder builder = new Uri.Builder();
                builder.scheme(uri.getScheme())
                        .authority(uri.getAuthority());
                for (String path : paths) {
                    builder.appendPath(path);
                }
                builder.appendPath(INDEX_HTML);
                return builder.build();
            }
        }


        return uri;
    }

    public static void cleanupResolver(@NonNull Uri uri) {
        try {
            if (Objects.equals(uri.getScheme(), PNS_SCHEME)) {
                String host = getHost(uri);
                if (host != null) {
                    PeerId peerId = Lite.decodePeerId(host);
                    resolves.remove(peerId);
                }
            }
        } catch (Throwable ignore) {
            // ignore common failure
        }
    }


    @NonNull
    public static Peeraddrs bootstrap() {
        // "/ip4/104.131.131.82/udp/4001/quic/p2p/QmaCpDMGvV2BGHeYERUEnRQAwe3N8SzbUtfsmvsqQLuvuJ"
        Peeraddrs peeraddrs = new Peeraddrs();
        try {
            peeraddrs.add(Lite.createPeeraddr(
                    "QmaCpDMGvV2BGHeYERUEnRQAwe3N8SzbUtfsmvsqQLuvuJ",
                    "104.131.131.82", 4001));
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return peeraddrs;
    }

    @NonNull
    public static Uri redirectUri(@NonNull Connection connection, @NonNull Cid root,
                                  @NonNull Uri uri) throws Exception {
        List<String> paths = uri.getPathSegments();
        return redirect(connection, uri, root, paths);
    }

    @Nullable
    public static Uri downloadsUri(Context context, String mimeType, String name, String path) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(MediaStore.Downloads.DISPLAY_NAME, name);
        contentValues.put(MediaStore.Downloads.MIME_TYPE, mimeType);
        contentValues.put(MediaStore.Downloads.RELATIVE_PATH, path);

        ContentResolver contentResolver = context.getContentResolver();
        return contentResolver.insert(MediaStore.Downloads.EXTERNAL_CONTENT_URI, contentValues);
    }

    @Nullable
    public static String parseContentDisposition(String contentDisposition) {
        try {
            Matcher m = CONTENT_DISPOSITION_PATTERN.matcher(contentDisposition);
            if (m.find()) {
                return m.group(2);
            }
        } catch (IllegalStateException ex) {
            // This function is defined as returning null when it can't parse the header
        }
        return null;
    }

    public static boolean downloadActive(@Nullable String url) {
        if (url != null && !url.isEmpty()) {
            Uri uri = Uri.parse(url);
            return Objects.equals(uri.getScheme(), API.PNS_SCHEME) ||
                    Objects.equals(uri.getScheme(), API.HTTP_SCHEME) ||
                    Objects.equals(uri.getScheme(), API.HTTPS_SCHEME);
        }
        return false;
    }

    public static boolean hasRunningWork(List<WorkInfo> workInfos) {
        for (WorkInfo workInfo : workInfos) {
            if (workInfo.getState() == WorkInfo.State.RUNNING) {
                return true;
            }
        }
        return false;
    }


    @Nullable
    @TypeConverter
    public static Cid toCid(byte[] data) {
        if (data == null) {
            return null;
        }
        return Cid.toCid(data);
    }

    @Nullable
    @TypeConverter
    public static byte[] toArray(Cid cid) {
        if (cid == null) {
            return null;
        }
        return Cid.toArray(cid);
    }

    @NonNull
    @TypeConverter
    public static PeerId toPeerId(byte[] data) {
        return PeerId.toPeerId(data);
    }

    @NonNull
    @TypeConverter
    public static byte[] toArray(PeerId peerId) {
        return PeerId.toArray(peerId);
    }

    public static void storeRelay(Context context, PeerId peerId, @Nullable Peeraddr relay) {
        SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        Base64.Encoder encoder = Base64.getEncoder();
        if (relay != null) {
            editor.putString(peerId.toBase36(), encoder.encodeToString(relay.encoded()));
        } else {
            editor.putString(peerId.toBase36(), null);
        }
        editor.apply();
    }

    @Nullable
    public static Peeraddr getRelay(Context context, PeerId peerId) {
        try {
            SharedPreferences sharedPref = context.getSharedPreferences(APP_KEY, Context.MODE_PRIVATE);
            String relay = sharedPref.getString(peerId.toBase36(), null);
            if (relay != null) {
                Base64.Decoder decoder = Base64.getDecoder();
                return Peeraddr.create(peerId, decoder.decode(relay));
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return null;
    }

    public static void deleteCache(@NonNull Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private static boolean deleteDir(@Nullable File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            if (children != null) {
                for (String child : children) {
                    boolean success = deleteDir(new File(dir, child));
                    if (!success) {
                        return false;
                    }
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    public static void deleteFile(@NonNull File root, boolean deleteWhenDir) {
        try {
            if (root.isDirectory()) {
                File[] files = root.listFiles();
                if (files != null) {
                    for (File file : files) {
                        if (file.isDirectory()) {
                            deleteFile(file, true);
                            boolean result = file.delete();
                            if (!result) {
                                LogUtils.error(TAG, "File " + file.getName() + " not deleted");
                            }
                        } else {
                            boolean result = file.delete();
                            if (!result) {
                                LogUtils.error(TAG, "File " + file.getName() + " not deleted");
                            }
                        }
                    }
                }
                if (deleteWhenDir) {
                    boolean result = root.delete();
                    if (!result) {
                        LogUtils.error(TAG, "File " + root.getName() + " not deleted");
                    }
                }
            } else {
                boolean result = root.delete();
                if (!result) {
                    LogUtils.error(TAG, "File " + root.getName() + " not deleted");
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    public static File downloadsDir(@NonNull Context context) {
        File downloads = Objects.requireNonNull(
                context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS));
        if (!downloads.exists()) {
            boolean result = downloads.mkdir();
            if (!result) {
                throw new RuntimeException("image directory does not exists");
            }
        }
        return downloads;
    }

    public static void cleanDataDir(@NonNull Context context) {
        deleteFile(downloadsDir(context), false);
    }

    @NonNull
    public static String host(@NonNull Peeraddr peeraddr) throws UnknownHostException {
        return Objects.requireNonNull(InetAddress.getByAddress(peeraddr.address()).getHostAddress());
    }

    @Nullable
    public static Uri extractUri(WorkInfo workInfo) {
        Set<String> tags = workInfo.getTags();
        tags.remove(API.WORK_TAG);

        for (String tag : tags) {
            try {
                Uri uri = Uri.parse(tag);
                String scheme = uri.getScheme();
                if (Objects.equals(scheme, API.PNS_SCHEME) ||
                        Objects.equals(scheme, API.MAGNET_SCHEME) ||
                        Objects.equals(scheme, API.HTTP_SCHEME) ||
                        Objects.equals(scheme, API.HTTPS_SCHEME) ||
                        Objects.equals(scheme, API.FILE_SCHEME)) {
                    return uri;
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }
        return null;
    }

    @Nullable
    public static Tags extractTags(WorkInfo workInfo) {
        Set<String> tags = workInfo.getTags();
        tags.remove(API.WORK_TAG);

        for (String tag : tags) {
            try {
                Tags test = Tags.decode(tag);
                if (test != null) {
                    return test;
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }
        return null;
    }

    public static boolean isFileUri(@Nullable String url) {
        try {
            if (url != null) {
                Uri uri = Uri.parse(url);
                return Objects.equals(uri.getScheme(), API.FILE_SCHEME);
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return false;
    }

    @NonNull
    private WebResourceResponse getContentResponse(@NonNull Cid cid, @NonNull String mimeType)
            throws Exception {

        try (InputStream in = Lite.getInputStream(session, cid)) {

            Map<String, String> responseHeaders = new HashMap<>();

            return new WebResourceResponse(mimeType, StandardCharsets.UTF_8.name(), 200,
                    "OK", responseHeaders, new BufferedInputStream(in));
        }
    }

    @NonNull
    private Uri redirect(@NonNull Uri uri, @NonNull Cid root, @NonNull List<String> paths)
            throws Exception {

        Info info = Lite.resolvePath(session, root, paths);
        if (info instanceof Dir dir) {
            boolean exists = Lite.hasChild(session, dir, INDEX_HTML);

            if (exists) {
                Uri.Builder builder = new Uri.Builder();
                builder.scheme(uri.getScheme())
                        .authority(uri.getAuthority());
                for (String path : paths) {
                    builder.appendPath(path);
                }
                builder.appendPath(INDEX_HTML);
                return builder.build();
            }
        }


        return uri;
    }

    @NonNull
    public Uri redirectUri(@NonNull Cid root, @NonNull Uri uri) throws Exception {
        List<String> paths = uri.getPathSegments();
        return redirect(uri, root, paths);
    }

    @NonNull
    public WebResourceResponse response(@NonNull Connection connection, @NonNull Cid root,
                                        @NonNull Uri uri)
            throws Exception {
        List<String> paths = uri.getPathSegments();

        Info info = Lite.resolvePath(connection, root, paths); // is resolved
        if (info instanceof Dir dir) {
            List<Info> childs = Lite.childs(session, dir);
            String answer = generateDirectoryHtml(session, root, uri, paths, childs);
            return new WebResourceResponse(MimeType.HTML_MIME_TYPE,
                    StandardCharsets.UTF_8.name(), new ByteArrayInputStream(answer.getBytes()));

        } else {
            String mimeType = getMimeType(uri, info);
            return getContentResponse(connection, info.cid(), mimeType);
        }

    }

    @NonNull
    public WebResourceResponse response(@NonNull Cid root, @NonNull Uri uri) throws Exception {
        List<String> paths = uri.getPathSegments();

        Info info = Lite.resolvePath(session, root, paths); // is resolved
        if (info instanceof Dir dir) {
            List<Info> childs = Lite.childs(session, dir);
            String answer = generateDirectoryHtml(session, root, uri, paths, childs);
            return new WebResourceResponse(MimeType.HTML_MIME_TYPE,
                    StandardCharsets.UTF_8.name(), new ByteArrayInputStream(answer.getBytes()));

        } else {
            String mimeType = getMimeType(uri, info);
            return getContentResponse(info.cid(), mimeType);
        }

    }

    @Nullable
    public Cid resolveName(@NonNull PeerId peerId) {
        Page page = pages.page(peerId);
        if (page != null) {
            return page.cid();
        }
        return null;
    }

    @NonNull
    public Cid resolveName(@NonNull Connection connection, @NonNull PeerId peerId) throws Exception {

        Cid resolved = resolves.get(peerId);
        if (resolved != null) {
            return resolved;
        }

        Envelope envelope = Lite.pullEnvelope(connection, PNS_RECORD);
        Objects.requireNonNull(envelope);
        if (!Objects.equals(envelope.peerId(), peerId)) {
            throw new Exception("Envelope not from requested peer");
        }
        Cid entry = envelope.cid();

        pages.store(new Page(peerId, entry));
        addResolves(peerId, entry);
        return entry;
    }

    @NonNull
    public Connection connect(@NonNull Peeraddr peeraddr) throws Exception {
        Connection connection = swarm.get(peeraddr.peerId());
        if (connection != null) {
            if (connection.isConnected()) {
                return connection;
            } else {
                swarm.remove(peeraddr.peerId());
            }
        }
        connection = Lite.dial(session, peeraddr, Parameters.create(ALPN.lite, true));
        swarm.put(peeraddr.peerId(), connection);
        return connection;
    }

    @NonNull
    public Connection connect(Context context, PeerId peerId,
                              Consumer<Peeraddr> tries, Consumer<Peeraddr> failures) throws Exception {
        synchronized (peerId.toString().intern()) {
            Connection connection = swarm.get(peerId);
            if (connection != null) {
                if (connection.isConnected()) {
                    return connection;
                } else {
                    swarm.remove(peerId);
                }
            }
            LogUtils.error(TAG, "connect to " + peerId.toBase58());

            connection = hopConnect(context, peerId, tries, failures);
            if (connection != null) {
                swarm.put(peerId, connection);
                return connection;
            }
            throw new ConnectException("Connection failed to host " + peerId.toBase36());
        }
    }

    @Nullable
    public Connection hopConnect(Context context, PeerId peerId,
                                 Consumer<Peeraddr> tries, Consumer<Peeraddr> failures) {
        AtomicReference<Connection> connection = new AtomicReference<>();


        Parameters parameters = Parameters.create(ALPN.lite, true);

        Peeraddr relay = getRelay(context, peerId);
        int threads = 1;
        if (relay != null) {
            threads = 2;
        }
        ExecutorService service = Executors.newFixedThreadPool(threads);
        ExecutorService previous = services.put(peerId, service);
        if (previous != null) {
            throw new IllegalStateException("not allowed");
        }
        if (relay != null) {
            service.execute(() -> {
                try {
                    connection.set(Lite.hopConnect(session, relay, peerId, parameters));
                    service.shutdownNow();
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            });
        }
        service.execute(() -> {
            try {
                HopConnect hopInfo = Lite.hopConnect(session, peerId, parameters,
                        tries, failures,
                        Integer.MAX_VALUE);

                if (hopInfo != null) {
                    storeRelay(context, peerId, hopInfo.relay());
                    connection.set(hopInfo.connection());
                }
                service.shutdownNow();
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });
        service.shutdown();

        try {
            boolean result = service.awaitTermination(Integer.MAX_VALUE, TimeUnit.SECONDS);
            LogUtils.error(TAG, "Terminate normally " + result);
        } catch (InterruptedException ignore) {
        } finally {
            service.shutdownNow();
            services.remove(peerId);
        }

        return connection.get();
    }

    public boolean hasConnection(PeerId peerId) {
        Connection connection = swarm.get(peerId);
        if (connection != null) {
            if (connection.isConnected()) {
                return true;
            } else {
                swarm.remove(peerId);
            }
        }
        return false;
    }

    public void abortConnection(PeerId peerId) {
        ExecutorService service = services.get(peerId);
        if (service != null) {
            LogUtils.error(TAG, "shutdown executor service of peerId " + peerId.toBase58());
            service.shutdownNow();
        }
    }

    @NonNull
    public Session session() {
        return session;
    }

    public record Engine(String name, Uri uri, String query) {
    }

    public record Tags(String name, String mimeType, long size) {

        @Nullable
        public static Tags decode(String tag) {
            if (tag.startsWith(API.FILE_INFO)) {
                String test = tag.replaceFirst(API.FILE_INFO, "");
                String[] tokens = test.split(";");
                Utils.checkArgument(tokens.length, 3, "three tags expected");
                return new Tags(tokens[0], tokens[1], Long.parseLong(tokens[2]));
            }
            return null;
        }

        public String encoded() {
            return API.FILE_INFO + name + ";" + mimeType + ";" + size();
        }
    }
}

package threads.thor.state;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.net.Uri;
import android.webkit.CookieManager;
import android.webkit.WebResourceResponse;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.room.Room;
import androidx.work.WorkManager;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

import tech.lp2p.Lite;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Connection;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import threads.thor.LogUtils;
import threads.thor.R;
import threads.thor.data.blocks.BLOCKS;
import threads.thor.data.books.Bookmark;
import threads.thor.data.books.Bookmarks;
import threads.thor.data.pages.PAGES;
import threads.thor.data.relays.Relay;
import threads.thor.data.relays.Relays;
import threads.thor.data.tabs.Tab;
import threads.thor.data.tabs.Tabs;
import threads.thor.model.API;
import threads.thor.utils.AdBlocker;

public class StateModel extends AndroidViewModel {
    private static final String TAG = StateModel.class.getSimpleName();
    private static final String TAB_KEY = "tabKey";
    @NonNull
    private final Tabs tabs;
    @NonNull
    private final Bookmarks bookmarks;
    @NonNull
    private final Relays relays;
    @NonNull
    private final MutableLiveData<Map<Long, String>> urls = new MutableLiveData<>(new HashMap<>());
    @NonNull
    private final MutableLiveData<String> permission = new MutableLiveData<>(null);
    @NonNull
    private final MutableLiveData<String> error = new MutableLiveData<>(null);
    @NonNull
    private final MutableLiveData<String> warning = new MutableLiveData<>(null);
    @NonNull
    private final MutableLiveData<PeerId> connectTo = new MutableLiveData<>(null);
    @NonNull
    private final MutableLiveData<Boolean> showDownloads = new MutableLiveData<>(null);
    @NonNull
    private final MutableLiveData<Boolean> online = new MutableLiveData<>(null);
    @NonNull
    private final MutableLiveData<Bookmark> removed = new MutableLiveData<>(null);
    @NonNull
    private final ConcurrentHashMap<PeerId, Peeraddr> locals = new ConcurrentHashMap<>();

    public StateModel(@NonNull Application application) {
        super(application);
        tabs = Room.inMemoryDatabaseBuilder(getApplication(), Tabs.class).build();
        bookmarks = Room.databaseBuilder(getApplication(), Bookmarks.class,
                Bookmarks.class.getSimpleName()).fallbackToDestructiveMigration().build();
        relays = Room.inMemoryDatabaseBuilder(application, Relays.class).build();
        // todo virtual thread
        new Thread(() -> AdBlocker.init(application)).start();
    }

    @NonNull
    private static String getTitle(@NonNull String url, @Nullable String webViewTitle) {
        try {
            String title = webViewTitle;
            if (title == null) {
                title = Uri.parse(url).getAuthority();
            }
            Objects.requireNonNull(title);
            return title;
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return "";
    }

    public void local(Peeraddr peeraddr) {
        try {
            locals.put(peeraddr.peerId(), peeraddr);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    @NonNull
    public Peeraddr local(PeerId peerId) {
        return Objects.requireNonNull(locals.get(peerId));
    }

    public boolean hasLocal(PeerId peerId) {
        return locals.containsKey(peerId);
    }

    @NonNull
    public LiveData<List<Relay>> liveDataRelays() {
        return relays.relayDao().relays();
    }

    public LiveData<List<Bookmark>> bookmarks() {
        return bookmarks.bookmarkDao().getLiveDataBookmarks();
    }

    public LiveData<List<Tab>> tabs() {
        return tabs.tabDao().getLiveDataTabs();
    }

    public LiveData<String> url(long tabItem) {
        return Transformations.map(urls, longStringMap -> longStringMap.get(tabItem));
    }

    public void addUrl(long tabItem, String url) {
        Map<Long, String> map = urls.getValue();
        Objects.requireNonNull(map);
        map.put(tabItem, url);
        urls.postValue(map);
    }

    public void rmUrl(long tabItem) {
        Map<Long, String> map = urls.getValue();
        Objects.requireNonNull(map);
        map.remove(tabItem);
        urls.postValue(map);
    }

    public LiveData<List<Long>> idxs() {
        return tabs.tabDao().getLiveDataIdxs();
    }


    @NonNull
    public MutableLiveData<Boolean> online() {
        return online;
    }

    public void online(boolean online) {
        online().postValue(online);
    }

    public MutableLiveData<String> error() {
        return error;
    }

    public MutableLiveData<String> warning() {
        return warning;
    }


    public MutableLiveData<PeerId> connectTo() {
        return connectTo;
    }


    public MutableLiveData<Bookmark> removed() {
        return removed;
    }

    public void removed(@Nullable Bookmark bookmark) {
        removed.postValue(bookmark);
    }

    public MutableLiveData<String> permission() {
        return permission;
    }

    public void permission(@Nullable String content) {
        permission().postValue(content);
    }

    public void error(@Nullable String content) {
        error().postValue(content);
    }

    public void warning(@Nullable String content) {
        warning().postValue(content);
    }

    public LiveData<Tab> tab(long tabIdx) {
        return tabs.tabDao().getLiveDataTab(tabIdx);
    }


    public void connectTo(@Nullable PeerId peerId) {
        connectTo().postValue(peerId);
    }


    public boolean hasBookmark(@Nullable String url) {
        try {
            if (url != null && !url.isEmpty()) {
                return bookmarks.bookmarkDao().getBookmark(url) != null;
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return false;
    }

    public void bookmark(@Nullable String url, @Nullable String title, @Nullable Bitmap bitmap) {
        try {
            Objects.requireNonNull(url);
            Bookmark bookmark = bookmarks.bookmarkDao().getBookmark(url);
            if (bookmark != null) {
                removeBookmark(bookmark);
            } else {
                bookmark = Bookmark.createBookmark(url,
                        getTitle(url, title), bitmap);
                bookmarks.bookmarkDao().insertBookmark(bookmark);

                String msg = bookmark.title();
                if (msg.isEmpty()) {
                    msg = url;
                }
                warning(super.getApplication().getString(R.string.bookmark_added, msg));
            }

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }


    public void removeBookmark(Bookmark bookmark) {
        try {
            bookmarks.bookmarkDao().removeBookmark(bookmark);
            removed(bookmark);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    public void clearTabs() {
        try {
            tabs.clearAllTables();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private boolean hasNoTabs() {
        return !tabs.tabDao().hasTabs();
    }

    public void ensureHasTab() {
        try {
            if (hasNoTabs()) {
                createDefaultTab();
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    public void removeTab(@NonNull Tab tab) {
        try {
            tabs.tabDao().deleteTab(tab);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    public void updateTabTitle(long tabItem, @NonNull String title) {
        try {
            tabs.tabDao().updateTab(tabItem, title);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    public void updateTab(long tabIdx, @NonNull Bookmark bookmark) {
        try {
            tabs.tabDao().updateTab(tabIdx, bookmark.title(), bookmark.uri(), bookmark.icon());
            addUrl(tabIdx, bookmark.uri());
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    public void updateTab(@NonNull Uri uri) {
        try {
            String url = uri.toString();
            long tabIdx = tabIdx();
            if (hasNoTabs() || tabIdx == 0L) {
                createTab(API.getUriTitle(uri), url);
            } else {
                updateTab(tabIdx, uri);
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    public void updateTab(long tabIdx, @NonNull Uri uri) {
        try {
            tabs.tabDao().updateTab(tabIdx, API.getUriTitle(uri), uri.toString());
            addUrl(tabIdx, uri.toString());
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    public void updateTabIcon(long tabIdx, String uri, @Nullable Bitmap bitmap) {
        try {
            tabs.tabDao().updateTabIcon(tabIdx, uri, API.bytes(bitmap));
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    public void updateTabIcon(long tabIdx, @Nullable Bitmap bitmap) {
        try {
            tabs.tabDao().updateTabIcon(tabIdx, API.bytes(bitmap));
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void createTab(@NonNull String title, @NonNull String url) {
        Tab tab = Tab.createTab(title, url, null);
        long tabIdx = tabs.tabDao().insertTab(tab);
        addUrl(tabIdx, url);
    }

    public void restoreBookmark(Bookmark bookmark) {
        try {
            bookmarks.bookmarkDao().insertBookmark(bookmark);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    public WebResourceResponse response(@NonNull Uri uri) {

        try {
            API api = API.getInstance(getApplication());

            // when offline or no public ipv6 check if it is possible to load previous site
            PeerId peerId = Lite.decodePeerId(
                    Objects.requireNonNull(uri.getHost()));

            // check if local is available
            if (hasLocal(peerId)) {
                Peeraddr peeraddr = local(peerId);
                Connection connection = api.connect(peeraddr);
                Cid root = api.resolveName(connection, peerId);
                Uri redirectUri = API.redirectUri(connection, root,
                        uri);
                if (!Objects.equals(uri, redirectUri)) {
                    return API.createRedirectMessage(redirectUri);
                }
                return api.response(connection, root, redirectUri);
            }

            if (isNetworkConnected()) {

                boolean hasConnection = api.hasConnection(peerId);
                if (!hasConnection) {
                    // check if any IPv6 addresses are available (site local and public)
                    if (!Lite.reservationFeaturePossible()) {
                        return API.createErrorMessage(
                                getApplication().getString(R.string.pns_service_limitation_ip));
                    }
                }
                if (!hasConnection) {
                    connectTo(peerId);
                }
                try {
                    relays.clearAllTables();
                    Connection connection = api.connect(getApplication(), peerId,
                            relays::insert, relays::remove);
                    connectTo(null); // dismiss the message
                    Cid root = api.resolveName(connection, peerId);
                    Uri redirectUri = API.redirectUri(connection, root, uri);
                    if (!Objects.equals(uri, redirectUri)) {
                        return API.createRedirectMessage(redirectUri);
                    }
                    return api.response(connection, root, redirectUri);
                } finally {
                    connectTo(null); // dismiss the message
                }
            } else {
                // when offline check if it is possible to load previous site
                Cid root = api.resolveName(peerId);
                if (root == null) {
                    return API.createErrorMessage(
                            getApplication().getString(R.string.no_offline_page));
                } else {
                    Uri redirectUri = api.redirectUri(root, uri);
                    if (!Objects.equals(uri, redirectUri)) {
                        return API.createRedirectMessage(redirectUri);
                    }
                    return api.response(root, redirectUri);
                }
            }
        } catch (Throwable throwable) {
            return API.createErrorMessage(throwable);
        }
    }

    @NonNull
    public Uri homepage() {
        String uri = API.getHomepage(getApplication());
        if (uri == null) {
            return API.getEngine(getApplication()).uri();
        }
        return Uri.parse(uri);
    }

    public void tabIdx(long tabIdx) {
        SharedPreferences sharedPref = getApplication().getSharedPreferences(
                API.APP_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putLong(TAB_KEY, tabIdx);
        editor.apply();
    }


    public long tabIdx() {
        SharedPreferences sharedPref = getApplication().getSharedPreferences(
                API.APP_KEY, Context.MODE_PRIVATE);
        return sharedPref.getLong(TAB_KEY, 0L);
    }

    public boolean isNetworkConnected() {
        ConnectivityManager connectivityManager = (ConnectivityManager)
                getApplication().getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.Network nw = connectivityManager.getActiveNetwork();
        if (nw == null) return false;
        NetworkCapabilities actNw = connectivityManager.getNetworkCapabilities(nw);
        return actNw != null && (actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)
                || actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)
                || actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET));
    }

    public void reset() {
        try {
            // cancel all jobs
            WorkManager.getInstance(getApplication()).cancelAllWorkByTag(API.WORK_TAG);

            // Clear all the cookies
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();

            // Clear blocks and pages data
            BLOCKS.getInstance(getApplication()).clear();
            PAGES.getInstance(getApplication()).clear();

            // Clear downloads data
            API.cleanDataDir(getApplication());
            API.deleteCache(getApplication());

            WorkManager.getInstance(getApplication()).pruneWork();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    public void abortConnection(@NonNull String pid) {
        try {
            API api = API.getInstance(getApplication());
            api.abortConnection(Lite.decodePeerId(pid));
        } catch (Throwable throwable) {
            error(getApplication().getString(R.string.pns_service_error));
        }
    }

    public void createDefaultTab() {
        try {
            Uri uri = homepage();
            createTab(API.getUriTitle(uri), uri.toString());
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    @NonNull
    public MutableLiveData<Boolean> showDownloads() {
        return showDownloads;
    }

    public void showDownloads(boolean value) {
        showDownloads().postValue(value);
    }

    public void checkOnline() {
        online(isNetworkConnected());
    }
}

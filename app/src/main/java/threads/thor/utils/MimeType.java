package threads.thor.utils;

import android.provider.DocumentsContract;

public class MimeType {

    public static final String MHT_MIME_TYPE = "multipart/related";
    public static final String PDF_MIME_TYPE = "application/pdf";
    public static final String OCTET_MIME_TYPE = "application/octet-stream";
    public static final String JSON_MIME_TYPE = "application/json";
    public static final String PLAIN_MIME_TYPE = "text/plain";
    public static final String TORRENT_MIME_TYPE = "application/x-bittorrent";
    public static final String CSV_MIME_TYPE = "text/csv";
    public static final String PGP_KEYS_MIME_TYPE = "application/pgp-keys";
    public static final String EXCEL_MIME_TYPE = "application/msexcel";
    public static final String OPEN_EXCEL_MIME_TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    public static final String WORD_MIME_TYPE = "application/msword";
    public static final String DIR_MIME_TYPE = DocumentsContract.Document.MIME_TYPE_DIR;
    public static final String AUDIO = "audio";
    public static final String VIDEO = "video";
    public static final String TEXT = "text";
    public static final String APPLICATION = "application";
    public static final String IMAGE = "image";
    public static final String CONTACT = "contact";
    public static final String HTML_MIME_TYPE = "text/html";


}

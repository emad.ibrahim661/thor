package threads.thor.utils;

import androidx.recyclerview.widget.DiffUtil;
import androidx.work.WorkInfo;

import java.util.List;
import java.util.Objects;


@SuppressWarnings("WeakerAccess")
public class WorkInfoDiffCallback extends DiffUtil.Callback {
    private final List<WorkInfo> mOldList;
    private final List<WorkInfo> mNewList;

    public WorkInfoDiffCallback(List<WorkInfo> oldWorkInfo, List<WorkInfo> newWorkInfo) {
        this.mOldList = oldWorkInfo;
        this.mNewList = newWorkInfo;
    }

    public static boolean areItemsTheSame(WorkInfo oldWorkInfo, WorkInfo newWorkInfo) {
        return Objects.equals(oldWorkInfo.getId(), newWorkInfo.getId());
    }

    public static boolean areContentsTheSame(WorkInfo oldWorkInfo, WorkInfo newWorkInfo) {
        return Objects.equals(oldWorkInfo, newWorkInfo);
    }

    @Override
    public int getOldListSize() {
        return mOldList.size();
    }

    @Override
    public int getNewListSize() {
        return mNewList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return areItemsTheSame(mOldList.get(oldItemPosition), mNewList.get(newItemPosition));
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return areContentsTheSame(mOldList.get(oldItemPosition), mNewList.get(newItemPosition));
    }
}

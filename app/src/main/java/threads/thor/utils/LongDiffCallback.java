package threads.thor.utils;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

import java.util.List;


public final class LongDiffCallback extends DiffUtil.Callback {
    private final List<Long> mOldList;
    private final List<Long> mNewList;

    public LongDiffCallback(List<Long> tabs, List<Long> newTabs) {
        this.mOldList = tabs;
        this.mNewList = newTabs;
    }

    private static boolean sameContent(@NonNull Long t, @NonNull Long o) {
        return t.longValue() == o.longValue();
    }

    private static boolean areItemsTheSame(@NonNull Long t, @NonNull Long o) {
        return t.longValue() == o.longValue();
    }

    @Override
    public int getOldListSize() {
        return mOldList.size();
    }

    @Override
    public int getNewListSize() {
        return mNewList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return areItemsTheSame(mOldList.get(oldItemPosition), mNewList.get(newItemPosition));
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return sameContent(mOldList.get(oldItemPosition), mNewList.get(newItemPosition));
    }

}

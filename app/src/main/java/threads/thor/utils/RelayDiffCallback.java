package threads.thor.utils;

import androidx.recyclerview.widget.DiffUtil;

import java.util.List;
import java.util.Objects;

import threads.thor.data.relays.Relay;


@SuppressWarnings("WeakerAccess")
public class RelayDiffCallback extends DiffUtil.Callback {
    private final List<Relay> mOldList;
    private final List<Relay> mNewList;

    public RelayDiffCallback(List<Relay> oldRelays, List<Relay> newRelay) {
        this.mOldList = oldRelays;
        this.mNewList = newRelay;
    }

    public static boolean areItemsTheSame(Relay oldRelay, Relay newRelay) {
        return Objects.equals(oldRelay.peeraddr(), newRelay.peeraddr());
    }

    public static boolean areContentsTheSame(Relay oldRelay, Relay newRelay) {
        return Objects.equals(oldRelay.peeraddr(), newRelay.peeraddr());
    }

    @Override
    public int getOldListSize() {
        return mOldList.size();
    }

    @Override
    public int getNewListSize() {
        return mNewList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return areItemsTheSame(mOldList.get(oldItemPosition), mNewList.get(newItemPosition));
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return areContentsTheSame(mOldList.get(oldItemPosition), mNewList.get(newItemPosition));
    }
}

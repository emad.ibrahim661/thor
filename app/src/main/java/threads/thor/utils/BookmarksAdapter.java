package threads.thor.utils;

import android.graphics.Bitmap;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.behavior.SwipeDismissBehavior;
import com.google.android.material.card.MaterialCardView;

import java.util.ArrayList;
import java.util.List;

import threads.thor.LogUtils;
import threads.thor.R;
import threads.thor.data.books.Bookmark;
import threads.thor.model.API;


public class BookmarksAdapter extends RecyclerView.Adapter<BookmarksAdapter.ViewHolder> {
    private static final String TAG = BookmarksAdapter.class.getSimpleName();
    private final BookmarkListener bookmarkListener;
    private final List<Bookmark> bookmarks = new ArrayList<>();

    public BookmarksAdapter(@NonNull BookmarkListener bookmarkListener) {
        this.bookmarkListener = bookmarkListener;
    }


    public void updateData(@NonNull List<Bookmark> bookmarks) {
        final BookmarkDiffCallback diffCallback = new BookmarkDiffCallback(this.bookmarks, bookmarks);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);

        this.bookmarks.clear();
        this.bookmarks.addAll(bookmarks);
        diffResult.dispatchUpdatesTo(this);
    }


    @Override
    public int getItemViewType(int position) {
        return R.layout.card_bookmarks;
    }

    @Override
    @NonNull
    public BookmarksAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                          int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(viewType, parent, false);
        return new ViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Bookmark bookmark = bookmarks.get(position);
        holder.onBind(bookmark, bookmarkListener);
    }


    @Override
    public int getItemCount() {
        return bookmarks.size();
    }


    public interface BookmarkListener {
        void onClick(@NonNull Bookmark bookmark);

        void onDismiss(@NonNull Bookmark bookmark);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView uri;
        private final TextView title;
        private final ImageView image;
        private final MaterialCardView cardView;


        ViewHolder(View v) {
            super(v);

            this.cardView = itemView.findViewById(R.id.card_view);
            this.title = itemView.findViewById(R.id.bookmark_title);
            this.uri = itemView.findViewById(R.id.bookmark_uri);
            this.image = itemView.findViewById(R.id.bookmark_image);


        }

        void onBind(Bookmark bookmark, @NonNull BookmarkListener listener) {

            try {
                this.title.setText(bookmark.title());
                this.uri.setText(bookmark.uri());

                Bitmap image = bookmark.bitmap();
                if (image != null) {
                    this.image.setImageBitmap(image);
                } else {
                    this.image.setImageResource(API.getImageResource(Uri.parse(bookmark.uri())));
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

            cardView.setOnClickListener((view) -> {
                try {
                    listener.onClick(bookmark);
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }

            });

            SwipeDismissBehavior<View> swipeDismissBehavior = new SwipeDismissBehavior<>();
            swipeDismissBehavior.setSwipeDirection(SwipeDismissBehavior.SWIPE_DIRECTION_END_TO_START);


            CoordinatorLayout.LayoutParams coordinatorParams =
                    (CoordinatorLayout.LayoutParams) cardView.getLayoutParams();
            coordinatorParams.setMargins(0, 0, 0, 0);
            cardView.setAlpha(1.0f);
            cardView.requestLayout();

            coordinatorParams.setBehavior(swipeDismissBehavior);

            swipeDismissBehavior.setListener(new SwipeDismissBehavior.OnDismissListener() {
                @Override
                public void onDismiss(View view) {
                    listener.onDismiss(bookmark);
                }

                @Override
                public void onDragStateChanged(int state) {
                    switch (state) {
                        case SwipeDismissBehavior.STATE_DRAGGING,
                             SwipeDismissBehavior.STATE_SETTLING -> cardView.setDragged(true);
                        case SwipeDismissBehavior.STATE_IDLE -> cardView.setDragged(false);
                        default -> {
                        }
                    }
                }
            });
        }

    }
}

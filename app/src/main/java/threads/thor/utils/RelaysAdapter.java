package threads.thor.utils;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import threads.thor.LogUtils;
import threads.thor.R;
import threads.thor.data.relays.Relay;
import threads.thor.model.API;

public class RelaysAdapter extends RecyclerView.Adapter<RelaysAdapter.ViewHolder> {
    private static final String TAG = RelaysAdapter.class.getSimpleName();
    private final List<Relay> relays = new ArrayList<>();

    public RelaysAdapter() {
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.card_peeraddr;
    }

    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(viewType, parent, false);
        return new ViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.onBind(relays.get(position));
    }


    @Override
    public int getItemCount() {
        return relays.size();
    }

    public void updateData(@NonNull List<Relay> relays) {
        final RelayDiffCallback diffCallback = new RelayDiffCallback(this.relays, relays);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);

        this.relays.clear();
        this.relays.addAll(relays);
        diffResult.dispatchUpdatesTo(this);
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        final TextView address;
        final TextView title;
        final ImageView image;

        private ViewHolder(View v) {
            super(v);

            this.title = itemView.findViewById(R.id.peerId);
            this.address = itemView.findViewById(R.id.address);
            this.image = itemView.findViewById(R.id.image);
        }

        void onBind(@NonNull Relay relay) {
            try {
                this.title.setText(relay.peeraddr().peerId().toBase58());
                this.image.setImageResource(R.drawable.server_network);
                String address = API.host(relay.peeraddr()) + ":" + relay.peeraddr().port();
                this.address.setText(address);

            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }
    }
}

package threads.thor.utils;

import androidx.annotation.NonNull;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.function.Consumer;

import threads.magnet.data.StorageUnit;
import threads.magnet.event.PieceVerifiedEvent;
import threads.magnet.metainfo.TorrentFile;
import threads.magnet.net.buffer.ByteBufferView;
import threads.thor.LogUtils;


class ContentStorageUnit implements StorageUnit, Consumer<PieceVerifiedEvent> {

    private static final String TAG = ContentStorageUnit.class.getSimpleName();
    private final File file;
    private final long capacity;
    private final Set<Integer> pieces = new HashSet<>();
    private final ContentStorage contentStorage;
    private SeekableByteChannel sbc;
    private volatile boolean closed;

    ContentStorageUnit(@NonNull ContentStorage contentStorage,
                       @NonNull TorrentFile torrentFile) throws IOException {
        this.contentStorage = contentStorage;
        this.file = getFile(contentStorage, torrentFile);
        this.capacity = torrentFile.getSize();
        this.closed = true;
        init();
        this.closed = false;
        this.pieces.addAll(torrentFile.getPieces());
        long piecesSize = pieces.size();


        if (piecesSize > 0) {
            contentStorage.getEventBus().onPieceVerified(this);
        }


        LogUtils.info(TAG, "Paths : " + torrentFile.getPathElements().toString());
        LogUtils.info(TAG, "Pieces : " + torrentFile.getPieces().size());

    }

    private static File getFile(@NonNull ContentStorage contentStorage,
                                @NonNull TorrentFile torrentFile) throws IOException {
        File file = contentStorage.getRootFile();
        if (!file.exists()) {
            throw new IllegalArgumentException("Root directory does not exists");
        }
        File pathToFile = file;
        for (String path : torrentFile.getPathElements()) {
            String normalizePath = PathNormalizer.normalize(path);
            pathToFile = new File(pathToFile, normalizePath);
        }
        if (pathToFile.exists()) {
            return pathToFile;
        }
        Files.createDirectories(Paths.get(pathToFile.getParent()));
        Files.createFile(pathToFile.toPath());
        return pathToFile;
    }

    private void init() {
        if (!file.exists()) {
            throw new IllegalArgumentException("File does not exists");
        }

        LogUtils.info(TAG, "File Size : " + file.length());

        try {
            sbc = Files.newByteChannel(file.toPath(),
                    StandardOpenOption.READ, StandardOpenOption.WRITE, StandardOpenOption.DSYNC);
        } catch (IOException e) {
            throw new UncheckedIOException("Unexpected I/O error", e);
        }
    }

    @Override
    public synchronized int readBlock(ByteBuffer buffer, long offset) {
        if (closed) {
            return -1;
        }

        if (offset < 0) {
            throw new IllegalArgumentException("Negative offset: " + offset);
        } else if (offset > capacity - buffer.remaining()) {
            throw new IllegalArgumentException("Received a request to read past the end of file (offset: " + offset +
                    ", requested block length: " + buffer.remaining() + ", file capacity: " + capacity);
        }

        try {
            sbc.position(offset);
            return sbc.read(buffer);
        } catch (IOException e) {
            throw new UncheckedIOException("Failed to read bytes (offset: " + offset +
                    ", requested block length: " + buffer.remaining() + ", file capacity: " + capacity + ")", e);
        }
    }

    @Override
    public synchronized void readBlockFully(ByteBuffer buffer, long offset) {
        int read = 0, total = 0;
        do {
            total += read;
            read = readBlock(buffer, offset + total);
        } while (read >= 0 && buffer.hasRemaining());
    }

    private synchronized int writeBlock(ByteBuffer buffer, long offset) {
        if (closed) {
            return -1;
        }

        if (offset < 0) {
            throw new IllegalArgumentException("Negative offset: " + offset);
        } else if (offset > capacity - buffer.remaining()) {
            throw new IllegalArgumentException("Received a request to write past the end of file (offset: " + offset +
                    ", block length: " + buffer.remaining() + ", file capacity: " + capacity);
        }

        try {
            sbc.position(offset);
            return sbc.write(buffer);
        } catch (IOException e) {
            throw new UncheckedIOException("Failed to write bytes (offset: " + offset +
                    ", block length: " + buffer.remaining() + ", file capacity: " + capacity + ")", e);
        }
    }

    @Override
    public synchronized void writeBlockFully(ByteBuffer buffer, long offset) {
        int written = 0, total = 0;
        do {
            total += written;
            written = writeBlock(buffer, offset + total);
        } while (written >= 0 && buffer.hasRemaining());
    }

    private synchronized int writeBlock(ByteBufferView buffer, long offset) {
        if (closed) {
            return -1;
        }

        if (offset < 0) {
            throw new IllegalArgumentException("Negative offset: " + offset);
        } else if (offset > capacity - buffer.remaining()) {
            throw new IllegalArgumentException("Received a request to write past the end of file (offset: " + offset +
                    ", block length: " + buffer.remaining() + ", file capacity: " + capacity);
        }

        try {
            sbc.position(offset);
            return buffer.transferTo(sbc);
        } catch (IOException e) {
            throw new UncheckedIOException("Failed to write bytes (offset: " + offset +
                    ", block length: " + buffer.remaining() + ", file capacity: " + capacity + ")", e);
        }
    }

    @Override
    public synchronized void writeBlockFully(ByteBufferView buffer, long offset) {
        int written = 0, total = 0;
        do {
            total += written;
            written = writeBlock(buffer, offset + total);
        } while (written >= 0 && buffer.hasRemaining());

    }

    @Override
    public long capacity() {
        return capacity;
    }

    @Override
    public long size() {
        return file.length();
    }

    @NonNull
    @Override
    public String toString() {
        return "(" + size() + " of " + capacity + " B) ";
    }

    @Override
    public void close() {

        LogUtils.error(TAG, "close " + this);
        if (!closed) {
            try {
                sbc.close();
            } catch (IOException e) {
                LogUtils.error(TAG, e);
            } finally {
                closed = true;
            }
        }
    }

    private boolean isComplete() {
        return pieces.isEmpty();
    }

    @Override
    public void accept(PieceVerifiedEvent pieceVerifiedEvent) {
        int piece = pieceVerifiedEvent.getPieceIndex();
        boolean removed = pieces.remove(piece);
        if (removed) {

            if (isComplete()) {
                boolean rm = contentStorage.getEventBus().removePieceVerified(this);
                if (!rm) {
                    LogUtils.error(TAG, "PieceVerifiedEventListener not removed");
                }
            }
        }
    }

    static class PathNormalizer {
        private static final String separator = File.separator;


        static String normalize(String path) {
            String normalized = path.trim();
            if (normalized.isEmpty()) {
                return "_";
            }

            StringTokenizer tokenizer = new StringTokenizer(normalized, separator, true);
            StringBuilder buf = new StringBuilder(normalized.length());
            boolean first = true;
            while (tokenizer.hasMoreTokens()) {
                String element = tokenizer.nextToken();
                if (separator.equals(element)) {
                    if (first) {
                        buf.append("_");
                    }
                    buf.append(separator);
                    // this will handle inner slash sequences, like ...a//b...
                    first = true;
                } else {
                    buf.append(normalizePathElement(element));
                    first = false;
                }
            }

            normalized = buf.toString();
            return replaceTrailingSlashes(normalized);
        }

        private static String normalizePathElement(String pathElement) {
            // truncate leading and trailing whitespaces
            String normalized = pathElement.trim();
            if (normalized.isEmpty()) {
                return "_";
            }

            // truncate trailing whitespaces and dots;
            // this will also eliminate '.' and '..' relative names
            char[] value = normalized.toCharArray();
            int to = value.length;
            while (to > 0 && (value[to - 1] == '.' || value[to - 1] == ' ')) {
                to--;
            }
            if (to == 0) {
                normalized = "";
            } else if (to < value.length) {
                normalized = normalized.substring(0, to);
            }

            return normalized.isEmpty() ? "_" : normalized;
        }

        private static String replaceTrailingSlashes(String path) {
            if (path.isEmpty()) {
                return path;
            }

            int k = 0;
            while (path.endsWith(separator)) {
                path = path.substring(0, path.length() - separator.length());
                k++;
            }
            if (k > 0) {
                char[] separatorChars = separator.toCharArray();
                char[] value = new char[path.length() + (separatorChars.length + 1) * k];
                System.arraycopy(path.toCharArray(), 0, value, 0, path.length());
                for (int offset = path.length(); offset < value.length; offset += separatorChars.length + 1) {
                    System.arraycopy(separatorChars, 0, value, offset, separatorChars.length);
                    value[offset + separatorChars.length] = '_';
                }
                path = new String(value);
            }

            return path;
        }
    }

}
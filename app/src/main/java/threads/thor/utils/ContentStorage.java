package threads.thor.utils;

import android.content.Context;

import androidx.annotation.NonNull;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.concurrent.ThreadLocalRandom;

import threads.magnet.data.Storage;
import threads.magnet.data.StorageUnit;
import threads.magnet.event.EventBus;
import threads.magnet.metainfo.TorrentFile;
import threads.thor.model.API;


public class ContentStorage implements Storage {
    private final EventBus eventBus;
    private final String root;
    private final File dataDir;

    public ContentStorage(@NonNull Context context,
                          @NonNull EventBus eventbus,
                          @NonNull String root) {
        this.eventBus = eventbus;
        this.dataDir = API.downloadsDir(context);
        this.root = root;
    }

    public static int nextFreePort() {
        int port = ThreadLocalRandom.current().nextInt(4001, 65535);
        while (true) {
            if (isLocalPortFree(port)) {
                return port;
            } else {
                port = ThreadLocalRandom.current().nextInt(4001, 65535);
            }
        }
    }

    private static boolean isLocalPortFree(int port) {
        try {
            new ServerSocket(port).close();
            return true;
        } catch (IOException e) {
            return false;
        }
    }


    @NonNull
    public File getRootFile() {

        if (!dataDir.exists()) {
            throw new IllegalArgumentException("data directory does not exists");
        }
        File rootDir = new File(dataDir, root);
        if (!rootDir.exists()) {
            boolean created = rootDir.mkdir();
            if (!created) {
                throw new IllegalArgumentException("root directory couldn't be created");
            }
        }
        return rootDir;
    }

    @NonNull
    EventBus getEventBus() {
        return eventBus;
    }

    @Override
    public StorageUnit getUnit(@NonNull TorrentFile torrentFile) {
        try {
            return new ContentStorageUnit(this, torrentFile);
        } catch (Throwable e) {
            throw new IllegalArgumentException(e);
        }
    }
}

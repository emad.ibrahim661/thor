package threads.thor.utils;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import androidx.work.WorkInfo;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.progressindicator.CircularProgressIndicator;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import threads.thor.LogUtils;
import threads.thor.R;
import threads.thor.model.API;

public class TasksAdapter extends RecyclerView.Adapter<TasksAdapter.ViewHolder> {
    private static final String TAG = TasksAdapter.class.getSimpleName();
    private final List<WorkInfo> tasks = new ArrayList<>();
    private final TaskCallback taskCallback;

    public TasksAdapter(@NonNull TaskCallback taskCallback) {
        this.taskCallback = taskCallback;
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.card_task;
    }

    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(viewType, parent, false);
        return new ViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.onBind(taskCallback, tasks.get(position));
    }

    @Override
    public int getItemCount() {
        return tasks.size();
    }

    public void updateData(@NonNull List<WorkInfo> tasks) {
        final WorkInfoDiffCallback diffCallback = new WorkInfoDiffCallback(this.tasks, tasks);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);

        this.tasks.clear();
        this.tasks.addAll(tasks);
        diffResult.dispatchUpdatesTo(this);
    }

    public interface TaskCallback {
        void cancel(@NonNull WorkInfo workInfo);

        void start(@NonNull WorkInfo workInfo);

        void downloads();

        default void nothing() {
        }

        void invoke(@NonNull WorkInfo workInfo);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        final TextView title;

        final CircularProgressIndicator progress;
        final MaterialButton image;
        final MaterialButton action;

        private ViewHolder(View v) {
            super(v);

            this.title = itemView.findViewById(R.id.title);
            this.progress = itemView.findViewById(R.id.progress);
            this.action = itemView.findViewById(R.id.action);
            this.image = itemView.findViewById(R.id.image);
        }


        void onBind(@NonNull TaskCallback taskCallback, @NonNull WorkInfo workInfo) {
            try {
                Uri uri = API.extractUri(workInfo);
                API.Tags tags = null;
                if (uri != null) {
                    String scheme = uri.getScheme();
                    if (Objects.equals(scheme, API.HTTP_SCHEME) ||
                            Objects.equals(scheme, API.HTTPS_SCHEME) ||
                            Objects.equals(scheme, API.FILE_SCHEME)) {
                        tags = API.extractTags(workInfo);
                    }
                }

                // set the text
                if (tags != null) {
                    title.setText(tags.name());
                } else {
                    if (uri != null) {
                        title.setText(API.getUriTitle(uri));
                    } else {
                        title.setText(workInfo.getId().toString());
                    }
                }

                if (workInfo.getState() == WorkInfo.State.SUCCEEDED) {
                    title.setClickable(true);
                    title.setOnClickListener(v -> taskCallback.invoke(workInfo));
                } else {
                    title.setClickable(false);
                }


                int value = workInfo.getProgress().getInt(API.APP_KEY, 0);

                if (value > 0) {
                    progress.setVisibility(View.VISIBLE);
                    progress.setProgress(value);
                } else {
                    progress.setVisibility(View.GONE);
                    progress.setProgress(0);
                }

                switch (workInfo.getState()) {
                    case BLOCKED, ENQUEUED -> action.setIconResource(R.drawable.timer_sand);
                    case FAILED, CANCELLED -> action.setIconResource(R.drawable.restart);
                    case RUNNING -> action.setIconResource(R.drawable.pause);
                    case SUCCEEDED -> {


                        try {
                            if (uri != null) {
                                if (Objects.equals(uri.getScheme(), API.FILE_SCHEME)) {
                                    action.setVisibility(View.GONE);
                                } else {
                                    action.setVisibility(View.VISIBLE);
                                    action.setIconResource(R.drawable.folder_open_outline);
                                }
                            } else {
                                action.setVisibility(View.GONE);
                            }

                            if (tags != null) {
                                image.setIconResource(
                                        MimeTypeService.getMediaResource(tags.mimeType()));
                            } else {
                                if (uri != null) {
                                    image.setIconResource(MimeTypeService.getMediaResource(uri));
                                } else {
                                    image.setIconResource(R.drawable.help);
                                }
                            }
                        } catch (Throwable throwable) {
                            LogUtils.error(TAG, throwable);
                        }
                    }
                }

                action.setOnClickListener(v -> {
                    switch (workInfo.getState()) {
                        case SUCCEEDED -> taskCallback.downloads();
                        case FAILED, CANCELLED -> taskCallback.start(workInfo);
                        case RUNNING -> taskCallback.cancel(workInfo);
                        case BLOCKED, ENQUEUED -> taskCallback.nothing();
                    }
                });

            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }
    }
}

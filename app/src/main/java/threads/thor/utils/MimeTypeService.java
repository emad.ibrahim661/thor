package threads.thor.utils;

import android.net.Uri;
import android.webkit.MimeTypeMap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import tech.lp2p.core.Info;
import threads.magnet.magnet.MagnetUri;
import threads.magnet.magnet.MagnetUriParser;
import threads.thor.LogUtils;
import threads.thor.R;
import threads.thor.model.API;


public class MimeTypeService {


    public static final String META =
            "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=2, user-scalable=yes\">" +
                    "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">";
    public static final String STYLE = """
            <style>
                  body {
                    background-color: white;
                    color: black;
                    font-size: 18px;
                  }
                  h4 {
                    text-align: center;
                  }
                 .footer {
                    position: fixed;
                    left: 0;
                    bottom: 0;
                    width: 100%;
                    background-color: white;
                    color: black;
                    font-size: 14px;
                    text-align: center;
                 }
            \s
             @media (prefers-color-scheme: dark) {
                  body {
                    background-color: #222222;
                    color: white;
                    font-size: 18px;
                  }
                 .footer {
                    position: fixed;
                    left: 0;
                    bottom: 0;
                    width: 100%;
                    background-color: #222222;
                    color: white;
                    font-size: 14px;
                    text-align: center;
                 }
                 /* unvisited link */
                 a:link {
                    color: #0F82AF;
                 }
                \s
                 /* visited link */
                 a:visited {
                    color: green;
                 }
                \s
                 /* mouse over link */
                 a:hover {
                    color: inherit;
                 }
                \s
                 /* selected link */
                 a:active {
                    color: inherit;
                 } }

            @media (prefers-color-scheme: light) {
                  body {
                    background-color: white;
                    color: black;
                    font-size: 18px;
                  }
                 .footer {
                    position: fixed;
                    left: 0;
                    bottom: 0;
                    width: 100%;
                    background-color: white;
                    color: black;
                    font-size: 14px;
                    text-align: center;
                 }
            }</style>""";
    public static final String FOLDER_UP = "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\"><path fill=\"currentColor\" d=\"M13.5 21C9.36 21 6 17.64 6 13.5V11H2L8 4L14 11H10V13.5C10 15.43 11.57 17 13.5 17H21V21H13.5Z\" /></svg>";

    private static final String SVG_FOLDER = "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\"  version=\"1.1\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\"><path fill=\"currentColor\" d=\"M10,4H4C2.89,4 2,4.89 2,6V18C2,19.1 2.9,20 4,20H20C21.1,20 22,19.1 22,18V8C22,6.89 21.1,6 20,6H12L10,4Z\" /></svg>";
    private static final String TAG = MimeTypeService.class.getSimpleName();
    private static final String SVG_DOWNLOAD = "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\"><path fill=\"currentColor\" d=\"M5,20H19V18H5M19,9H15V3H9V9H5L12,16L19,9Z\" /></svg>";
    private static final String SVG_OCTET = "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\"  version=\"1.1\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\"><path fill=\"currentColor\" d=\"M14,2H6C4.89,2 4,2.9 4,4V20C4,21.1 4.9,22 6,22H18C19.1,22 20,21.1 20,20V8L14,2M14.5,18.9L12,17.5L9.5,19L10.2,16.2L8,14.3L10.9,14.1L12,11.4L13.1,14L16,14.2L13.8,16.1L14.5,18.9M13,9V3.5L18.5,9H13Z\" /></svg>";
    private static final String SVG_TEXT = "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\"><path fill=\"currentColor\" d=\"M6,2C4.9,2 4,2.9 4,4V20C4,21.1 4.9,22 6,22H18C19.1,22 20,21.1 20,20V8L14,2H6M6,4H13V9H18V20H6V4M8,12V14H16V12H8M8,16V18H13V16H8Z\" /></svg>";
    private static final String SVG_APPLICATION = "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\"><path fill=\"currentColor\" d=\"M19,4C20.11,4 21,4.9 21,6V18C21,19.1 20.1,20 19,20H5C3.89,20 3,19.1 3,18V6C3,4.9 3.9,4 5,4H19M19,18V8H5V18H19Z\" /></svg>";
    private static final String SVG_PDF = "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\"><path fill=\"currentColor\" d=\"M12,10.5H13V13.5H12V10.5M7,11.5H8V10.5H7V11.5M20,6V18C20,19.1 19.1,20 18,20H6C4.9,20 4,19.1 4,18V6C4,4.9 4.9,4 6,4H18C19.1,4 20,4.9 20,6M9.5,10.5C9.5,9.67 8.83,9 8,9H5.5V15H7V13H8C8.83,13 9.5,12.33 9.5,11.5V10.5M14.5,10.5C14.5,9.67 13.83,9 13,9H10.5V15H13C13.83,15 14.5,14.33 14.5,13.5V10.5M18.5,9H15.5V15H17V13H18.5V11.5H17V10.5H18.5V9Z\" /></svg>";
    private static final String SVG_MOVIE = "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\"><path fill=\"currentColor\" d=\"M5.76,10H20V18H4V6.47M22,4H18L20,8H17L15,4H13L15,8H12L10,4H8L10,8H7L5,4H4C2.9,4 2,4.9 2,6V18C2,19.1 2.9,20 4,20H20C21.1,20 22,19.1 22,18V4Z\" /></svg>";
    private static final String SVG_IMAGE = "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\"><path fill=\"currentColor\"  d=\"M12,10L11.06,12.06L9,13L11.06,13.94L12,16L12.94,13.94L15,13L12.94,12.06L12,10M20,5H16.83L15,3H9L7.17,5H4A2,2 0 0,0 2,7V19A2,2 0 0,0 4,21H20A2,2 0 0,0 22,19V7A2,2 0 0,0 20,5M20,19H4V7H8.05L8.64,6.35L9.88,5H14.12L15.36,6.35L15.95,7H20V19M12,8A5,5 0 0,0 7,13A5,5 0 0,0 12,18A5,5 0 0,0 17,13A5,5 0 0,0 12,8M12,16A3,3 0 0,1 9,13A3,3 0 0,1 12,10A3,3 0 0,1 15,13A3,3 0 0,1 12,16Z\" /></svg>";
    private static final String SVG_AUDIO = "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\"><path fill=\"currentColor\"  d=\"M14,3.23V5.29C16.89,6.15 19,8.83 19,12C19,15.17 16.89,17.84 14,18.7V20.77C18,19.86 21,16.28 21,12C21,7.72 18,4.14 14,3.23M16.5,12C16.5,10.23 15.5,8.71 14,7.97V16C15.5,15.29 16.5,13.76 16.5,12M3,9V15H7L12,20V4L7,9H3Z\" /></svg>";

    public static String getSvgDownload() {
        return SVG_DOWNLOAD;
    }

    @NonNull
    public static String getSvgResource(@NonNull Info info) {
        String mimeType = MimeType.DIR_MIME_TYPE;
        if (info.isDir()) {
            String name = info.name();
            if (name != null) {
                mimeType = getMimeType(name);
            }
        }
        if (!mimeType.isEmpty()) {
            switch (mimeType) {
                case MimeType.DIR_MIME_TYPE -> {
                    return SVG_FOLDER;
                }
                case MimeType.OCTET_MIME_TYPE -> {
                    return SVG_OCTET;
                }
                case MimeType.PDF_MIME_TYPE -> {
                    return SVG_PDF;
                }
            }
            if (mimeType.startsWith(MimeType.TEXT)) {
                return SVG_TEXT;
            }
            if (mimeType.startsWith(MimeType.VIDEO)) {
                return SVG_MOVIE;
            }
            if (mimeType.startsWith(MimeType.IMAGE)) {
                return SVG_IMAGE;
            }
            if (mimeType.startsWith(MimeType.AUDIO)) {
                return SVG_AUDIO;
            }
            if (mimeType.startsWith(MimeType.APPLICATION)) {
                return SVG_APPLICATION;
            }
        }
        return SVG_OCTET;
    }

    public static int getMediaResource(@NonNull Uri uri) {
        if (Objects.equals(uri.getScheme(), API.MAGNET_SCHEME)) {
            MagnetUri magnetUri = MagnetUriParser.parse(uri.toString());
            if (magnetUri.getDisplayName().isPresent()) {
                String name = magnetUri.getDisplayName().get();
                String mimeType = MimeTypeService.getMimeType(name);
                return getMediaResource(mimeType);
            } else {
                return R.drawable.folder;
            }
        }

        List<String> paths = uri.getPathSegments();
        if (!paths.isEmpty()) {
            String name = paths.get(paths.size() - 1);
            String mimeType = MimeTypeService.getMimeType(name);
            return getMediaResource(mimeType);
        }
        return R.drawable.help;
    }

    public static int getMediaResource(@NonNull String mimeType) {

        if (!mimeType.isEmpty()) {
            switch (mimeType) {
                case MimeType.TORRENT_MIME_TYPE -> {
                    return R.drawable.arrow_up_down_bold;
                }
                case MimeType.OCTET_MIME_TYPE -> {
                    return R.drawable.file_star;
                }
                case MimeType.JSON_MIME_TYPE -> {
                    return R.drawable.json;
                }
                case MimeType.PLAIN_MIME_TYPE -> {
                    return R.drawable.file;
                }
                case MimeType.PDF_MIME_TYPE -> {
                    return R.drawable.pdf;
                }
                case MimeType.MHT_MIME_TYPE -> {
                    return R.drawable.mht;
                }
                case MimeType.WORD_MIME_TYPE -> {
                    return R.drawable.microsoft_word;
                }
                case MimeType.EXCEL_MIME_TYPE, MimeType.CSV_MIME_TYPE,
                     MimeType.OPEN_EXCEL_MIME_TYPE -> {
                    return R.drawable.microsoft_excel;
                }
            }
            if (mimeType.startsWith(MimeType.TEXT)) {
                return R.drawable.file;
            }
            if (mimeType.equals(MimeType.DIR_MIME_TYPE)) {
                return R.drawable.folder;
            }
            if (mimeType.startsWith(MimeType.VIDEO)) {
                return R.drawable.movie_outline;
            }
            if (mimeType.startsWith(MimeType.IMAGE)) {
                return R.drawable.camera;
            }
            if (mimeType.startsWith(MimeType.AUDIO)) {
                return R.drawable.audio;
            }
            if (mimeType.startsWith(MimeType.PGP_KEYS_MIME_TYPE)) {
                return R.drawable.fingerprint;
            }
            if (mimeType.startsWith(MimeType.CONTACT)) {
                return R.drawable.card;
            }
            if (mimeType.startsWith(MimeType.APPLICATION)) {
                return R.drawable.application;
            }

            return R.drawable.settings;
        }

        return R.drawable.help;

    }

    @NonNull
    public static String getMimeType(@NonNull String name) {
        String mimeType = evaluateMimeType(name);
        if (mimeType != null) {
            return mimeType;
        }
        return MimeType.OCTET_MIME_TYPE;
    }

    private static Optional<String> getExtension(@Nullable String filename) {
        return Optional.ofNullable(filename)
                .filter(f -> f.contains("."))
                .map(f -> f.substring(filename.lastIndexOf('.') + 1));
    }


    @Nullable
    private static String evaluateMimeType(@NonNull String filename) {
        try {
            Optional<String> extension = getExtension(filename);
            if (extension.isPresent()) {
                String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                        extension.get());
                if (mimeType != null) {
                    return mimeType;
                }
            } else {
                // no extension directory assumed
                return MimeType.DIR_MIME_TYPE;
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return null;
    }
}
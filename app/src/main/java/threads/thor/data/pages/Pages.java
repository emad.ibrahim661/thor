package threads.thor.data.pages;

import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import threads.thor.model.API;


@androidx.room.Database(entities = {Page.class}, version = 1, exportSchema = false)
@TypeConverters({API.class})
public abstract class Pages extends RoomDatabase {

    public abstract PageDao pageDao();

}

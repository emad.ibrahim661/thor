package threads.thor.data.pages;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Room;

import tech.lp2p.core.Cid;
import tech.lp2p.core.PeerId;


public final class PAGES {

    private static volatile PAGES INSTANCE = null;

    private final Pages pages;

    private PAGES(Pages database) {
        this.pages = database;
    }

    public static PAGES getInstance(@NonNull Context context) {

        if (INSTANCE == null) {
            synchronized (PAGES.class) {
                if (INSTANCE == null) {

                    INSTANCE = new PAGES(Room.databaseBuilder(context,
                                    Pages.class,
                                    Pages.class.getSimpleName()).
                            fallbackToDestructiveMigration().build());
                }
            }
        }
        return INSTANCE;
    }


    public void store(@NonNull Page page) {
        pages.pageDao().insertPage(page);
    }

    @Nullable
    public Page page(@NonNull PeerId peerId) {
        return pages.pageDao().getPage(peerId);
    }


    public void clear() {
        pages.clearAllTables();
    }


    @Nullable
    public Cid cid(@NonNull PeerId peerId) {
        return pages.pageDao().getCid(peerId);
    }

}

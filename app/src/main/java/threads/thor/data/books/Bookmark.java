package threads.thor.data.books;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.PrimaryKey;

import threads.thor.model.API;

@androidx.room.Entity
public record Bookmark(@PrimaryKey @NonNull @ColumnInfo(name = "uri") String uri,
                       @NonNull @ColumnInfo(name = "title") String title,
                       @Nullable @ColumnInfo(typeAffinity = ColumnInfo.BLOB) byte[] icon) {

    @NonNull
    public static Bookmark createBookmark(@NonNull String uri,
                                          @NonNull String title,
                                          @Nullable Bitmap bitmap) {
        return new Bookmark(uri, title, API.bytes(bitmap));
    }

    @Override
    public byte[] icon() {
        return icon;
    }

    @Nullable
    public Bitmap bitmap() {
        if (icon != null) {
            return BitmapFactory.decodeByteArray(icon, 0, icon.length);
        }
        return null;
    }

    @Override
    @NonNull
    public String uri() {
        return uri;
    }

    @Override
    @NonNull
    public String title() {
        return title;
    }

}

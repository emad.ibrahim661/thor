package threads.thor.data.peers;

import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import threads.thor.model.API;

@androidx.room.Database(entities = {Peer.class}, version = 2, exportSchema = false)
@TypeConverters({API.class})
public abstract class Peers extends RoomDatabase {

    public abstract PeerDao bootstrapDao();

}

package threads.thor.data.peers;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import tech.lp2p.core.PeerId;


@Dao
public interface PeerDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertPeer(Peer peer);

    @Query("SELECT * FROM Peer ORDER BY RANDOM() LIMIT :limit")
    List<Peer> randomPeers(int limit);

    @Query("DELETE FROM Peer WHERE peerId = :peerId")
    void removePeer(PeerId peerId);
}

package threads.thor.data.relays;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Objects;

import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;

@Entity
public record Relay(@PrimaryKey @ColumnInfo(name = "peerId") @NonNull PeerId peerId,
                    @ColumnInfo(typeAffinity = ColumnInfo.BLOB) byte[] raw) {


    @NonNull
    public static Relay create(Peeraddr peeraddr) {
        return new Relay(peeraddr.peerId(), peeraddr.encoded());
    }

    @NonNull
    public Peeraddr peeraddr() {
        return Objects.requireNonNull(Peeraddr.create(peerId, raw));
    }

    @Override
    @NonNull
    public PeerId peerId() {
        return peerId;
    }

    @Override
    public byte[] raw() {
        return raw;
    }
}

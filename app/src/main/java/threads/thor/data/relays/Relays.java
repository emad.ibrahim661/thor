package threads.thor.data.relays;


import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import tech.lp2p.core.Peeraddr;
import threads.thor.model.API;


@Database(entities = {Relay.class}, version = 2, exportSchema = false)
@TypeConverters({API.class})
public abstract class Relays extends RoomDatabase {

    public abstract RelayDao relayDao();

    public void insert(@NonNull Peeraddr peeraddr) {
        relayDao().insertRelay(Relay.create(peeraddr));
    }

    public void remove(@NonNull Peeraddr peeraddr) {
        relayDao().removeRelay(peeraddr.peerId());
    }
}

package threads.thor.data.blocks;

import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import threads.thor.model.API;

@androidx.room.Database(entities = {Block.class}, version = 1, exportSchema = false)
@TypeConverters({API.class})
public abstract class Blocks extends RoomDatabase {

    public abstract BlocksDao blockDao();

}

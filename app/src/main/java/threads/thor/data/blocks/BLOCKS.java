package threads.thor.data.blocks;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Room;

import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Cid;


public final class BLOCKS implements BlockStore {

    private static volatile BLOCKS INSTANCE = null;
    private final Blocks blocks;

    private BLOCKS(Blocks database) {
        this.blocks = database;
    }

    public static BLOCKS getInstance(@NonNull Context context) {

        if (INSTANCE == null) {
            synchronized (BLOCKS.class) {
                if (INSTANCE == null) {
                    INSTANCE = new BLOCKS(Room.databaseBuilder(context, Blocks.class,
                                    Blocks.class.getSimpleName()).
                            fallbackToDestructiveMigration().build());
                }
            }
        }
        return INSTANCE;
    }

    public void clear() {
        blocks.clearAllTables();
    }


    @Override
    public boolean hasBlock(@NonNull Cid cid) {
        return blocks.blockDao().hasBlock(cid);
    }

    @Nullable
    @Override
    public byte[] getBlock(@NonNull Cid cid) {
        return blocks.blockDao().getData(cid);
    }

    @Override
    public void storeBlock(@NonNull Cid cid, byte[] data) {
        blocks.blockDao().insertBlock(new Block(cid, data));
    }

    @Override
    public void deleteBlock(@NonNull Cid cid) {
        blocks.blockDao().deleteBlock(cid);
    }


}

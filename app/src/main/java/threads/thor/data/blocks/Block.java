package threads.thor.data.blocks;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import tech.lp2p.core.Cid;

@Entity
public record Block(
        @PrimaryKey @NonNull @ColumnInfo(name = "cid") Cid cid,
        @NonNull @ColumnInfo(name = "data", typeAffinity = ColumnInfo.BLOB) byte[] data) {


    @Override
    @NonNull
    public Cid cid() {
        return cid;
    }

    @Override
    @NonNull
    public byte[] data() {
        return data;
    }
}

package threads.thor.work;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;

import androidx.annotation.NonNull;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.ExistingWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.OutOfQuotaPolicy;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Objects;

import tech.lp2p.core.Progress;
import tech.lp2p.utils.Utils;
import threads.thor.LogUtils;
import threads.thor.model.API;

public final class DownloadFileWorker extends Worker {

    private static final String SIZE = "size";
    private static final String NAME = "name";
    private static final String TYPE = "type";
    private static final String URI = "uri";
    private static final String TAG = DownloadFileWorker.class.getSimpleName();

    /**
     * @noinspection WeakerAccess
     */
    public DownloadFileWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }

    private static OneTimeWorkRequest getWork(@NonNull Uri uri, @NonNull API.Tags tags) {

        Constraints.Builder builder = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED);

        Data.Builder data = new Data.Builder();
        data.putString(NAME, tags.name());
        data.putString(TYPE, tags.mimeType());
        data.putLong(SIZE, tags.size());
        data.putString(URI, uri.toString());

        return new OneTimeWorkRequest.Builder(DownloadFileWorker.class)
                .addTag(API.WORK_TAG)
                .addTag(uri.toString())
                .addTag(tags.encoded())
                .setInputData(data.build())
                .setConstraints(builder.build())
                .setExpedited(OutOfQuotaPolicy.RUN_AS_NON_EXPEDITED_WORK_REQUEST)
                .build();
    }

    public static void download(@NonNull Context context, @NonNull Uri uri,
                                @NonNull API.Tags tags) {
        WorkManager.getInstance(context).enqueueUniqueWork(uri.toString(),
                ExistingWorkPolicy.KEEP, getWork(uri, tags));
    }


    private static void downloadUrl(URL urlCon, OutputStream os,
                                    Progress progress, long size) throws IOException {
        HttpURLConnection.setFollowRedirects(false);

        HttpURLConnection huc = (HttpURLConnection) urlCon.openConnection();

        huc.setReadTimeout(30000); // 30 sec
        huc.connect();

        try (InputStream is = huc.getInputStream()) {
            Utils.copy(is, os, progress, size);
        }
    }

    @NonNull
    @Override
    public Result doWork() {

        long start = System.currentTimeMillis();
        LogUtils.error(TAG, " start ... ");

        try {
            long size = getInputData().getLong(SIZE, 0);
            String name = getInputData().getString(NAME);
            Objects.requireNonNull(name);
            String mimeType = getInputData().getString(TYPE);
            Objects.requireNonNull(mimeType);

            String url = getInputData().getString(URI);
            Objects.requireNonNull(url);
            Uri uri = Uri.parse(url);

            URL urlCon = new URL(uri.toString());


            Uri downloads = API.downloadsUri(getApplicationContext(), mimeType, name,
                    Environment.DIRECTORY_DOWNLOADS);
            Objects.requireNonNull(downloads);
            ContentResolver contentResolver = getApplicationContext().getContentResolver();

            try (OutputStream os = contentResolver.openOutputStream(downloads)) {
                Objects.requireNonNull(os, "Failed to open output stream");
                downloadUrl(urlCon, os, progress -> {
                    if (isStopped()) {
                        throw new IllegalStateException();
                    }
                    setProgressAsync(new Data.Builder()
                            .putInt(API.APP_KEY, progress).build());
                }, size);

            } catch (Throwable throwable) {
                contentResolver.delete(downloads, null, null);
                throw throwable;
            }

        } catch (Throwable throwable) {
            return Result.failure(new Data.Builder().putString(API.WORK_FAILURE,
                    throwable.getMessage()).build());
        } finally {
            LogUtils.error(TAG, " finish onStart [" +
                    (System.currentTimeMillis() - start) + "]...");
        }
        if (!isStopped()) {
            return Result.success();
        }
        return Result.retry();

    }
}

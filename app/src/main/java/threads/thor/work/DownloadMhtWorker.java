package threads.thor.work;

import android.content.Context;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.ExistingWorkPolicy;
import androidx.work.OneTimeWorkRequest;
import androidx.work.OutOfQuotaPolicy;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.util.Objects;

import threads.thor.model.API;

public final class DownloadMhtWorker extends Worker {
    private static final String SIZE = "size";
    private static final String NAME = "name";
    private static final String TYPE = "type";
    private static final String URI = "uri";


    /**
     * @noinspection WeakerAccess
     */
    public DownloadMhtWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }

    private static OneTimeWorkRequest getWork(@NonNull Uri uri, @NonNull API.Tags tags) {
        Data.Builder data = new Data.Builder();
        data.putString(URI, uri.toString());
        data.putString(NAME, tags.name());
        data.putString(TYPE, tags.mimeType());
        data.putLong(SIZE, tags.size());

        return new OneTimeWorkRequest.Builder(DownloadMhtWorker.class)
                .addTag(API.WORK_TAG)
                .addTag(uri.toString())
                .addTag(tags.encoded())
                .setInputData(data.build())
                .setExpedited(OutOfQuotaPolicy.RUN_AS_NON_EXPEDITED_WORK_REQUEST)
                .build();
    }

    public static void download(@NonNull Context context, @NonNull Uri uri,
                                @NonNull API.Tags tags) {
        WorkManager.getInstance(context).enqueueUniqueWork(uri.toString(),
                ExistingWorkPolicy.KEEP, getWork(uri, tags));
    }

    @NonNull
    @Override
    public Result doWork() {
        try {
            String name = getInputData().getString(NAME);
            Objects.requireNonNull(name);
            String mimeType = getInputData().getString(TYPE);
            Objects.requireNonNull(mimeType);

            String url = getInputData().getString(URI);
            Objects.requireNonNull(url);

            Uri result = Uri.parse(java.net.URI.create(url).toString());

            return Result.success(new Data.Builder().putString(URI, result.toString()).build());

        } catch (Throwable throwable) {
            return Result.failure(new Data.Builder().putString(API.WORK_FAILURE,
                    throwable.getMessage()).build());
        }
    }

}

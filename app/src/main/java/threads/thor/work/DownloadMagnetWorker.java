package threads.thor.work;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;

import androidx.annotation.NonNull;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.ExistingWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.OutOfQuotaPolicy;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Objects;
import java.util.Stack;

import tech.lp2p.utils.Utils;
import threads.magnet.Client;
import threads.magnet.ClientBuilder;
import threads.magnet.IdentityService;
import threads.magnet.Runtime;
import threads.magnet.event.EventBus;
import threads.magnet.magnet.MagnetUri;
import threads.magnet.magnet.MagnetUriParser;
import threads.magnet.net.PeerId;
import threads.thor.LogUtils;
import threads.thor.model.API;
import threads.thor.utils.ContentStorage;
import threads.thor.utils.MimeTypeService;

public final class DownloadMagnetWorker extends Worker {

    private static final String TAG = DownloadMagnetWorker.class.getSimpleName();

    /**
     * @noinspection WeakerAccess
     */
    public DownloadMagnetWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }

    private static OneTimeWorkRequest getWork(@NonNull Uri magnet) {

        Constraints.Builder builder = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED);


        Data.Builder data = new Data.Builder();
        data.putString(API.MAGNET_SCHEME, magnet.toString());

        return new OneTimeWorkRequest.Builder(DownloadMagnetWorker.class)
                .addTag(API.WORK_TAG)
                .addTag(magnet.toString())
                .setInputData(data.build())
                .setConstraints(builder.build())
                .setExpedited(OutOfQuotaPolicy.RUN_AS_NON_EXPEDITED_WORK_REQUEST)
                .build();

    }

    public static void download(@NonNull Context context, @NonNull Uri uri) {
        WorkManager.getInstance(context).enqueueUniqueWork(uri.toString(),
                ExistingWorkPolicy.KEEP, getWork(uri));
    }


    @NonNull
    @Override
    public Result doWork() {

        long start = System.currentTimeMillis();

        LogUtils.error(TAG, " start DownloadMagnetWorker [" +
                (System.currentTimeMillis() - start) + "]...");

        try {
            String magnet = getInputData().getString(API.MAGNET_SCHEME);
            Objects.requireNonNull(magnet);

            MagnetUri magnetUri = MagnetUriParser.parse(magnet);

            String name = magnet;
            if (magnetUri.getDisplayName().isPresent()) {
                name = magnetUri.getDisplayName().get();
            }

            byte[] id = new IdentityService().getID();

            EventBus eventBus = Runtime.provideEventBus();
            ContentStorage storage = new ContentStorage(
                    getApplicationContext(), eventBus, name);
            Runtime runtime = new Runtime(PeerId.fromBytes(id), eventBus,
                    ContentStorage.nextFreePort());

            Client client = new ClientBuilder().runtime(runtime)
                    .storage(storage).magnet(magnet).build();


            client.startAsync((torrentSessionState) -> {

                long completePieces = torrentSessionState.getPiecesComplete();
                long totalPieces = torrentSessionState.getPiecesTotal();
                int progress = (int) ((completePieces * 100.0f) / totalPieces);

                LogUtils.error(TAG, "progress : " + progress +
                        " pieces : " + completePieces + "/" + totalPieces);


                setProgressAsync(new Data.Builder().putInt(API.APP_KEY, progress).build());

                if (isStopped()) {
                    try {
                        client.stop();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                }
            }, 1000).join();

            if (!isStopped()) {
                File file = storage.getRootFile();
                Stack<String> dirs = new Stack<>();
                if (file.isDirectory()) {
                    uploadDirectory(file, dirs);
                } else {
                    uploadFile(file, dirs);
                }
                API.deleteFile(file, true);
            }
        } catch (Throwable throwable) {
            return Result.failure(new Data.Builder().putString(API.WORK_FAILURE,
                    throwable.getMessage()).build());
        } finally {
            LogUtils.error(TAG, " finish DownloadMagnetWorker [" +
                    (System.currentTimeMillis() - start) + "]...");
        }
        if (!isStopped()) {
            return Result.success();
        }
        return Result.retry();
    }

    private void uploadFile(@NonNull File file, @NonNull Stack<String> dirs) throws IOException {
        String name = file.getName();
        Objects.requireNonNull(name);

        String mimeType = MimeTypeService.getMimeType(name);
        String path = Environment.DIRECTORY_DOWNLOADS + File.separator + String.join((File.separator), dirs);

        Uri downloads = API.downloadsUri(getApplicationContext(), mimeType, name, path);
        Objects.requireNonNull(downloads);
        ContentResolver contentResolver = getApplicationContext().getContentResolver();

        try (InputStream is = new FileInputStream(file)) {
            try (OutputStream os = contentResolver.openOutputStream(downloads)) {
                Objects.requireNonNull(os, "Failed to open output stream");

                Utils.copy(is, os, progress -> setProgressAsync(new Data.Builder()
                        .putInt(API.APP_KEY, progress).build()), file.length());
            }
        } catch (Throwable throwable) {
            contentResolver.delete(downloads, null, null);
            throw throwable;
        }

    }

    private void uploadDirectory(@NonNull File dir, Stack<String> dirs) throws IOException {
        dirs.add(dir.getName());
        String[] children = dir.list();
        if (children != null) {
            for (String child : children) {
                File fileChild = new File(dir, child);
                if (fileChild.isDirectory()) {
                    uploadDirectory(fileChild, dirs);
                } else {
                    uploadFile(fileChild, dirs);
                }
            }
        }
        dirs.pop();
    }
}

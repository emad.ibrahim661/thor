Version 149
-------------
- Support JDK version 17
- Support Android SDK 34
- Update dependencies (libraries)
- switch to immutable java records (jdk 16)
- use of pattern variables (jdk 16)
- support of enhanced switch statements (jdk 14)
- use CodedInputStream instead of ByteBuffer
- mplex deprecated (therefore noise not required anymore)
- UI nicer reset browser message
- simplify the sequence in ipns record (now it is System.currentTimeMillis() when creating)
- Identify (identify.proto) contains a SignedPeerRecord (envelope.proto, peer_record.proto)
- Support of own protocol "/lite/swap/1.0.0", used for faster data transfer instead of bitswap
- Strict quic-v1 support
- Removing of unnecessary wrapper classes (e.g. around protobuf)
- Identity ProtocolVersion will not be reported anymore
- QUIC: first steps to support Datagram [required for MASQUE]
- QUIC: Fix of server name extension [TLS]
- UI: Enter Uri Dialog (for specifying inps and ipfs uris)

Version 150
-------------
- QUIC: Optimize Memory usage
- Update dependencies (libraries)
- Remove Feature: Protocols can not be added dynamically [faster access to protocols]
- Integrate MDNS which is compatible to kubo (go implementation)

Version 151
-------------
- UI: Automatic reload of current web page after network is available again
- UI: Progress indicator is now circular and in the main menu
- UI: Download Folder changed to the Android System Download folder (no internal download folder)
- UI: Min SDK version is now SDK 29 (due to some features regarding Download Folder)
- Feature : Define a sorting order on Multiaddr (might be important for dialing)
- Bugfix : Inline data for CID of type File, instead of creating always a Raw child node
- UI: Simplify the Tabs View (only icons are shown instead of a bitmap of the web view)

Version 152
-------------
- API: Simplify MDNS and Swarm interface
- Feature: Introduce Request and Response message style starting with own protocols
- API: Update network has to be triggered from the outside (when network changed,
fixes a bug, that also a server should be triggered)
- API: Server publish in identity only public [dialable] addresses (before it also publish
circuit addresses when available)
- API: Own identity [IdentityHandler] of a client or server connection are now pre-stored [faster access]
- API: Identify [IdentityHandler] does not include the observed address anymore [faster generation and faster access]
- UI: tiny bugfixes
- Feature: Initial version of upnp [not yet active]
- Feature: better ALPN support for QUIC [plan to use ALPN different from libp2p for own protocols]
- MDNS: Support of protocols in the discovery service
- MDNS: Support of registration service
- MDNS: Kubo mdns is not supported anymore (due to bad kubo performance)
- API: fixes + coding style issues
- Android: MinSdk Version is now 29 (Android Q) This enables the use of a faster DNS Resolver,
 which is non blocking [though not yet used, but will be important for virtual threads]
- API: DnsResolver based on Android DnsResolver (MinSdk 29)


Version 153
-----------
- API: Finish Lite ALPN (request and response style)
- API: Optimization memory and performance
- API: Slowly migrate API to be used for virtual threads [no/less completable future]
- API: feat(ipns): refactored IPNS package with lean records (https://github.com/ipfs/boxo/pull/339)
- UI: Introduce Home button to default search engine web page or default home page
- UI: Enhance Search Engines with Ecosia and Bing

Version 154
-----------
- UI: Improve Search-Enter URI Dialog (more like Chrome Browser)
- Update Libraries

Version 155
------------------
- Update Libraries
- Use Fetch protocol
- Rename IPFS to LITE
- Rename package threads.lite to tech.lp2p (for future maven release)
- Rename ProtobufDag to DAG_PB
- Cleanup Multihash
- dht folder classes starts with Dht...
- Bugfix LITE.createEnvelope()
- Simplify BlockStore
- Remove PageStore
- Introduce Envelope class for pushing protocol
- Introduce IpnsRecord class for pull protocol
- Improve documentation for multiaddr, peeraddr
- Remove autonat protocol (not required for ipv6)
- PeerStore now part of Session and Server definition (not global anymore)
- Network class in Android free now
- Rename Page to Entry (IpnsEntry) and IpnsRecord to Record
- Remove observed attribute completely from PeerInfo
- Remove mdns functionality to calling Android apps
- Only IPv6 addresses are now supported for connections
- Remove bitswap protocol

Version 159
------------------
- Fix of text in tabs (etc. bookmark)
- UI improve progress indication for loading urls
- Fix name of apk Downloads
- Fix multiple instance of download manager
- Simplify state model

Version 160
------------------
- UI Fix of handling tabs and content

Version 1.6.1
------------------
- Update libraries
- Min SDK is 33

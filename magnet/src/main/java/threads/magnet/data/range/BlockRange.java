package threads.magnet.data.range;

import java.nio.ByteBuffer;

import threads.magnet.data.BlockSet;
import threads.magnet.net.buffer.ByteBufferView;

/**
 * @since 1.3
 */
public record BlockRange(Range delegate, long offset, MutableBlockSet blockSet) implements Range {


    /**
     * Create a block-structured data range.
     *
     * @since 1.2
     */
    static BlockRange createBlockRange(Range delegate, long blockSize) {
        return new BlockRange(delegate, 0,
                MutableBlockSet.createMutableBlockSet(delegate.length(), blockSize));
    }

    /**
     * @since 1.3
     */
    public BlockSet getBlockSet() {
        return blockSet;
    }

    @Override
    public long length() {
        return delegate.length();
    }

    @Override
    public BlockRange getSubrange(long offset, long length) {
        return new BlockRange(delegate.getSubrange(offset, length), offset, blockSet);
    }

    @Override
    public BlockRange getSubrange(long offset) {
        return new BlockRange(delegate.getSubrange(offset), offset, blockSet);
    }

    @Override
    public byte[] getBytes() {
        return delegate.getBytes();
    }

    @Override
    public boolean getBytes(ByteBuffer buffer) {
        return delegate.getBytes(buffer);
    }

    @Override
    public void putBytes(byte[] block) {
        delegate.putBytes(block);
        blockSet.markAvailable(offset, block.length);
    }

    @Override
    public void putBytes(ByteBufferView buffer) {
        int length = buffer.remaining();
        delegate.putBytes(buffer);
        blockSet.markAvailable(offset, length);
    }


    public Range getDelegate() {
        return delegate;
    }
}

package threads.magnet.torrent;

import java.util.Deque;
import java.util.concurrent.LinkedBlockingDeque;

import threads.magnet.net.ConnectionKey;
import threads.magnet.protocol.Cancel;
import threads.magnet.protocol.Choke;
import threads.magnet.protocol.Interested;
import threads.magnet.protocol.Message;
import threads.magnet.protocol.NotInterested;
import threads.magnet.protocol.Piece;
import threads.magnet.protocol.Unchoke;

record RoutingPeerWorker(ConnectionState connectionState, MessageRouter router,
                         MessageContext context, Deque<Message> outgoingMessages)
        implements PeerWorker {


    static RoutingPeerWorker create(ConnectionKey connectionKey, MessageRouter router) {
        ConnectionState connectionState = new ConnectionState();
        return new RoutingPeerWorker(connectionState, router,
                new MessageContext(connectionKey, connectionState), new LinkedBlockingDeque<>());
    }

    private static boolean isUrgent(Message message) {
        Class<? extends Message> messageType = message.getClass();
        return Choke.class.equals(messageType) || Unchoke.class.equals(messageType) || Cancel.class.equals(messageType);
    }

    @Override
    public ConnectionState getConnectionState() {
        return connectionState;
    }

    @Override
    public void accept(Message message) {
        router.consume(message, context);
        updateConnection();
    }

    private void postMessage(Message message) {
        if (isUrgent(message)) {
            addUrgent(message);
        } else {
            add(message);
        }
    }

    private void add(Message message) {
        outgoingMessages.add(message);
    }

    private void addUrgent(Message message) {
        outgoingMessages.addFirst(message);
    }

    @Override
    public Message get() {
        if (outgoingMessages.isEmpty()) {
            router.produce(this::postMessage, context);
            updateConnection();
        }
        return postProcessOutgoingMessage(outgoingMessages.poll());
    }

    private Message postProcessOutgoingMessage(Message message) {

        if (message == null) {
            return null;
        }

        Class<? extends Message> messageType = message.getClass();

        if (Piece.class.equals(messageType)) {
            Piece piece = (Piece) message;
            // check that peer hadn't sent cancel while we were preparing the requested block
            if (isCancelled(piece)) {
                // dispose of message
                return null;
            }
        }
        if (Interested.class.equals(messageType)) {
            connectionState.setInterested(true);
        }
        if (NotInterested.class.equals(messageType)) {
            connectionState.setInterested(false);
        }
        if (Choke.class.equals(messageType)) {
            connectionState.setShouldChoke(true);
        }
        if (Unchoke.class.equals(messageType)) {
            connectionState.setShouldChoke(false);
        }

        return message;
    }

    private boolean isCancelled(Piece piece) {

        int pieceIndex = piece.pieceIndex(),
                offset = piece.offset(),
                length = piece.length();

        return connectionState.getCancelledPeerRequests().remove(Mapper.buildKey(pieceIndex, offset, length));
    }

    private void updateConnection() {
        Choker.handleConnection(connectionState, this::postMessage);
    }
}

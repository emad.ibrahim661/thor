package threads.magnet.processor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Objects;
import java.util.function.BiFunction;

public class ListenerSource extends HashMap<ProcessingEvent, Collection<BiFunction<MagnetContext, ProcessingStage, ProcessingStage>>> {

    /**
     * Add processing event listener.
     * <p>
     * Processing event listener is a generic {@link BiFunction},
     * that accepts the processing context and default next stage
     * and returns the actual next stage (i.e. it can also be considered a router).
     *
     * @param event    Type of processing event to be notified of
     * @param listener Routing function
     * @since 1.5
     */
    public void addListener(ProcessingEvent event, BiFunction<MagnetContext, ProcessingStage, ProcessingStage> listener) {
        this.computeIfAbsent(event, it -> new ArrayList<>()).add(listener);
    }

    /**
     * @param event Type of processing event
     * @return Collection of listeners, that are interested in being notified of a given event
     * @since 1.5
     */
    public Collection<BiFunction<MagnetContext, ProcessingStage, ProcessingStage>> getListeners(ProcessingEvent event) {
        Objects.requireNonNull(event);
        return this.getOrDefault(event, Collections.emptyList());
    }
}

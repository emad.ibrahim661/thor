package threads.magnet.processor;

import threads.magnet.event.EventSink;
import threads.magnet.torrent.TorrentRegistry;

class ProcessMagnetTorrentStage extends ProcessTorrentStage {

    ProcessMagnetTorrentStage(TorrentRegistry torrentRegistry,
                              EventSink eventSink) {
        super(torrentRegistry, eventSink);
    }
}

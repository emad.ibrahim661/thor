package threads.magnet;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Objects;

public interface LogUtils {
    String TAG = LogUtils.class.getSimpleName();

    @SuppressWarnings("SameReturnValue")
    static boolean isDebug() {
        return false;
    }

    static void debug(@Nullable final String tag, @NonNull String message) {
        if (isDebug()) {
            System.out.println(tag + " " + message);
        }
    }

    static void verbose(@Nullable final String tag, @NonNull String message) {
        if (isDebug()) {
            System.out.println(tag + " " + message);
        }
    }

    static void warning(@Nullable final String tag, @Nullable String message) {
        if (isDebug()) {
            System.out.println(tag + " " + Objects.requireNonNullElse(message, "No warning message defined"));
        }
    }

    static void info(@Nullable final String tag, @NonNull String message) {
        if (isDebug()) {
            System.out.println(tag + " " + message);
        }
    }

    static void error(@Nullable final String tag, @Nullable String message) {
        if (isDebug()) {
            System.err.println(tag + " " + Objects.requireNonNullElse(message, "No error message defined"));
        }
    }

    static void error(@Nullable final String tag, @Nullable String message,
                      @NonNull Throwable throwable) {
        if (isDebug()) {
            System.err.println(tag + " " + message);
            throwable.printStackTrace(System.err);
        }
    }

    static void error(final String tag, @NonNull Throwable throwable) {
        if (isDebug()) {
            System.err.println(tag + " " + throwable.getLocalizedMessage());
            throwable.printStackTrace(System.err);
        }
    }
}

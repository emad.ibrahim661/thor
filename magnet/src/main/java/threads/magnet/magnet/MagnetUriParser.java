package threads.magnet.magnet;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import threads.magnet.LogUtils;
import threads.magnet.metainfo.TorrentId;
import threads.magnet.net.InetPeerAddress;
import threads.magnet.protocol.Protocols;


public interface MagnetUriParser {

    String TAG = MagnetUriParser.class.getSimpleName();
    String SCHEME = "magnet";
    String INFOHASH_PREFIX = "urn:btih:";


    /**
     * Create a magnet URI from its' string representation in BEP-9 format.
     * Current limitations:
     * - only v1 links are supported (xt=urn:btih:&lt;info-hash&gt;)
     * - base32-encoded info hashes are not supported
     *
     * @since 1.3
     */
    static MagnetUri parse(String uriString) {
        try {
            return parse(new URI(uriString));
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException("Invalid URI: " + uriString, e);
        }
    }

    /**
     * Create a magnet URI from its' URI representation in BEP-9 format.
     * Current limitations:
     * - only v1 links are supported (xt=urn:btih:&lt;info-hash&gt;)
     * - base32-encoded info hashes are not supported
     *
     * @since 1.3
     */
    private static MagnetUri parse(URI uri) {
        if (!SCHEME.equals(uri.getScheme())) {
            throw new IllegalArgumentException("Invalid scheme: " + uri.getScheme());
        }

        Map<String, List<String>> paramsMap = collectParams(uri);

        Set<String> infoHashes = getRequiredParam(paramsMap).stream()
                .filter(value -> value.startsWith(INFOHASH_PREFIX))
                .map(value -> value.substring(INFOHASH_PREFIX.length()))
                .collect(Collectors.toSet());
        if (infoHashes.size() != 1) {
            throw new IllegalStateException(String.format("Parameter '%s' has invalid number of values with prefix '%s': %s",
                    UriParams.TORRENT_ID, INFOHASH_PREFIX, infoHashes.size()));
        }
        TorrentId torrentId = buildTorrentId(infoHashes.iterator().next());
        MagnetUri.Builder builder = MagnetUri.torrentId(torrentId);

        getOptionalParam(UriParams.DISPLAY_NAME, paramsMap).stream().findAny().ifPresent(builder::name);
        getOptionalParam(UriParams.TRACKER_URL, paramsMap).forEach(builder::tracker);
        getOptionalParam(UriParams.PEER, paramsMap).forEach(value -> {
            try {
                builder.peer(parsePeer(value));
            } catch (Exception e) {
                LogUtils.warning(TAG, "Failed to parse peer address: " + value);
            }
        });

        return builder.buildUri();
    }

    private static Map<String, List<String>> collectParams(URI uri) {
        Map<String, List<String>> paramsMap = new HashMap<>();

        // magnet:?param1=value1...
        // uri.getSchemeSpecificPart() will start with the question mark and contain all name-value pairs
        String[] params = uri.getSchemeSpecificPart().substring(1).split("&");
        for (String param : params) {
            String[] parts = param.split("=");
            String name = parts[0];
            String value = parts[1];
            List<String> values = paramsMap.computeIfAbsent(name, k -> new ArrayList<>());
            values.add(value);
        }

        return paramsMap;
    }

    private static List<String> getRequiredParam(Map<String, List<String>> paramsMap) {
        List<String> values = paramsMap.getOrDefault(UriParams.TORRENT_ID, Collections.emptyList());
        Objects.requireNonNull(values);
        if (values.isEmpty()) {
            throw new IllegalStateException(String.format("Required parameter '%s' is missing: %s",
                    UriParams.TORRENT_ID, 0));
        }
        return values;
    }

    private static List<String> getOptionalParam(String paramName,
                                                 Map<String, List<String>> paramsMap) {
        return paramsMap.getOrDefault(paramName, Collections.emptyList());
    }

    private static TorrentId buildTorrentId(String infoHash) {
        byte[] bytes;
        int len = infoHash.length();
        if (len == 40) {
            bytes = Protocols.fromHex(infoHash);
        } else if (len == 32) {
            bytes = Protocols.infoHashFromBase32(infoHash);
        } else {
            throw new IllegalArgumentException("Invalid info hash length: " + len);
        }
        return TorrentId.fromBytes(bytes);
    }

    private static InetPeerAddress parsePeer(String value) {
        String[] parts = value.split(":");
        if (parts.length != 2) {
            throw new IllegalArgumentException("Invalid peer format: " + value + "; should be <host>:<port>");
        }
        String hostname = parts[0];
        int port = Integer.parseInt(parts[1]);
        return new InetPeerAddress(hostname, port);
    }

    class UriParams {
        private static final String TORRENT_ID = "xt";
        private static final String DISPLAY_NAME = "dn";
        private static final String TRACKER_URL = "tr";
        private static final String PEER = "x.pe";
    }
}

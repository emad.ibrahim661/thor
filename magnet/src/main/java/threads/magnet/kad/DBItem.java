package threads.magnet.kad;


import java.util.Arrays;


public interface DBItem extends Comparable<DBItem> {

    byte[] item();

    // sort by raw data. only really useful for binary search
    default int compareTo(DBItem other) {
        return Arrays.compareUnsigned(item(), other.item());
    }
}

package threads.magnet.protocol.extended;

import threads.magnet.protocol.Message;

public interface ExtendedMessage extends Message {

    @Override
    default Integer getMessageId() {
        return ExtendedProtocol.EXTENDED_MESSAGE_ID;
    }
}

package threads.magnet.protocol.handler;

import java.nio.ByteBuffer;
import java.util.Objects;

import threads.magnet.net.buffer.ByteBufferView;
import threads.magnet.protocol.Bitfield;
import threads.magnet.protocol.DecodingContext;
import threads.magnet.protocol.EncodingContext;
import threads.magnet.protocol.Message;

public final class BitfieldHandler extends UniqueMessageHandler<Bitfield> {

    public BitfieldHandler() {
        super(Bitfield.class);
    }

    // bitfield: <len=0001+X><id=5><bitfield>
    private static int decodeBitfield(DecodingContext context, ByteBufferView buffer, int length) {

        int consumed = 0;

        if (buffer.remaining() >= length) {
            byte[] bitfield = new byte[length];
            buffer.get(bitfield);
            context.setMessage(new Bitfield(bitfield));
            consumed = length;
        }

        return consumed;
    }

    @Override
    public int doDecode(DecodingContext context, ByteBufferView buffer) {
        return decodeBitfield(context, buffer, buffer.remaining());
    }

    @Override
    public boolean doEncode(EncodingContext context, Message base, ByteBuffer buffer) {
        Bitfield message = (Bitfield) base;
        Objects.requireNonNull(message);
        if (buffer.remaining() < message.bitfield().length) {
            return false;
        }
        buffer.put(message.bitfield());
        return true;
    }
}

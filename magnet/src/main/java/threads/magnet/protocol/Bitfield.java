package threads.magnet.protocol;

import androidx.annotation.NonNull;

public record Bitfield(byte[] bitfield) implements Message {

    /**
     * @since 1.0
     */
    public Bitfield {
    }

    /**
     * @since 1.0
     */
    @Override
    public byte[] bitfield() {
        return bitfield;
    }

    @NonNull
    @Override
    public String toString() {
        return "[" + this.getClass().getSimpleName() + "] bitfield {" + bitfield.length + " bytes}";
    }

    @Override
    public Integer getMessageId() {
        return StandardBittorrentProtocol.BITFIELD_ID;
    }
}

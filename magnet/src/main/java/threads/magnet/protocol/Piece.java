package threads.magnet.protocol;

import java.nio.ByteBuffer;
import java.util.Objects;

import threads.magnet.torrent.BlockReader;

public record Piece(BlockReader reader, int pieceIndex, int offset, int length) implements Message {


    // TODO: using BlockReader here is sloppy... just temporary
    public static Piece create(int pieceIndex, int offset, int length, BlockReader reader) throws InvalidMessageException {
        if (pieceIndex < 0 || offset < 0 || length <= 0) {
            throw new InvalidMessageException("Invalid arguments: piece index (" +
                    pieceIndex + "), offset (" + offset + "), block length (" + length + ")");
        }
        return new Piece(reader, pieceIndex, offset, length);
    }

    // TODO: Temporary (used only for incoming pieces)
    public static Piece create(int pieceIndex, int offset, int length) throws InvalidMessageException {
        if (pieceIndex < 0 || offset < 0 || length <= 0) {
            throw new InvalidMessageException("Invalid arguments: piece index (" +
                    pieceIndex + "), offset (" + offset + "), block length (" + length + ")");
        }
        return new Piece(null, pieceIndex, offset, length);
    }


    public boolean writeBlockTo(ByteBuffer buffer) {
        Objects.requireNonNull(reader);
        return reader.readTo(buffer);
    }


    @Override
    public Integer getMessageId() {
        return StandardBittorrentProtocol.PIECE_ID;
    }
}

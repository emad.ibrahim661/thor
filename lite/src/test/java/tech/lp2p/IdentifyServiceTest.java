package tech.lp2p;


import static org.junit.Assert.assertNotNull;

import junit.framework.TestCase;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import tech.lp2p.core.Identify;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Server;

public class IdentifyServiceTest {


    @BeforeClass
    public static void setup() {
        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }

    @Test
    public void identifyTest() throws Exception {

        Lite lite = TestEnv.getTestInstance();
        assertNotNull(lite);

        Server server = TestEnv.getServer();
        TestCase.assertNotNull(server);

        Identify info = server.identify();
        assertNotNull(info);

        assertNotNull(info.agent());
        assertNotNull(info.peerId());

        Peeraddr[] list = info.peeraddrs();
        assertNotNull(list);
        for (Peeraddr addr : list) {
            TestEnv.error(addr.toString());
        }

    }
}

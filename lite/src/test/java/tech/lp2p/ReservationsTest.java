package tech.lp2p;


import static junit.framework.TestCase.assertNotNull;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Reservation;
import tech.lp2p.core.Server;


public class ReservationsTest {


    @BeforeClass
    public static void setup() {
        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }

    @Test
    public void test_reservations() throws Exception {

        Lite lite = TestEnv.getTestInstance();
        assertNotNull(lite);
        Server server = TestEnv.getServer();
        assertNotNull(server);

        if (!Lite.hasReservations(server)) {
            TestEnv.error("nothing to test no dialable addresses");
            return;
        }

        for (Peeraddr ma : Lite.reservationPeeraddrs(server)) {
            TestEnv.error(ma.toString());
        }


        int timeInMinutes = 1; // make higher for long run


        // test 1 minutes
        for (int i = 0; i < timeInMinutes; i++) {
            Thread.sleep(TimeUnit.MINUTES.toMillis(1));

            Set<Reservation> reservations = Lite.reservations(server);
            for (Reservation reservation : reservations) {
                TestEnv.error("Expire in minutes " +
                        reservation.expireInMinutes() + " " + reservation);
                assertNotNull(reservation.peeraddr());
                assertNotNull(reservation.peerId());
            }
        }

    }

}

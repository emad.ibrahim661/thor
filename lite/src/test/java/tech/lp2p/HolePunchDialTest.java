package tech.lp2p;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Objects;

import tech.lp2p.core.Connection;
import tech.lp2p.core.Envelope;
import tech.lp2p.core.HopConnect;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;

public class HolePunchDialTest {


    @BeforeClass
    public static void setup() {
        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }

    @Test
    public void testDialLite() throws Throwable {

        Lite lite = TestEnv.getTestInstance();

        Server server = TestEnv.getServer();
        assertNotNull(server);


        if (!Lite.hasReservations(server)) {
            TestEnv.error("nothing to test no dialable addresses");
            return;
        }

        Dummy dummy = new Dummy();

        Session dummySession = dummy.createSession();

        HopConnect hopConnect = Lite.hopConnect(dummySession, lite.self(),
                Lite.createParameters(),
                peeraddr -> TestEnv.error("Try " + peeraddr.toString()),
                peeraddr -> TestEnv.error("Failure " + peeraddr.toString()), 120);

        assertNotNull(hopConnect);
        assertNotNull(hopConnect.relay());
        assertNotNull(hopConnect.connection());


        try (Connection connection = hopConnect.connection()) {
            Objects.requireNonNull(connection);

            Envelope envelope = Lite.pullEnvelope(connection, TestEnv.CID);
            assertNotNull(envelope);

            assertTrue(connection.isConnected());
        }
    }

}
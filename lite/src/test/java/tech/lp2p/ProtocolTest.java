package tech.lp2p;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import junit.framework.TestCase;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Map;
import java.util.Objects;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Handler;
import tech.lp2p.core.Identify;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Protocol;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;
import tech.lp2p.ident.IdentifyService;

public class ProtocolTest {


    @BeforeClass
    public static void setup() {
        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }

    @Test
    public void protocol_test() throws Exception {
        Lite lite = TestEnv.getTestInstance();
        Server server = TestEnv.getServer();
        TestCase.assertNotNull(server);


        int libp2pServerProtocols = 3;

        Map<Protocol, Handler> serverProtocols = server.protocols(ALPN.libp2p);
        assertNotNull(serverProtocols);

        assertTrue(serverProtocols.containsKey(Protocol.MULTISTREAM_PROTOCOL));
        assertTrue(serverProtocols.containsKey(Protocol.IDENTITY_PROTOCOL));
        assertTrue(serverProtocols.containsKey(Protocol.RELAY_PROTOCOL_HOP));
        assertEquals(serverProtocols.size(), libp2pServerProtocols);


        serverProtocols = server.protocols(ALPN.lite);
        assertNotNull(serverProtocols);

        assertTrue(serverProtocols.containsKey(Protocol.LITE_PUSH_PROTOCOL));
        assertTrue(serverProtocols.containsKey(Protocol.LITE_PULL_PROTOCOL));
        assertTrue(serverProtocols.containsKey(Protocol.LITE_FETCH_PROTOCOL));
        assertEquals(serverProtocols.size(), 3);

        Session session = lite.createSession(TestEnv.getBlockStore(),
                TestEnv.PEERS);


        Map<Protocol, Handler> protocols = session.protocols(ALPN.libp2p);
        assertNotNull(protocols);

        assertTrue(protocols.containsKey(Protocol.MULTISTREAM_PROTOCOL));
        assertTrue(protocols.containsKey(Protocol.IDENTITY_PROTOCOL));
        assertEquals(protocols.size(), 2);


        protocols = session.protocols(ALPN.lite);
        assertNotNull(protocols);
        assertEquals(protocols.size(), 0);

        // for testing we are connecting to our own server
        Peeraddr ownLocalServerAddress = Peeraddr.loopbackPeeraddr(
                lite.self(), server.port());
        Objects.requireNonNull(ownLocalServerAddress);

        try (Connection connection = Lite.dial(session, ownLocalServerAddress,
                Parameters.create(ALPN.libp2p))) {
            assertNotNull(connection);

            Identify info = IdentifyService.identify(connection);
            TestCase.assertNotNull(info);

            assertEquals(libp2pServerProtocols, info.protocols().length);

        }


        Thread.sleep(3000);
        assertFalse(server.hasConnection(lite.self()));

    }

}

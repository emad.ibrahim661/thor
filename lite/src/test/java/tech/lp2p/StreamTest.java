package tech.lp2p;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import tech.lp2p.core.Raw;
import tech.lp2p.core.Session;

public class StreamTest {


    @BeforeClass
    public static void setup() {
        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }


    @Test
    public void test_string() throws Exception {
        Lite lite = TestEnv.getTestInstance();

        Session session = lite.createSession(TestEnv.getBlockStore(), TestEnv.PEERS);
        String text = "Hello Moin und Zehn Elf";
        Raw raw = Lite.storeText(session, text);
        assertNotNull(raw);

        byte[] result = Lite.fetchData(session, raw.cid());
        assertNotNull(result);
        assertEquals(text, new String(result));

        Raw raw1 = Lite.storeText(session, "TEST test");
        assertNotNull(raw1);
    }


}

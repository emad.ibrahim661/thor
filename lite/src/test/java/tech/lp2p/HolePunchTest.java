package tech.lp2p;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.net.DatagramSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Envelope;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;
import tech.lp2p.quic.ConnectionBuilder;

public class HolePunchTest {


    @BeforeClass
    public static void setup() {

        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }

    @Test
    public void testHolePunch() throws Throwable {

        Lite lite = TestEnv.getTestInstance();
        assertNotNull(lite);
        Server server = TestEnv.getServer();
        assertNotNull(server);
        Peeraddr peeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());
        assertNotNull(peeraddr);

        AtomicInteger success = new AtomicInteger(0);

        int instances = 10;
        ExecutorService service = Executors.newFixedThreadPool(4);

        for (int i = 0; i < instances; i++) {
            service.execute(() -> {
                try {
                    Parameters parameters = Lite.createParameters(false);
                    Dummy dummy = new Dummy();
                    try (DatagramSocket socket = new DatagramSocket()) {
                        Peeraddr dummyPeeraddr = Peeraddr.loopbackPeeraddr(dummy.self(),
                                socket.getLocalPort());

                        server.holePunching(dummyPeeraddr); // start punching

                        Session dummySession = dummy.createSession();

                        try (Connection connection = ConnectionBuilder.connect(dummySession,
                                peeraddr, parameters, dummySession.responder(ALPN.lite), socket)) {
                            assertNotNull(connection);

                            Thread.sleep(500);
                            Envelope envelope = Lite.pullEnvelope(connection, TestEnv.CID);
                            assertNotNull(envelope);

                            success.incrementAndGet();
                        }
                    }
                } catch (Throwable throwable) {
                    TestEnv.error(throwable); // not expected
                }
            });
        }

        service.shutdown();

        assertTrue(service.awaitTermination(60, TimeUnit.SECONDS));
        service.shutdownNow();

        assertEquals(success.get(), instances);
    }
}
package tech.lp2p;


import static org.junit.Assert.assertFalse;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Session;
import tech.lp2p.utils.Utils;

public class FindClosestPeersTest {


    @BeforeClass
    public static void setup() {
        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }


    @Test
    public void find_closest_peers() throws Exception {
        Lite lite = TestEnv.getTestInstance();


        Set<Peeraddr> found = ConcurrentHashMap.newKeySet();

        Session session = lite.createSession(TestEnv.getBlockStore(), TestEnv.PEERS);

        Utils.runnable(() -> Lite.findClosestPeeraddrs(session, lite.self(), found::add), 60);

        for (Peeraddr peeraddr : found) {
            TestEnv.error(peeraddr.toString());
        }

        if (TestEnv.hasNetwork()) {
            assertFalse(found.isEmpty());
        }
    }
}

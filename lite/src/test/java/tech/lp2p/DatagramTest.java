package tech.lp2p;

import static junit.framework.TestCase.assertNotNull;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Objects;

import tech.lp2p.core.Connection;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;

public class DatagramTest {

    @BeforeClass
    public static void setup() {
        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }

    @Test
    public void datagramTest() throws Exception {

        Lite lite = TestEnv.getTestInstance();
        Server server = TestEnv.getServer();
        assertNotNull(server);


        Dummy dummy = new Dummy();

        PeerId host = lite.self();
        assertNotNull(host);
        Peeraddr peeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());

        Session dummySession = dummy.createSession();
        Parameters parameters = Lite.createParameters(false);
        try (Connection connection = Lite.dial(dummySession, peeraddr, parameters)) {
            Objects.requireNonNull(connection);

            String text = "Moin Moin";

            // now send a datagram [no way yet implemented to check if the
            // datagram has arrived on the server, just an error log LogUtils.error,
            // since no application is defined to handle it]
            ((tech.lp2p.quic.Connection) connection).sendDatagram(text.getBytes());

        }
    }

}

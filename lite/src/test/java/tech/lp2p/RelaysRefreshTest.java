package tech.lp2p;


import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import tech.lp2p.core.Reservation;
import tech.lp2p.core.Server;


public class RelaysRefreshTest {


    @BeforeClass
    public static void setup() {
        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }

    @Test
    public void refreshReservations() throws Exception {

        Lite lite = TestEnv.getTestInstance();
        assertNotNull(lite);
        Server server = TestEnv.getServer();
        assertNotNull(server);

        if (!Lite.hasReservations(server)) {
            TestEnv.error("nothing to test no dialable addresses");
            return;
        }

        for (Reservation reservation : Lite.reservations(server)) {
            TestEnv.error(reservation.toString());
            assertTrue(reservation.expireInMinutes() > 0);
            TestEnv.error("Update required " + reservation.updateRequired());

            Reservation refreshed = Lite.refreshReservation(server, reservation);
            assertNotNull(refreshed);
            TestEnv.error(refreshed.toString());
            assertTrue(refreshed.expireInMinutes() >= reservation.expireInMinutes());
            assertEquals(refreshed.peerId(), reservation.peerId());
        }

    }

}

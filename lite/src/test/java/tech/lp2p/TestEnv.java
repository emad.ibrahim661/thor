package tech.lp2p;

import static org.junit.Assert.assertNotNull;

import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Envelope;
import tech.lp2p.core.Fid;
import tech.lp2p.core.Keys;
import tech.lp2p.core.Payload;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.PeerStore;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Progress;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;
import tech.lp2p.dag.DagService;
import tech.lp2p.utils.Utils;

final class TestEnv {
    public static final Payload RAW = new Payload(10);
    public static final Payload CID = new Payload(20);
    public static final String AGENT = "lite/1.0.0/";
    public static final AtomicReference<Envelope> PUSHED = new AtomicReference<>();
    public static final int ITERATIONS = Lite.CHUNK_DEFAULT;
    public static final Peeraddrs BOOTSTRAP = new Peeraddrs();
    public static final PeerStore PEERS = new PeerStore() {
        private final Set<Peeraddr> peers = ConcurrentHashMap.newKeySet();

        @Override
        public List<Peeraddr> peeraddrs(int limit) {
            return peers.stream().limit(limit).collect(Collectors.toList());
        }


        @Override
        public void storePeeraddr(Peeraddr peeraddr) {
            peers.add(peeraddr);
        }

        @Override
        public void removePeeraddr(Peeraddr peeraddr) {
            peers.remove(peeraddr);
        }
    };
    private static final boolean DEBUG = false;

    private static final AtomicReference<Server> SERVER = new AtomicReference<>();

    private static final ReentrantLock reserve = new ReentrantLock();
    private static volatile Lite INSTANCE = null;
    private static Lite.Settings SETTINGS;

    static {
        try {
            // "/ip4/104.131.131.82/udp/4001/quic/p2p/QmaCpDMGvV2BGHeYERUEnRQAwe3N8SzbUtfsmvsqQLuvuJ"
            BOOTSTRAP.add(Lite.createPeeraddr(
                    "QmaCpDMGvV2BGHeYERUEnRQAwe3N8SzbUtfsmvsqQLuvuJ",
                    "104.131.131.82", 4001));
            SETTINGS = Lite.createSettings(Lite.generateKeys(), BOOTSTRAP, AGENT);
        } catch (Throwable throwable) {
            TestEnv.error(throwable);
        }
    }


    public static Lite getInstance(Lite.Settings settings) throws Exception {
        if (INSTANCE == null) {
            synchronized (Lite.class) {
                if (INSTANCE == null) {
                    INSTANCE = new Lite(settings);
                }
            }
        }
        return INSTANCE;
    }

    public static PeerId random() {
        try {
            Keys keys = Lite.generateKeys();
            return keys.peerId();
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }


    static File createCacheFile() throws IOException {
        return Files.createTempFile("temp", ".cid").toFile();
    }

    public static byte[] getRandomBytes(int number) {
        byte[] bytes = new byte[number];
        new Random().nextBytes(bytes);
        return bytes;
    }

    public static Fid createContent(Session session, String name, byte[] data) throws Exception {
        try (InputStream inputStream = new ByteArrayInputStream(data)) {
            return Lite.storeInputStream(session, name, inputStream);
        }
    }


    /**
     * @noinspection BooleanMethodIsAlwaysInverted
     */
    public static boolean hasNetwork() {
        return Lite.reservationFeaturePossible(); // not 100 percent correct
    }

    public static BlockStore getBlockStore() {
        try {
            return Store.getInstance();
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }


    public static Server getServer() {
        return SERVER.get();
    }

    public static Lite getTestInstance() throws Exception {
        reserve.lock();
        try {
            Objects.requireNonNull(SETTINGS);
            Lite lite = TestEnv.getInstance(SETTINGS);

            if (SERVER.get() == null) {

                Server server = lite.startServer(Lite.createServerSettings(Utils.nextFreePort()),
                        getBlockStore(), PEERS,
                        peeraddr -> TestEnv.error("Incoming connection : "
                                + peeraddr.toString()),
                        peeraddr -> TestEnv.error("Closing connection : "
                                + peeraddr.toString()),
                        reservationGain -> always("Reservation gain " +
                                reservationGain.toString()),
                        reservationLost -> always("Reservation lost " +
                                reservationLost.toString()),
                        peerId -> {
                            TestEnv.error("Peer Gated : " + peerId.toString());
                            return false;
                        },
                        request -> {
                            try {
                                Payload payload = request.payload();
                                if (Objects.equals(payload, TestEnv.CID)) {
                                    Cid cid = DagService.createEmptyDirectory("");
                                    return Lite.createEnvelope(
                                            Objects.requireNonNull(getServer()), TestEnv.CID, cid);
                                } else {
                                    return null;
                                }
                            } catch (Throwable throwable) {
                                TestEnv.error(throwable);
                            }
                            return null;
                        }, PUSHED::set);


                // just to get coverage of shutdown method
                Runtime.getRuntime().addShutdownHook(new Thread(server::shutdown));
                SERVER.set(server);
                assertNotNull(server.socket());

                if (Lite.reservationFeaturePossible()) {
                    Lite.hopReserve(server, 25, 120);

                    Peeraddrs peeraddrs = Lite.reservationPeeraddrs(server);
                    for (Peeraddr addr : peeraddrs) {
                        TestEnv.error("Reservation Address " + addr.toString());
                    }
                }
            }

            return lite;
        } finally {
            reserve.unlock();
        }
    }

    public static void cleanup() {
        try {
            Store.getInstance().clear();

            if (DEBUG) {
                Server server = getServer();
                if (server != null) {
                    Thread.sleep(3000);
                    always("Number of connections " + server.numConnections());
                }
            }
        } catch (Throwable throwable) {
            TestEnv.error(throwable);
        }
    }

    static void error(String message) {
        if (DEBUG) {
            System.err.println(message);
        }
    }

    static void error(Throwable throwable) {
        throwable.printStackTrace(System.err);
    }

    static void always(String message) {
        System.err.println(message);
    }

    public static class DummyBlockStore implements BlockStore {
        @Override
        public boolean hasBlock(@NotNull Cid cid) {
            return false;
        }


        @Override
        public byte[] getBlock(@NotNull Cid cid) {
            return null;
        }


        @Override
        public void deleteBlock(@NotNull Cid cid) {

        }

        @Override
        public void storeBlock(@NotNull Cid cid, byte[] block) {

        }


    }

    public static class DummyProgress implements Progress {
        @Override
        public void setProgress(int progress) {
            TestEnv.error("Progress " + progress);
        }
    }

    public static class DummyPeerStore implements PeerStore {
        @Override
        public List<Peeraddr> peeraddrs(int limit) {
            return Collections.emptyList();
        }

        @Override
        public void storePeeraddr(Peeraddr peeraddr) {
            always(peeraddr.toString());
        }

        @Override
        public void removePeeraddr(Peeraddr peeraddr) {

        }
    }
}

package tech.lp2p;

import static org.junit.Assert.assertNotNull;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.InputStream;
import java.util.concurrent.atomic.AtomicInteger;

import tech.lp2p.core.Fid;
import tech.lp2p.core.Session;

public class TimeTest {


    @BeforeClass
    public static void setup() {

        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }

    @Test
    public void test_adding_performance() throws Exception {


        Lite lite = TestEnv.getTestInstance();

        long start = System.currentTimeMillis();
        // create dummy session
        int maxNumberBytes = 100 * 1000 * 1000; // 100 MB
        Session session = lite.createSession(
                new TestEnv.DummyBlockStore(), TestEnv.PEERS);
        AtomicInteger counter = new AtomicInteger(0);
        //noinspection AnonymousInnerClassMayBeStatic,AnonymousInnerClass
        Fid cid = Lite.storeInputStream(session, "random.bin", new InputStream() {
            @Override
            public int read() {
                int count = counter.incrementAndGet();
                if (count > maxNumberBytes) {
                    return -1;
                }
                return 99;
            }
        });
        assertNotNull(cid);

        long end = System.currentTimeMillis();
        TestEnv.error("Time for hashing " + (end - start) / 1000 + "[s]");


        start = System.currentTimeMillis();
        session = lite.createSession(TestEnv.getBlockStore(), TestEnv.PEERS);
        counter.set(0);
        //noinspection AnonymousInnerClassMayBeStatic,AnonymousInnerClass
        cid = Lite.storeInputStream(session, "test.bin", new InputStream() {
            @Override
            public int read() {
                int count = counter.incrementAndGet();
                if (count > maxNumberBytes) {
                    return -1;
                }
                return 99;
            }
        });
        assertNotNull(cid);

        end = System.currentTimeMillis();
        TestEnv.error("Time for hashing and storing " + (end - start) / 1000 + "[s]");
    }

}

package tech.lp2p;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;
import java.util.Objects;

import tech.lp2p.core.Cid;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Envelope;
import tech.lp2p.core.Fid;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;
import tech.lp2p.dag.DagService;

public class AlpnTest {


    @BeforeClass
    public static void setup() {
        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }

    @Test
    public void alpnTest() throws Exception {

        Lite lite = TestEnv.getTestInstance();
        Server server = TestEnv.getServer();
        assertNotNull(server);

        Session session = lite.createSession(TestEnv.getBlockStore(),
                TestEnv.PEERS);
        Dummy dummy = new Dummy();

        Peeraddr peeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());

        Session dummySession = dummy.createSession();


        byte[] input = TestEnv.getRandomBytes(10000000); // 10 MB

        Fid fid = TestEnv.createContent(session, "random.bin", input);
        assertNotNull(fid);

        byte[] cmp = Lite.fetchData(session, fid.cid());
        assertArrayEquals(input, cmp);

        List<Cid> cids = Lite.blocks(session, fid.cid());
        assertNotNull(cids);

        Parameters parameters = Lite.createParameters(false);
        try (Connection connection = Lite.dial(dummySession, peeraddr, parameters)) {
            Objects.requireNonNull(connection);

            // simple push test
            Cid cid = Lite.rawCid("moin");
            Envelope data = Lite.createEnvelope(dummySession, TestEnv.RAW, cid);
            Lite.pushEnvelope(connection, data);

            Thread.sleep(1000);
            assertArrayEquals(TestEnv.PUSHED.get().cid().hash(), cid.hash());


            // pull test
            Envelope envelope = Lite.pullEnvelope(connection, TestEnv.CID);
            assertNotNull(envelope);
            PeerId from = envelope.peerId();
            assertEquals(from, lite.self());
            Cid entry = envelope.cid();
            assertNotNull(entry);
            assertEquals(entry, DagService.createEmptyDirectory(""));


            byte[] output = Lite.fetchData(connection, fid.cid(), new TestEnv.DummyProgress());
            assertArrayEquals(input, output);

        }
    }

}

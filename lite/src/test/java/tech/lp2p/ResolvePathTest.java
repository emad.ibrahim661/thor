package tech.lp2p;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.Stack;

import tech.lp2p.core.Connection;
import tech.lp2p.core.Dir;
import tech.lp2p.core.Fid;
import tech.lp2p.core.Info;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;
import tech.lp2p.utils.Utils;

public class ResolvePathTest {


    @BeforeClass
    public static void setup() {

        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }


    @Test
    public void resolvePath() throws Throwable {

        Lite lite = TestEnv.getTestInstance();

        Server server = TestEnv.getServer();
        assertNotNull(server);
        Dir root;
        Stack<String> pathIndex = new Stack<>();
        Stack<String> pathData = new Stack<>();
        String content = "Moin Moin";
        byte[] bytes = TestEnv.getRandomBytes(5000);
        // prepare data
        Session session = lite.createSession(TestEnv.getBlockStore(), TestEnv.PEERS);

        Fid fid = TestEnv.createContent(session, "index.txt", content.getBytes());
        assertNotNull(fid);

        Fid bin = TestEnv.createContent(session, "data.bin", bytes);
        assertNotNull(bin);


        Dir a = Lite.createEmptyDirectory(session, "a");
        assertNotNull(a);
        Dir b = Lite.createEmptyDirectory(session, "b");
        assertNotNull(b);
        b = Lite.addToDirectory(session, b, fid);
        assertNotNull(b);
        a = Lite.addToDirectory(session, a, b);
        assertNotNull(a);
        a = Lite.addToDirectory(session, a, bin);
        assertNotNull(a);

        pathIndex.push("b");
        pathIndex.push("index.txt");

        pathData.push("data.bin");
        root = a;


        assertNotNull(root);
        assertFalse(pathIndex.isEmpty());
        assertFalse(pathData.isEmpty());


        Dummy dummy = new Dummy();

        Session dummySession = dummy.createSession();

        PeerId host = lite.self();
        assertNotNull(host);
        Peeraddr peeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());

        Parameters parameters = Lite.createParameters();
        try (Connection connection = Lite.dial(dummySession, peeraddr, parameters)) {

            Info info = Lite.resolvePath(connection, root.cid(), pathIndex);
            assertNotNull(info);
            assertEquals(info.name(), "index.txt");

            byte[] data;
            try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
                try (InputStream inputStream =
                             Lite.getInputStream(connection,
                                     info.cid())) {
                    Utils.copy(inputStream, outputStream);
                    data = outputStream.toByteArray();
                }
            }
            assertNotNull(data);
            assertArrayEquals(data, content.getBytes());

            info = Lite.resolvePath(connection, root.cid(), pathData);
            assertNotNull(info);
            assertEquals(info.name(), "data.bin");

            try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
                try (InputStream inputStream = Lite.getInputStream(connection,
                        info.cid(), new TestEnv.DummyProgress())) {
                    Utils.copy(inputStream, outputStream);
                    data = outputStream.toByteArray();
                }
            }
            assertNotNull(data);
            assertArrayEquals(data, bytes);
        }
    }


    @Test
    public void hasChild() throws Throwable {

        Lite lite = TestEnv.getTestInstance();

        Server server = TestEnv.getServer();
        assertNotNull(server);
        Dir root;
        // prepare data
        Session session = lite.createSession(TestEnv.getBlockStore(), TestEnv.PEERS);

        Fid fid = TestEnv.createContent(session, "index.txt", "Moin".getBytes());
        assertNotNull(fid);

        Dir a = Lite.createEmptyDirectory(session, "a");
        assertNotNull(a);

        a = Lite.addToDirectory(session, a, fid);
        assertNotNull(a);

        root = a;


        assertNotNull(root);

        Dummy dummy = new Dummy();

        Session dummySession = dummy.createSession();

        PeerId host = lite.self();
        assertNotNull(host);
        Peeraddr peeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());

        Parameters parameters = Lite.createParameters();
        try (Connection connection = Lite.dial(dummySession, peeraddr, parameters)) {

            boolean result = Lite.hasChild(connection,
                    root, "index.txt");
            assertTrue(result);

            result = Lite.hasChild(connection, root, "not_there");
            assertFalse(result);

        }

    }

}

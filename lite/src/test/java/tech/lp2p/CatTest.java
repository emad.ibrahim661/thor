package tech.lp2p;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.fail;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import tech.lp2p.core.Cid;
import tech.lp2p.core.Raw;
import tech.lp2p.core.Session;


public class CatTest {


    @BeforeClass
    public static void setup() {
        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }


    @Test
    public void catNotExist() throws Exception {

        Lite lite = TestEnv.getTestInstance();

        Session session = lite.createSession(TestEnv.getBlockStore(), TestEnv.PEERS);
        Cid cid = Lite.rawCid("Hallo Welt");
        try {
            Lite.fetchData(session, cid);
            fail();
        } catch (Exception ignore) {
            //
        }

    }


    @Test
    public void catLocalTest() throws Exception {

        Lite lite = TestEnv.getTestInstance();

        Session session = lite.createSession(TestEnv.getBlockStore(), TestEnv.PEERS);
        Raw local = Lite.storeText(session, "Moin Moin Moin");
        assertNotNull(local);

        byte[] content = Lite.fetchData(session, local.cid());

        assertNotNull(content);

    }

}
package tech.lp2p;

import static junit.framework.TestCase.assertNotNull;

import junit.framework.TestCase;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import tech.lp2p.core.Connection;
import tech.lp2p.core.Envelope;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;

public class PullStressTest {


    @BeforeClass
    public static void setup() {

        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }

    @Test
    public void pullIterations() throws Exception {
        Lite lite = TestEnv.getTestInstance();
        Server server = TestEnv.getServer();
        TestCase.assertNotNull(server);


        Dummy dummy = new Dummy();

        Peeraddr peeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());

        Session dummySession = dummy.createSession();

        Parameters parameters = Lite.createParameters(false);


        try (Connection connection = Lite.dial(dummySession, peeraddr, parameters)) {


            for (int i = 0; i < TestEnv.ITERATIONS; i++) {
                Envelope envelope = Lite.pullEnvelope(connection, TestEnv.CID);
                assertNotNull(envelope);
            }
        }

    }

}

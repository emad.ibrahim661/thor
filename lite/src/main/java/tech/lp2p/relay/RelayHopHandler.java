package tech.lp2p.relay;

import static tech.lp2p.proto.Circuit.Status.CONNECTION_FAILED;
import static tech.lp2p.proto.Circuit.Status.MALFORMED_MESSAGE;
import static tech.lp2p.proto.Circuit.Status.NO_RESERVATION;
import static tech.lp2p.proto.Circuit.Status.PERMISSION_DENIED;
import static tech.lp2p.proto.Circuit.Status.UNEXPECTED_MESSAGE;
import static tech.lp2p.proto.Circuit.Status.UNRECOGNIZED;
import static tech.lp2p.quic.Requester.RELAYED;
import static tech.lp2p.quic.Requester.STREAM;

import com.google.protobuf.ByteString;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import tech.lp2p.Lite;
import tech.lp2p.core.Handler;
import tech.lp2p.core.Limit;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Protocol;
import tech.lp2p.lite.LiteErrorCode;
import tech.lp2p.lite.LiteServer;
import tech.lp2p.proto.Circuit;
import tech.lp2p.quic.Connection;
import tech.lp2p.quic.Requester;
import tech.lp2p.quic.Stream;
import tech.lp2p.utils.Utils;

public record RelayHopHandler(LiteServer server, Limit limit) implements Handler {

    public static void createStatusMessage(Stream stream, Circuit.Status status) {
        Circuit.HopMessage.Builder builder =
                Circuit.HopMessage.newBuilder()
                        .setType(Circuit.HopMessage.Type.STATUS);
        builder.setStatus(status);
        stream.writeOutput(Utils.encode(builder.build()), true);
    }


    @NotNull
    RelayStopRequest stopRequest(Connection connection, PeerId peerId) throws Exception {

        CompletableFuture<Circuit.StopMessage> done = new CompletableFuture<>();


        Circuit.Peer.Builder peerBuilder = Circuit.Peer.newBuilder().setId(
                ByteString.copyFrom(PeerId.multihash(peerId)));

        Circuit.StopMessage message = Circuit.StopMessage.newBuilder()
                .setType(Circuit.StopMessage.Type.CONNECT)
                .setPeer(peerBuilder.build())
                .build();

        Stream stream = Requester.createStream(connection,
                Protocol.RELAY_PROTOCOL_STOP.readDelimiter(), new StopRequest(done, limit));
        stream.writeOutput(Utils.encode(message, Protocol.MULTISTREAM_PROTOCOL,
                Protocol.RELAY_PROTOCOL_STOP), false);

        return new RelayStopRequest(stream,
                done.get(Lite.DEFAULT_TIMEOUT, TimeUnit.SECONDS));
    }

    private Circuit.Reservation createReservation() {
        Circuit.Reservation.Builder builder = Circuit.Reservation.newBuilder();
        for (Peeraddr peeraddr : server.peeraddrs()) {
            builder.addAddrs(ByteString.copyFrom(peeraddr.encoded()));
        }
        builder.setExpire(createExpire());
        return builder.build();

    }

    private long createExpire() {
        return (System.currentTimeMillis() * 1000) + limit.duration();
    }

    private Circuit.Limit createLimit() {
        return Circuit.Limit.newBuilder()
                .setData(limit.data())
                .setDuration(limit.duration())
                .build();
    }

    private void handleReserveMessage(Stream stream, Circuit.HopMessage hopMessage) {

        try {
            PeerId peerId = stream.connection().remotePeerId();
            if (Objects.equals(peerId, server.self())) {
                createStatusMessage(stream, PERMISSION_DENIED);
                return;
            }

            // like others nodes rejects hop reserve message without peer
            if (!hopMessage.hasPeer()) {
                createStatusMessage(stream, MALFORMED_MESSAGE);
                return;
            }

            Circuit.Peer peer = hopMessage.getPeer();
            // check if peer is like remote peerId
            PeerId hopPeerId = PeerId.parse(peer.getId().toByteArray());

            if (!Objects.equals(peerId, hopPeerId)) {
                createStatusMessage(stream, PERMISSION_DENIED);
                return;
            }


            Connection connection = getRelayedConnection(peerId);

            if (connection != null) {
                // remote user has already a reserved connection
                // case 1: the connections are different -> refused connection
                // case 2: just update the connection with new limit
                if (connection != stream.connection()) {
                    createStatusMessage(stream, Circuit.Status.RESERVATION_REFUSED);
                    return;
                }
            }

            Circuit.Reservation reservation = createReservation();
            Circuit.HopMessage.Builder builder = Circuit.HopMessage.newBuilder()
                    .setType(Circuit.HopMessage.Type.STATUS);
            builder.setReservation(reservation);
            builder.setLimit(createLimit());
            builder.setStatus(Circuit.Status.OK);

            stream.connection().setAttribute(RELAYED, true);

            stream.writeOutput(Utils.encode(builder.build()), true);

        } catch (Throwable throwable) {
            createStatusMessage(stream, Circuit.Status.MALFORMED_MESSAGE);
        }
    }

    @Override
    public void protocol(Stream stream) {
        stream.writeOutput(Utils.encodeProtocols(Protocol.MULTISTREAM_PROTOCOL,
                Protocol.RELAY_PROTOCOL_HOP), false);
    }

    @Override
    public void data(Stream stream, byte[] data) {
        if (stream.hasAttribute(STREAM)) {
            Relayed relayed = (Relayed) stream.getAttribute(STREAM);
            Objects.requireNonNull(relayed, "Relayed is not defined");
            relayed.incrementBytes(data.length);
            if (relayed.isExpired(limit) || relayed.isDataLimitReached(limit)) {
                // when data limit is reached or expired a reset is performed on the
                // stream and remote stream
                stream.resetStream(LiteErrorCode.LIMIT_REACHED);
                relayed.stream.resetStream(LiteErrorCode.LIMIT_REACHED);
                return;
            }
            try {
                Stream target = relayed.stream();
                Objects.requireNonNull(target);
                target.writeOutput(Utils.encode(data), false);
            } catch (Throwable throwable) {
                stream.resetStream(LiteErrorCode.INTERNAL_ERROR);
                relayed.stream.resetStream(LiteErrorCode.INTERNAL_ERROR);
            }
        } else {
            try {
                Circuit.HopMessage hopMessage = Circuit.HopMessage.parseFrom(data);
                Objects.requireNonNull(hopMessage);

                switch (hopMessage.getType()) {
                    case RESERVE -> handleReserveMessage(stream, hopMessage);
                    case CONNECT -> handleConnectMessage(stream, hopMessage);
                    case UNRECOGNIZED -> createStatusMessage(stream, UNRECOGNIZED);
                    case STATUS -> createStatusMessage(stream, UNEXPECTED_MESSAGE);
                }
            } catch (Throwable throwable) {
                createStatusMessage(stream, UNEXPECTED_MESSAGE);
            }
        }
    }

    @Nullable
    private Connection getRelayedConnection(PeerId peerId) {
        Collection<Connection> connections = server.serverConnector().serverConnections(peerId);
        for (Connection connection : connections) {
            if (connection.hasAttribute(RELAYED)) {
                return connection;
            }
        }
        return null;
    }


    private void handleConnectMessage(Stream stream, Circuit.HopMessage hopMessage) {

        PeerId remotePeerId = stream.connection().remotePeerId();
        if (Objects.equals(remotePeerId, server.self())) {
            createStatusMessage(stream, PERMISSION_DENIED);
            return;
        }

        if (getRelayedConnection(remotePeerId) != null) {
            // check if the user has an connect request
            createStatusMessage(stream, PERMISSION_DENIED);
            return;
        }

        if (!hopMessage.hasPeer()) {
            createStatusMessage(stream, Circuit.Status.MALFORMED_MESSAGE);
            return;
        }

        Circuit.Peer peer = hopMessage.getPeer();
        Objects.requireNonNull(peer);

        PeerId targetId = PeerId.parse(peer.getId().toByteArray());

        if (Objects.equals(targetId, server.self())) {
            createStatusMessage(stream, PERMISSION_DENIED);
            return;
        }

        Connection connection = getRelayedConnection(targetId);

        if (connection != null) {
            new Thread(() -> {
                try {
                    RelayStopRequest request = stopRequest(connection, remotePeerId);
                    Circuit.StopMessage message = request.message();

                    switch (message.getType()) {
                        case STATUS -> {
                            Circuit.HopMessage.Builder builder =
                                    Circuit.HopMessage.newBuilder()
                                            .setType(Circuit.HopMessage.Type.STATUS);
                            builder.setStatus(message.getStatus());

                            boolean isFinal = message.getStatus() != Circuit.Status.OK;
                            if (!isFinal) {
                                builder.setLimit(createLimit());
                                stream.setAttribute(STREAM, new Relayed(request.stream()));
                                request.stream().setAttribute(STREAM, new Relayed(stream));
                            }
                            stream.writeOutput(Utils.encode(builder.build()), isFinal);
                        }
                        case CONNECT -> createStatusMessage(stream, UNEXPECTED_MESSAGE);
                        case UNRECOGNIZED -> createStatusMessage(stream, UNRECOGNIZED);
                    }
                } catch (Throwable throwable) {
                    createStatusMessage(stream, CONNECTION_FAILED);
                }
            }).start();
        } else {
            createStatusMessage(stream, NO_RESERVATION);
        }
    }

    @Override
    public void terminated(Stream stream) {
        if (stream.hasAttribute(STREAM)) {
            Relayed relayed = (Relayed) stream.getAttribute(STREAM);
            Objects.requireNonNull(relayed, "Relayed is not defined");
            relayed.stream.resetStream(LiteErrorCode.STREAM_CLOSED);
        }
        stream.removeAttribute(STREAM);
    }

    @Override
    public void fin(Stream stream) {
        if (stream.hasAttribute(STREAM)) {
            Relayed relayed = (Relayed) stream.getAttribute(STREAM);
            Objects.requireNonNull(relayed, "Relayed is not defined");
            relayed.stream.fin();
        }
        stream.removeAttribute(STREAM);
    }

    private static class Relayed {
        private final Stream stream;
        private final long timestamp;
        private long bytes = 0;

        private Relayed(Stream stream) {
            this.stream = stream;
            this.timestamp = System.currentTimeMillis();
        }

        public Stream stream() {
            return stream;
        }

        public boolean isExpired(Limit limit) {
            if (limit.duration() == 0) {
                return false;
            }
            return System.currentTimeMillis() > (timestamp + (limit.duration() * 1000L));
        }

        public void incrementBytes(int length) {
            bytes = bytes + length;
        }

        public boolean isDataLimitReached(Limit limit) {
            if (limit.data() == 0) {
                return false;
            }
            return bytes >= limit.data();
        }
    }

    record StopRequest(CompletableFuture<Circuit.StopMessage> done,
                       Limit limit) implements Requester {
        @Override
        public void throwable(Throwable throwable) {
            done.completeExceptionally(throwable);
        }

        @Override
        public void terminated(Stream stream) {
            if (stream.hasAttribute(STREAM)) {
                Relayed relayed = (Relayed) stream.getAttribute(STREAM);
                Objects.requireNonNull(relayed, "Relayed is not defined");
                relayed.stream.resetStream(LiteErrorCode.STREAM_CLOSED);
            }
            stream.removeAttribute(STREAM);
            if (!done.isDone()) {
                done.completeExceptionally(new Throwable("stream terminated"));
            }
        }

        @Override
        public void fin(Stream stream) {
            if (stream.hasAttribute(STREAM)) {
                Relayed relayed = (Relayed) stream.getAttribute(STREAM);
                Objects.requireNonNull(relayed, "Relayed is not defined");
                relayed.stream.fin();
            }
            stream.removeAttribute(STREAM);
            if (!done.isDone()) {
                done.completeExceptionally(new Throwable("stream finished before data"));
            }
        }

        @Override
        public void data(Stream stream, byte[] data) {
            if (stream.hasAttribute(STREAM)) {
                RelayHopHandler.Relayed relayed = (RelayHopHandler.Relayed) stream.getAttribute(STREAM);
                Objects.requireNonNull(relayed, "Relayed is not defined");
                relayed.incrementBytes(data.length);
                if (relayed.isExpired(limit) || relayed.isDataLimitReached(limit)) {
                    // when data limit is reached or expired a reset is performed on the
                    // stream and remote stream
                    stream.resetStream(LiteErrorCode.LIMIT_REACHED);
                    relayed.stream.resetStream(LiteErrorCode.LIMIT_REACHED);
                    return;
                }
                try {
                    Stream target = relayed.stream();
                    Objects.requireNonNull(target);
                    target.writeOutput(Utils.encode(data), false);
                } catch (Throwable throwable) {
                    stream.resetStream(LiteErrorCode.INTERNAL_ERROR);
                    relayed.stream.resetStream(LiteErrorCode.INTERNAL_ERROR);
                }

            } else {
                try {
                    done.complete(Circuit.StopMessage.parseFrom(data));
                } catch (Throwable throwable) {
                    createStatusMessage(stream, MALFORMED_MESSAGE);
                }
            }
        }
    }

    private record RelayStopRequest(Stream stream, Circuit.StopMessage message) {
    }
}

package tech.lp2p.relay;

import org.jetbrains.annotations.Nullable;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

import tech.lp2p.core.HopConnect;
import tech.lp2p.core.Key;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Session;
import tech.lp2p.quic.Connection;
import tech.lp2p.utils.Utils;

public interface RelayDialer {

    @Nullable
    static HopConnect hopConnect(Session session,
                                 PeerId peerId,
                                 Parameters parameters,
                                 Consumer<Peeraddr> tries,
                                 Consumer<Peeraddr> failures,
                                 int timeout) {


        AtomicReference<HopConnect> done = new AtomicReference<>();

        Key key = peerId.createKey();

        ExecutorService service = Executors.newSingleThreadExecutor();
        try {
            service.execute(() -> session.findClosestPeers(key, peeraddr -> {

                tries.accept(peeraddr);

                try {
                    Connection connection = (Connection)
                            RelayService.hopConnect(session, peeraddr, peerId, parameters);
                    // maybe a hop connect was already set before
                    if (done.compareAndSet(null,
                            new HopConnect(connection, peeraddr))) {
                        service.shutdownNow();
                    } else {
                        // this could only happen in very rare cases
                        connection.close();
                        service.shutdownNow();
                    }
                } catch (Throwable throwable) {
                    failures.accept(peeraddr);
                    Utils.error("RelayDialer hopConnect failure " + throwable.getMessage());
                }
            }));

            service.shutdown();
            if (!service.awaitTermination(timeout, TimeUnit.SECONDS)) {
                service.shutdownNow();
            }
        } catch (InterruptedException interruptedException) {
            service.shutdownNow();
        }

        return done.get();
    }
}

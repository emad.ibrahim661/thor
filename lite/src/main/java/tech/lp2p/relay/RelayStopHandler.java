package tech.lp2p.relay;

import com.google.protobuf.ByteString;

import org.jetbrains.annotations.NotNull;

import java.net.InetAddress;
import java.util.Objects;

import tech.lp2p.core.Handler;
import tech.lp2p.core.Network;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Protocol;
import tech.lp2p.core.Server;
import tech.lp2p.proto.Circuit;
import tech.lp2p.proto.Holepunch;
import tech.lp2p.quic.Requester;
import tech.lp2p.quic.Stream;
import tech.lp2p.utils.Utils;


public final class RelayStopHandler implements Handler {
    @NotNull
    private final Server server;

    public RelayStopHandler(@NotNull Server server) {
        this.server = server;
    }


    private static void createStatusMessage(Stream stream, Circuit.Status status) {
        Circuit.StopMessage.Builder builder =
                Circuit.StopMessage.newBuilder()
                        .setType(Circuit.StopMessage.Type.STATUS);
        builder.setStatus(status);
        stream.writeOutput(Utils.encode(builder.build()), true);
    }

    @Override
    public void protocol(Stream stream) {
        stream.writeOutput(Utils.encodeProtocols(Protocol.MULTISTREAM_PROTOCOL,
                Protocol.RELAY_PROTOCOL_STOP), false);
    }

    @Override
    public void data(Stream stream, byte[] data) throws Exception {

        if (stream.hasAttribute(Requester.PEER)) {
            PeerId target = (PeerId) stream.getAttribute(Requester.PEER);
            Objects.requireNonNull(target);

            Holepunch.HolePunch holePunch = Holepunch.HolePunch.parseFrom(data);
            if (!holePunch.hasType()) {
                throw new Exception("invalid hole punch message");
            }

            switch (holePunch.getType()) {
                case CONNECT -> {
                    Peeraddrs peeraddrs = Peeraddr.create(target, holePunch.getObsAddrsList());

                    if (peeraddrs.isEmpty()) {
                        throw new Exception("Received empty peeraddrs");
                    }
                    stream.setAttribute(Requester.ADDRS, peeraddrs);

                    Holepunch.HolePunch.Builder builder =
                            Holepunch.HolePunch.newBuilder()
                                    .setType(Holepunch.HolePunch.Type.CONNECT);
                    Peeraddrs serverPeeraddrs = Peeraddr.peeraddrs(server.self(), server.port());
                    for (Peeraddr peeraddr : serverPeeraddrs) {
                        builder.addObsAddrs(ByteString.copyFrom(peeraddr.encoded()));
                    }

                    stream.writeOutput(Utils.encode(builder.build()), false);
                }
                case SYNC -> {
                    Peeraddrs peeraddrs = (Peeraddrs) stream.getAttribute(Requester.ADDRS);
                    Objects.requireNonNull(peeraddrs, "No Peeraddrs"); // should not happen


                    for (Peeraddr peeraddr : peeraddrs) {
                        InetAddress inetAddress = InetAddress.getByAddress(peeraddr.address());
                        if (!Network.isLocalAddress(inetAddress)) {
                            // hole punching not needed for local addressed
                            server.holePunching(peeraddr);
                        }
                    }


                    stream.fin(); // done here
                }
                default -> throw new Exception("invalid hole punch type");
            }
        } else {
            try {
                Circuit.StopMessage msg = Circuit.StopMessage.parseFrom(data);
                Objects.requireNonNull(msg);


                if (msg.getType() != Circuit.StopMessage.Type.CONNECT) {
                    createStatusMessage(stream, Circuit.Status.MALFORMED_MESSAGE);
                    return;
                }
                if (!msg.hasPeer()) {
                    createStatusMessage(stream, Circuit.Status.MALFORMED_MESSAGE);
                    return;
                }
                Circuit.Peer peer = msg.getPeer();

                PeerId peerId = PeerId.parse(peer.getId().toByteArray());

                if (Objects.equals(peerId, server.self())) {
                    createStatusMessage(stream, Circuit.Status.PERMISSION_DENIED);
                    return;
                }

                if (server.isGated(peerId)) {
                    createStatusMessage(stream, Circuit.Status.PERMISSION_DENIED);
                    return;
                }

                Circuit.StopMessage.Builder builder =
                        Circuit.StopMessage.newBuilder()
                                .setType(Circuit.StopMessage.Type.STATUS);
                builder.setStatus(Circuit.Status.OK);


                stream.writeOutput(Utils.encode(builder.build()), false);
                stream.setAttribute(Requester.PEER, peerId);


            } catch (Throwable throwable) {
                createStatusMessage(stream, Circuit.Status.UNEXPECTED_MESSAGE);
            }
        }
    }


    @Override
    public void terminated(Stream stream) {
        stream.removeAttribute(Requester.PEER);
    }

    @Override
    public void fin(Stream stream) {
        stream.removeAttribute(Requester.PEER);
    }
}

package tech.lp2p.relay;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;

import org.jetbrains.annotations.NotNull;

import java.net.ConnectException;
import java.net.DatagramSocket;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import tech.lp2p.Lite;
import tech.lp2p.core.ALPN;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Limit;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Protocol;
import tech.lp2p.core.Reservation;
import tech.lp2p.core.Session;
import tech.lp2p.proto.Circuit;
import tech.lp2p.proto.Holepunch;
import tech.lp2p.quic.ConnectionBuilder;
import tech.lp2p.quic.Requester;
import tech.lp2p.quic.Stream;
import tech.lp2p.utils.Utils;

public interface RelayService {

    @NotNull
    static Connection hopConnect(@NotNull Session session,
                                 @NotNull Peeraddr relayAddress,
                                 @NotNull PeerId target,
                                 @NotNull Parameters parameters) throws ConnectException {
        RelayInfo relayInfo = relayPunchInfo(session, target, parameters);
        try (Connection connection = ConnectionBuilder.connect(session,
                relayAddress, Parameters.create(ALPN.libp2p),
                session.responder(ALPN.libp2p))) {
            return hopConnect(session, connection, relayInfo);
        } catch (Throwable throwable) {
            relayInfo.socket().close(); // not required anymore (no connection)
            throw new ConnectException(throwable.getClass().getSimpleName() +
                    " " + throwable.getMessage());
        }
    }


    private static Connection hopConnect(@NotNull Session session,
                                         @NotNull Connection connection,
                                         @NotNull RelayInfo relayInfo)
            throws InterruptedException, TimeoutException, ExecutionException, ConnectException {

        CompletableFuture<SyncInfo> done = new CompletableFuture<>();

        Circuit.Peer.Builder builder = Circuit.Peer.newBuilder()
                .setId(ByteString.copyFrom(PeerId.multihash(relayInfo.target())));

        Circuit.HopMessage message = Circuit.HopMessage.newBuilder()
                .setType(Circuit.HopMessage.Type.CONNECT)
                .setPeer(builder.build())
                .build();

        Requester.createStream((tech.lp2p.quic.Connection) connection,
                Protocol.RELAY_PROTOCOL_HOP.readDelimiter(), new ConnectRequest(
                        done, session, relayInfo)).writeOutput(
                Utils.encode(message, Protocol.MULTISTREAM_PROTOCOL,
                        Protocol.RELAY_PROTOCOL_HOP), false);

        SyncInfo syncInfo = done.get(Lite.RELAY_CONNECT_TIMEOUT, TimeUnit.SECONDS);

        // Next, wait for rtt/2
        Thread.sleep(syncInfo.rtt() / 2);

        Parameters parameters = relayInfo.parameters();
        return ConnectionBuilder.connect(session,
                syncInfo.peeraddr(), parameters,
                session.responder(parameters.alpn()),
                relayInfo.socket());
    }

    @NotNull
    static Reservation hopReserve(Connection connection, PeerId self)
            throws InterruptedException, TimeoutException, ConnectException {

        Circuit.Peer.Builder peerBuilder = Circuit.Peer.newBuilder()
                .setId(ByteString.copyFrom(PeerId.multihash(self)));

        Circuit.HopMessage message = Circuit.HopMessage.newBuilder()
                .setPeer(peerBuilder.build())
                .setType(Circuit.HopMessage.Type.RESERVE).build();


        byte[] data = Requester.createStream((tech.lp2p.quic.Connection) connection,
                Protocol.RELAY_PROTOCOL_HOP.readDelimiter()).request(
                Utils.encode(message, Protocol.MULTISTREAM_PROTOCOL,
                        Protocol.RELAY_PROTOCOL_HOP), Lite.CONNECT_TIMEOUT);

        try {
            Circuit.HopMessage msg = Circuit.HopMessage.parseFrom(data);

            if (msg.getType() != Circuit.HopMessage.Type.STATUS) {
                throw new ConnectException("NO RESERVATION STATUS");
            }
            if (msg.getStatus() != Circuit.Status.OK) {
                throw new ConnectException("RESERVATION STATUS = " + msg.getStatus().toString());
            }
            if (!msg.hasReservation()) {
                throw new ConnectException("NO RESERVATION");
            }
            Circuit.Reservation reserve = msg.getReservation();
            Limit limit = limit(msg);

            return new Reservation(connection.remotePeeraddr(), limit, reserve.getExpire());
        } catch (InvalidProtocolBufferException invalidProtocolBufferException) {
            throw new IllegalArgumentException("Invalid buffer exception");
        }
    }


    @NotNull
    static RelayInfo relayPunchInfo(@NotNull Session session, @NotNull PeerId target,
                                    @NotNull Parameters parameters) throws ConnectException {

        DatagramSocket socket = getSocket();

        Peeraddrs peeraddrs = Peeraddr.peeraddrs(session.self(), socket.getLocalPort());
        if (peeraddrs.isEmpty()) {
            throw new ConnectException("Hole punching not possible [abort]");
        }
        return new RelayInfo(target, peeraddrs, socket, parameters);
    }

    private static DatagramSocket getSocket() throws ConnectException {
        try {
            return new DatagramSocket();
        } catch (Throwable throwable) {
            throw new ConnectException("can not create socket");
        }
    }

    static Limit limit(Circuit.HopMessage hopMessage) {
        long limitData = 0;
        int limitDuration = 0;

        if (hopMessage.hasLimit()) {
            Circuit.Limit limit = hopMessage.getLimit();
            limitData = limit.getData();
            limitDuration = limit.getDuration();
        }

        return new Limit(limitData, limitDuration);
    }


    record ConnectRequest(CompletableFuture<SyncInfo> done, Session session,
                          RelayInfo relayInfo) implements Requester {

        @Override
        public void throwable(Throwable throwable) {
            done.completeExceptionally(throwable);
        }

        @Override
        public void fin(Stream stream) {
            if (!done.isDone()) {
                done.completeExceptionally(new Throwable("stream finished before message"));
            }
            stream.removeAttribute(INITIALIZED);
        }


        @Override
        public void terminated(Stream stream) {
            if (!done.isDone()) {
                done.completeExceptionally(new Throwable("stream terminated"));
            }
            stream.removeAttribute(INITIALIZED);
        }

        @Override
        public void data(Stream stream, byte[] data) throws Exception {

            if (stream.hasAttribute(INITIALIZED)) {
                SyncInfo syncInfo = sendSync(stream, data);
                done.complete(syncInfo);
            } else {
                Circuit.HopMessage msg = Circuit.HopMessage.parseFrom(data);
                Objects.requireNonNull(msg);

                if (msg.getType() != Circuit.HopMessage.Type.STATUS) {
                    throwable(new Exception("Malformed message"));
                    stream.close();
                    return;
                }

                if (msg.getStatus() != Circuit.Status.OK) {
                    throwable(new Exception("No reservation reason " + msg.getStatus().name()));
                    stream.close();
                    return;
                }

                Limit limit = RelayService.limit(msg);
                Objects.requireNonNull(limit);
                // Note the limit will not be checked even it is a non limited relay
                // always a hole punch will be done

                initializeConnect(stream);
                stream.setAttribute(INITIALIZED, true);

            }
        }


        @NotNull
        private SyncInfo sendSync(Stream stream, byte[] data) throws Exception {

            Long timer = (Long) stream.getAttribute(Requester.TIMER); // timer set earlier
            Objects.requireNonNull(timer, "Timer not set on stream");
            long rtt = System.currentTimeMillis() - timer;

            // B receives the Connect message from A

            Holepunch.HolePunch msg = Holepunch.HolePunch.parseFrom(data);
            Objects.requireNonNull(msg, "Message is not defined");

            if (msg.getType() != Holepunch.HolePunch.Type.CONNECT) {
                throw new Exception("[A] send wrong message connect payloadType, abort");
            }

            Peeraddrs peeraddrs = Peeraddr.create(relayInfo.target(), msg.getObsAddrsList());

            if (peeraddrs.isEmpty()) {
                throw new Exception("[A] send no observed addresses, abort");
            }

            Holepunch.HolePunch response = Holepunch.HolePunch.newBuilder().
                    setType(Holepunch.HolePunch.Type.SYNC).build();
            stream.writeOutput(Utils.encode(response), true);

            Peeraddr peeraddr = Peeraddrs.best(peeraddrs);
            Objects.requireNonNull(peeraddr);
            return new SyncInfo(peeraddr, rtt);
        }

        void initializeConnect(Stream stream) {
            Holepunch.HolePunch.Builder builder = Holepunch.HolePunch.newBuilder()
                    .setType(Holepunch.HolePunch.Type.CONNECT);

            for (Peeraddr peeraddr : relayInfo.peeraddrs()) {
                builder.addObsAddrs(ByteString.copyFrom(peeraddr.encoded()));
            }

            Holepunch.HolePunch message = builder.build();
            stream.setAttribute(Requester.TIMER, System.currentTimeMillis());
            stream.writeOutput(Utils.encode(message), false);
        }
    }

    record SyncInfo(Peeraddr peeraddr, long rtt) {
    }

    record RelayInfo(PeerId target, Peeraddrs peeraddrs, DatagramSocket socket,
                     Parameters parameters) {

    }
}
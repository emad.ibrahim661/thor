package tech.lp2p.ident;

import tech.lp2p.core.Handler;
import tech.lp2p.core.Protocol;
import tech.lp2p.quic.Stream;
import tech.lp2p.utils.Utils;

public final class IdentifyPushHandler implements Handler {


    public IdentifyPushHandler() {
    }

    @Override
    public void protocol(Stream stream) {
        stream.writeOutput(Utils.encodeProtocols(Protocol.MULTISTREAM_PROTOCOL,
                Protocol.IDENTITY_PUSH_PROTOCOL), true);
    }

    @Override
    public void data(Stream stream, byte[] data) {
        // just read out the identity (later store them into the connection
        // when a use-case is there
    }

    @Override
    public void terminated(Stream stream) {
        // nothing to do here
    }

    @Override
    public void fin(Stream stream) {
        // nothing to do here
    }

}

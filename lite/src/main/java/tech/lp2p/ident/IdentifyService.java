package tech.lp2p.ident;

import static tech.lp2p.proto.Crypto.KeyType.Ed25519;

import com.google.protobuf.ByteString;

import org.jetbrains.annotations.NotNull;

import java.util.Set;

import tech.lp2p.Lite;
import tech.lp2p.core.ALPN;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Identify;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Protocol;
import tech.lp2p.proto.Crypto;
import tech.lp2p.proto.IdentifyOuterClass;
import tech.lp2p.quic.Requester;
import tech.lp2p.utils.Utils;

public interface IdentifyService {
    String PROTOCOL_VERSION = "ipfs/0.1.0";

    @NotNull
    static IdentifyOuterClass.Identify identify(@NotNull PeerId self,
                                                @NotNull String agent,
                                                @NotNull Set<String> protocols,
                                                @NotNull Peeraddrs peeraddrs) {

        Crypto.PublicKey cryptoKey = Crypto.PublicKey.newBuilder()
                .setType(Ed25519)
                .setData(ByteString.copyFrom(self.hash()))
                .build();

        IdentifyOuterClass.Identify.Builder builder = IdentifyOuterClass.Identify.newBuilder()
                .setAgentVersion(agent)
                .setPublicKey(ByteString.copyFrom(cryptoKey.toByteArray()))
                .setProtocolVersion(PROTOCOL_VERSION);


        for (Peeraddr peeraddr : peeraddrs) {
            builder.addListenAddrs(ByteString.copyFrom(peeraddr.encoded()));
        }

        for (String protocol : protocols) {
            builder.addProtocols(protocol);
        }
        return builder.build();
    }

    @NotNull
    static Identify identify(Connection connection) throws Exception {
        if (connection.alpn() != ALPN.libp2p) {
            throw new IllegalStateException("ALPN must be libp2p");
        }

        byte[] data = Requester.createStream((tech.lp2p.quic.Connection) connection,
                        Protocol.IDENTITY_PROTOCOL.readDelimiter())
                .request(Utils.encodeProtocols(Protocol.MULTISTREAM_PROTOCOL,
                        Protocol.IDENTITY_PROTOCOL), Lite.CONNECT_TIMEOUT);

        IdentifyOuterClass.Identify identify = IdentifyOuterClass.Identify.parseFrom(data);

        return identify(identify, connection.remotePeerId());
    }

    @NotNull
    static Identify identify(IdentifyOuterClass.Identify identify, PeerId expected)
            throws Exception {

        String agent = identify.getAgentVersion();

        Crypto.PublicKey publicKey = Crypto.PublicKey.parseFrom(
                identify.getPublicKey().toByteArray());

        Utils.checkArgument(publicKey.getType(), Ed25519, "Only Ed25519 expected");

        byte[] raw = publicKey.getData().toByteArray();
        PeerId peerId = PeerId.create(raw);

        Utils.checkArgument(peerId, expected, "PeerId is not what is expected");


        String[] protocols = new String[identify.getProtocolsCount()];
        identify.getProtocolsList().toArray(protocols);

        Peeraddrs peeraddrs = Peeraddr.create(peerId, identify.getListenAddrsList());
        Peeraddr[] addresses = new Peeraddr[peeraddrs.size()];
        peeraddrs.toArray(addresses);

        return new Identify(peerId, agent, addresses, protocols);
    }

}

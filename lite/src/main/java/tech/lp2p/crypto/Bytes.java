// Copyright 2017 Google Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
////////////////////////////////////////////////////////////////////////////////

package tech.lp2p.crypto;

import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.util.Arrays;

/**
 * Immutable Wrapper around a byte array.
 *
 * <p>Wrap a bytearray so it prevents callers from modifying its contents. It does this by making a
 * copy upon initialization, and also makes a copy if the underlying bytes are requested.
 *
 * @since 1.0.0
 */

public final class Bytes {
    @SuppressWarnings("Immutable") // We copy the data on input and output.
    private final byte[] data;

    private Bytes(final byte[] buf, final int start, final int len) {
        data = new byte[len];
        System.arraycopy(buf, start, data, 0, len);
    }

    /**
     * @param data the byte array to be wrapped.
     * @return an immutable wrapper around the provided bytes.
     */
    public static Bytes copyFrom(final byte[] data) {
        if (data == null) {
            throw new NullPointerException("data must be non-null");
        }
        return copyFrom(data, 0, data.length);
    }

    /**
     * Wrap an immutable byte array over a slice of a Bytes
     *
     * @param data  the byte array to be wrapped.
     * @param start the starting index of the slice
     * @param len   the length of the slice. If start + len is larger than the size of {@code data}, the
     *              remaining data will be returned.
     * @return an immutable wrapper around the bytes in the slice from {@code start} to {@code start +
     * len}
     */
    public static Bytes copyFrom(byte[] data, int start, int len) {
        if (data == null) {
            throw new NullPointerException("data must be non-null");
        }
        if (start + len > data.length) {
            len = data.length - start;
        }
        return new Bytes(data, start, len);
    }

    /**
     * Best effort fix-timing array comparison.
     *
     * @return true if two arrays are equal.
     */
    public static boolean equal(final byte[] x, final byte[] y) {
        return MessageDigest.isEqual(x, y);
    }

    /**
     * Returns the concatenation of the input arrays in a single array. For example, {@code concat(new
     * byte[] {a, b}, new byte[] {}, new byte[] {c}} returns the array {@code {a, b, c}}.
     *
     * @return a single array containing all the values from the source arrays, in order
     */
    public static byte[] concat(byte[]... chunks) throws GeneralSecurityException {
        int length = 0;
        for (byte[] chunk : chunks) {
            if (length > Integer.MAX_VALUE - chunk.length) {
                throw new GeneralSecurityException("exceeded size limit");
            }
            length += chunk.length;
        }
        byte[] res = new byte[length];
        int pos = 0;
        for (byte[] chunk : chunks) {
            System.arraycopy(chunk, 0, res, pos, chunk.length);
            pos += chunk.length;
        }
        return res;
    }

    /**
     * @return a copy of the bytes wrapped by this object.
     */
    public byte[] toByteArray() {
        byte[] result = new byte[data.length];
        System.arraycopy(data, 0, result, 0, data.length);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Bytes other)) {
            return false;
        }
        return Arrays.equals(other.data, data);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(data);
    }
}

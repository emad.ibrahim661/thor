package tech.lp2p.dht;

import org.jetbrains.annotations.NotNull;

import tech.lp2p.core.Key;
import tech.lp2p.core.Peeraddr;

public record DhtPeer(Key key, Peeraddr peeraddr, boolean replaceable) {

    @NotNull
    public static DhtPeer create(@NotNull Peeraddr peeraddr, boolean replaceable) {
        Key key = peeraddr.peerId().createKey();
        return new DhtPeer(key, peeraddr, replaceable);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DhtPeer dhtPeer = (DhtPeer) o;
        return key.equals(dhtPeer.key);
    }

    @Override
    public int hashCode() {
        return key.hashCode(); // ok, checked, maybe opt
    }

}

package tech.lp2p.dht;

import java.util.Objects;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class DhtQueryPeers extends ConcurrentSkipListSet<DhtQueryPeer> {

    public final int saturation;
    private final Lock lock = new ReentrantLock();

    public DhtQueryPeers(int saturation) {
        this.saturation = saturation;
    }


    @Override
    public boolean add(DhtQueryPeer dhtQueryPeer) {
        if (size() < saturation) {
            return super.add(dhtQueryPeer);
        } else {
            lock.lock();
            try {
                DhtQueryPeer last = this.last();
                Objects.requireNonNull(last);
                // check if dhtQueryPeer is closer
                int value = dhtQueryPeer.compareTo(last);
                // if value is negative dhtQueryPeer is closer to key
                if (value < 0) {
                    // it is save now to remove last and insert
                    boolean result = super.add(dhtQueryPeer);
                    if (result) {
                        // the new peer is added, now remove the last entry
                        // when it is possible
                        DhtPeerState state = last.getState();
                        if (state == DhtPeerState.PeerUnreachable ||
                                state == DhtPeerState.PeerQueried) {
                            remove(last);
                        }
                    }
                    return result;
                }
                // else case not added
                return false;
            } finally {
                lock.unlock();
            }
        }
    }
}

package tech.lp2p.dht;

import org.jetbrains.annotations.NotNull;

import java.net.ConnectException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import tech.lp2p.Lite;
import tech.lp2p.core.Key;
import tech.lp2p.utils.Utils;


interface DhtQuery {

    private static List<DhtQueryPeer> transform(@NotNull Key key, @NotNull List<DhtPeer> dhtPeers) {
        List<DhtQueryPeer> result = new ArrayList<>();
        dhtPeers.forEach(peer -> result.add(DhtQueryPeer.create(peer, key)));
        return result;
    }

    static void runQuery(@NotNull DhtKademlia dht,
                         @NotNull Key key, @NotNull List<DhtQueryPeer> peers,
                         @NotNull DhtKademlia.QueryFunc queryFn) {
        DhtQueryPeers dhtQueryPeers = new DhtQueryPeers(Lite.DHT_ALPHA);
        enhanceSet(dhtQueryPeers, peers);
        iteration(dht, dhtQueryPeers, key, queryFn);
    }

    private static boolean enhanceSet(DhtQueryPeers dhtQueryPeers, List<DhtQueryPeer> peers) {

        // the peers in query update are added to the queryPeers
        boolean enhanceSet = false;
        for (DhtQueryPeer peer : peers) {
            boolean result = dhtQueryPeers.add(peer);  // initial state is PeerHeard
            if (result) {
                enhanceSet = true; // element was added
            }
        }
        return enhanceSet;
    }


    static void iteration(DhtKademlia dht, DhtQueryPeers dhtQueryPeers,
                          Key key, DhtKademlia.QueryFunc queryFn) {
        if (Thread.currentThread().isInterrupted()) {
            return;
        }
        ExecutorService service = Executors.newFixedThreadPool(
                Runtime.getRuntime().availableProcessors());
        try {
            List<DhtQueryPeer> nextPeersToQuery = nextHeardPeers(dhtQueryPeers);

            for (DhtQueryPeer dhtQueryPeer : nextPeersToQuery) {
                dhtQueryPeer.setState(DhtPeerState.PeerWaiting);
                service.execute(() -> {

                    if (Thread.currentThread().isInterrupted()) {
                        return;
                    }
                    try {
                        List<DhtPeer> newDhtPeers = queryFn.query(dhtQueryPeer.dhtPeer());

                        dhtQueryPeer.setState(DhtPeerState.PeerQueried);

                        if (Thread.currentThread().isInterrupted()) {
                            return;
                        }

                        // query successful, try to add to routing table
                        if (enhanceSet(dhtQueryPeers, transform(key, newDhtPeers))) {
                            // only when the query peer is returning something
                            // it will be added to the routing table
                            dht.addToRouting(dhtQueryPeer);
                        }

                        iteration(dht, dhtQueryPeers, key, queryFn);

                    } catch (InterruptedException ignore) {
                        service.shutdownNow();
                    } catch (ConnectException connectException) {
                        dht.removeFromRouting(dhtQueryPeer, false);
                        dhtQueryPeer.setState(DhtPeerState.PeerUnreachable);
                    } catch (TimeoutException timeoutException) {
                        dht.removeFromRouting(dhtQueryPeer, true);
                        dhtQueryPeer.setState(DhtPeerState.PeerUnreachable);
                    } catch (Throwable throwable) {
                        Utils.error(throwable); // not expected
                        dht.removeFromRouting(dhtQueryPeer, true);
                        dhtQueryPeer.setState(DhtPeerState.PeerUnreachable);
                    }
                });
            }
            service.shutdown();
            if (!service.awaitTermination(Integer.MAX_VALUE, TimeUnit.SECONDS)) {
                service.shutdownNow();
            }
        } catch (InterruptedException interruptedException) {
            service.shutdownNow();
        }
    }

    static List<DhtQueryPeer> nextHeardPeers(ConcurrentSkipListSet<DhtQueryPeer> dhtQueryPeers) {
        // The peers we query next should be ones that we have only Heard about.
        List<DhtQueryPeer> peers = new ArrayList<>();
        int count = 0;
        for (DhtQueryPeer dhtQueryPeer : dhtQueryPeers) {
            if (dhtQueryPeer.getState() == DhtPeerState.PeerHeard) {
                peers.add(dhtQueryPeer);
                count++;
                if (count == Lite.DHT_CONCURRENCY) {
                    break;
                }
            }
        }
        return peers;
    }

}

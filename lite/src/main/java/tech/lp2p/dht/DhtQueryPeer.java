package tech.lp2p.dht;

import org.jetbrains.annotations.NotNull;

import java.math.BigInteger;
import java.util.concurrent.atomic.AtomicReference;

import tech.lp2p.core.Key;


public record DhtQueryPeer(@NotNull DhtPeer dhtPeer, @NotNull BigInteger distance,
                           @NotNull AtomicReference<DhtPeerState> state)
        implements Comparable<DhtQueryPeer> {

    public static DhtQueryPeer create(@NotNull DhtPeer dhtPeer, @NotNull Key key) {
        BigInteger distance = Key.distance(key, dhtPeer.key());
        return new DhtQueryPeer(dhtPeer, distance, new AtomicReference<>(DhtPeerState.PeerHeard));
    }

    @NotNull
    public DhtPeerState getState() {
        return state.get();
    }

    public void setState(@NotNull DhtPeerState state) {
        this.state.set(state);
    }

    @Override
    public int compareTo(DhtQueryPeer o) {
        return distance.compareTo(o.distance);
    }


}

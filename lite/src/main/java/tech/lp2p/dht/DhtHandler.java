package tech.lp2p.dht;

import com.google.protobuf.ByteString;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import tech.lp2p.core.Handler;
import tech.lp2p.core.Key;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Protocol;
import tech.lp2p.proto.Dht;
import tech.lp2p.quic.Stream;
import tech.lp2p.utils.Utils;

public final class DhtHandler implements Handler {

    private final DhtKademlia dhtKademlia;

    public DhtHandler(@NotNull DhtKademlia dhtKademlia) {
        this.dhtKademlia = dhtKademlia;
    }

    @Override
    public void protocol(Stream stream) {
        stream.writeOutput(Utils.encodeProtocols(Protocol.MULTISTREAM_PROTOCOL,
                Protocol.DHT_PROTOCOL), false);
    }

    @Override
    public void data(Stream stream, byte[] data) throws Exception {
        Dht.Message msg = Dht.Message.parseFrom(data);
        Dht.Message.MessageType type = msg.getType();
        if (Objects.requireNonNull(type) == Dht.Message.MessageType.FIND_NODE) {
            byte[] target = msg.getKey().toByteArray();
            Key key = Key.convertKey(target);
            List<Dht.Message.Peer> closer = closer(key);
            Dht.Message.Builder builder = Dht.Message.newBuilder()
                    .setType(Dht.Message.MessageType.FIND_NODE)
                    .setClusterLevelRaw(0);

            for (Dht.Message.Peer peer : closer) {
                builder.addCloserPeers(peer);
            }
            Dht.Message response = builder.build();
            stream.writeOutput(Utils.encode(response), true);
        } else {
            stream.fin();
        }
    }


    @Override
    public void terminated(Stream stream) {
        // nothing to do here
    }

    @Override
    public void fin(Stream stream) {
        // nothing to do here
    }

    private List<Dht.Message.Peer> closer(Key key) {
        Peeraddrs peeraddrs = dhtKademlia.nearestPeers(key);
        List<Dht.Message.Peer> closer = new ArrayList<>();
        for (Peeraddr peeraddr : peeraddrs) {
            byte[] id = PeerId.multihash(peeraddr.peerId());
            Dht.Message.Peer peer = Dht.Message.Peer.newBuilder()
                    .setId(ByteString.copyFrom(id))
                    .addAddrs(ByteString.copyFrom(peeraddr.encoded()))
                    .build();
            closer.add(peer);
        }
        return closer;
    }
}

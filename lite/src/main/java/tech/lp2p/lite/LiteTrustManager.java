package tech.lp2p.lite;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Objects;

import javax.net.ssl.X509TrustManager;

import tech.lp2p.core.Certificate;
import tech.lp2p.core.PeerId;
import tech.lp2p.utils.Utils;

public final class LiteTrustManager implements X509TrustManager {

    @Nullable
    private final PeerId expected;

    public LiteTrustManager(@NotNull PeerId expected) {
        this.expected = expected;
    }

    public LiteTrustManager() {
        this.expected = null;
    }

    @Override
    public void checkClientTrusted(X509Certificate[] chain, String authType)
            throws CertificateException {
        if (chain.length != 1) {
            throw new CertificateException("only one certificate allowed");
        }
        try {
            // here the expected peerId is null (because it is simply not known)
            // just check if the extracted peerId is not null
            for (X509Certificate cert : chain) {

                Certificate.validCertificate(cert);

                PeerId peerId = Certificate.extractPeerId(cert);
                Objects.requireNonNull(peerId);
            }
        } catch (Throwable throwable) {
            throw new CertificateException(throwable);
        }
    }

    @Override
    public void checkServerTrusted(X509Certificate[] chain, String authType)
            throws CertificateException {
        if (chain.length != 1) {
            throw new CertificateException("only one certificate allowed");
        }
        try {
            // now check the extracted peer ID with the expected value
            for (X509Certificate cert : chain) {

                Certificate.validCertificate(cert);

                if (expected != null) {
                    PeerId peerId = Certificate.extractPeerId(cert);
                    Objects.requireNonNull(peerId);
                    if (!Objects.equals(peerId, expected)) {
                        throw new CertificateException("PeerIds not match " + peerId + " " + expected);
                    }
                }
            }
        } catch (Throwable throwable) {
            throw new CertificateException(throwable);
        }
    }

    @Override
    public X509Certificate[] getAcceptedIssuers() {
        return Utils.CERTIFICATES_EMPTY;
    }

}

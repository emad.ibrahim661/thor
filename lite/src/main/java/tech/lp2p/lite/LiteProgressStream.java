package tech.lp2p.lite;

import org.jetbrains.annotations.NotNull;

import tech.lp2p.core.Progress;

public final class LiteProgressStream extends LiteReaderStream {

    private final Progress progress;
    private final long size;
    private int remember = 0;
    private long totalRead = 0L;

    public LiteProgressStream(@NotNull LiteReader liteReader, @NotNull Progress progress) {
        super(liteReader);
        this.progress = progress;
        this.size = liteReader.getSize();
    }


    @Override
    void loadNextData() throws Exception {
        super.loadNextData();

        if (buffer != null) {
            totalRead += buffer.size();
            if (size > 0) {
                int percent = (int) ((totalRead * 100.0f) / size);
                if (remember < percent) {
                    remember = percent;
                    progress.setProgress(percent);
                }
            }
        }
    }

}

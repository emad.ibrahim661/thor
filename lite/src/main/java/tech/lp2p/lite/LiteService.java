package tech.lp2p.lite;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

import tech.lp2p.Lite;
import tech.lp2p.core.ALPN;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Envelope;
import tech.lp2p.core.Payload;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Protocol;
import tech.lp2p.quic.Requester;
import tech.lp2p.utils.Utils;

public interface LiteService {

    static byte[] fetch(Connection connection, Cid cid) throws Exception {
        if (connection.alpn() != ALPN.lite) {
            throw new Exception("ALPN must be lite");
        }

        return Requester.createStream((tech.lp2p.quic.Connection) connection,
                        Protocol.LITE_FETCH_PROTOCOL.readDelimiter())
                .request(Utils.createMessage(Protocol.LITE_FETCH_PROTOCOL, cid.hash()),
                        Lite.GRACE_PERIOD);
    }

    @Nullable
    static Envelope pullEnvelope(@NotNull Connection connection,
                                 @NotNull Payload payload) throws Exception {
        if (connection.alpn() != ALPN.lite) {
            throw new Exception("ALPN must be lite");
        }

        byte[] data = Requester.createStream((tech.lp2p.quic.Connection) connection,
                        Protocol.LITE_PULL_PROTOCOL.readDelimiter())
                .request(Utils.createMessage(Protocol.LITE_PULL_PROTOCOL, Payload.toArray(payload)),
                        Lite.ENVELOPE_TIMEOUT);
        if (data.length > 0) {
            return LiteService.createEnvelope(data);
        }
        return null;

    }


    static void pushEnvelope(@NotNull Connection connection,
                             @NotNull Envelope envelope) throws Exception {
        if (connection.alpn() != ALPN.lite) {
            throw new Exception("ALPN must be lite");
        }

        Requester.createStream((tech.lp2p.quic.Connection) connection,
                        Protocol.LITE_PUSH_PROTOCOL.readDelimiter())
                .request(Utils.createMessage(Protocol.LITE_PUSH_PROTOCOL,
                        Envelope.toArray(envelope)), Lite.ENVELOPE_TIMEOUT);
    }

    @NotNull
    static Envelope createEnvelope(byte[] data) throws Exception {

        Envelope envelope = Envelope.toEnvelope(data);
        PeerId peerId = envelope.peerId();
        Objects.requireNonNull(peerId, "peerId not defined");

        Payload payload = envelope.payload();
        Objects.requireNonNull(payload, "payload not defined");

        Cid cid = envelope.cid();
        Objects.requireNonNull(payload, "cid not defined");

        byte[] signature = envelope.signature();
        Objects.requireNonNull(signature, "signature not defined");

        peerId.verify(Payload.unsignedEnvelopePayload(payload, cid), signature);

        return new Envelope(peerId, cid, payload, signature);
    }

}

package tech.lp2p.lite;


import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Hash;
import tech.lp2p.dag.DagFetch;
import tech.lp2p.proto.Merkledag;


public final class LiteFetchManager implements DagFetch {

    @NotNull
    private final Connection connection;
    @NotNull
    private final BlockStore blockStore;

    public LiteFetchManager(@NotNull Connection connection) {
        this.connection = connection;
        this.blockStore = connection.host().blockStore();
    }

    @Override
    @NotNull
    public Merkledag.PBNode getBlock(Cid cid) throws Exception {
        byte[] data = LiteService.fetch(connection, cid);

        if (data.length > 0) {

            // only Multicodec.DAG_PB is supported here
            Cid cmp = Cid.createCid(Hash.createHash(data));

            if (!Objects.equals(cmp, cid)) {
                throw new Exception("Invalid Cid block returned");
            }
            Merkledag.PBNode node = Merkledag.PBNode.parseFrom(data);

            blockStore.storeBlock(cid, node.toByteArray());
            return node;
        }

        throw new Exception("Block not available");

    }

    @NotNull
    @Override
    public BlockStore blockStore() {
        return blockStore;
    }


}

package tech.lp2p.lite;


import org.jetbrains.annotations.NotNull;

import tech.lp2p.core.Certificate;
import tech.lp2p.core.Keys;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddrs;


public final class LiteHost {
    @NotNull
    private final Keys keys;

    @NotNull
    private final Certificate certificate;
    @NotNull
    private final String agent;
    @NotNull
    private final Peeraddrs bootstrap;

    public LiteHost(@NotNull Keys keys,
                    @NotNull Peeraddrs bootstrap,
                    @NotNull String agent) throws Exception {
        this.keys = keys;
        this.bootstrap = bootstrap;
        this.agent = agent;
        this.certificate = Certificate.createCertificate(keys);
    }

    @NotNull
    public Keys keys() {
        return keys;
    }

    @NotNull
    public PeerId self() {
        return keys.peerId();
    }

    @NotNull
    public Peeraddrs bootstrap() {
        return bootstrap;
    }

    @NotNull
    public Certificate certificate() {
        return certificate;
    }


    @NotNull
    public String agent() {
        return agent;
    }

}



package tech.lp2p.lite;

import tech.lp2p.core.Handler;
import tech.lp2p.quic.Stream;

public final class LiteStreamHandler implements Handler {

    @Override
    public void protocol(Stream stream) {
        // nothing to do here
    }

    @Override
    public void data(Stream stream, byte[] data) throws Exception {
        if (data.length > 0) {
            throw new Exception("not expected data received for multistream " + new String(data));
        }
    }

    @Override
    public void terminated(Stream stream) {
        // nothing to do here
    }

    @Override
    public void fin(Stream stream) {
        // nothing to do here
    }
}

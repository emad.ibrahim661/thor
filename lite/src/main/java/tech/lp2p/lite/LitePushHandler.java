package tech.lp2p.lite;

import org.jetbrains.annotations.NotNull;

import tech.lp2p.Lite;
import tech.lp2p.core.Envelope;
import tech.lp2p.core.Handler;
import tech.lp2p.quic.Stream;
import tech.lp2p.utils.Utils;

public final class LitePushHandler implements Handler {

    private final LiteServer liteServer;

    LitePushHandler(@NotNull LiteServer liteServer) {
        this.liteServer = liteServer;
    }


    @Override
    public void protocol(Stream stream) {
        throw new IllegalStateException("should never be invoked here");
    }

    @Override
    public void data(Stream stream, byte[] data) throws Exception {
        Envelope envelope = LiteService.createEnvelope(data);
        stream.response(Utils.BYTES_EMPTY, Lite.DEFAULT_TIMEOUT);
        liteServer.push(envelope);
    }

    @Override
    public void terminated(Stream stream) {
        throw new IllegalStateException("should never be invoked here");
    }

    @Override
    public void fin(Stream stream) {
        throw new IllegalStateException("should never be invoked here");
    }

}

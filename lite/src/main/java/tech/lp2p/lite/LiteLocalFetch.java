package tech.lp2p.lite;

import org.jetbrains.annotations.NotNull;

import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Session;
import tech.lp2p.dag.DagFetch;
import tech.lp2p.proto.Merkledag;

public record LiteLocalFetch(Session session) implements DagFetch {
    @NotNull
    @Override
    public Merkledag.PBNode getBlock(Cid cid) throws Exception {
        throw new Exception("data not available");
    }

    @NotNull
    @Override
    public BlockStore blockStore() {
        return session.blockStore();
    }
}

package tech.lp2p.lite;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.net.ConnectException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Function;

import tech.lp2p.Lite;
import tech.lp2p.core.ALPN;
import tech.lp2p.core.Acceptor;
import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Certificate;
import tech.lp2p.core.Envelope;
import tech.lp2p.core.Identify;
import tech.lp2p.core.Key;
import tech.lp2p.core.Keys;
import tech.lp2p.core.Limit;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.PeerStore;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Protocol;
import tech.lp2p.core.Protocols;
import tech.lp2p.core.Request;
import tech.lp2p.core.Reservation;
import tech.lp2p.core.Responder;
import tech.lp2p.core.Server;
import tech.lp2p.dht.DhtHandler;
import tech.lp2p.dht.DhtKademlia;
import tech.lp2p.ident.IdentifyHandler;
import tech.lp2p.ident.IdentifyPushHandler;
import tech.lp2p.ident.IdentifyService;
import tech.lp2p.quic.AlpnLibp2pResponder;
import tech.lp2p.quic.AlpnLiteResponder;
import tech.lp2p.quic.ApplicationProtocolConnectionFactory;
import tech.lp2p.quic.Connection;
import tech.lp2p.quic.ConnectionBuilder;
import tech.lp2p.quic.Requester;
import tech.lp2p.quic.ServerConnection;
import tech.lp2p.quic.ServerConnector;
import tech.lp2p.quic.Stream;
import tech.lp2p.quic.StreamHandler;
import tech.lp2p.quic.TransportError;
import tech.lp2p.quic.Version;
import tech.lp2p.relay.RelayHopHandler;
import tech.lp2p.relay.RelayService;
import tech.lp2p.relay.RelayStopHandler;
import tech.lp2p.utils.Utils;

public final class LiteServer implements Server {

    @NotNull
    private final DatagramSocket socket;
    @NotNull
    private final ServerConnector serverConnector;
    @NotNull
    private final Responder responderLibp2p;
    @NotNull
    private final Responder responderLite;
    @NotNull
    private final LiteHost host;
    @NotNull
    private final Function<Request, Envelope> envelopeSupplier;
    @NotNull
    private final Consumer<Envelope> envelopeConsumer;
    @NotNull
    private final DhtKademlia dhtKademlia;
    @NotNull
    private final BlockStore blockStore;
    @NotNull
    private final Consumer<Reservation> reservationGain;
    @NotNull
    private final Consumer<Reservation> reservationLost;

    @NotNull
    private final Function<PeerId, Boolean> isGated;

    private LiteServer(@NotNull Server.Settings settings,
                       @NotNull LiteHost host,
                       @NotNull BlockStore blockStore,
                       @NotNull PeerStore peerStore,
                       @NotNull Consumer<Reservation> reservationGain,
                       @NotNull Consumer<Reservation> reservationLost,
                       @NotNull Function<PeerId, Boolean> isGated,
                       @NotNull Function<Request, Envelope> envelopeSupplier,
                       @NotNull Consumer<Envelope> envelopeConsumer,
                       @NotNull DatagramSocket socket) {
        this.host = host;
        this.blockStore = blockStore;
        this.socket = socket;
        this.reservationGain = reservationGain;
        this.reservationLost = reservationLost;
        this.isGated = isGated;
        this.envelopeSupplier = envelopeSupplier;
        this.envelopeConsumer = envelopeConsumer;

        this.dhtKademlia = new DhtKademlia(this, peerStore);

        Limit limit = settings.limit();
        Protocols libp2p = new Protocols();
        libp2p.put(Protocol.MULTISTREAM_PROTOCOL, new LiteStreamHandler());
        libp2p.put(Protocol.IDENTITY_PROTOCOL, new IdentifyHandler(this));
        if (Protocol.IDENTITY_PUSH_PROTOCOL.enabled()) {
            libp2p.put(Protocol.IDENTITY_PUSH_PROTOCOL, new IdentifyPushHandler());
        }
        if (Protocol.DHT_PROTOCOL.enabled()) {
            libp2p.put(Protocol.DHT_PROTOCOL, new DhtHandler(dhtKademlia));
        }
        libp2p.put(Protocol.RELAY_PROTOCOL_HOP, new RelayHopHandler(this, limit));
        this.responderLibp2p = new Responder(libp2p);

        Protocols lite = new Protocols();
        lite.put(Protocol.LITE_PUSH_PROTOCOL, new LitePushHandler(this));
        lite.put(Protocol.LITE_PULL_PROTOCOL, new LitePullHandler(this));
        lite.put(Protocol.LITE_FETCH_PROTOCOL, new LiteFetchHandler(blockStore));
        this.responderLite = new Responder(lite);


        Map<ALPN, Function<Stream, StreamHandler>> streamDataConsumers =
                Map.of(ALPN.libp2p,
                        quicStream -> AlpnLibp2pResponder.create(responderLibp2p),
                        ALPN.lite,
                        quicStream -> AlpnLiteResponder.create(responderLite));

        this.serverConnector = new ServerConnector(this, socket, new LiteTrustManager(),
                host.certificate().certificate(),
                host.certificate().privateKey(), List.of(Version.QUIC_version_1),
                streamDataConsumers);
    }


    @NotNull
    public static Server startServer(@NotNull Server.Settings settings,
                                     @NotNull LiteHost host,
                                     @NotNull BlockStore blockStore,
                                     @NotNull PeerStore peerStore,
                                     @NotNull Consumer<Peeraddr> connectionGain,
                                     @NotNull Consumer<Peeraddr> connectionLost,
                                     @NotNull Consumer<Reservation> reservationGain,
                                     @NotNull Consumer<Reservation> reservationLost,
                                     @NotNull Function<PeerId, Boolean> isGated,
                                     @NotNull Function<Request, Envelope> envelopeSupplier,
                                     @NotNull Consumer<Envelope> envelopeConsumer) {

        DatagramSocket socket = Utils.getSocket(settings.port());

        LiteServer server = new LiteServer(settings, host, blockStore, peerStore,
                reservationGain, reservationLost, isGated, envelopeSupplier,
                envelopeConsumer, socket);

        ServerConnector serverConnector = server.serverConnector;

        serverConnector.setClosedConsumer(connectionLost);
        ApplicationProtocolConnection protocolConnection =
                new ApplicationProtocolConnection(isGated, connectionGain);

        serverConnector.registerApplicationProtocol(ALPN.libp2p.name(), protocolConnection);

        serverConnector.registerApplicationProtocol(ALPN.lite.name(), protocolConnection);

        serverConnector.start();
        return server;
    }

    @NotNull
    public ServerConnector serverConnector() {
        return serverConnector;
    }

    void push(@NotNull Envelope envelope) {
        envelopeConsumer.accept(envelope);
    }

    @Override
    @NotNull
    public Responder responder(@NotNull ALPN alpn) {
        switch (alpn) {
            case lite -> {
                return responderLite;
            }
            case libp2p -> {
                return responderLibp2p;
            }
        }
        throw new IllegalStateException();
    }

    @NotNull
    @Override
    public Keys keys() {
        return host.keys();
    }


    @Nullable
    Envelope envelope(@NotNull Request request) {
        return envelopeSupplier.apply(request);
    }

    @NotNull
    public DatagramSocket socket() {
        return socket;
    }

    @Override
    public void shutdown() {
        serverConnector.shutdown();
    }

    @Override
    public int port() {
        return socket.getLocalPort();
    }

    private void punching(Peeraddr peeraddr, long expireTimeStamp) {

        // Upon expiry of the timer, B starts to send UDP packets filled with random bytes to A's
        // address. Packets should be sent repeatedly in random intervals between 10 and 200 ms.

        try {
            InetAddress address = InetAddress.getByAddress(peeraddr.address());
            int port = peeraddr.port();
            serverConnector.punching(address, port, expireTimeStamp);
        } catch (Throwable throwable) {
            Utils.error(throwable); // should not occur
        }
    }

    @Override
    public void holePunching(@NotNull Peeraddr peeraddr) {
        long expireTimeStamp = System.currentTimeMillis() + (Lite.CONNECT_TIMEOUT * 1000); // ms
        new Thread(() -> punching(peeraddr, expireTimeStamp)).start();
    }

    @NotNull
    @Override
    public PeerId self() {
        return host.self();
    }

    @NotNull
    @Override
    public Peeraddrs bootstrap() {
        return host.bootstrap();
    }

    @NotNull
    @Override
    public Protocols protocols(@NotNull ALPN alpn) {
        return responder(alpn).protocols();
    }

    @NotNull
    @Override
    public Certificate certificate() {
        return host.certificate();
    }

    @NotNull
    public Set<Reservation> reservations() {
        Set<Reservation> result = new HashSet<>();
        for (Connection connection : serverConnector.clientConnections()) {
            Reservation reservation = (Reservation) connection.getAttribute(
                    Requester.RESERVATION);
            if (reservation != null) {
                result.add(reservation);
            } else {
                removeReservation(connection, null);
            }
        }
        return result;
    }


    @Override
    @NotNull
    public Reservation hopReserve(Peeraddr relay) throws InterruptedException, ConnectException,
            TimeoutException {

        Parameters parameters = Parameters.create(ALPN.libp2p, true);

        Protocols libp2p = new Protocols();
        libp2p.put(Protocol.MULTISTREAM_PROTOCOL, new LiteStreamHandler());
        libp2p.put(Protocol.IDENTITY_PROTOCOL, new IdentifyHandler(this));
        if (Protocol.IDENTITY_PUSH_PROTOCOL.enabled()) {
            libp2p.put(Protocol.IDENTITY_PUSH_PROTOCOL, new IdentifyPushHandler());
        }
        libp2p.put(Protocol.RELAY_PROTOCOL_STOP, new RelayStopHandler(this));

        Connection connection = ConnectionBuilder.connect(this, relay,
                parameters, new Responder(libp2p), serverConnector());
        connection.setCloseConsumer(conn -> {
            Reservation reservation = (Reservation) conn.getAttribute(Requester.RESERVATION);
            removeReservation(conn, reservation);
        });
        try {
            Reservation reservation = RelayService.hopReserve(connection, self());
            connection.setAttribute(Requester.RESERVATION, reservation);
            reservationGain.accept(reservation);
            return reservation;
        } catch (Throwable throwable) {
            connection.close();
            throw throwable;
        }
    }

    public boolean hasReservations() {
        return !reservations().isEmpty();
    }

    @Override
    public void hopReserve(int maxReservation, int timeout) {

        Set<PeerId> validRelay = new HashSet<>();
        Set<Connection> refreshRequired = new HashSet<>();


        // check if reservations are still valid and not expired
        for (Connection connection : serverConnector.clientConnections()) {
            Reservation reservation = (Reservation) connection.getAttribute(
                    Requester.RESERVATION);
            if (reservation != null) {
                validRelay.add(reservation.peerId());  // still valid
                if (reservation.updateRequired()) {
                    // update the limited connection
                    refreshRequired.add(connection);
                }
            } else {
                removeReservation(connection, null);
            }
        }

        AtomicInteger valid = new AtomicInteger(validRelay.size());

        ExecutorService service = Executors.newFixedThreadPool(
                Runtime.getRuntime().availableProcessors());
        try {
            for (Connection connection : refreshRequired) {
                service.execute(() -> {
                    try {
                        if (Thread.currentThread().isInterrupted()) {
                            return;
                        }
                        connection.setAttribute(Requester.RESERVATION,
                                RelayService.hopReserve(connection, self()));

                        valid.incrementAndGet();
                    } catch (Throwable throwable) {
                        Reservation reservation = (Reservation) connection.getAttribute(
                                Requester.RESERVATION);
                        removeReservation(connection, reservation);
                    }
                });

            }

            Key key = self().createKey();
            service.execute(() -> {
                // fill up reservations [not yet enough]
                dhtKademlia.findClosestPeers(key, peeraddr -> {

                    // there is a valid reservation
                    if (validRelay.contains(peeraddr.peerId())) {
                        return;
                    }

                    if (valid.get() > maxReservation) {
                        // no more reservations
                        return; // just return, let the refresh mechanism finished
                    }

                    try {
                        hopReserve(peeraddr);

                        if (valid.incrementAndGet() > maxReservation) {
                            // done
                            service.shutdownNow();
                        }
                    } catch (ConnectException | TimeoutException | InterruptedException e) {
                        Utils.error("LiteServer Exception hopReserve " + e.getMessage());
                    }
                });
            });

            service.shutdown();
            if (!service.awaitTermination(timeout, TimeUnit.SECONDS)) {
                service.shutdownNow();
            }
        } catch (InterruptedException interruptedException) {
            service.shutdownNow();
        }
    }

    @NotNull
    public Peeraddrs peeraddrs() {
        return Peeraddr.peeraddrs(self(), port());
    }

    @NotNull
    public Identify identify() throws Exception {
        return IdentifyService.identify(IdentifyService.identify(host.self(), host.agent(),
                protocols(ALPN.libp2p).names(), peeraddrs()), self());

    }

    @NotNull
    public Peeraddrs reservationPeeraddrs() {
        Peeraddrs result = new Peeraddrs();
        for (Reservation reservation : reservations()) {
            result.add(reservation.peeraddr());
        }
        return result;
    }

    @Override
    public void provideKey(@NotNull Key key, @NotNull Acceptor acceptor) {
        dhtKademlia.provideKey(this, acceptor, key);
    }

    @Override
    public Reservation refreshReservation(Reservation reservation)
            throws ConnectException, InterruptedException, TimeoutException {
        for (Connection connection : serverConnector.clientConnections()) {
            Reservation stored = (Reservation) connection.getAttribute(Requester.RESERVATION);
            if (Objects.equals(stored, reservation)) {
                try {
                    Reservation refreshed = RelayService.hopReserve(connection, self());
                    connection.setAttribute(Requester.RESERVATION, refreshed);
                    return refreshed;
                } catch (Throwable throwable) {
                    removeReservation(connection, stored);
                    throw throwable;
                }
            }
        }
        throw new ConnectException("Reservation is not valid anymore");
    }

    private void removeReservation(Connection connection, @Nullable Reservation reservation) {
        connection.removeAttribute(Requester.RESERVATION);
        if (reservation != null) {
            reservationLost.accept(reservation);
        }
    }

    @Override
    public void closeReservation(Reservation reservation) {
        for (Connection connection : serverConnector.clientConnections()) {
            Reservation stored = (Reservation) connection.getAttribute(Requester.RESERVATION);
            if (Objects.equals(stored, reservation)) {
                removeReservation(connection, stored);
                connection.close(LiteErrorCode.CLOSE);
            }
        }
    }

    @Override
    public List<tech.lp2p.core.Connection> connections(PeerId peerId) {
        return new ArrayList<>(serverConnector().serverConnections(peerId));
    }

    @Override
    public boolean isGated(PeerId peerId) {
        return isGated.apply(peerId);
    }

    @Override
    public boolean hasConnection(PeerId peerId) {
        return !serverConnector().serverConnections(peerId).isEmpty();
    }

    @Override
    public int numConnections() {
        return serverConnector.numConnections();
    }

    @NotNull
    public String agent() {
        return host.agent();
    }


    @NotNull
    @Override
    public BlockStore blockStore() {
        return blockStore;
    }

    private static class ApplicationProtocolConnection extends ApplicationProtocolConnectionFactory {
        private final Function<PeerId, Boolean> isGated;
        private final Consumer<Peeraddr> connectConsumer;

        private ApplicationProtocolConnection(
                Function<PeerId, Boolean> isGated, Consumer<Peeraddr> connectConsumer) {
            this.isGated = isGated;
            this.connectConsumer = connectConsumer;
        }

        @Override
        public void createConnection(String protocol, ServerConnection connection) throws TransportError {
            PeerId remotePeerId;

            try {
                X509Certificate certificate = connection.remoteCertificate();
                Objects.requireNonNull(certificate);
                remotePeerId = Certificate.extractPeerId(certificate);
                Objects.requireNonNull(remotePeerId);
            } catch (Throwable throwable) {
                throw new TransportError(TransportError.Code.CRYPTO_ERROR,
                        throwable.getMessage());
            }

            // now the remote PeerId is available
            connection.setRemotePeerId(remotePeerId);

            if (isGated.apply(remotePeerId)) {
                throw new TransportError(LiteErrorCode.GATED.code(),
                        "Peer is gated " + remotePeerId);
            }

            try {
                connectConsumer.accept(connection.remotePeeraddr());
            } catch (Throwable throwable) {
                Utils.error(throwable); // should not occur
            }

        }
    }
}

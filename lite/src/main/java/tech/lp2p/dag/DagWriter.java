package tech.lp2p.dag;

import com.google.protobuf.ByteString;

import org.jetbrains.annotations.NotNull;

import tech.lp2p.Lite;
import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Fid;
import tech.lp2p.core.Hash;
import tech.lp2p.proto.Merkledag;
import tech.lp2p.proto.Unixfs;


interface DagWriter {
    int DEPTH_REPEAT = 4;
    int LINKS_PER_BLOCK = 1000; // kubo it is 175 [but not big performance gain]


    static long fillNodeLayer(DagInputStream reader,
                              BlockStore blockStore,
                              Unixfs.Data.Builder data,
                              Merkledag.PBNode.Builder pbn) throws Exception {
        byte[] bytes = new byte[Lite.CHUNK_DATA];
        int numChildren = 0;
        long size = 0L;
        while ((numChildren < LINKS_PER_BLOCK && !reader.done())) {
            int read = reader.read(bytes);
            if (read > 0) {
                if (reader.done() && numChildren == 0) {
                    data.setData(ByteString.copyFrom(bytes, 0, read));
                    size = size + read;
                    break;
                } else {
                    createRawBlock(blockStore, data, pbn, bytes, read);
                    size = size + read;
                }
            }
            numChildren++;
        }
        commit(data, pbn, size);
        return size;
    }

    private static void createRawBlock(BlockStore blockStore,
                                       Unixfs.Data.Builder data,
                                       Merkledag.PBNode.Builder pbn,
                                       byte[] bytes, int read) {
        Unixfs.Data.Builder unixfs =
                Unixfs.Data.newBuilder().setType(Unixfs.Data.DataType.Raw)
                        .setFilesize(read)
                        .setData(ByteString.copyFrom(bytes, 0, read));

        Merkledag.PBNode.Builder builder = Merkledag.PBNode.newBuilder();
        builder.setData(ByteString.copyFrom(unixfs.build().toByteArray()));

        byte[] block = builder.build().toByteArray();
        Cid cid = Cid.createCid(Hash.createHash(block));
        blockStore.storeBlock(cid, block);
        addChild(data, pbn, cid, read);
    }


    static Fid trickle(@NotNull String name,
                       @NotNull DagInputStream reader,
                       @NotNull BlockStore blockStore) throws Exception {
        Result result = fillTrickleRec(reader, blockStore, Unixfs.Data.newBuilder()
                .setType(Unixfs.Data.DataType.File)
                .setName(name)
                .setFilesize(0L), Merkledag.PBNode.newBuilder(), -1);
        return new Fid(result.cid(), result.size, name);
    }


    private static Result fillTrickleRec(@NotNull DagInputStream reader,
                                         @NotNull BlockStore blockStore,
                                         Unixfs.Data.Builder data,
                                         Merkledag.PBNode.Builder pbn,
                                         int maxDepth) throws Exception {
        // Always do this, even in the base case
        long size = fillNodeLayer(reader, blockStore, data, pbn);

        for (int depth = 1; maxDepth == -1 || depth < maxDepth; depth++) {
            if (reader.done()) {
                break;
            }

            for (int repeatIndex = 0; repeatIndex < DEPTH_REPEAT && !reader.done(); repeatIndex++) {

                Result result = fillTrickleRec(reader, blockStore, Unixfs.Data.newBuilder()
                        .setType(Unixfs.Data.DataType.Raw)
                        .setFilesize(0L), Merkledag.PBNode.newBuilder(), depth);

                long fileSize = result.size();
                addChild(data, pbn, result.cid(), fileSize);
                size = size + fileSize;
            }
        }
        Merkledag.PBNode node = commit(data, pbn, size);

        byte[] bytes = node.toByteArray();
        Cid cid = Cid.createCid(Hash.createHash(bytes));
        blockStore.storeBlock(cid, bytes);

        return new Result(cid, size);
    }

    private static Merkledag.PBNode commit(
            Unixfs.Data.Builder data, Merkledag.PBNode.Builder pbn, long size) {
        data.setFilesize(size);
        pbn.setData(ByteString.copyFrom(data.build().toByteArray()));
        return pbn.build();
    }

    private static void addChild(Unixfs.Data.Builder data,
                                 Merkledag.PBNode.Builder pbn,
                                 Cid cid, long fileSize) {
        Merkledag.PBLink.Builder lnb = Merkledag.PBLink.newBuilder().setTsize(fileSize);
        lnb.setHash(ByteString.copyFrom(cid.hash()));

        pbn.addLinks(lnb.build());
        data.addBlocksizes(fileSize);
    }

    record Result(Cid cid, long size) {
    }
}

package tech.lp2p.dag;

import org.jetbrains.annotations.NotNull;

import java.util.Stack;
import java.util.concurrent.atomic.AtomicBoolean;

import tech.lp2p.proto.Merkledag;

public record DagVisitor(@NotNull Stack<DagStage> stack, @NotNull AtomicBoolean rootVisited) {

    public static DagVisitor createDagVisitor(@NotNull Merkledag.PBNode root) {
        DagVisitor dagVisitor = new DagVisitor(new Stack<>(), new AtomicBoolean(false));
        dagVisitor.pushActiveNode(root);
        return dagVisitor;
    }

    public void reset(@NotNull Stack<DagStage> dagStages) {
        stack.clear();
        stack.addAll(dagStages);
        rootVisited.set(false);
    }

    public void pushActiveNode(@NotNull Merkledag.PBNode node) {
        stack.push(DagStage.createDagStage(node));
    }

    public void popStage() {
        stack.pop();
    }

    public DagStage peekStage() {
        return stack.peek();
    }

    public boolean isRootVisited(boolean visited) {
        return rootVisited.getAndSet(visited);
    }

    public boolean isPresent() {
        return !stack.isEmpty();
    }
}

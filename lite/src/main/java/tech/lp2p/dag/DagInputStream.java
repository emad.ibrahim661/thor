package tech.lp2p.dag;

import org.jetbrains.annotations.NotNull;

import java.io.InputStream;

import tech.lp2p.core.Progress;

public final class DagInputStream {

    private final InputStream inputStream;
    private final Progress mProgress;
    private final long size;
    private int progress = 0;
    private long totalRead = 0;
    private boolean done;


    public DagInputStream(@NotNull InputStream inputStream, @NotNull Progress progress, long size) {
        this.inputStream = inputStream;
        this.mProgress = progress;
        this.size = size;
    }

    public DagInputStream(@NotNull InputStream inputStream, long size) {
        this.inputStream = inputStream;
        this.mProgress = null;
        this.size = size;
    }

    public int read(byte[] bytes) throws Exception {


        int read = inputStream.read(bytes);
        if (read < bytes.length) {
            done = true;
        }
        if (read > 0) {
            totalRead += read;
            if (mProgress != null) {

                if (size > 0) {
                    int percent = (int) ((totalRead * 100.0f) / size);
                    if (progress < percent) {
                        progress = percent;
                        mProgress.setProgress(percent);
                    }
                }

            }
        }
        return read;

    }

    public boolean done() {
        return done;
    }
}

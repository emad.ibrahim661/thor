package tech.lp2p.dag;


import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Objects;

import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Hash;
import tech.lp2p.core.Info;
import tech.lp2p.proto.Merkledag;


public interface DagResolver {

    @NotNull
    static Info resolvePath(@NotNull DagFetch dagFetch, @NotNull Cid root,
                            @NotNull List<String> path) throws Exception {

        Cid cid = resolveToLastNode(dagFetch, root, path);
        Objects.requireNonNull(cid);
        Merkledag.PBNode node = DagService.getNode(dagFetch, cid);
        Objects.requireNonNull(node);
        return DagStream.info(cid, node);
    }

    @NotNull
    static Info resolvePath(@NotNull BlockStore blockStore, @NotNull Cid root,
                            @NotNull List<String> path) throws Exception {

        Cid cid = resolveToLastNode(blockStore, root, path);
        Objects.requireNonNull(cid);
        Merkledag.PBNode node = DagService.getNode(blockStore, cid);
        Objects.requireNonNull(node);
        return DagStream.info(cid, node);
    }

    @NotNull
    private static Cid resolveToLastNode(@NotNull BlockStore blockStore,
                                         @NotNull Cid root,
                                         @NotNull List<String> path)
            throws Exception {

        if (path.isEmpty()) {
            return root;
        }

        Cid cid = root;
        Merkledag.PBNode node = DagService.getNode(blockStore, cid);

        for (String name : path) {
            Merkledag.PBLink lnk = DagReader.getLinkByName(node, name);
            Objects.requireNonNull(lnk, name + " not found");
            cid = Cid.createCid(Hash.toHash(lnk.getHash().toByteArray()));
            node = DagService.getNode(blockStore, cid);
        }

        return cid;

    }

    @NotNull
    private static Cid resolveToLastNode(@NotNull DagFetch dagFetch,
                                         @NotNull Cid root,
                                         @NotNull List<String> path)
            throws Exception {

        if (path.isEmpty()) {
            return root;
        }

        Cid cid = root;

        Merkledag.PBNode node = DagService.getNode(dagFetch, cid);
        for (String name : path) {
            Merkledag.PBLink lnk = DagReader.getLinkByName(node, name);
            Objects.requireNonNull(lnk, name + " not found");
            cid = Cid.createCid(Hash.toHash(lnk.getHash().toByteArray()));
            node = DagService.getNode(dagFetch, cid);
        }

        return cid;

    }


}

package tech.lp2p.dag;

import org.jetbrains.annotations.NotNull;

import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Cid;
import tech.lp2p.proto.Merkledag;

public interface DagFetch {

    @NotNull
    Merkledag.PBNode getBlock(Cid cid) throws Exception;

    @NotNull
    BlockStore blockStore();

}

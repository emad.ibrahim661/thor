package tech.lp2p.tls;

import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.Map;

public enum HandshakeType {
    client_hello(1),
    server_hello(2),
    new_session_ticket(4),
    end_of_early_data(5),
    encrypted_extensions(8),
    certificate(11),
    certificate_request(13),
    certificate_verify(15),
    finished(20),
    key_update(24),
    message_hash(254),
    ;

    private static final Map<Integer, HandshakeType> byValue = new HashMap<>();

    static {
        for (HandshakeType t : HandshakeType.values()) {
            byValue.put((int) t.value, t);
        }
    }

    public final byte value;

    HandshakeType(int value) {
        this.value = (byte) value;
    }

    @Nullable
    public static HandshakeType get(int value) {
        return byValue.get(value);
    }
}

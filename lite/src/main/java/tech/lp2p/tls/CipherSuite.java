package tech.lp2p.tls;

import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.Map;

public enum CipherSuite {
    TLS_AES_128_GCM_SHA256(0x1301),
    TLS_AES_256_GCM_SHA384(0x1302),
    TLS_AES_128_CCM_SHA256(0x1304),
    TLS_AES_128_CCM_8_SHA256(0x1305);

    private static final Map<Short, CipherSuite> byValue = new HashMap<>();

    static {
        for (CipherSuite t : CipherSuite.values()) {
            byValue.put(t.value, t);
        }
    }

    public final short value;

    CipherSuite(int value) {
        this.value = (short) value;
    }

    @Nullable
    public static CipherSuite get(short value) {
        return byValue.get(value);
    }


}

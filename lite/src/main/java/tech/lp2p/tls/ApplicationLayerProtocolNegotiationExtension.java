/*
 * Copyright © 2019, 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Agent15, an implementation of TLS 1.3 in Java.
 *
 * Agent15 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Agent15 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package tech.lp2p.tls;

import org.jetbrains.annotations.NotNull;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public record ApplicationLayerProtocolNegotiationExtension(
        String[] protocols) implements Extension {


    public static ApplicationLayerProtocolNegotiationExtension create(String protocol) {
        if (protocol == null || protocol.trim().isEmpty()) {
            throw new IllegalArgumentException("protocol cannot be empty");
        }
        String[] protocols = new String[]{protocol};
        return new ApplicationLayerProtocolNegotiationExtension(protocols);
    }

    public static ApplicationLayerProtocolNegotiationExtension parse(ByteBuffer buffer) throws DecodeErrorException {
        int extensionDataLength = Extension.parseExtensionHeader(
                buffer, TlsConstants.ExtensionType.application_layer_protocol_negotiation.value, 3);

        int protocolsLength = buffer.getShort();
        if (protocolsLength != extensionDataLength - 2) {
            throw new DecodeErrorException("inconsistent lengths");
        }

        List<String> protocols = new ArrayList<>();
        while (protocolsLength > 0) {
            int protocolNameLength = buffer.get() & 0xff;
            if (protocolNameLength > protocolsLength - 1) {
                throw new DecodeErrorException("incorrect length");
            }
            byte[] protocolBytes = new byte[protocolNameLength];
            buffer.get(protocolBytes);
            protocols.add(new String(protocolBytes));
            protocolsLength -= (1 + protocolNameLength);
        }

        String[] protocolsArray = new String[protocols.size()];
        return new ApplicationLayerProtocolNegotiationExtension(
                protocols.toArray(protocolsArray));
    }

    @Override
    public byte[] getBytes() {
        int protocolNamesLength = Arrays.stream(protocols).mapToInt(p -> p.getBytes(StandardCharsets.UTF_8).length).sum();
        int size = 4 + 2 + protocols.length + protocolNamesLength;
        ByteBuffer buffer = ByteBuffer.allocate(size);
        buffer.putShort(TlsConstants.ExtensionType.application_layer_protocol_negotiation.value);
        buffer.putShort((short) (size - 4));
        buffer.putShort((short) (size - 6));
        Arrays.asList(protocols).forEach(protocol -> {
            byte[] protocolName = protocol.getBytes(StandardCharsets.UTF_8);
            buffer.put((byte) protocolName.length);
            buffer.put(protocolName);
        });

        return buffer.array();
    }


    @NotNull
    @Override
    public String toString() {
        return "AlpnExtension " + Arrays.toString(protocols);
    }
}

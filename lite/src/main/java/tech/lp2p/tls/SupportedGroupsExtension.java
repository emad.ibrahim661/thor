/*
 * Copyright © 2019, 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Agent15, an implementation of TLS 1.3 in Java.
 *
 * Agent15 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Agent15 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package tech.lp2p.tls;

import org.jetbrains.annotations.NotNull;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * The TLS supported groups extension.
 * See <a href="https://tools.ietf.org/html/rfc8446#section-4.2.7">...</a>
 */
public record SupportedGroupsExtension(
        NamedGroup[] namedGroups) implements Extension {

    public static SupportedGroupsExtension createSupportedGroupsExtension(NamedGroup namedGroup) {
        NamedGroup[] namedGroups = new NamedGroup[]{namedGroup};
        return new SupportedGroupsExtension(namedGroups);
    }

    public static SupportedGroupsExtension parse(ByteBuffer buffer) throws DecodeErrorException {
        List<NamedGroup> namedGroups = new ArrayList<>();
        int extensionDataLength = Extension.parseExtensionHeader(
                buffer, TlsConstants.ExtensionType.supported_groups, 2 + 2);
        int namedGroupsLength = buffer.getShort();
        if (extensionDataLength != 2 + namedGroupsLength) {
            throw new DecodeErrorException("inconsistent length");
        }
        if (namedGroupsLength % 2 != 0) {
            throw new DecodeErrorException("invalid group length");
        }

        for (int i = 0; i < namedGroupsLength; i += 2) {
            short namedGroupBytes = (short) (buffer.getShort() % 0xffff);
            NamedGroup namedGroup = NamedGroup.get(namedGroupBytes);
            namedGroups.add(namedGroup);
        }

        NamedGroup[] namedGroupsArray = new NamedGroup[namedGroups.size()];
        return new SupportedGroupsExtension(namedGroups.toArray(namedGroupsArray));
    }

    @Override
    public byte[] getBytes() {
        int extensionLength = 2 + namedGroups.length * 2;
        ByteBuffer buffer = ByteBuffer.allocate(4 + extensionLength);
        buffer.putShort(TlsConstants.ExtensionType.supported_groups.value);
        buffer.putShort((short) extensionLength);  // Extension data length (in bytes)

        buffer.putShort((short) (namedGroups.length * 2));
        for (NamedGroup namedGroup : namedGroups) {
            buffer.putShort(namedGroup.value);
        }

        return buffer.array();
    }

    @NotNull
    @Override
    public String toString() {
        return "SupportedGroupsExtension" + Arrays.toString(namedGroups);
    }

}

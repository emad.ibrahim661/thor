/*
 * Copyright © 2019, 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Agent15, an implementation of TLS 1.3 in Java.
 *
 * Agent15 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Agent15 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package tech.lp2p.tls;


import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.net.ssl.X509TrustManager;

public record TlsServerEngineFactory(X509Certificate[] serverCertificates,
                                     PrivateKey certificateKey) {


    public static TlsServerEngineFactory createTlsServerEngineFactory(byte[] certificateFile, byte[] certificateKeyFile) {
        try {
            return new TlsServerEngineFactory(readCertificates(certificateFile), readPrivateKey(certificateKeyFile));
        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }
    }

    private static X509Certificate[] readCertificates(byte[] bytes) throws CertificateException {
        String fileContent = new String(bytes, Charset.defaultCharset());
        String[] chunks = fileContent.split("-----END CERTIFICATE-----\n");

        List<X509Certificate> certs = new ArrayList<>();

        CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
        for (String chunk : chunks) {
            if (chunk.startsWith("-----BEGIN CERTIFICATE-----")) {
                String encodedCertificate = chunk
                        .replace("-----BEGIN CERTIFICATE-----", "")
                        .replaceAll(System.lineSeparator(), "")
                        .replace("-----END CERTIFICATE-----", "");
                try (InputStream inputStream = new ByteArrayInputStream(
                        Base64.getDecoder().decode(encodedCertificate))) {
                    Certificate certificate = certificateFactory.generateCertificate(inputStream);
                    certs.add((X509Certificate) certificate);
                } catch (Throwable throwable) {
                    throw new CertificateException(throwable);
                }
            }
        }

        X509Certificate[] x509Certificates = new X509Certificate[certs.size()];
        return certs.toArray(x509Certificates);
    }

    private static PrivateKey readPrivateKey(byte[] bytes) throws InvalidKeySpecException {
        String key = new String(bytes, Charset.defaultCharset());

        String privateKeyPEM = key
                .replace("-----BEGIN PRIVATE KEY-----", "")
                .replaceAll(System.lineSeparator(), "")
                .replace("-----END PRIVATE KEY-----", "");
        byte[] encoded = Base64.getMimeDecoder().decode(privateKeyPEM);

        try {
            KeyFactory keyFactory = KeyFactory.getInstance("EC");
            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(encoded);
            return keyFactory.generatePrivate(keySpec);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Missing key algorithm");
        }
    }

    public TlsServerEngine createServerEngine(X509TrustManager trustManager,
                                              ServerMessageSender serverMessageSender,
                                              TlsStatusEventHandler tlsStatusHandler) {
        TlsServerEngine tlsServerEngine = new TlsServerEngine(trustManager, serverCertificates, certificateKey,
                serverMessageSender, tlsStatusHandler);
        tlsServerEngine.addSupportedCiphers(List.of(CipherSuite.TLS_AES_128_GCM_SHA256));
        return tlsServerEngine;
    }
}

package tech.lp2p.tls;

import java.util.HashMap;
import java.util.Map;


public enum ExtendedHandshakeType {
    client_hello,
    server_hello,
    new_session_ticket,
    end_of_early_data,
    encrypted_extensions,
    certificate,
    certificate_request,
    certificate_verify,
    finished,
    key_update,
    server_certificate,
    server_certificate_verify,
    server_finished,
    client_certificate,
    client_certificate_verify,
    client_finished;

    private static final Map<Integer, ExtendedHandshakeType> byOrdinal = new HashMap<>();

    static {
        for (ExtendedHandshakeType t : ExtendedHandshakeType.values()) {
            byOrdinal.put(t.ordinal(), t);
        }
    }


    public static ExtendedHandshakeType get(int ordinal) {
        ExtendedHandshakeType type = byOrdinal.get(ordinal);
        if (type == null) {
            throw new IllegalStateException("ExtendedHandshakeType not found");
        }
        return type;
    }

}

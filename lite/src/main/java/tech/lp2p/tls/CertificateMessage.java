/*
 * Copyright © 2018, 2019, 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Agent15, an implementation of TLS 1.3 in Java.
 *
 * Agent15 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Agent15 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package tech.lp2p.tls;

import org.jetbrains.annotations.Nullable;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import tech.lp2p.utils.Utils;

// https://tools.ietf.org/html/rfc8446#section-4.4.2
public record CertificateMessage(byte[] requestContext,
                                 X509Certificate[] certificateChain,
                                 byte[] raw) implements HandshakeMessage {

    private static final int MINIMUM_MESSAGE_SIZE = 1 + 3 + 1 + 3 + 3 + 2;

    static CertificateMessage createCertificateMessage(@Nullable X509Certificate certificate) {
        byte[] requestContext = Utils.BYTES_EMPTY;
        X509Certificate[] certificateChain;
        if (certificate != null) {
            certificateChain = new X509Certificate[]{certificate};
        } else {
            certificateChain = Utils.CERTIFICATES_EMPTY;
        }

        return new CertificateMessage(requestContext, certificateChain, serialize(certificateChain));
    }

    /**
     * @param certificateChain The server certificate must be the first in the list
     */
    static CertificateMessage createCertificateMessage(X509Certificate[] certificateChain) {
        Objects.requireNonNull(certificateChain);
        if (certificateChain.length < 1) {
            throw new IllegalArgumentException();
        }
        byte[] requestContext = Utils.BYTES_EMPTY;

        return new CertificateMessage(requestContext, certificateChain, serialize(certificateChain));
    }


    public static CertificateMessage parse(ByteBuffer buffer) throws DecodeErrorException, BadCertificateAlert {
        int startPosition = buffer.position();
        int remainingLength = HandshakeMessage.parseHandshakeHeader(buffer, HandshakeType.certificate, MINIMUM_MESSAGE_SIZE);

        try {
            byte[] requestContext;
            int certificateRequestContextSize = buffer.get() & 0xff;
            if (certificateRequestContextSize > 0) {
                requestContext = new byte[certificateRequestContextSize];
                buffer.get(requestContext);
            } else {
                requestContext = Utils.BYTES_EMPTY;
            }
            List<X509Certificate> certificateChain = parseCertificateEntries(buffer);

            // Update state.
            byte[] raw = new byte[4 + remainingLength];
            buffer.position(startPosition);
            buffer.get(raw);

            X509Certificate[] x509Certificates = new X509Certificate[certificateChain.size()];
            return new CertificateMessage(requestContext,
                    certificateChain.toArray(x509Certificates), raw);
        } catch (BufferUnderflowException notEnoughBytes) {
            throw new DecodeErrorException("message underflow");
        }
    }

    private static List<X509Certificate> parseCertificateEntries(ByteBuffer buffer) throws BadCertificateAlert {
        int remainingCertificateBytes = ((buffer.get() & 0xff) << 16) | ((buffer.get() & 0xff) << 8) | (buffer.get() & 0xff);

        List<X509Certificate> certificateChain = new ArrayList<>();
        while (remainingCertificateBytes > 0) {
            int certSize = ((buffer.get() & 0xff) << 16) | ((buffer.get() & 0xff) << 8) | (buffer.get() & 0xff);
            byte[] certificateData = new byte[certSize];
            buffer.get(certificateData);

            if (certSize > 0) {
                // https://tools.ietf.org/html/rfc8446#section-4.4.2
                // "If the corresponding certificate payloadType extension ("server_certificate_type" or "client_certificate_type")
                // was not negotiated in EncryptedExtensions, or the X.509 certificate payloadType was negotiated, then each
                // CertificateEntry contains a DER-encoded X.509 certificate."
                // This implementation does not support raw-public-key certificates, so the only payloadType supported is X509.
                try (InputStream inputStream = new ByteArrayInputStream(certificateData)) {
                    CertificateFactory cf = CertificateFactory.getInstance("X.509");
                    X509Certificate certificate = (X509Certificate) cf.generateCertificate(inputStream);
                    certificateChain.add(certificate);
                } catch (CertificateException | IOException e) {
                    throw new BadCertificateAlert("could not parse certificate");
                }
            }

            remainingCertificateBytes -= (3 + certSize);

            int extensionsSize = buffer.getShort() & 0xffff;
            // https://tools.ietf.org/html/rfc8446#section-4.4.2
            // "Valid extensions for server certificates at present include the OCSP Status extension [RFC6066]
            // and the SignedCertificateTimestamp extension [RFC6962];..."
            // None of them is (yet) supported by this implementation.
            byte[] extensionData = new byte[extensionsSize];
            buffer.get(extensionData);
            remainingCertificateBytes -= (2 + extensionsSize);
        }
        return certificateChain;
    }

    private static byte[] serialize(X509Certificate[] certificateChain) {
        int nrOfCerts = certificateChain.length;
        List<byte[]> encodedCerts = new ArrayList<>();
        for (X509Certificate certificate : certificateChain) {
            encodedCerts.add(encode(certificate));
        }

        int msgSize = 4 + 1 + 3 + nrOfCerts * (3 + 2) + encodedCerts.stream().mapToInt(bytes -> bytes.length).sum();
        ByteBuffer buffer = ByteBuffer.allocate(msgSize);

        buffer.putInt((HandshakeType.certificate.value << 24) | (msgSize - 4));
        // cert request context size
        buffer.put((byte) 0x00);
        // certificate_list size (3 bytes)
        buffer.put((byte) 0); // assuming < 65535
        buffer.putShort((short) (msgSize - 4 - 1 - 3));

        encodedCerts.forEach(encodedCert -> {
            if (encodedCert.length > 0xfff0) {
                throw new RuntimeException("Certificate size not supported");
            }
            // certificate size
            buffer.put((byte) 0);
            buffer.putShort((short) encodedCert.length);
            // certificate
            buffer.put(encodedCert);
            // extensions size
            buffer.putShort((short) 0);
        });
        return buffer.array();
    }

    private static byte[] encode(X509Certificate certificate) {
        try {
            return certificate.getEncoded();
        } catch (CertificateEncodingException e) {
            // Impossible with valid certificate
            throw new RuntimeException(e);
        }
    }

    @Override
    public HandshakeType getType() {
        return HandshakeType.certificate;
    }

    @Override
    public byte[] getBytes() {
        return raw;
    }


    // https://tools.ietf.org/html/rfc8446#section-4.4.2
    // "The sender's certificate MUST come in the first CertificateEntry in the list. "
    X509Certificate getEndEntityCertificate() {
        return certificateChain[0];
    }

}

/*
 * Copyright © 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Agent15, an implementation of TLS 1.3 in Java.
 *
 * Agent15 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Agent15 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package tech.lp2p.tls;


import java.nio.ByteBuffer;

public record TlsMessageParser(ExtensionParser customExtensionParser) {


    public void parseAndProcessHandshakeMessage(
            ByteBuffer buffer, MessageProcessor messageProcessor, ProtectionKeysType protectedBy)
            throws ErrorAlert {
        // https://tools.ietf.org/html/rfc8446#section-4
        // "      struct {
        //          HandshakeType msg_type;    /* handshake payloadType */
        //          uint24 length;             /* remaining bytes in message */
        //          ...
        //      } Handshake;"
        buffer.mark();
        int messageType = buffer.get();
        int length = ((buffer.get() & 0xff) << 16) | ((buffer.get() & 0xff) << 8) | (buffer.get() & 0xff);
        buffer.reset();

        HandshakeType type = HandshakeType.get(messageType);
        if (type == null) {
            throw new HandshakeFailureAlert("Invalid/unsupported message payloadType (" + messageType + ")");
        }

        switch (type) {
            case client_hello -> {
                ClientHello ch = ClientHello.parse(buffer, customExtensionParser);
                messageProcessor.received(ch);
            }
            case server_hello -> {
                ServerHello sh = ServerHello.parse(buffer, length + 4);
                messageProcessor.received(sh);
            }
            case encrypted_extensions -> {
                EncryptedExtensions ee = EncryptedExtensions.parse(buffer,
                        length + 4, customExtensionParser);
                messageProcessor.received(ee, protectedBy);
            }
            case certificate -> {
                CertificateMessage cm = CertificateMessage.parse(buffer);
                messageProcessor.received(cm, protectedBy);
            }
            case certificate_request -> {
                CertificateRequestMessage cr = CertificateRequestMessage.parse(buffer);
                messageProcessor.received(cr, protectedBy);
            }
            case certificate_verify -> {
                CertificateVerifyMessage cv = CertificateVerifyMessage.parse(buffer, length + 4);
                messageProcessor.received(cv, protectedBy);
            }
            case finished -> {
                FinishedMessage fm = FinishedMessage.parse(buffer, length + 4);
                messageProcessor.received(fm, protectedBy);
            }
            default ->
                    throw new HandshakeFailureAlert("Invalid/unsupported message payloadType (" + messageType + ")");
        }

    }

}

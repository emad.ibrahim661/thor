/*
 * Copyright © 2019, 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Agent15, an implementation of TLS 1.3 in Java.
 *
 * Agent15 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Agent15 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package tech.lp2p.tls;

import java.nio.ByteBuffer;
import java.security.SecureRandom;
import java.util.Arrays;


public record ServerHello(byte[] random, CipherSuite cipherSuite, Extension[] extensions,
                          byte[] raw) implements HandshakeMessage {

    private static final int MINIMAL_MESSAGE_LENGTH = 1 + 3 + 2 + 32 + 1 + 2 + 1 + 2;
    private static final SecureRandom secureRandom = new SecureRandom();


    static ServerHello createServerHello(CipherSuite cipher, Extension[] extensions) {
        byte[] random = new byte[32];
        secureRandom.nextBytes(random);


        int extensionsSize = Arrays.stream(extensions).mapToInt(extension -> extension.getBytes().length).sum();
        byte[] raw = new byte[1 + 3 + 2 + 32 + 1 + 2 + 1 + 2 + extensionsSize];
        ByteBuffer buffer = ByteBuffer.wrap(raw);
        // https://tools.ietf.org/html/rfc8446#section-4
        // "uint24 length;             /* remaining bytes in message */"
        buffer.putInt((raw.length - 4) | 0x02000000);
        buffer.putShort((short) 0x0303);
        buffer.put(random);
        buffer.put((byte) 0);
        buffer.putShort(cipher.value);
        buffer.put((byte) 0);
        buffer.putShort((short) extensionsSize);
        Arrays.stream(extensions).forEach(extension -> buffer.put(extension.getBytes()));

        return new ServerHello(random, cipher, extensions, raw);
    }

    public static ServerHello parse(ByteBuffer buffer, int length) throws ErrorAlert {
        if (buffer.remaining() < MINIMAL_MESSAGE_LENGTH) {
            throw new DecodeErrorException("Message too short");
        }
        buffer.getInt();  // Skip message payloadType and 3 bytes length

        int versionHigh = buffer.get();
        int versionLow = buffer.get();
        if (versionHigh != 3 || versionLow != 3)
            throw new IllegalParameterAlert("Invalid version number (should be 0x0303)");

        byte[] random = new byte[32];
        buffer.get(random);


        int sessionIdLength = buffer.get() & 0xff;
        if (sessionIdLength > 32) {
            throw new DecodeErrorException("session id length exceeds 32");
        }
        byte[] legacySessionIdEcho = new byte[sessionIdLength];
        buffer.get(legacySessionIdEcho);   // must match, see 4.1.3

        short cipherSuiteCode = buffer.getShort();

        // https://tools.ietf.org/html/rfc8446#section-4.1.2
        // "If the list contains cipher suites that the server does not recognize, support, or wish to use,
        // the server MUST ignore those cipher suites and process the remaining ones as usual."
        CipherSuite cipherSuite = CipherSuite.get(cipherSuiteCode);

        if (cipherSuite == null) {
            throw new DecodeErrorException("Unknown cipher suite (" + cipherSuiteCode + ")");
        }

        int legacyCompressionMethod = buffer.get();
        if (legacyCompressionMethod != 0) {
            // https://www.davidwong.fr/tls13/#section-4.1.3
            // "legacy_compression_method: A single byte which MUST have the value 0."
            throw new DecodeErrorException("Legacy compression method must have the value 0");
        }

        Extension[] extensions = HandshakeMessage.parseExtensions(buffer, HandshakeType.server_hello);

        // Update state.
        byte[] raw = new byte[length];
        buffer.rewind();
        buffer.get(raw);

        return new ServerHello(random, cipherSuite, extensions, raw);
    }

    @Override
    public HandshakeType getType() {
        return HandshakeType.server_hello;
    }

    @Override
    public byte[] getBytes() {
        return raw;
    }


}

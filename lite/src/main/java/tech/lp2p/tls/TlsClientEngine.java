/*
 * Copyright © 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Agent15, an implementation of TLS 1.3 in Java.
 *
 * Agent15 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Agent15 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package tech.lp2p.tls;

import static tech.lp2p.tls.SignatureScheme.ecdsa_secp256r1_sha256;
import static tech.lp2p.tls.SignatureScheme.rsa_pss_rsae_sha256;
import static tech.lp2p.tls.SignatureScheme.rsa_pss_rsae_sha384;
import static tech.lp2p.tls.SignatureScheme.rsa_pss_rsae_sha512;

import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.net.ssl.X509TrustManager;
import javax.security.auth.x500.X500Principal;


public final class TlsClientEngine extends TlsEngine implements ClientMessageProcessor {

    private static final X500Principal[] PRINCIPALS_EMPTY = new X500Principal[0];

    private static final List<SignatureScheme> AVAILABLE_SIGNATURES = List.of(
            rsa_pss_rsae_sha256,
            rsa_pss_rsae_sha384,
            rsa_pss_rsae_sha512,
            ecdsa_secp256r1_sha256);
    private final ClientMessageSender sender;
    private final TlsStatusEventHandler statusHandler;
    private final List<CipherSuite> supportedCiphers;
    private final List<Extension> requestedExtensions;
    private final String serverName;
    private CipherSuite selectedCipher;
    private Extension[] sentExtensions;
    private Status status = Status.Initial;
    private TranscriptHash transcriptHash;
    private SignatureScheme[] supportedSignatures;
    private HostnameVerifier hostnameVerifier;
    private boolean pskAccepted = false;
    private boolean clientAuthRequested;
    private X500Principal[] clientCertificateAuthorities;
    private Function<X500Principal[], CertificateWithPrivateKey> clientCertificateSelector;
    private SignatureScheme[] serverSupportedSignatureSchemes;

    public TlsClientEngine(String serverName,
                           X509TrustManager customTrustManager,
                           List<CipherSuite> supportedCiphers,
                           List<Extension> requestedExtensions,
                           ClientMessageSender clientMessageSender,
                           TlsStatusEventHandler tlsStatusHandler) {
        super(customTrustManager);
        this.serverName = serverName;
        this.sender = clientMessageSender;
        this.statusHandler = tlsStatusHandler;
        this.supportedCiphers = supportedCiphers;
        this.requestedExtensions = requestedExtensions;
        hostnameVerifier = new DefaultHostnameVerifier();
        clientCertificateSelector = l -> null;
    }

    private static boolean certificateSupportsSignature(X509Certificate cert, SignatureScheme signatureScheme) {
        String certSignAlg = cert.getSigAlgName();
        if (certSignAlg.toLowerCase().contains("withrsa")) {
            return List.of(rsa_pss_rsae_sha256, rsa_pss_rsae_sha384).contains(signatureScheme);
        } else if (certSignAlg.toLowerCase().contains("withecdsa")) {
            return ecdsa_secp256r1_sha256 == signatureScheme;
        } else {
            return false;
        }
    }

    public void startHandshake() throws BadRecordMacAlert {
        SignatureScheme[] signatureSchemes = new SignatureScheme[]{
                rsa_pss_rsae_sha256, ecdsa_secp256r1_sha256
        };
        startHandshake(signatureSchemes);
    }

    private void startHandshake(SignatureScheme[] signatureSchemes) throws BadRecordMacAlert {
        if (Arrays.stream(signatureSchemes).anyMatch(scheme -> !AVAILABLE_SIGNATURES.contains(scheme))) {
            // Remove available leaves the ones that are not available (cannot be supported)
            List<SignatureScheme> unsupportedSignatures = Arrays.asList(signatureSchemes);
            unsupportedSignatures.removeAll(AVAILABLE_SIGNATURES);
            throw new IllegalArgumentException("Unsupported signature scheme(s): " + unsupportedSignatures);
        }

        supportedSignatures = signatureSchemes;
        generateKeys(NamedGroup.secp256r1);
        if (serverName == null || supportedCiphers.isEmpty()) {
            throw new IllegalStateException("not all mandatory properties are set");
        }

        transcriptHash = new TranscriptHash(32);
        state = new TlsState(transcriptHash);

        ClientHello clientHello = ClientHello.createClientHello(serverName, publicKey,
                false, supportedCiphers, supportedSignatures,
                NamedGroup.secp256r1, requestedExtensions, ClientHello.PskKeyEstablishmentMode.PSKwithDHE);
        sentExtensions = clientHello.extensions();
        sender.send(clientHello);
        status = Status.ClientHelloSent;

        transcriptHash.record(clientHello);
        state.setOwnKey(privateKey);
        state.computeEarlyTrafficSecret();

    }

    /**
     * Updates the (handshake) state with a received Server Hello message.
     */
    @Override
    public void received(ServerHello serverHello) throws MissingExtensionAlert, IllegalParameterAlert, BadRecordMacAlert {
        boolean containsSupportedVersionExt = Arrays.stream(serverHello.extensions()).anyMatch(ext -> ext instanceof SupportedVersionsExtension);
        boolean containsKeyExt = Arrays.stream(serverHello.extensions()).anyMatch(ext -> ext instanceof PreSharedKeyExtension || ext instanceof KeyShareExtension);
        // https://tools.ietf.org/html/rfc8446#section-4.1.3
        // "All TLS 1.3 ServerHello messages MUST contain the "supported_versions" extension.
        // Current ServerHello messages additionally contain either the "pre_shared_key" extension or the "key_share"
        // extension, or both (when using a PSK with (EC)DHE key establishment)."
        if (!containsSupportedVersionExt || !containsKeyExt) {
            throw new MissingExtensionAlert();
        }

        // https://tools.ietf.org/html/rfc8446#section-4.2.1
        // "A server which negotiates TLS 1.3 MUST respond by sending a "supported_versions" extension containing the selected version value (0x0304)."
        Optional<Short> tlsVersionOpt = Arrays.stream(serverHello.extensions())
                .filter(extension -> extension instanceof SupportedVersionsExtension)
                .map(extension -> ((SupportedVersionsExtension) extension).tlsVersion())
                .findFirst();
        if (tlsVersionOpt.isPresent()) {
            short tlsVersion = tlsVersionOpt.get();
            if (tlsVersion != 0x0304) {
                throw new IllegalParameterAlert("invalid tls version");
            }
        } else {
            throw new IllegalParameterAlert("invalid tls version");
        }

        // https://tools.ietf.org/html/rfc8446#section-4.2
        // "If an implementation receives an extension which it recognizes and which is not specified for the message in
        // which it appears, it MUST abort the handshake with an "illegal_parameter" alert."
        if (Arrays.stream(serverHello.extensions())
                .anyMatch(ext -> !(ext instanceof SupportedVersionsExtension) &&
                        !(ext instanceof PreSharedKeyExtension) &&
                        !(ext instanceof KeyShareExtension))) {
            throw new IllegalParameterAlert("illegal extension in server hello");
        }

        Optional<KeyShareExtension.KeyShareEntry> keyShare = Arrays.stream(serverHello.extensions())
                .filter(extension -> extension instanceof KeyShareExtension)
                // In the context of a server hello, the key share extension contains exactly one key share entry
                .map(extension -> ((KeyShareExtension) extension).keyShareEntries()[0])
                .findFirst();

        Optional<Extension> preSharedKey = Arrays.stream(serverHello.extensions())
                .filter(extension -> extension instanceof ServerPreSharedKeyExtension)
                .findFirst();

        // https://tools.ietf.org/html/rfc8446#section-4.1.3
        // "ServerHello messages additionally contain either the "pre_shared_key" extension or the "key_share" extension,
        // or both (when using a PSK with (EC)DHE key establishment)."
        //noinspection SimplifyOptionalCallChains
        if (!keyShare.isPresent() && !preSharedKey.isPresent()) {
            throw new MissingExtensionAlert(" either the pre_shared_key extension or the key_share extension must be present");
        }

        if (preSharedKey.isPresent()) {
            // https://tools.ietf.org/html/rfc8446#section-4.2.11
            // "In order to accept PSK key establishment, the server sends a "pre_shared_key" extension indicating the selected identity."
            pskAccepted = true;
        }

        if (!supportedCiphers.contains(serverHello.cipherSuite())) {
            // https://tools.ietf.org/html/rfc8446#section-4.1.3
            // "A client which receives a cipher suite that was not offered MUST abort the handshake with an "illegal_parameter" alert."
            throw new IllegalParameterAlert("cipher suite does not match");
        }
        selectedCipher = serverHello.cipherSuite();

        if (preSharedKey.isPresent()) {
            state.setPskSelected();
        } else {
            state.setNoPskSelected();
        }
        if (keyShare.isPresent()) {
            state.setPeerKey(keyShare.get().key());
            state.computeSharedSecret();
        }
        transcriptHash.record(serverHello);
        state.computeHandshakeSecrets();
        status = Status.ServerHelloReceived;
        statusHandler.handshakeSecretsKnown();
    }

    @Override
    public void received(EncryptedExtensions encryptedExtensions, ProtectionKeysType protectedBy) throws ErrorAlert {
        if (protectedBy != ProtectionKeysType.Handshake) {
            throw new UnexpectedMessageAlert("incorrect protection level");
        }
        if (status != Status.ServerHelloReceived) {
            // https://tools.ietf.org/html/rfc8446#section-4.3.1
            // "the server MUST send the EncryptedExtensions message immediately after the ServerHello message"
            throw new UnexpectedMessageAlert("unexpected encrypted extensions message");
        }

        //noinspection SimplifyStreamApiCallChains
        List<?> clientExtensionTypes = Arrays.stream(sentExtensions)
                .map(Extension::getClass).collect(Collectors.toList());
        boolean allClientResponses = Arrays.stream(encryptedExtensions.extensions())
                .filter(ext -> !(ext instanceof UnknownExtension))
                .allMatch(ext -> clientExtensionTypes.contains(ext.getClass()));
        if (!allClientResponses) {
            // https://tools.ietf.org/html/rfc8446#section-4.2
            // "Implementations MUST NOT send extension responses if the remote endpoint did not send the corresponding
            // extension requests, with the exception of the "cookie" extension in the HelloRetryRequest. Upon receiving
            // such an extension, an endpoint MUST abort the handshake with an "unsupported_extension" alert."
            throw new UnsupportedExtensionAlert("extension response to missing request");
        }

        int uniqueExtensions = Arrays.stream(encryptedExtensions.extensions())
                .map(Extension::getClass)
                .collect(Collectors.toSet())
                .size();
        if (uniqueExtensions != encryptedExtensions.extensions().length) {
            // "There MUST NOT be more than one extension of the same payloadType in a given extension block."
            throw new UnsupportedExtensionAlert("duplicate extensions not allowed");
        }

        transcriptHash.record(encryptedExtensions);
        status = Status.EncryptedExtensionsReceived;
        statusHandler.extensionsReceived(encryptedExtensions.extensions());
    }

    @Override
    public void received(CertificateMessage certificateMessage, ProtectionKeysType protectedBy) throws ErrorAlert {
        if (protectedBy != ProtectionKeysType.Handshake) {
            throw new UnexpectedMessageAlert("incorrect protection level");
        }
        if (status != Status.EncryptedExtensionsReceived && status != Status.CertificateRequestReceived) {
            // https://tools.ietf.org/html/rfc8446#section-4.4
            // "TLS generally uses a common set of messages for authentication, key confirmation, and handshake
            //   integrity: Certificate, CertificateVerify, and Finished.  (...)  These three messages are always
            //   sent as the last messages in their handshake flight."
            throw new UnexpectedMessageAlert("unexpected certificate message");
        }

        if (certificateMessage.requestContext().length > 0) {
            // https://tools.ietf.org/html/rfc8446#section-4.4.2
            // "If this message is in response to a CertificateRequest, the value of certificate_request_context in that
            // message. Otherwise (in the case of server authentication), this field SHALL be zero length."
            throw new IllegalParameterAlert("certificate request context should be zero length");
        }
        if (certificateMessage.getEndEntityCertificate() == null) {
            throw new IllegalParameterAlert("missing certificate");
        }

        remoteCertificate = certificateMessage.getEndEntityCertificate();
        remoteCertificateChain = certificateMessage.certificateChain();
        transcriptHash.recordServer(certificateMessage);
        status = Status.CertificateReceived;
    }

    @Override
    public void received(CertificateVerifyMessage certificateVerifyMessage, ProtectionKeysType protectedBy) throws ErrorAlert {
        if (protectedBy != ProtectionKeysType.Handshake) {
            throw new UnexpectedMessageAlert("incorrect protection level");
        }
        if (status != Status.CertificateReceived) {
            // https://tools.ietf.org/html/rfc8446#section-4.4.3
            // "When sent, this message MUST appear immediately after the Certificate message and immediately prior to
            // the Finished message."
            throw new UnexpectedMessageAlert("unexpected certificate verify message");
        }

        SignatureScheme signatureScheme = certificateVerifyMessage.signatureScheme();
        if (!Arrays.asList(supportedSignatures).contains(signatureScheme)) {
            // https://tools.ietf.org/html/rfc8446#section-4.4.3
            // "If the CertificateVerify message is sent by a server, the signature algorithm MUST be one offered in
            // the client's "signature_algorithms" extension"
            throw new IllegalParameterAlert("signature scheme does not match");
        }

        byte[] signature = certificateVerifyMessage.signature();
        if (!verifySignature(signature, signatureScheme, remoteCertificate,
                transcriptHash.getServerHash(HandshakeType.certificate), false)) {
            throw new DecryptErrorAlert("signature verification fails");
        }

        // Now the certificate signature has been validated, check the certificate validity
        checkCertificateValidity(remoteCertificateChain, true);
        if (!hostnameVerifier.verify(serverName, remoteCertificate)) {
            throw new CertificateUnknownAlert("servername does not match");
        }

        transcriptHash.recordServer(certificateVerifyMessage);
        status = Status.CertificateVerifyReceived;
    }

    @Override
    public void received(FinishedMessage finishedMessage, ProtectionKeysType protectedBy) throws ErrorAlert {
        if (protectedBy != ProtectionKeysType.Handshake) {
            throw new UnexpectedMessageAlert("incorrect protection level");
        }
        Status expectedStatus;
        if (pskAccepted) {
            expectedStatus = Status.EncryptedExtensionsReceived;
        } else {
            expectedStatus = Status.CertificateVerifyReceived;
        }
        if (status != expectedStatus) {
            throw new UnexpectedMessageAlert("unexpected finished message");
        }

        transcriptHash.recordServer(finishedMessage);

        // https://tools.ietf.org/html/rfc8446#section-4.4
        // "   | Mode      | Handshake Context       | Base Key                    |
        //     +-----------+-------------------------+-----------------------------+
        //     | Server    | ClientHello ... later   | server_handshake_traffic_   |
        //     |           | of EncryptedExtensions/ | secret                      |
        //     |           | CertificateRequest      |                             |"
        // https://datatracker.ietf.org/doc/html/rfc8446#section-4.4.4
        // "The verify_data value is computed as follows:
        //   verify_data = HMAC(finished_key, Transcript-Hash(Handshake Context, Certificate*, CertificateVerify*))
        //      * Only included if present."
        byte[] serverHmac = computeFinishedVerifyData(transcriptHash.getServerHash(HandshakeType.certificate_verify), state.getServerHandshakeTrafficSecret());
        // https://tools.ietf.org/html/rfc8446#section-4.4
        // "Recipients of Finished messages MUST verify that the contents are correct and if incorrect MUST terminate the connection with a "decrypt_error" alert."
        if (!Arrays.equals(finishedMessage.verifyData(), serverHmac)) {
            throw new DecryptErrorAlert("incorrect finished message");
        }

        if (clientAuthRequested) {
            sendClientAuth();
        }

        // https://tools.ietf.org/html/rfc8446#section-4.4
        // "   | Mode      | Handshake Context       | Base Key                    |
        //     | Client    | ClientHello ... later   | client_handshake_traffic_   |
        //     |           | of server               | secret                      |
        //     |           | Finished/EndOfEarlyData |                             |"
        // https://datatracker.ietf.org/doc/html/rfc8446#section-4.4.4
        // "The verify_data value is computed as follows:
        //   verify_data = HMAC(finished_key, Transcript-Hash(Handshake Context, Certificate*, CertificateVerify*))
        //      * Only included if present."
        byte[] clientHmac = computeFinishedVerifyData(transcriptHash.getClientHash(HandshakeType.certificate_verify), state.getClientHandshakeTrafficSecret());
        FinishedMessage clientFinished = FinishedMessage.createFinishedMessage(clientHmac);
        sender.send(clientFinished);

        transcriptHash.recordClient(clientFinished);
        state.computeApplicationSecrets();
        status = Status.Finished;
        statusHandler.handshakeFinished();
    }

    @Override
    public void received(CertificateRequestMessage certificateRequestMessage,
                         ProtectionKeysType protectedBy) throws ErrorAlert {
        if (protectedBy != ProtectionKeysType.Handshake) {
            throw new UnexpectedMessageAlert("incorrect protection level");
        }
        if (status != Status.EncryptedExtensionsReceived) {
            throw new UnexpectedMessageAlert("unexpected certificate request message");
        }

        serverSupportedSignatureSchemes = Arrays.stream(certificateRequestMessage.extensions())
                .filter(extension -> extension instanceof SignatureAlgorithmsExtension)
                .findFirst()
                .map(extension -> ((SignatureAlgorithmsExtension) extension).algorithms())
                // https://datatracker.ietf.org/doc/html/rfc8446#section-4.3.2
                // "The "signature_algorithms" extension MUST be specified..."
                .orElseThrow(MissingExtensionAlert::new);

        transcriptHash.record(certificateRequestMessage);

        clientCertificateAuthorities = Arrays.stream(certificateRequestMessage.extensions())
                .filter(extension -> extension instanceof CertificateAuthoritiesExtension)
                .findFirst()
                .map(extension -> ((CertificateAuthoritiesExtension) extension).authorities())
                .orElse(PRINCIPALS_EMPTY);
        clientAuthRequested = true;

        status = Status.CertificateRequestReceived;
    }

    private void sendClientAuth() throws ErrorAlert {
        CertificateWithPrivateKey certificateWithKey = clientCertificateSelector.apply(clientCertificateAuthorities);

        // Send certificate message (with possible null value for client certificate)
        CertificateMessage certificateMessage =
                CertificateMessage.createCertificateMessage(
                        certificateWithKey != null ? certificateWithKey.certificate() : null);
        sender.send(certificateMessage);
        transcriptHash.recordClient(certificateMessage);

        // When certificate is sent, also send a certificate verify message
        if (certificateWithKey != null) {
            SignatureScheme selectedSignatureScheme = Arrays.stream(serverSupportedSignatureSchemes)
                    .filter(Arrays.asList(supportedSignatures)::contains)
                    .filter(scheme -> certificateSupportsSignature(certificateWithKey.certificate(), scheme))
                    .findFirst()
                    .orElseThrow(() -> new HandshakeFailureAlert("failed to negotiate signature scheme"));

            PrivateKey privateKey = certificateWithKey.privateKey();
            byte[] hash = transcriptHash.getClientHash(HandshakeType.certificate);
            byte[] signature = computeSignature(hash, privateKey, selectedSignatureScheme, true);
            CertificateVerifyMessage certificateVerify =
                    CertificateVerifyMessage.createCertificateVerifyMessage(
                            selectedSignatureScheme, signature);
            sender.send(certificateVerify);
            transcriptHash.recordClient(certificateVerify);
        }
    }


    public CipherSuite getSelectedCipher() {
        if (selectedCipher != null) {
            return selectedCipher;
        } else {
            throw new IllegalStateException("No (valid) server hello received yet");
        }
    }

    public void setHostnameVerifier(HostnameVerifier hostnameVerifier) {
        if (hostnameVerifier != null) {
            this.hostnameVerifier = hostnameVerifier;
        }
    }

    public void setClientCertificateCallback(Function<X500Principal[], CertificateWithPrivateKey> callback) {
        clientCertificateSelector = callback;
    }

}

/*
 * Copyright © 2019, 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Agent15, an implementation of TLS 1.3 in Java.
 *
 * Agent15 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Agent15 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package tech.lp2p.tls;

import org.jetbrains.annotations.NotNull;

import java.nio.ByteBuffer;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import tech.lp2p.utils.Utils;


public record ClientHello(byte[] data, int pskExtensionStartPosition,
                          byte[] clientRandom, Extension[] extensions, CipherSuite[] cipherSuites
) implements HandshakeMessage {

    private static final int MAX_CLIENT_HELLO_SIZE = 3000;
    private static final int MINIMAL_MESSAGE_LENGTH = 1 + 3 + 2 + 32 + 1 + 2 + 2 + 2 + 2;
    private static final Random random = new Random();
    private static final SecureRandom secureRandom = new SecureRandom();


    /**
     * Parses a ClientHello message from a byte stream.
     */
    public static ClientHello parse(ByteBuffer buffer, ExtensionParser customExtensionParser) throws ErrorAlert {
        int startPosition = buffer.position();

        if (buffer.remaining() < 4) {
            throw new DecodeErrorException("message underflow");
        }
        if (buffer.remaining() < MINIMAL_MESSAGE_LENGTH) {
            throw new DecodeErrorException("message underflow");
        }

        int messageType = buffer.get();
        if (messageType != HandshakeType.client_hello.value) {
            throw new RuntimeException();  // Programming error
        }
        int length = ((buffer.get() & 0xff) << 16) | ((buffer.get() & 0xff) << 8) | (buffer.get() & 0xff);
        if (buffer.remaining() < length) {
            throw new DecodeErrorException("message underflow");
        }

        int legacyVersion = buffer.getShort();
        if (legacyVersion != 0x0303) {
            throw new DecodeErrorException("legacy version must be 0303");
        }
        List<CipherSuite> cipherSuites = new ArrayList<>();
        byte[] clientRandom = new byte[32];
        buffer.get(clientRandom);

        int sessionIdLength = buffer.get();
        if (sessionIdLength > 0) {
            buffer.get(new byte[sessionIdLength]);
        }

        int cipherSuitesLength = buffer.getShort();
        for (int i = 0; i < cipherSuitesLength; i += 2) {
            short cipherSuiteValue = buffer.getShort();
            // https://tools.ietf.org/html/rfc8446#section-4.1.2
            // "If the list contains cipher suites that the server does not recognize, support, or wish to use,
            // the server MUST ignore those cipher suites and process the remaining ones as usual."
            CipherSuite cipherSuite = CipherSuite.get(cipherSuiteValue);
            if (cipherSuite != null) cipherSuites.add(cipherSuite);
        }

        int legacyCompressionMethodsLength = buffer.get();
        int legacyCompressionMethod = buffer.get();
        if (legacyCompressionMethodsLength != 1 || legacyCompressionMethod != 0) {
            throw new IllegalParameterAlert("Invalid legacy compression method");
        }
        int pskExtensionStartPosition;
        int extensionStart = buffer.position();
        Extension[] extensions = HandshakeMessage.parseExtensions(buffer, HandshakeType.client_hello, customExtensionParser);
        if (Arrays.stream(extensions).anyMatch(ext -> ext instanceof PreSharedKeyExtension)) {
            buffer.position(extensionStart);
            pskExtensionStartPosition = HandshakeMessage.findPositionLastExtension(buffer);
            // https://datatracker.ietf.org/doc/html/rfc8446#section-4.2.11
            // "The "pre_shared_key" extension MUST be the last extension in the ClientHello (...). Servers MUST check
            //  that it is the last extension and otherwise fail the handshake with an "illegal_parameter" alert."
            if (!(extensions[extensions.length - 1] instanceof PreSharedKeyExtension)) {
                throw new IllegalParameterAlert("pre_shared_key extension MUST be the last extension in the ClientHello");
            }
        } else {
            pskExtensionStartPosition = -1;
        }

        byte[] data = new byte[buffer.position() - startPosition];
        buffer.position(startPosition);
        buffer.get(data);

        CipherSuite[] cipherSuitesArray = new CipherSuite[cipherSuites.size()];
        return new ClientHello(data, pskExtensionStartPosition, clientRandom, extensions,
                cipherSuites.toArray(cipherSuitesArray));
    }

    /**
     *
     */
    public static ClientHello createClientHello(String serverName, PublicKey publicKey,
                                                boolean compatibilityMode,
                                                List<CipherSuite> supportedCiphers,
                                                SignatureScheme[] supportedSignatures,
                                                NamedGroup ecCurve, List<Extension> extraExtensions,
                                                PskKeyEstablishmentMode pskKeyEstablishmentMode) {

        ByteBuffer buffer = ByteBuffer.allocate(MAX_CLIENT_HELLO_SIZE);

        // HandshakeType client_hello(1),
        buffer.put((byte) 1);

        // Reserve 3 bytes for length
        byte[] length = new byte[3];
        buffer.put(length);

        // client version
        buffer.put((byte) 0x03);
        buffer.put((byte) 0x03);

        // client random 32 bytes
        byte[] clientRandom = new byte[32];
        secureRandom.nextBytes(clientRandom);
        buffer.put(clientRandom);

        byte[] sessionId;
        if (compatibilityMode) {
            sessionId = new byte[32];
            random.nextBytes(sessionId);
        } else {
            sessionId = Utils.BYTES_EMPTY;
        }
        buffer.put((byte) sessionId.length);
        if (sessionId.length > 0)
            buffer.put(sessionId);

        buffer.putShort((short) (supportedCiphers.size() * 2));
        for (CipherSuite cipher : supportedCiphers) {
            buffer.putShort(cipher.value);
        }

        // Compression
        // "For every TLS 1.3 ClientHello, this vector MUST contain exactly one byte, set to zero, which corresponds to
        // the "null" compression method in prior versions of TLS. "
        buffer.put(new byte[]{
                (byte) 0x01, (byte) 0x00
        });

        Extension[] defaultExtensions = new Extension[]{
                new ServerNameExtension(serverName),
                SupportedVersionsExtension.createSupportedVersionsExtension(HandshakeType.client_hello),
                SupportedGroupsExtension.createSupportedGroupsExtension(ecCurve),
                new SignatureAlgorithmsExtension(supportedSignatures),
                KeyShareExtension.create(publicKey, ecCurve, HandshakeType.client_hello),
        };

        List<Extension> extensions = new ArrayList<>(List.of(defaultExtensions));
        if (pskKeyEstablishmentMode != PskKeyEstablishmentMode.none) {
            extensions.add(createPskKeyExchangeModesExtension(pskKeyEstablishmentMode));
        }
        extensions.addAll(extraExtensions);


        int extensionsLength = extensions.stream().mapToInt(ext -> ext.getBytes().length).sum();
        buffer.putShort((short) extensionsLength);
        int pskExtensionStartPosition = -1;
        for (Extension extension : extensions) {
            buffer.put(extension.getBytes());
        }

        buffer.limit(buffer.position());
        int clientHelloLength = buffer.position() - 4;
        buffer.putShort(2, (short) clientHelloLength);

        byte[] data = new byte[clientHelloLength + 4];
        buffer.rewind();
        buffer.get(data);

        CipherSuite[] cipherSuitesArray = new CipherSuite[supportedCiphers.size()];
        Extension[] extensionsArray = new Extension[extensions.size()];

        return new ClientHello(data, pskExtensionStartPosition, clientRandom,
                extensions.toArray(extensionsArray),
                supportedCiphers.toArray(cipherSuitesArray));
    }

    private static PskKeyExchangeModesExtension createPskKeyExchangeModesExtension(PskKeyEstablishmentMode pskKeyEstablishmentMode) {
        return switch (pskKeyEstablishmentMode) {
            case PSKonly -> PskKeyExchangeModesExtension.createPskKeyExchangeModesExtension(
                    TlsConstants.PskKeyExchangeMode.psk_ke);
            case PSKwithDHE -> PskKeyExchangeModesExtension.createPskKeyExchangeModesExtension(
                    TlsConstants.PskKeyExchangeMode.psk_dhe_ke);
            case both -> PskKeyExchangeModesExtension.createPskKeyExchangeModesExtension(
                    TlsConstants.PskKeyExchangeMode.psk_ke, TlsConstants.PskKeyExchangeMode.psk_dhe_ke);
            default -> throw new IllegalArgumentException();
        };
    }

    @Override
    public HandshakeType getType() {
        return HandshakeType.client_hello;
    }

    @Override
    public byte[] getBytes() {
        return data;
    }


    @NotNull
    @Override
    public String toString() {
        return "ClientHello["
                + Arrays.stream(cipherSuites).map(Enum::toString).collect(Collectors.joining(",")) + "|"
                + Arrays.stream(extensions).map(Object::toString).collect(Collectors.joining(","))
                + "]";
    }

    public enum PskKeyEstablishmentMode {
        none,
        PSKonly,
        PSKwithDHE,
        both
    }

}

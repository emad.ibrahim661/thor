/*
 * Copyright © 2019, 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Agent15, an implementation of TLS 1.3 in Java.
 *
 * Agent15 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Agent15 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package tech.lp2p.tls;

import static tech.lp2p.tls.NamedGroup.secp256r1;
import static tech.lp2p.tls.NamedGroup.x25519;
import static tech.lp2p.tls.NamedGroup.x448;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.security.AlgorithmParameters;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.interfaces.ECPublicKey;
import java.security.interfaces.XECPublicKey;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.ECParameterSpec;
import java.security.spec.ECPoint;
import java.security.spec.ECPublicKeySpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.security.spec.NamedParameterSpec;
import java.security.spec.XECPublicKeySpec;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;


/**
 * The TLS "key_share" extension contains the endpoint's cryptographic parameters.
 * See <a href="https://tools.ietf.org/html/rfc8446#section-4.2.8">...</a>
 */
public record KeyShareExtension(HandshakeType handshakeType,
                                KeyShareEntry[] keyShareEntries) implements Extension {

    private static final List<NamedGroup> supportedCurves = List.of(secp256r1, x25519, x448);
    private static final Map<NamedGroup, Integer> CURVE_KEY_LENGTHS = Map.of(
            secp256r1, 65,
            x25519, 32,
            x448, 56
    );


    public static KeyShareExtension create(PublicKey publicKey, NamedGroup ecCurve, HandshakeType handshakeType) {


        if (!supportedCurves.contains(ecCurve)) {
            throw new RuntimeException("Only curves supported: " + supportedCurves);
        }
        KeyShareEntry[] keyShareEntries = new KeyShareEntry[]{
                new BasicKeyShareEntry(ecCurve, publicKey)};
        return new KeyShareExtension(handshakeType, keyShareEntries);
    }

    private static KeyShareExtension createKeyShareExtension(ByteBuffer buffer, HandshakeType handshakeType) throws ErrorAlert {

        List<KeyShareEntry> keyShareEntries = new ArrayList<>();

        int extensionDataLength = Extension.parseExtensionHeader(
                buffer, TlsConstants.ExtensionType.key_share, 1);
        if (extensionDataLength < 2) {
            throw new DecodeErrorException("extension underflow");
        }

        if (handshakeType == HandshakeType.client_hello) {
            int keyShareEntriesSize = buffer.getShort();
            if (extensionDataLength != 2 + keyShareEntriesSize) {
                throw new DecodeErrorException("inconsistent length");
            }
            int remaining = keyShareEntriesSize;
            while (remaining > 0) {
                remaining -= parseKeyShareEntry(keyShareEntries, buffer);
            }
            if (remaining != 0) {
                throw new DecodeErrorException("inconsistent length");
            }
        } else if (handshakeType == HandshakeType.server_hello) {
            int remaining = extensionDataLength;
            remaining -= parseKeyShareEntry(keyShareEntries, buffer);
            if (remaining != 0) {
                throw new DecodeErrorException("inconsistent length");
            }
        } else {
            throw new IllegalArgumentException();
        }

        KeyShareEntry[] keyShareEntriesArray = new KeyShareEntry[keyShareEntries.size()];
        return new KeyShareExtension(handshakeType,
                keyShareEntries.toArray(keyShareEntriesArray));
    }

    /**
     * Assuming KeyShareServerHello:
     * "In a ServerHello message, the "extension_data" field of this extension contains a KeyShareServerHello value..."
     */
    public static KeyShareExtension create(ByteBuffer buffer, HandshakeType handshakeType) throws ErrorAlert {
        return KeyShareExtension.createKeyShareExtension(buffer, handshakeType);
    }

    private static short getCurveKeyLength(NamedGroup namedGroup) {
        Integer length = CURVE_KEY_LENGTHS.get(namedGroup);
        if (length == null) {
            throw new NullPointerException();
        }
        return length.shortValue();
    }

    private static ECPublicKey rawToEncodedECPublicKey(NamedGroup curveName, byte[] rawBytes) {
        try {
            KeyFactory kf = KeyFactory.getInstance("EC");
            byte[] x = Arrays.copyOfRange(rawBytes, 0, rawBytes.length / 2);
            byte[] y = Arrays.copyOfRange(rawBytes, rawBytes.length / 2, rawBytes.length);
            ECPoint w = new ECPoint(new BigInteger(1, x), new BigInteger(1, y));
            return (ECPublicKey) kf.generatePublic(new ECPublicKeySpec(w, ecParameterSpecForCurve(curveName.name())));
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Missing support for EC algorithm");
        } catch (InvalidKeySpecException e) {
            throw new RuntimeException("Inappropriate parameter specification");
        }
    }

    private static ECParameterSpec ecParameterSpecForCurve(String curveName) {
        try {
            AlgorithmParameters params = AlgorithmParameters.getInstance("EC");
            params.init(new ECGenParameterSpec(curveName));
            return params.getParameterSpec(ECParameterSpec.class);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Missing support for EC algorithm");
        } catch (InvalidParameterSpecException e) {
            throw new RuntimeException("Inappropriate parameter specification");
        }
    }

    private static PublicKey rawToEncodedXDHPublicKey(NamedGroup curve, byte[] keyData) {
        try {
            // Encoding is little endian, so reverse the bytes.
            reverse(keyData);
            BigInteger u = new BigInteger(keyData);
            KeyFactory kf = KeyFactory.getInstance("XDH");
            NamedParameterSpec paramSpec = new NamedParameterSpec(curve.name().toUpperCase());
            XECPublicKeySpec pubSpec = new XECPublicKeySpec(paramSpec, u);
            return kf.generatePublic(pubSpec);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Missing support for XDH algorithm");
        } catch (InvalidKeySpecException e) {
            throw new RuntimeException("Inappropriate parameter specification");
        }
    }

    private static void reverse(byte[] array) {
        if (array == null) {
            return;
        }
        int i = 0;
        int j = array.length - 1;
        byte tmp;
        while (j > i) {
            tmp = array[j];
            array[j] = array[i];
            array[i] = tmp;
            j--;
            i++;
        }
    }

    private static int parseKeyShareEntry(List<KeyShareEntry> keyShareEntries,
                                          ByteBuffer buffer) throws ErrorAlert {
        int startPosition = buffer.position();
        if (buffer.remaining() < 4) {
            throw new DecodeErrorException("extension underflow");
        }

        short namedGroupValue = buffer.getShort();
        NamedGroup namedGroup = NamedGroup.get(namedGroupValue);

        if (!supportedCurves.contains(namedGroup)) {
            throw new RuntimeException("Curve '" + namedGroup + "' not supported");
        }

        int keyLength = buffer.getShort();
        if (buffer.remaining() < keyLength) {
            throw new DecodeErrorException("extension underflow");
        }
        if (keyLength != getCurveKeyLength(namedGroup)) {
            throw new DecodeErrorException("Invalid " + namedGroup.name() + " key length: " + keyLength);
        }

        if (namedGroup == secp256r1) {
            int headerByte = buffer.get();
            if (headerByte == 4) {
                byte[] keyData = new byte[keyLength - 1];
                buffer.get(keyData);
                ECPublicKey ecPublicKey = rawToEncodedECPublicKey(namedGroup, keyData);
                keyShareEntries.add(new ECKeyShareEntry(namedGroup, ecPublicKey));
            } else {
                throw new DecodeErrorException("EC keys must be in legacy form");
            }
        } else if (namedGroup == x25519 || namedGroup == x448) {
            byte[] keyData = new byte[keyLength];
            buffer.get(keyData);
            PublicKey publicKey = rawToEncodedXDHPublicKey(namedGroup, keyData);
            keyShareEntries.add(new BasicKeyShareEntry(namedGroup, publicKey));
        }
        return buffer.position() - startPosition;
    }

    private static void writeAffine(ByteBuffer buffer, byte[] affine) {
        if (affine.length == 32) {
            buffer.put(affine);
        } else if (affine.length < 32) {
            for (int i = 0; i < 32 - affine.length; i++) {
                buffer.put((byte) 0);
            }
            buffer.put(affine, 0, affine.length);
        } else {
            // affine.length > 32
            for (int i = 0; i < affine.length - 32; i++) {
                if (affine[i] != 0) {
                    throw new RuntimeException("W Affine more then 32 bytes, leading bytes not 0 ");
                }
            }
            buffer.put(affine, affine.length - 32, 32);
        }
    }

    @Override
    public byte[] getBytes() {
        short keyShareEntryLength = (short) Arrays.stream(keyShareEntries)
                .map(KeyShareEntry::namedGroup)
                .mapToInt(CURVE_KEY_LENGTHS::get)
                .map(s -> 2 + 2 + s)  // Named Group: 2 bytes, key length: 2 bytes
                .sum();
        short extensionLength = keyShareEntryLength;
        if (handshakeType == HandshakeType.client_hello) {
            extensionLength += 2;
        }

        ByteBuffer buffer = ByteBuffer.allocate(4 + extensionLength);
        buffer.putShort(TlsConstants.ExtensionType.key_share.value);
        buffer.putShort(extensionLength);  // Extension data length (in bytes)

        if (handshakeType == HandshakeType.client_hello) {
            buffer.putShort(keyShareEntryLength);
        }

        for (KeyShareEntry keyShare : keyShareEntries) {
            buffer.putShort(keyShare.namedGroup().value);
            buffer.putShort(getCurveKeyLength(keyShare.namedGroup()));
            if (keyShare.namedGroup() == secp256r1) {
                // See https://tools.ietf.org/html/rfc8446#section-4.2.8.2, "For secp256r1, secp384r1, and secp521r1, ..."
                buffer.put((byte) 4);
                byte[] affineX = ((ECPublicKey) keyShare.key()).getW().getAffineX().toByteArray();
                writeAffine(buffer, affineX);
                byte[] affineY = ((ECPublicKey) keyShare.key()).getW().getAffineY().toByteArray();
                writeAffine(buffer, affineY);
            } else if (keyShare.namedGroup() == x25519 || keyShare.namedGroup() == x448) {

                Integer keyShareEntry = CURVE_KEY_LENGTHS.get(keyShare.namedGroup());
                Objects.requireNonNull(keyShareEntry);
                byte[] raw = ((XECPublicKey) keyShare.key()).getU().toByteArray();
                if (raw.length > keyShareEntry) {
                    throw new RuntimeException("Invalid " + keyShare.namedGroup() + " key length: " + raw.length);
                }
                if (raw.length < keyShareEntry) {
                    // Must pad with leading zeros, but as the encoding is little endian, it is easier to first reverse...
                    reverse(raw);
                    // ... and than pad with zeroes up to the required ledngth
                    raw = Arrays.copyOf(raw, keyShareEntry);
                } else {
                    // Encoding is little endian, so reverse the bytes.
                    reverse(raw);
                }
                buffer.put(raw);

            } else {
                throw new RuntimeException();
            }
        }

        return buffer.array();
    }

    public interface KeyShareEntry {
        NamedGroup namedGroup();

        PublicKey key();
    }

    private record BasicKeyShareEntry(NamedGroup namedGroup,
                                      PublicKey key) implements KeyShareEntry {
    }

    private record ECKeyShareEntry(NamedGroup namedGroup,
                                   ECPublicKey key) implements KeyShareEntry {
    }
}

package tech.lp2p.cert;

import java.io.IOException;

/**
 * ASN.1 UniversalString object - encodes UNICODE (ISO 10646) characters using 32-bit format. In Java we
 * have no way of representing this directly so we rely on byte arrays to carry these.
 */
public abstract class ASN1UniversalString extends ASN1Primitive implements ASN1String {

    private final byte[] contents;

    ASN1UniversalString(byte[] contents) {
        this.contents = contents;
    }

    static ASN1UniversalString createPrimitive(byte[] contents) {
        return new DERUniversalString(contents);
    }


    final boolean encodeConstructed() {
        return false;
    }

    final int encodedLength(boolean withTag) {
        return ASN1OutputStream.getLengthOfEncodingDL(withTag, contents.length);
    }

    final void encode(ASN1OutputStream out, boolean withTag) throws IOException {
        out.writeEncodingDL(withTag, BERTags.UNIVERSAL_STRING, contents);
    }

    final boolean asn1Equals(ASN1Primitive other) {
        if (!(other instanceof ASN1UniversalString that)) {
            return false;
        }

        return Arrays.areEqual(this.contents, that.contents);
    }

}

package tech.lp2p.cert;

import java.io.InputStream;

/**
 * Internal use stream that allows reading of a limited number of bytes from a wrapped stream.
 */
abstract class LimitedInputStream extends InputStream {
    final InputStream _in;
    private final int _limit;

    LimitedInputStream(InputStream in, int limit) {
        this._in = in;
        this._limit = limit;
    }

    int getLimit() {
        return _limit;
    }


}

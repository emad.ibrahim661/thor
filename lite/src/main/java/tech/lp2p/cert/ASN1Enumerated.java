package tech.lp2p.cert;

import java.io.IOException;

/**
 * Class representing the ASN.1 ENUMERATED payloadType.
 */
public final class ASN1Enumerated extends ASN1Primitive {
    private static final ASN1Enumerated[] cache = new ASN1Enumerated[12];

    private final byte[] contents;

    private ASN1Enumerated(byte[] contents) {
        if (ASN1Integer.isMalformed(contents)) {
            throw new IllegalArgumentException("malformed enumerated");
        }
        if (0 != (contents[0] & 0x80)) {
            throw new IllegalArgumentException("enumerated must be non-negative");
        }

        this.contents = Arrays.clone(contents);
    }

    static ASN1Enumerated createPrimitive(byte[] contents) {
        if (contents.length > 1) {
            return new ASN1Enumerated(contents);
        }

        if (contents.length == 0) {
            throw new IllegalArgumentException("ENUMERATED has zero length");
        }
        int value = contents[0] & 0xff;

        if (value >= cache.length) {
            return new ASN1Enumerated(contents);
        }

        ASN1Enumerated possibleMatch = cache[value];

        if (possibleMatch == null) {
            possibleMatch = cache[value] = new ASN1Enumerated(contents);
        }

        return possibleMatch;
    }

    boolean encodeConstructed() {
        return false;
    }

    int encodedLength(boolean withTag) {
        return ASN1OutputStream.getLengthOfEncodingDL(withTag, contents.length);
    }

    void encode(ASN1OutputStream out, boolean withTag) throws IOException {
        out.writeEncodingDL(withTag, BERTags.ENUMERATED, contents);
    }

    boolean asn1Equals(
            ASN1Primitive o) {
        if (!(o instanceof ASN1Enumerated other)) {
            return false;
        }

        return Arrays.areEqual(this.contents, other.contents);
    }
}

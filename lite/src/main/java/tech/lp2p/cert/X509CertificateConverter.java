package tech.lp2p.cert;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

/**
 * Converter for producing X509Certificate objects tied to a specific provider from X509CertificateHolder objects.
 */
public interface X509CertificateConverter {

    /**
     * Use the configured converter to produce a X509Certificate object from a X509CertificateHolder object.
     *
     * @param certHolder the holder to be converted
     * @return a X509Certificate object
     * @throws CertificateException if the conversion is unable to be made.
     */
    static X509Certificate getCertificate(X509CertificateHolder certHolder)
            throws CertificateException {
        try {
            CertificateFactory cFact = CertificateFactory.getInstance("X.509");

            return (X509Certificate) cFact.generateCertificate(new ByteArrayInputStream(certHolder.getEncoded()));
        } catch (IOException e) {
            throw new CertificateException("exception parsing certificate: " + e.getMessage(), e);
        }
    }
}
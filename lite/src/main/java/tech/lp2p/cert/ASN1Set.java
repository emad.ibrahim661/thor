package tech.lp2p.cert;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

/**
 * ASN.1 <code>SET</code> and <code>SET OF</code> constructs.
 * <p>
 * Note: This does not know which syntax the set is!
 * (The difference: ordering of SET elements or not ordering.)
 * </p><p>
 * DER form is always definite form length fields, while
 * BER support uses indefinite form.
 * </p><p>
 * The CER form support does not exist.
 * </p>
 * <h2>X.690</h2>
 * <h3>8: Basic encoding rules</h3>
 * <h4>8.11 Encoding of a set value </h4>
 * <b>8.11.1</b> The encoding of a set value shall be constructed
 * <p>
 * <b>8.11.2</b> The contents octets shall consist of the complete
 * encoding of a data value from each of the types listed in the
 * ASN.1 definition of the set payloadType, in an order chosen by the sender,
 * unless the payloadType was referenced with the keyword
 * <b>OPTIONAL</b> or the keyword <b>DEFAULT</b>.
 * </p><p>
 * <b>8.11.3</b> The encoding of a data value may, but need not,
 * be present for a payloadType which was referenced with the keyword
 * <b>OPTIONAL</b> or the keyword <b>DEFAULT</b>.
 * <blockquote>
 * NOTE &mdash; The order of data values in a set value is not significant,
 * and places no constraints on the order during transfer
 * </blockquote>
 * <h4>8.12 Encoding of a set-of value</h4>
 * <p>
 * <b>8.12.1</b> The encoding of a set-of value shall be constructed.
 * </p><p>
 * <b>8.12.2</b> The text of 8.10.2 applies:
 * <i>The contents octets shall consist of zero,
 * one or more complete encodings of data values from the payloadType listed in
 * the ASN.1 definition.</i>
 * </p><p>
 * <b>8.12.3</b> The order of data values need not be preserved by
 * the encoding and subsequent decoding.
 *
 * <h3>9: Canonical encoding rules</h3>
 * <h4>9.1 Length forms</h4>
 * If the encoding is constructed, it shall employ the indefinite-length form.
 * If the encoding is primitive, it shall include the fewest length octets necessary.
 * [Contrast with 8.1.3.2 b).]
 * <h4>9.3 Set components</h4>
 * The encodings of the component values of a set value shall
 * appear in an order determined by their tags as specified
 * in 8.6 of ITU-T Rec. X.680 | ISO/IEC 8824-1.
 * Additionally, for the purposes of determining the order in which
 * components are encoded when one or more component is an untagged
 * choice payloadType, each untagged choice payloadType is ordered as though it
 * has a tag equal to that of the smallest tag in that choice payloadType
 * or any untagged choice types nested within.
 *
 * <h3>10: Distinguished encoding rules</h3>
 * <h4>10.1 Length forms</h4>
 * The definite form of length encoding shall be used,
 * encoded in the minimum number of octets.
 * [Contrast with 8.1.3.2 b).]
 * <h4>10.3 Set components</h4>
 * The encodings of the component values of a set value shall appear
 * in an order determined by their tags as specified
 * in 8.6 of ITU-T Rec. X.680 | ISO/IEC 8824-1.
 * <blockquote>
 * NOTE &mdash; Where a component of the set is an untagged choice payloadType,
 * the location of that component in the ordering will depend on
 * the tag of the choice component being encoded.
 * </blockquote>
 *
 * <h3>11: Restrictions on BER employed by both CER and DER</h3>
 * <h4>11.5 Set and sequence components with default value </h4>
 * The encoding of a set value or sequence value shall not include
 * an encoding for any component value which is equal to
 * its default value.
 * <h4>11.6 Set-of components </h4>
 * <p>
 * The encodings of the component values of a set-of value
 * shall appear in ascending order, the encodings being compared
 * as octet strings with the shorter components being padded at
 * their trailing end with 0-octets.
 * <blockquote>
 * NOTE &mdash; The padding octets are for comparison purposes only
 * and do not appear in the encodings.
 * </blockquote>
 */
public abstract class ASN1Set extends ASN1Primitive {


    final ASN1Encodable[] elements;
    final boolean isSorted;

    ASN1Set() {
        this.elements = ASN1EncodableVector.EMPTY_ELEMENTS;
        this.isSorted = true;
    }

    /**
     * Create a SET containing one object
     *
     * @param element object to be added to the SET.
     */
    ASN1Set(ASN1Encodable element) {
        if (null == element) {
            throw new NullPointerException("'element' cannot be null");
        }

        this.elements = new ASN1Encodable[]{element};
        this.isSorted = true;
    }

    /**
     * Create a SET containing a vector of objects.
     *
     * @param elementVector a vector of objects to make up the SET.
     */
    ASN1Set(ASN1EncodableVector elementVector) {
        if (null == elementVector) {
            throw new NullPointerException("'elementVector' cannot be null");
        }

        ASN1Encodable[] tmp;
        tmp = elementVector.takeElements();

        this.elements = tmp;
        this.isSorted = tmp.length < 2;
    }

    /**
     * Create a SET containing an array of objects.
     *
     * @param elements an array of objects to make up the SET.
     */
    ASN1Set(ASN1Encodable[] elements) {
        if (Arrays.isNullOrContainsNull(elements)) {
            throw new NullPointerException("'elements' cannot be null, or contain null");
        }

        ASN1Encodable[] tmp = ASN1EncodableVector.cloneElements(elements);
        if (tmp.length >= 2) {
            sort(tmp);
        }

        this.elements = tmp;
        this.isSorted = true;
    }


    private static byte[] getDEREncoded(ASN1Encodable obj) {
        try {
            return obj.toASN1Primitive().getEncoded(ASN1Encoding.DER);
        } catch (IOException e) {
            throw new IllegalArgumentException("cannot encode object added to SET");
        }
    }

    /**
     * return true if a <= b (arrays are assumed padded with zeros).
     */
    private static boolean lessThanOrEqual(byte[] a, byte[] b) {
//        assert a.length >= 2 && b.length >= 2;

        /*
         * NOTE: Set elements in DER encodings are ordered first according to their tags (class and
         * number); the CONSTRUCTED bit is not part of the tag.
         *
         * For SET-OF, this is unimportant. All elements have the same tag and DER requires them to
         * either all be in constructed form or all in primitive form, according to that tag. The
         * elements are effectively ordered according to their content octets.
         *
         * For SET, the elements will have distinct tags, and each will be in constructed or
         * primitive form accordingly. Failing to ignore the CONSTRUCTED bit could therefore lead to
         * ordering inversions.
         */
        int a0 = a[0] & ~BERTags.CONSTRUCTED;
        int b0 = b[0] & ~BERTags.CONSTRUCTED;
        if (a0 != b0) {
            return a0 < b0;
        }

        int last = Math.min(a.length, b.length) - 1;
        for (int i = 1; i < last; ++i) {
            if (a[i] != b[i]) {
                return (a[i] & 0xFF) < (b[i] & 0xFF);
            }
        }
        return (a[last] & 0xFF) <= (b[last] & 0xFF);
    }

    private static void sort(ASN1Encodable[] t) {
        int count = t.length;
        if (count < 2) {
            return;
        }

        ASN1Encodable eh = t[0], ei = t[1];
        byte[] bh = getDEREncoded(eh), bi = getDEREncoded(ei);

        if (lessThanOrEqual(bi, bh)) {
            ASN1Encodable et = ei;
            ei = eh;
            eh = et;
            byte[] bt = bi;
            bi = bh;
            bh = bt;
        }

        for (int i = 2; i < count; ++i) {
            ASN1Encodable e2 = t[i];
            byte[] b2 = getDEREncoded(e2);

            if (lessThanOrEqual(bi, b2)) {
                t[i - 2] = eh;
                eh = ei;
                bh = bi;
                ei = e2;
                bi = b2;
                continue;
            }

            if (lessThanOrEqual(bh, b2)) {
                t[i - 2] = eh;
                eh = e2;
                bh = b2;
                continue;
            }

            int j = i - 1;
            while (--j > 0) {
                ASN1Encodable e1 = t[j - 1];
                byte[] b1 = getDEREncoded(e1);

                if (lessThanOrEqual(b1, b2)) {
                    break;
                }

                t[j] = e1;
            }

            t[j] = e2;
        }

        t[count - 2] = eh;
        t[count - 1] = ei;
    }


    /**
     * return the number of objects in this set.
     *
     * @return the number of objects in this set.
     */
    public int size() {
        return elements.length;
    }


    /**
     * Change current SET object to be encoded as {@link DERSet}.
     * This is part of Distinguished Encoding Rules form serialization.
     */
    ASN1Primitive toDERObject() {
        ASN1Encodable[] tmp;
        if (isSorted) {
            tmp = elements;
        } else {
            tmp = elements.clone();
            sort(tmp);
        }

        return new DERSet(tmp);
    }


    boolean asn1Equals(ASN1Primitive other) {
        if (!(other instanceof ASN1Set that)) {
            return false;
        }

        int count = this.size();
        if (that.size() != count) {
            return false;
        }

        DERSet dis = (DERSet) this.toDERObject();
        DERSet dat = (DERSet) that.toDERObject();

        for (int i = 0; i < count; ++i) {
            ASN1Primitive p1 = dis.elements[i].toASN1Primitive();
            ASN1Primitive p2 = dat.elements[i].toASN1Primitive();

            if (p1 != p2 && !p1.asn1Equals(p2)) {
                return false;
            }
        }

        return true;
    }

    boolean encodeConstructed() {
        return true;
    }

    @NotNull
    public String toString() {
        int count = size();
        if (0 == count) {
            return "[]";
        }

        StringBuilder sb = new StringBuilder();
        sb.append('[');
        for (int i = 0; ; ) {
            sb.append(elements[i]);
            if (++i >= count) {
                break;
            }
            sb.append(", ");
        }
        sb.append(']');
        return sb.toString();
    }


}

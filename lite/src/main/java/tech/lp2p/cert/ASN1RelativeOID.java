package tech.lp2p.cert;

import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;

public final class ASN1RelativeOID extends ASN1Primitive {
    private static final long LONG_LIMIT = (Long.MAX_VALUE >> 7) - 0x7F;
    private final String identifier;
    private final byte[] contents;

    private ASN1RelativeOID(byte[] contents) {
        StringBuilder objId = new StringBuilder();
        long value = 0;
        BigInteger bigValue = null;
        boolean first = true;

        for (int i = 0; i != contents.length; i++) {
            int b = contents[i] & 0xff;

            if (value <= LONG_LIMIT) {
                value += b & 0x7F;
                if ((b & 0x80) == 0) {
                    if (first) {
                        first = false;
                    } else {
                        objId.append('.');
                    }

                    objId.append(value);
                    value = 0;
                } else {
                    value <<= 7;
                }
            } else {
                if (bigValue == null) {
                    bigValue = BigInteger.valueOf(value);
                }
                bigValue = bigValue.or(BigInteger.valueOf(b & 0x7F));
                if ((b & 0x80) == 0) {
                    if (first) {
                        first = false;
                    } else {
                        objId.append('.');
                    }

                    objId.append(bigValue);
                    bigValue = null;
                    value = 0;
                } else {
                    bigValue = bigValue.shiftLeft(7);
                }
            }
        }

        this.identifier = objId.toString();
        this.contents = contents;
    }

    static ASN1RelativeOID createPrimitive(byte[] contents) {
        return new ASN1RelativeOID(contents);
    }

    static boolean isValidIdentifier(String identifier, int from) {
        int digitCount = 0;

        int pos = identifier.length();
        while (--pos >= from) {
            char ch = identifier.charAt(pos);

            if (ch == '.') {
                if (0 == digitCount
                        || (digitCount > 1 && identifier.charAt(pos + 1) == '0')) {
                    return false;
                }

                digitCount = 0;
            } else if ('0' <= ch && ch <= '9') {
                ++digitCount;
            } else {
                return false;
            }
        }

        return 0 != digitCount
                && (digitCount <= 1 || identifier.charAt(pos + 1) != '0');
    }

    static void writeField(ByteArrayOutputStream out, long fieldValue) {
        byte[] result = new byte[9];
        int pos = 8;
        result[pos] = (byte) ((int) fieldValue & 0x7F);
        while (fieldValue >= (1L << 7)) {
            fieldValue >>= 7;
            result[--pos] = (byte) ((int) fieldValue | 0x80);
        }
        out.write(result, pos, 9 - pos);
    }

    static void writeField(ByteArrayOutputStream out, BigInteger fieldValue) {
        int byteCount = (fieldValue.bitLength() + 6) / 7;
        if (byteCount == 0) {
            out.write(0);
        } else {
            BigInteger tmpValue = fieldValue;
            byte[] tmp = new byte[byteCount];
            for (int i = byteCount - 1; i >= 0; i--) {
                tmp[i] = (byte) (tmpValue.intValue() | 0x80);
                tmpValue = tmpValue.shiftRight(7);
            }
            tmp[byteCount - 1] &= 0x7F;
            out.write(tmp, 0, tmp.length);
        }
    }

    @NotNull
    public String toString() {
        return identifier;
    }

    boolean asn1Equals(ASN1Primitive other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof ASN1RelativeOID that)) {
            return false;
        }

        return this.identifier.equals(that.identifier);
    }

    int encodedLength(boolean withTag) {
        return ASN1OutputStream.getLengthOfEncodingDL(withTag, getContents().length);
    }

    void encode(ASN1OutputStream out, boolean withTag) throws IOException {
        out.writeEncodingDL(withTag, BERTags.RELATIVE_OID, getContents());
    }

    boolean encodeConstructed() {
        return false;
    }

    private byte[] getContents() {
        return contents;
    }
}

package tech.lp2p.cert;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

/**
 * Public facade of ASN.1 Boolean data.
 * <p>
 * Use following to place a new instance of ASN.1 Boolean in your data:
 * <ul>
 * <li> ASN1Boolean.TRUE literal</li>
 * <li> ASN1Boolean.FALSE literal</li>
 * </ul>
 */
public final class ASN1Boolean extends ASN1Primitive {
    private static final byte FALSE_VALUE = 0x00;
    private static final ASN1Boolean FALSE = new ASN1Boolean(FALSE_VALUE);
    private static final byte TRUE_VALUE = (byte) 0xFF;
    private static final ASN1Boolean TRUE = new ASN1Boolean(TRUE_VALUE);
    /**
     * @noinspection AnonymousInnerClass
     */
    private static final ASN1UniversalType TYPE = new ASN1UniversalType(ASN1Boolean.class) {
    };
    private final byte value;

    private ASN1Boolean(byte value) {
        this.value = value;
    }

    /**
     * Return a boolean from the passed in object.
     *
     * @param obj an ASN1Boolean or an object that can be converted into one.
     * @return an ASN1Boolean instance.
     * @throws IllegalArgumentException if the object cannot be converted.
     */
    public static ASN1Boolean getInstance(
            Object obj) {
        if (obj == null || obj instanceof ASN1Boolean) {
            return (ASN1Boolean) obj;
        }

        if (obj instanceof byte[] enc) {
            try {
                return (ASN1Boolean) TYPE.fromByteArray(enc);
            } catch (IOException e) {
                throw new IllegalArgumentException("failed to construct boolean from byte[]: " + e.getMessage());
            }
        }

        throw new IllegalArgumentException("illegal object in getInstance: " + obj.getClass().getName());
    }

    /**
     * Return an ASN1Boolean from the passed in boolean.
     *
     * @param value true or false depending on the ASN1Boolean wanted.
     * @return an ASN1Boolean instance.
     */
    public static ASN1Boolean getInstance(boolean value) {
        return value ? TRUE : FALSE;
    }

    static ASN1Boolean createPrimitive(byte[] contents) {
        if (contents.length != 1) {
            throw new IllegalArgumentException("BOOLEAN value should have 1 byte in it");
        }

        byte b = contents[0];
        return switch (b) {
            case FALSE_VALUE -> FALSE;
            case TRUE_VALUE -> TRUE;
            default -> new ASN1Boolean(b);
        };
    }

    public boolean isTrue() {
        return value != FALSE_VALUE;
    }

    boolean encodeConstructed() {
        return false;
    }

    int encodedLength(boolean withTag) {
        return ASN1OutputStream.getLengthOfEncodingDL(withTag, 1);
    }

    void encode(ASN1OutputStream out, boolean withTag) throws IOException {
        out.writeEncodingDL(withTag, value);
    }

    boolean asn1Equals(ASN1Primitive other) {
        if (!(other instanceof ASN1Boolean that)) {
            return false;
        }

        return this.isTrue() == that.isTrue();
    }

    ASN1Primitive toDERObject() {
        return isTrue() ? TRUE : FALSE;
    }

    @NotNull
    public String toString() {
        return isTrue() ? "TRUE" : "FALSE";
    }
}

package tech.lp2p.cert;

import java.io.IOException;

public final class ASN1ObjectDescriptor extends ASN1Primitive {
    private final ASN1GraphicString baseGraphicString;

    private ASN1ObjectDescriptor(ASN1GraphicString baseGraphicString) {
        if (null == baseGraphicString) {
            throw new NullPointerException("'baseGraphicString' cannot be null");
        }

        this.baseGraphicString = baseGraphicString;
    }

    static ASN1ObjectDescriptor createPrimitive(byte[] contents) {
        return new ASN1ObjectDescriptor(ASN1GraphicString.createPrimitive(contents));
    }

    boolean encodeConstructed() {
        return false;
    }

    int encodedLength(boolean withTag) {
        return baseGraphicString.encodedLength(withTag);
    }

    void encode(ASN1OutputStream out, boolean withTag) throws IOException {
        out.writeIdentifier(withTag, BERTags.OBJECT_DESCRIPTOR);
        baseGraphicString.encode(out, false);
    }


    boolean asn1Equals(ASN1Primitive other) {
        if (!(other instanceof ASN1ObjectDescriptor that)) {
            return false;
        }

        return this.baseGraphicString.asn1Equals(that.baseGraphicString);
    }
}

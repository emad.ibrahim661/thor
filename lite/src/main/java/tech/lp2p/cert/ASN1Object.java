package tech.lp2p.cert;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Base class for defining an ASN.1 object.
 */
public abstract class ASN1Object implements ASN1Encodable {

    public void encodeTo(OutputStream output, String encoding) throws IOException {
        toASN1Primitive().encodeTo(output, encoding);
    }

    /**
     * Return the default BER or DER encoding for this object.
     *
     * @return BER/DER byte encoded object.
     * @throws java.io.IOException on encoding error.
     */
    public byte[] getEncoded() throws IOException {
        ByteArrayOutputStream bOut = new ByteArrayOutputStream();
        toASN1Primitive().encodeTo(bOut);
        return bOut.toByteArray();
    }

    /**
     * Return either the default for "BER" or a DER encoding if "DER" is specified.
     *
     * @param encoding name of encoding to use.
     * @return byte encoded object.
     * @throws IOException on encoding error.
     */
    public byte[] getEncoded(String encoding) throws IOException {
        ByteArrayOutputStream bOut = new ByteArrayOutputStream();
        toASN1Primitive().encodeTo(bOut, encoding);
        return bOut.toByteArray();
    }

    /**
     * Method providing a primitive representation of this object suitable for encoding.
     *
     * @return a primitive representation of this object.
     */
    public abstract ASN1Primitive toASN1Primitive();
}

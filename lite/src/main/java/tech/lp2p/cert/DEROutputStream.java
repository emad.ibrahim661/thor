package tech.lp2p.cert;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Stream that outputs encoding based on distinguished encoding rules.
 */
final class DEROutputStream extends DLOutputStream {
    DEROutputStream(OutputStream os) {
        super(os);
    }

    DEROutputStream getDERSubStream() {
        return this;
    }

    void writePrimitive(ASN1Primitive primitive) throws IOException {
        primitive.toDERObject().encode(this, true);
    }

    void writePrimitives(ASN1Primitive[] primitives) throws IOException {
        for (ASN1Primitive primitive : primitives) {
            primitive.toDERObject().encode(this, true);
        }
    }
}

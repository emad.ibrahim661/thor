package tech.lp2p.cert;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigInteger;
import java.security.PublicKey;
import java.security.SignatureException;
import java.util.Date;
import java.util.Locale;


/**
 * class to produce an X.509 Version 3 certificate.
 */
public final class X509v3CertificateBuilder {
    private final V3TBSCertificateGenerator tbsGen;
    private final ExtensionsGenerator extGenerator;

    /**
     * Create a builder for a version 3 certificate. You may need to use this constructor if the default locale
     * doesn't use a Gregorian calender so that the Time produced is compatible with other ASN.1 implementations.
     *
     * @param issuer        the certificate issuer
     * @param serial        the certificate serial number
     * @param notBefore     the date before which the certificate is not valid
     * @param notAfter      the date after which the certificate is not valid
     * @param dateLocale    locale to be used for date interpretation.
     * @param subject       the certificate subject
     * @param publicKeyInfo the info structure for the public key to be associated with this certificate.
     */
    private X509v3CertificateBuilder(X500Name issuer, BigInteger serial, Date notBefore,
                                     Date notAfter, Locale dateLocale, X500Name subject,
                                     SubjectPublicKeyInfo publicKeyInfo) {
        this(issuer, serial, new Time(notBefore, dateLocale), new Time(notAfter, dateLocale),
                subject, publicKeyInfo);
    }

    public X509v3CertificateBuilder(X500Name issuer, BigInteger serial, Date notBefore,
                                    Date notAfter, Locale locale, X500Name subject,
                                    PublicKey publicKey) {
        this(issuer, serial, notBefore, notAfter, locale, subject,
                SubjectPublicKeyInfo.getInstance(publicKey.getEncoded()));
    }

    /**
     * Create a builder for a version 3 certificate.
     *
     * @param issuer        the certificate issuer
     * @param serial        the certificate serial number
     * @param notBefore     the Time before which the certificate is not valid
     * @param notAfter      the Time after which the certificate is not valid
     * @param subject       the certificate subject
     * @param publicKeyInfo the info structure for the public key to be associated with this certificate.
     */
    private X509v3CertificateBuilder(X500Name issuer, BigInteger serial, Time notBefore,
                                     Time notAfter, X500Name subject,
                                     SubjectPublicKeyInfo publicKeyInfo) {
        tbsGen = new V3TBSCertificateGenerator();
        tbsGen.setSerialNumber(new ASN1Integer(serial));
        tbsGen.setIssuer(issuer);
        tbsGen.setStartDate(notBefore);
        tbsGen.setEndDate(notAfter);
        tbsGen.setSubject(subject);
        tbsGen.setSubjectPublicKeyInfo(publicKeyInfo);

        extGenerator = new ExtensionsGenerator();
    }

    private static byte[] generateSig(ContentSigner signer, ASN1Object tbsObj)
            throws IOException, SignatureException {
        OutputStream sOut = signer.getOutputStream();
        tbsObj.encodeTo(sOut, ASN1Encoding.DER);
        sOut.close();

        return signer.getSignature();
    }

    private static Certificate generateStructure(TBSCertificate tbsCert, AlgorithmIdentifier sigAlgId, byte[] signature) {
        ASN1EncodableVector v = new ASN1EncodableVector();

        v.add(tbsCert);
        v.add(sigAlgId);
        v.add(new DERBitString(signature, 0));

        return Certificate.getInstance(new DERSequence(v));
    }

    /**
     * Add a given extension field for the standard extensions tag (tag 3)
     *
     * @param oid        the OID defining the extension payloadType.
     * @param isCritical true if the extension is critical, false otherwise.
     * @param value      the ASN.1 structure that forms the extension's value.
     * @return this builder object.
     */
    public X509v3CertificateBuilder addExtension(ASN1ObjectIdentifier oid, boolean isCritical,
                                                 ASN1Encodable value) throws Exception {
        extGenerator.addExtension(oid, isCritical, value);
        return this;
    }

    /**
     * Generate an X.509 certificate, based on the current issuer and subject
     * using the passed in signer.
     *
     * @param signer the content signer to be used to generate the signature validating the certificate.
     * @return a holder containing the resulting signed certificate.
     */
    public X509CertificateHolder build(ContentSigner signer) {
        tbsGen.setSignature(signer.getAlgorithmIdentifier());

        if (!extGenerator.isEmpty()) {
            tbsGen.setExtensions(extGenerator.generate());
        }

        try {
            TBSCertificate tbsCert = tbsGen.generateTBSCertificate();
            return new X509CertificateHolder(generateStructure(tbsCert,
                    signer.getAlgorithmIdentifier(), generateSig(signer, tbsCert)));
        } catch (IOException | SignatureException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
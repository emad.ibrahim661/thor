package tech.lp2p.cert;

import java.util.Vector;

/**
 * A builder class for making X.500 Name objects.
 */
public final class X500NameBuilder {
    private final X500NameStyle template;
    private final Vector<RDN> rdns = new Vector<>();

    /**
     * Constructor using a specified style.
     *
     * @param template the style template for string to DN conversion.
     */
    public X500NameBuilder(X500NameStyle template) {
        this.template = template;
    }

    /**
     * Add an RDN based on a single OID and a string representation of its value.
     *
     * @param oid   the OID for this RDN.
     * @param value the string representation of the value the OID refers to.
     * @return the current builder instance.
     */
    @SuppressWarnings("UnusedReturnValue")
    public X500NameBuilder addRDN(ASN1ObjectIdentifier oid, String value) {
        this.addRDN(oid, template.stringToValue(oid, value));

        return this;
    }

    /**
     * Add an RDN based on a single OID and an ASN.1 value.
     *
     * @param oid   the OID for this RDN.
     * @param value the ASN.1 value the OID refers to.
     * @return the current builder instance.
     */
    @SuppressWarnings("UnusedReturnValue")
    private X500NameBuilder addRDN(ASN1ObjectIdentifier oid, ASN1Encodable value) {
        rdns.addElement(new RDN(oid, value));

        return this;
    }

    /**
     * Build an X.500 name for the current builder state.
     *
     * @return a new X.500 name.
     */
    public X500Name build() {
        RDN[] vals = new RDN[rdns.size()];

        for (int i = 0; i != vals.length; i++) {
            vals[i] = rdns.elementAt(i);
        }

        return new X500Name(vals);
    }
}
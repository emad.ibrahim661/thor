package tech.lp2p.cert;

import java.io.IOException;

/**
 * Base class for BIT STRING objects
 */
public abstract class ASN1BitString
        extends ASN1Primitive
        implements ASN1String, ASN1Encodable {
    /**
     * @noinspection AnonymousInnerClass
     */
    private static final ASN1UniversalType TYPE = new ASN1UniversalType(ASN1BitString.class) {

    };
    final byte[] contents;

    /**
     * Base constructor.
     *
     * @param data    the octets making up the bit string.
     * @param padBits the number of extra bits at the end of the string.
     */
    ASN1BitString(byte[] data, int padBits) {
        if (data == null) {
            throw new NullPointerException("'data' cannot be null");
        }
        if (data.length == 0 && padBits != 0) {
            throw new IllegalArgumentException("zero length data with non-zero pad bits");
        }
        if (padBits > 7 || padBits < 0) {
            throw new IllegalArgumentException("pad bits cannot be greater than 7 or less than 0");
        }

        this.contents = Arrays.prepend(data, (byte) padBits);
    }

    ASN1BitString(byte[] contents) {

        this.contents = contents;
    }

    public static ASN1BitString getInstance(Object obj) {
        if (obj == null || obj instanceof ASN1BitString) {
            return (ASN1BitString) obj;
        }
//      else if (obj instanceof ASN1BitStringParser)
        else if (obj instanceof ASN1Encodable) {
            ASN1Primitive primitive = ((ASN1Encodable) obj).toASN1Primitive();
            if (primitive instanceof ASN1BitString) {
                return (ASN1BitString) primitive;
            }
        } else if (obj instanceof byte[]) {
            try {
                return (ASN1BitString) TYPE.fromByteArray((byte[]) obj);
            } catch (IOException e) {
                throw new IllegalArgumentException("failed to construct BIT STRING from byte[]: " + e.getMessage());
            }
        }

        throw new IllegalArgumentException("illegal object in getInstance: " + obj.getClass().getName());
    }

    public static ASN1BitString getInstance(ASN1TaggedObject taggedObject, boolean explicit) {
        return (ASN1BitString) TYPE.getContextInstance(taggedObject, explicit);
    }

    static ASN1BitString createPrimitive(byte[] contents) {
        int length = contents.length;
        if (length < 1) {
            throw new IllegalArgumentException("truncated BIT STRING detected");
        }

        int padBits = contents[0] & 0xFF;
        if (padBits > 0) {
            if (padBits > 7 || length < 2) {
                throw new IllegalArgumentException("invalid pad bits detected");
            }

            byte finalOctet = contents[length - 1];
            if (finalOctet != (byte) (finalOctet & (0xFF << padBits))) {
                return new DLBitString(contents);
            }
        }

        return new DERBitString(contents);
    }


    boolean asn1Equals(ASN1Primitive other) {
        if (!(other instanceof ASN1BitString that)) {
            return false;
        }

        byte[] thisContents = this.contents, thatContents = that.contents;

        int length = thisContents.length;
        if (thatContents.length != length) {
            return false;
        }
        if (length == 1) {
            return true;
        }

        int last = length - 1;
        for (int i = 0; i < last; ++i) {
            if (thisContents[i] != thatContents[i]) {
                return false;
            }
        }

        int padBits = thisContents[0] & 0xFF;
        byte thisLastOctetDER = (byte) (thisContents[last] & (0xFF << padBits));
        byte thatLastOctetDER = (byte) (thatContents[last] & (0xFF << padBits));

        return thisLastOctetDER == thatLastOctetDER;
    }

    ASN1Primitive toDERObject() {
        return new DERBitString(contents);
    }

    ASN1Primitive toDLObject() {
        return new DLBitString(contents);
    }
}

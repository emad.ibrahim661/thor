package tech.lp2p.core;

import org.jetbrains.annotations.NotNull;

public interface BlockStore {

    boolean hasBlock(@NotNull Cid cid);

    byte[] getBlock(@NotNull Cid cid);

    void deleteBlock(@NotNull Cid cid);

    void storeBlock(@NotNull Cid cid, byte[] data);

}



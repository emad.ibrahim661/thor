package tech.lp2p.core;

import org.jetbrains.annotations.Nullable;

public record Raw(Cid cid, long size) implements Info {

    @Nullable
    @Override
    public String name() {
        return null;
    }
}

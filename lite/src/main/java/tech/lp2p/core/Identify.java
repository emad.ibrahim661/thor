package tech.lp2p.core;

import org.jetbrains.annotations.NotNull;

import java.util.Arrays;

public record Identify(@NotNull PeerId peerId,
                       @NotNull String agent,
                       @NotNull Peeraddr[] peeraddrs,
                       @NotNull String[] protocols) {

    public boolean hasProtocol(String protocol) {
        return Arrays.asList(protocols).contains(protocol);
    }

    public boolean hasRelayHop() {
        return hasProtocol(Protocol.RELAY_PROTOCOL_HOP.name());
    }

}

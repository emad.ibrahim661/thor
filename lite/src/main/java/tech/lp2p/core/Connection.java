package tech.lp2p.core;

public interface Connection extends AutoCloseable {
    ALPN alpn();

    void close();

    Peeraddr remotePeeraddr();

    PeerId remotePeerId();

    boolean isConnected();

    Host host();


    boolean hasAttribute(String key);

}

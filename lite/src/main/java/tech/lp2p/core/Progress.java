package tech.lp2p.core;


public interface Progress {
    void setProgress(int progress);
}

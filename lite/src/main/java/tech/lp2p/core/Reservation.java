package tech.lp2p.core;


import org.jetbrains.annotations.NotNull;

import java.util.concurrent.TimeUnit;

public record Reservation(Peeraddr peeraddr, Limit limit, long expire) {

    private static final int UPDATE_THRESHOLD = 5; // 5 minutes

    // when 0, there is not limitation of bytes during communication
    public long limitData() {
        return limit.data();
    }

    // when 0, there is no time limitation
    public long limitDuration() {
        return limit.duration();
    }


    public long expireInMinutes() {
        long duration = (expire * 1000) - System.currentTimeMillis(); // all in millis
        return TimeUnit.MILLISECONDS.toMinutes(duration);
    }

    @NotNull
    public PeerId peerId() {
        return peeraddr.peerId();
    }

    public boolean updateRequired() {
        if (limit().limited()) {
            return expireInMinutes() < UPDATE_THRESHOLD;
        }
        return false;
    }
}

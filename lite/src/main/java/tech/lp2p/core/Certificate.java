package tech.lp2p.core;


import static tech.lp2p.proto.Crypto.KeyType.Ed25519;

import com.google.protobuf.ByteString;

import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayInputStream;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.ECGenParameterSpec;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;

import tech.lp2p.cert.ASN1Encodable;
import tech.lp2p.cert.ASN1Object;
import tech.lp2p.cert.ASN1ObjectIdentifier;
import tech.lp2p.cert.ASN1OctetString;
import tech.lp2p.cert.ASN1Primitive;
import tech.lp2p.cert.ContentSigner;
import tech.lp2p.cert.DEROctetString;
import tech.lp2p.cert.DERSequence;
import tech.lp2p.cert.DLSequence;
import tech.lp2p.cert.JcaContentSignerBuilder;
import tech.lp2p.cert.SubjectPublicKeyInfo;
import tech.lp2p.cert.X500Name;
import tech.lp2p.cert.X509CertificateConverter;
import tech.lp2p.cert.X509CertificateHolder;
import tech.lp2p.cert.X509v3CertificateBuilder;
import tech.lp2p.proto.Crypto;
import tech.lp2p.utils.Utils;


public record Certificate(X509Certificate x509Certificate, PrivateKey key, byte[] certificate,
                          byte[] privateKey) {
    private static final String TLS_HANDSHAKE = "libp2p-tls-handshake:";
    private static final String NAMED_CURVE = "secp256r1";
    private static final int[] EXTENSION_PREFIX = new int[]{1, 3, 6, 1, 4, 1, 53594};
    public static final int[] PREFIXED_EXTENSION_ID = getPrefixedExtensionID(new int[]{1, 1});

    // The libp2p handshake uses TLS 1.3 (and higher). Endpoints MUST NOT negotiate lower TLS versions.
    //
    // During the handshake, peers authenticate each other’s identity as described in Peer
    // Authentication. Endpoints MUST verify the peer's identity. Specifically,
    // this means that servers MUST require client authentication during the TLS handshake,
    // and MUST abort a connection attempt if the client fails to provide the requested
    // authentication information.
    //
    // When negotiating the usage of this handshake dynamically, via a protocol agreement mechanism
    // like multistream-select 1.0, it MUST be identified with the following protocol ID: /tls/1.0.0
    //
    // [-> done see Connection.remoteCertificate() and the usage]
    //
    // In order to be able to use arbitrary key types, peers don’t use their host key to sign the
    // X.509 certificate they send during the handshake. Instead, the host key is encoded into the
    // libp2p Public Key Extension, which is carried in a self-signed certificate.
    // [-> done see createCertificate]
    //
    // The key used to generate and sign this certificate SHOULD NOT be related to the host's key.
    // Endpoints MAY generate a new key and certificate for every connection attempt, or they MAY
    // reuse the same key and certificate for multiple connections.
    // [-> done see createCertificate, use the certification for multiple connections, but
    // generates a new one each time the application is started]
    //
    // Endpoints MUST choose a key that will allow the peer to verify the certificate (i.e.
    // choose a signature algorithm that the peer supports), and SHOULD use a key payloadType that (a)
    // allows for efficient signature computation, and (b) reduces the combined size of the
    // certificate and the signature. In particular, RSA SHOULD NOT be used unless no elliptic
    // curve algorithms are supported.
    // [-> elliptic curve is used, NAMED_CURVE = "secp256r1"]
    //
    // Endpoints MUST NOT send a certificate chain that contains more than one certificate.
    // The certificate MUST have NotBefore and NotAfter fields set such that the certificate
    // is valid at the time it is received by the peer. When receiving the certificate chain,
    // an endpoint MUST check these conditions and abort the connection attempt if (a) the
    // presented certificate is not yet valid, OR (b) if it is expired. Endpoints MUST abort
    // the connection attempt if more than one certificate is received, or if the certificate’s
    // self-signature is not valid.
    // [-> LiteCertificate.validCertificate used in LiteTrust: checks "cert.checkValidity()"
    // which cover (a) and (b)]
    // [-> LiteTrust.checkServerTrusted and LiteTrust.checkClientTrusted checks number of
    // certificates and aborts]
    // [-> LiteCertificate.validCertificate used in LiteTrust: checks
    // "cert.verify(cert.getPublicKey())" if self-signature is valid]
    //
    // The certificate MUST contain the libp2p Public Key Extension. If this extension is
    // missing, endpoints MUST abort the connection attempt. This extension MAY be marked
    // critical. The certificate MAY contain other extensions. Implementations MUST ignore
    // non-critical extensions with unknown OIDs. Endpoints MUST abort the connection attempt
    // if the certificate contains critical extensions that the endpoint does not understand.
    // [-> LiteCertificate.validCertificate used in LiteTrust: both checks done]
    //
    // Certificates MUST omit the deprecated subjectUniqueId and issuerUniqueId fields.
    // Endpoints MAY abort the connection attempt if either is present.
    // [Not done, because it is not required, but easy to do]
    //
    // Note for clients: Since clients complete the TLS handshake immediately after sending the
    // certificate (and the TLS ClientFinished message), the handshake will appear as having
    // succeeded before the server had the chance to verify the certificate. In this state,
    // the client can already send application data. If certificate verification fails on
    // the server side, the server will close the connection without processing any data that
    // the client sent.
    // [-> done see Connection.remoteCertificate() and the usage]

    public static Certificate createCertificate(Keys keys) throws Exception {

        SecureRandom random = new SecureRandom();

        Calendar end = Calendar.getInstance();
        end.set(Calendar.YEAR, 2099);
        Calendar begin = Calendar.getInstance();
        begin.set(Calendar.YEAR, 2020);

        Date notBefore = begin.getTime();
        Date notAfter = end.getTime();

        KeyPairGenerator generator = KeyPairGenerator.getInstance("EC");
        generator.initialize(new ECGenParameterSpec(NAMED_CURVE));
        KeyPair keypair = generator.genKeyPair();
        PrivateKey key = keypair.getPrivate();
        PublicKey pubKey = keypair.getPublic();


        BigInteger bigInteger = new BigInteger(64, random);
        // Prepare the information required for generating an X.509 certificate.
        X500Name owner = new X500Name("CN=" + "localhost");


        X509v3CertificateBuilder builder = new X509v3CertificateBuilder(owner, bigInteger,
                notBefore, notAfter, Locale.US, owner, keypair.getPublic());


        // The publicKey field of SignedKey contains the public host key of the endpoint,
        // encoded using the following protobuf:
        // enum KeyType {
        //	RSA = 0;
        //	Ed25519 = 1;
        //	Secp256k1 = 2;
        //	ECDSA = 3;
        // }
        //
        // message PublicKey {
        //	required KeyType Type = 1;
        //	required bytes Data = 2;
        // }
        Crypto.PublicKey cryptoKey = Crypto.PublicKey.newBuilder()
                .setType(Crypto.KeyType.Ed25519)
                .setData(ByteString.copyFrom(keys.peerId().hash()))
                .build();
        byte[] keyBytes = cryptoKey.toByteArray();


        // The public host key allows the peer to calculate the peer ID of the peer it is
        // connecting to. Clients MUST verify that the peer ID derived from the certificate
        // matches the peer ID they intended to connect to, and MUST abort the connection if
        // there is a mismatch.
        //
        // The peer signs the concatenation of the string libp2p-tls-handshake: and the encoded
        // public key that is used to generate the certificate carrying the libp2p
        // Public Key Extension, using its private host key. The public key is encoded as a
        // SubjectPublicKeyInfo structure as described in RFC 5280, Section 4.1:

        // SubjectPublicKeyInfo ::= SEQUENCE {
        //  algorithm             AlgorithmIdentifier,
        //  subject_public_key    BIT STRING
        // }
        // AlgorithmIdentifier  ::= SEQUENCE {
        //  algorithm             OBJECT IDENTIFIER,
        //  parameters            ANY DEFINED BY algorithm OPTIONAL
        // }


        SubjectPublicKeyInfo subjectPublicKeyInfo = SubjectPublicKeyInfo.
                getInstance(pubKey.getEncoded());

        Objects.requireNonNull(subjectPublicKeyInfo);

        byte[] signature = keys.sign(Utils.concat(
                TLS_HANDSHAKE.getBytes(), subjectPublicKeyInfo.getEncoded()));


        // This signature provides cryptographic proof that the peer was in possession of the
        // private host key at the time the certificate was signed. Peers MUST verify the
        // signature, and abort the connection attempt if signature verification fails.
        //
        // The public host key and the signature are ANS.1-encoded into the SignedKey data
        // structure, which is carried in the libp2p Public Key Extension.
        // The libp2p Public Key Extension is a X.509 extension with the Object
        // Identier 1.3.6.1.4.1.53594.1.1, allocated by IANA to the libp2p project at Protocol Labs.
        SignedKey signedKey = new SignedKey(keyBytes, signature);

        ASN1ObjectIdentifier indent = new ASN1ObjectIdentifier(getLiteExtension());


        // The certificate MUST contain the libp2p Public Key Extension. If this extension is
        // missing, endpoints MUST abort the connection attempt. This extension MAY be
        // marked critical. The certificate MAY contain other extensions. Implementations
        // MUST ignore non-critical extensions with unknown OIDs. Endpoints MUST abort the
        // connection attempt if the certificate contains critical extensions that the
        // endpoint does not understand.
        ContentSigner signer = new JcaContentSignerBuilder(
                "SHA256withECDSA").build(key);

        X509CertificateHolder certHolder = builder.addExtension(indent, false, signedKey)
                .build(signer);

        X509Certificate x509Certificate = X509CertificateConverter.getCertificate(certHolder);
        x509Certificate.verify(pubKey);

        final String keyText = "-----BEGIN PRIVATE KEY-----\n" +
                Base64.getEncoder().encodeToString(key.getEncoded()) +
                "\n-----END PRIVATE KEY-----\n";


        byte[] privateKey = keyText.getBytes(StandardCharsets.US_ASCII);


        final String certText = "-----BEGIN CERTIFICATE-----\n" +
                Base64.getEncoder().encodeToString(x509Certificate.getEncoded()) +
                "\n-----END CERTIFICATE-----\n";


        byte[] certificate = certText.getBytes(StandardCharsets.US_ASCII);

        // Certificates MUST omit the deprecated subjectUniqueId and issuerUniqueId fields.
        // Endpoints MAY abort the connection attempt if either is present.
        //
        // Note for clients: Since clients complete the TLS handshake immediately after
        // sending the certificate (and the TLS ClientFinished message), the handshake
        // will appear as having succeeded before the server had the chance to verify the
        // certificate. In this state, the client can already send application data.
        // If certificate verification fails on the server side, the server will close the
        // connection without processing any data that the client sent.

        try (ByteArrayInputStream certificateInput = new ByteArrayInputStream(certificate)) {
            x509Certificate = (X509Certificate) CertificateFactory.getInstance("X509")
                    .generateCertificate(certificateInput);

            return new Certificate(x509Certificate, key, certificate, privateKey);
        }
    }

    private static int[] getPrefixedExtensionID(int[] suffix) {
        return Utils.concat(EXTENSION_PREFIX, suffix);
    }

    @NotNull
    public static String getLiteExtension() {
        return Certificate.integersToString(Certificate.PREFIXED_EXTENSION_ID);
    }

    public static String integersToString(int[] values) {
        String s = "";
        for (int i = 0; i < values.length; ++i) {
            if (i > 0) {
                s = s.concat(".");
            }
            s = s.concat(String.valueOf(values[i]));
        }
        return s;
    }

    @NotNull
    public static PeerId extractPeerId(@NotNull X509Certificate cert) throws Exception {
        byte[] extension = cert.getExtensionValue(Certificate.getLiteExtension());
        Objects.requireNonNull(extension);

        ASN1OctetString octs = (ASN1OctetString) ASN1Primitive.fromByteArray(extension);
        ASN1Primitive primitive = ASN1Primitive.fromByteArray(octs.getOctets());
        DLSequence sequence = (DLSequence) DERSequence.getInstance(primitive);
        DEROctetString pubKeyRaw = (DEROctetString) sequence.getObjectAt(0);

        Crypto.PublicKey publicKey = Crypto.PublicKey.parseFrom(pubKeyRaw.getOctets());

        Utils.checkArgument(publicKey.getType(), Ed25519, "Only Ed25519 expected");

        byte[] raw = publicKey.getData().toByteArray();
        PeerId peerId = PeerId.create(raw);

        DEROctetString signature = (DEROctetString) sequence.getObjectAt(1);
        byte[] skSignature = signature.getOctets();

        byte[] certKeyPub = cert.getPublicKey().getEncoded();

        byte[] verify = Utils.concat(Certificate.TLS_HANDSHAKE.getBytes(), certKeyPub);

        peerId.verify(verify, skSignature);
        return peerId;
    }


    public static void validCertificate(@NotNull X509Certificate cert) throws Exception {
        // Checks that the certificate is currently valid. It is if
        // the current date and time are within the validity period given in the
        // certificate.
        //
        // The validity period consists of two date/time values:
        // the first and last dates (and times) on which the certificate
        // is valid.
        cert.checkValidity();


        // check if the certificate’s self-signature is valid (verified)
        // Verifies that this certificate was signed using the
        // private key that corresponds to the specified public key.
        cert.verify(cert.getPublicKey());

        boolean found = false;
        Set<String> critical = cert.getCriticalExtensionOIDs();
        if (critical.contains(Certificate.getLiteExtension())) {
            found = true;
            Utils.checkTrue(critical.size() == 1, "unknown critical extensions");
        } else {
            Utils.checkTrue(critical.isEmpty(), "unknown critical extensions");
        }

        if (!found) {
            Set<String> nonCritical = cert.getNonCriticalExtensionOIDs();
            if (nonCritical.contains(Certificate.getLiteExtension())) {
                found = true;
            }
        }
        Utils.checkTrue(found, "libp2p Public Key Extension is missing");

        // Certificates MUST omit the deprecated subjectUniqueId and issuerUniqueId fields.
        // Endpoints MAY abort the connection attempt if either is present.

        // Could be done, but left out is not required
        // Utils.error(TAG, Arrays.toString(cert.getSubjectUniqueID()));

        // Could be done, but left out is not required
        // Utils.error(TAG, Arrays.toString( cert.getIssuerUniqueID()));

    }


    public static class SignedKey extends ASN1Object {
        private final ASN1OctetString PubKey;
        private final ASN1OctetString Signature;

        SignedKey(byte[] pubKey, byte[] signature) {
            PubKey = new DEROctetString(pubKey);
            Signature = new DEROctetString(signature);
        }

        @Override
        public ASN1Primitive toASN1Primitive() {
            ASN1Encodable[] asn1Encodables = new ASN1Encodable[]{this.PubKey, this.Signature};
            return new DERSequence(asn1Encodables);
        }

    }
}

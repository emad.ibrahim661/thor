package tech.lp2p.core;

import com.google.protobuf.ByteString;

import org.jetbrains.annotations.Nullable;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import tech.lp2p.utils.Utils;

public final class Peeraddrs extends ArrayList<Peeraddr> {

    private static final int REACHABLE_TIMEOUT = 250; // in ms

    public Peeraddrs() {
        super();
    }

    public Peeraddrs(Set<Peeraddr> set) {
        super(set);
    }

    @Nullable
    public static Peeraddr best(Peeraddrs peeraddrs) {

        if (peeraddrs.size() > 1) {
            removeLoopbacks(peeraddrs);
        }
        if (peeraddrs.size() > 1) {
            removeLinkLocals(peeraddrs);
        }
        if (peeraddrs.size() > 1) {
            removeSiteLocals(peeraddrs);
        }

        if (peeraddrs.size() == 1) {
            return peeraddrs.get(0);
        }

        for (Peeraddr resolvedAddr : peeraddrs) {
            if (isReachable(resolvedAddr)) { // only reachable
                return resolvedAddr;
            }
        }
        return null;
    }

    @Nullable
    public static Peeraddr reduce(PeerId peerId, List<ByteString> byteStrings) {
        Peeraddrs peeraddrs = Peeraddr.create(peerId, byteStrings);

        if (peeraddrs.isEmpty()) {
            return null;
        }

        if (peeraddrs.size() > 1) {
            removeLoopbacks(peeraddrs);
        }
        if (peeraddrs.size() > 1) {
            removeLinkLocals(peeraddrs);
        }
        if (peeraddrs.size() > 1) {
            removeSiteLocals(peeraddrs);
        }

        if (peeraddrs.size() == 1) {
            return peeraddrs.get(0);
        }

        for (Peeraddr resolvedAddr : peeraddrs) {
            if (isReachable(resolvedAddr)) { // only reachable
                return resolvedAddr;
            }
        }

        return null;
    }


    private static void removeLoopbacks(Peeraddrs peeraddrs) {

        Iterator<Peeraddr> iterator = peeraddrs.iterator();

        while (iterator.hasNext()) {
            Peeraddr peeraddr = iterator.next();

            if (peeraddrs.size() == 1) {
                return;
            }
            try {
                InetAddress inetAddress = InetAddress.getByAddress(peeraddr.address());
                if (inetAddress.isLoopbackAddress()) {
                    iterator.remove();
                }
            } catch (Throwable throwable) {
                iterator.remove();
            }
        }
    }

    private static void removeLinkLocals(Peeraddrs peeraddrs) {

        Iterator<Peeraddr> iterator = peeraddrs.iterator();

        while (iterator.hasNext()) {
            Peeraddr peeraddr = iterator.next();

            if (peeraddrs.size() == 1) {
                return;
            }
            try {
                InetAddress inetAddress = InetAddress.getByAddress(peeraddr.address());
                if (inetAddress.isLinkLocalAddress()) {
                    iterator.remove();
                }
            } catch (Throwable throwable) {
                iterator.remove();
            }
        }

    }

    private static void removeSiteLocals(Peeraddrs peeraddrs) {

        Iterator<Peeraddr> iterator = peeraddrs.iterator();

        while (iterator.hasNext()) {
            Peeraddr peeraddr = iterator.next();

            if (peeraddrs.size() == 1) {
                return;
            }
            try {
                InetAddress inetAddress = InetAddress.getByAddress(peeraddr.address());
                if (inetAddress.isSiteLocalAddress()) {
                    iterator.remove();
                }
            } catch (Throwable throwable) {
                iterator.remove();
            }
        }

    }

    private static boolean isReachable(Peeraddr peeraddr) {
        try {
            InetAddress inetAddress = InetAddress.getByAddress(peeraddr.address());
            return inetAddress.isReachable(REACHABLE_TIMEOUT);
        } catch (Throwable throwable) {
            Utils.error(throwable); // should not occur
        }
        return false;
    }
}

package tech.lp2p.core;

public interface Acceptor {
    void consume(Peeraddr peeraddr);
}

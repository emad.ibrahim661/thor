package tech.lp2p.core;


import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

public record Peeraddr(PeerId peerId, byte[] address, int port) implements Serializable {

    @NotNull
    public static Peeraddr loopbackPeeraddr(PeerId peerId, int port) {
        InetAddress inetAddress = InetAddress.getLoopbackAddress();
        return Peeraddr.create(peerId, inetAddress.getAddress(), port);
    }

    @NotNull
    public static Peeraddrs peeraddrs(PeerId peerId, int port) {
        Collection<InetAddress> inetSocketAddresses = Network.addresses();
        Peeraddrs result = new Peeraddrs();
        for (InetAddress inetAddress : inetSocketAddresses) {
            result.add(create(peerId, inetAddress.getAddress(), port));
        }
        return result;
    }

    @Nullable
    public static Peeraddr create(PeerId peerId, byte[] raw) {
        return Multiaddr.parseAddress(CodedInputStream.newInstance(raw), peerId);
    }

    @NotNull
    public static Peeraddrs create(PeerId peerId, List<ByteString> byteStrings) {
        Peeraddrs peeraddrs = new Peeraddrs();
        for (ByteString entry : byteStrings) {
            Peeraddr peeraddr = Peeraddr.create(peerId, entry.toByteArray());
            if (peeraddr != null) {
                peeraddrs.add(peeraddr);
            }
        }
        return peeraddrs;
    }

    @NotNull
    public static Peeraddr create(PeerId peerId, InetSocketAddress inetSocketAddress) {
        InetAddress inetAddress = inetSocketAddress.getAddress();
        return create(peerId, inetAddress.getAddress(), inetSocketAddress.getPort());
    }

    @NotNull
    public static Peeraddr create(PeerId peerId, byte[] address, int port) {
        return new Peeraddr(peerId, address, port);
    }

    public byte[] encoded() {

        try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()) {
            InetAddress inetAddress = InetAddress.getByAddress(address);
            if (inetAddress instanceof Inet4Address) {
                Multiaddr.encodeProtocol(Multiaddr.IP4, byteArrayOutputStream);
            } else {
                Multiaddr.encodeProtocol(Multiaddr.IP6, byteArrayOutputStream);
            }
            Multiaddr.encodePart(address, byteArrayOutputStream);
            Multiaddr.encodeProtocol(Multiaddr.UDP, byteArrayOutputStream);
            Multiaddr.encodePart(port, byteArrayOutputStream);
            Multiaddr.encodeProtocol(Multiaddr.QUICV1, byteArrayOutputStream);

            return byteArrayOutputStream.toByteArray();
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Peeraddr peeraddr = (Peeraddr) o;
        return port == peeraddr.port && Objects.equals(peerId, peeraddr.peerId)
                && Arrays.equals(address, peeraddr.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(peerId, Arrays.hashCode(address), port);
    }

}

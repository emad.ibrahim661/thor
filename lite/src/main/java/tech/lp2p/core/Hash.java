package tech.lp2p.core;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Objects;

import tech.lp2p.utils.Utils;

// note this is always 32 bit width
public record Hash(byte[] bytes) implements Serializable {

    @NotNull
    public static Hash createHash(byte[] bytes) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            return Hash.toHash(digest.digest(bytes));
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }

    @NotNull
    public static BigInteger distance(Hash a, Hash b) {
        byte[] k3 = xor(a.bytes(), b.bytes());

        // SetBytes interprets buf as the bytes of a big-endian unsigned
        // integer, sets z to that value, and returns z.
        // big.NewInt(0).SetBytes(k3)

        return new BigInteger(k3);
    }

    private static byte[] xor(byte[] x1, byte[] x2) {
        byte[] out = new byte[x1.length];

        for (int i = 0; i < x1.length; i++) {
            out[i] = (byte) (0xff & ((int) x1[i]) ^ ((int) x2[i]));
        }
        return out;
    }

    @NotNull
    public static Hash toHash(byte[] data) {
        Objects.requireNonNull(data, "data can not be null");
        Utils.checkArgument(data.length, 32, "hash size must be 32");

        return new Hash(data);
    }


    public static byte[] toArray(Hash hash) {
        Objects.requireNonNull(hash, "hash can not be null");
        return hash.bytes();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Hash hash = (Hash) o;
        return Arrays.equals(bytes, hash.bytes);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(bytes);
    }
}

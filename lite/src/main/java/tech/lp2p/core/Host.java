package tech.lp2p.core;

import org.jetbrains.annotations.NotNull;


public interface Host {

    @NotNull
    Peeraddrs peeraddrs();

    @NotNull
    Peeraddrs bootstrap();

    @NotNull
    PeerId self();

    @NotNull
    Protocols protocols(@NotNull ALPN alpn);

    @NotNull
    Certificate certificate();

    @NotNull
    Responder responder(@NotNull ALPN alpn);

    @NotNull
    Keys keys();

    @NotNull
    String agent();

    @NotNull
    BlockStore blockStore();
}

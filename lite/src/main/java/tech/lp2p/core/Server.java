package tech.lp2p.core;

import java.net.DatagramSocket;
import java.util.List;
import java.util.Set;

public interface Server extends Host {

    void shutdown();

    int port();

    DatagramSocket socket();

    void holePunching(Peeraddr peeraddr);

    Identify identify() throws Exception;

    boolean hasReservations();

    Set<Reservation> reservations();


    Reservation hopReserve(Peeraddr relay) throws Exception;

    void hopReserve(int maxReservation, int timeout);

    Peeraddrs reservationPeeraddrs();

    void provideKey(Key key, Acceptor acceptor);


    Reservation refreshReservation(Reservation reservation) throws Exception;


    void closeReservation(Reservation reservation);


    List<Connection> connections(PeerId peerId);

    boolean isGated(PeerId peerId);


    boolean hasConnection(PeerId peerId);


    int numConnections();

    record Settings(int port, Limit limit) {
    }
}

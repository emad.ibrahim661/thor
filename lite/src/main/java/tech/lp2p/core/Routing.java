package tech.lp2p.core;

import org.jetbrains.annotations.NotNull;

public interface Routing {

    void providers(@NotNull Key key, @NotNull Acceptor acceptor);

    @NotNull
    Peeraddrs findPeeraddrs(@NotNull Key key, int timeout);

    void findClosestPeers(@NotNull Key key, @NotNull Acceptor acceptor);

}

package tech.lp2p.core;


import org.jetbrains.annotations.Nullable;

public sealed interface Info permits Raw, Fid, Dir {
    long size();

    Cid cid();

    @SuppressWarnings("SameReturnValue")
    @Nullable
    String name(); // in case of Raw it will return null

    default boolean isDir() {
        return this instanceof Dir;
    }

    default boolean isRaw() {
        return this instanceof Raw;
    }

    default boolean isFid() {
        return this instanceof Fid;
    }
}

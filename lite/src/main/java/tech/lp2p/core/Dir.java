package tech.lp2p.core;


public record Dir(Cid cid, long size, String name) implements Info {
}

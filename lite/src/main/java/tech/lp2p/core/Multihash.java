package tech.lp2p.core;

// https://github.com/multiformats/multicodec/blob/master/table.csv (see multihash)
public interface Multihash {
    int ID = 0;
    int SHA2_256 = 0x12;
}
package tech.lp2p.core;

import org.jetbrains.annotations.NotNull;

import java.io.Serial;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Objects;

public record Key(Hash hash, byte[] target) implements Serializable {
    @Serial
    private static final long serialVersionUID = 8008406472421724805L;

    @NotNull
    public static Key convertKey(byte[] target) {
        return new Key(Hash.createHash(target), target);
    }

    @NotNull
    public static BigInteger distance(@NotNull Key a, @NotNull Key b) {
        // SetBytes interprets buf as the bytes of a big-endian unsigned
        // integer, sets z to that value, and returns z.
        // big.NewInt(0).SetBytes(k3)

        return Hash.distance(a.hash(), b.hash());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Key key = (Key) o;
        return Objects.equals(hash, key.hash);
    }

    @Override
    public int hashCode() {
        return hash.hashCode(); // ok, checked, maybe opt
    }
}

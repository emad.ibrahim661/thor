package tech.lp2p.core;

// https://github.com/multiformats/multicodec/blob/master/table.csv (see ipld)
public interface Multicodec {
    int DAG_PB = 0x70; // dag-pb
    int LIBP2P_KEY = 0x72; // libp2p-key
}


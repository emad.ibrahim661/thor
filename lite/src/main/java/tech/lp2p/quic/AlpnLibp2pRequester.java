package tech.lp2p.quic;

import org.jetbrains.annotations.NotNull;


public record AlpnLibp2pRequester(Requester requester,
                                  StreamState streamState) implements StreamHandler {

    public static AlpnLibp2pRequester create(@NotNull Requester requester) {
        return new AlpnLibp2pRequester(requester, new Libp2pState(requester));
    }


    @Override
    public void terminated(Stream stream) {
        streamState.reset();
        requester.terminated(stream);
    }

    @Override
    public void fin(Stream stream) {
        streamState.reset();
        requester.fin(stream);
    }

    @Override
    public void throwable(Throwable throwable) {
        streamState.reset();
        requester.throwable(throwable);
    }


    static class Libp2pState extends StreamState {

        private final Requester requester;


        Libp2pState(Requester requester) {
            this.requester = requester;
        }

        public void accept(Stream stream, byte[] frame) throws Exception {
            if (frame.length > 0) {
                if (!StreamState.isProtocol(frame)) {
                    requester.data(stream, frame);
                }
            }
        }
    }
}

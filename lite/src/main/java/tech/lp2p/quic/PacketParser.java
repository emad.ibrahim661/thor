package tech.lp2p.quic;


import org.jetbrains.annotations.Nullable;

import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import tech.lp2p.tls.DecodeErrorException;
import tech.lp2p.tls.DecryptErrorAlert;

interface PacketParser {

    static boolean isInitial(int type, Version quicVersion) {
        if (quicVersion.isV2()) {
            return type == Settings.INITIAL_V2_type;
        } else {
            return type == Settings.INITIAL_V1_type;
        }
    }

    static PacketReceived parse(PacketHeader packetHeader, ByteBuffer data,
                                Keys keys,
                                long largestPacketNumber,
                                @Nullable TransportParameters transportParameters)
            throws DecryptErrorAlert, DecodeErrorException {
        return switch (packetHeader.level()) {
            case Initial -> parseInitial(packetHeader, data, keys, largestPacketNumber,
                    transportParameters);
            case Handshake -> parseHandshake(packetHeader, data, keys, largestPacketNumber,
                    transportParameters);
            case App -> parseShortHeader(packetHeader, data, keys, largestPacketNumber,
                    transportParameters);
        };
    }


    private static byte getInitialPacketType(Version version) {
        if (version.isV2()) {
            return (byte) Settings.INITIAL_V2_type;
        } else {
            return (byte) Settings.INITIAL_V1_type;
        }
    }

    private static void checkInitialPacketType(Version version, int type) {
        if (type != getInitialPacketType(version)) {
            // Programming error: this method shouldn't have been called if packet is not Initial
            throw new RuntimeException();
        }
    }


    private static byte[] parseToken(ByteBuffer buffer) throws DecodeErrorException {
        // https://tools.ietf.org/html/draft-ietf-quic-transport-16#section-17.5:
        // "An Initial packet (shown in Figure 13) has two additional header
        // fields that are added to the Long Header before the Length field."

        long tokenLength = VariableLengthInteger.parseLong(buffer);
        if (tokenLength > 0) {
            if (tokenLength <= buffer.remaining()) {
                byte[] token = new byte[(int) tokenLength];
                buffer.get(token);
                return token;
            } else {
                throw new DecodeErrorException("invalid packet length");
            }
        }

        return null;
    }

    static PacketHeader parseInitialPacketHeader(ByteBuffer buffer, byte flags, int posFlags, Version version) throws DecodeErrorException {
        checkInitialPacketType(version, (flags & 0x30) >> 4);

        int dstConnIdLength = buffer.get();
        // https://tools.ietf.org/html/draft-ietf-quic-transport-27#section-17.2
        // "In QUIC version 1, this value MUST NOT exceed 20.  Endpoints that receive a version 1 long header with a
        // value larger than 20 MUST drop the packet."
        if (dstConnIdLength < 0 || dstConnIdLength > 20) {
            throw new DecodeErrorException("invalid packet length");
        }
        if (buffer.remaining() < dstConnIdLength) {
            throw new DecodeErrorException("invalid packet length");
        }
        byte[] destinationConnectionId = new byte[dstConnIdLength];
        buffer.get(destinationConnectionId);

        int srcConnIdLength = buffer.get();
        if (srcConnIdLength < 0 || srcConnIdLength > 20) {
            throw new DecodeErrorException("invalid packet length");
        }
        if (buffer.remaining() < srcConnIdLength) {
            throw new DecodeErrorException("invalid packet length");
        }
        byte[] sourceConnectionId = new byte[srcConnIdLength];
        buffer.get(sourceConnectionId);

        byte[] token = parseToken(buffer);
        return new PacketHeader(Level.Initial, version, flags, posFlags,
                destinationConnectionId, sourceConnectionId, token);

    }


    static PacketReceived parseInitial(PacketHeader packetHeader, ByteBuffer buffer, Keys keys,
                                       long largestPacketNumber,
                                       @Nullable TransportParameters transportParameters)
            throws DecodeErrorException, DecryptErrorAlert {

        // "The length of the remainder of the packet (that is, the Packet Number and Payload fields) in bytes"
        int length = VariableLengthInteger.parse(buffer);

        return parsePacketNumberAndPayload(packetHeader, buffer,
                length, keys, largestPacketNumber, transportParameters);

    }

    static PacketHeader parseShortPacketHeader(ByteBuffer buffer, byte flags, int posFlags,
                                               Version version, int sourceConnectionIdLength)
            throws DecodeErrorException {

        if (buffer.remaining() < sourceConnectionIdLength) {
            throw new DecodeErrorException("invalid packet length");
        }

        // https://tools.ietf.org/html/draft-ietf-quic-transport-24#section-5.1
        // "Packets with short headers (Section 17.3) only include the
        //   Destination Connection ID and omit the explicit length.  The length
        //   of the Destination Connection ID field is expected to be known to
        //   endpoints."
        byte[] destinationConnectionId = new byte[sourceConnectionIdLength];
        buffer.get(destinationConnectionId);

        return new PacketHeader(Level.App, version, flags, posFlags,
                destinationConnectionId, null, null);
    }


    static PacketReceived parseShortHeader(PacketHeader packetHeader, ByteBuffer buffer,
                                           Keys keys, long largestPacketNumber,
                                           @Nullable TransportParameters transportParameters)
            throws DecodeErrorException, DecryptErrorAlert {

        return parsePacketNumberAndPayload(packetHeader, buffer,
                buffer.limit() - buffer.position(), keys,
                largestPacketNumber, transportParameters);
    }

    private static byte getHandshakePacketType(Version version) {
        if (version.isV2()) {
            return (byte) Settings.HANDSHAKE_V2_type;
        } else {
            return (byte) Settings.HANDSHAKE_V1_type;
        }
    }

    private static void checkHandshakePacketType(Version version, int type) {
        if (type != getHandshakePacketType(version)) {
            throw new IllegalStateException("Programming error: this method shouldn't " +
                    "have been called if packet is not Initial");
        }
    }

    static PacketHeader parseHandshakePackageHeader(ByteBuffer buffer, byte flags, int posFlags,
                                                    Version version) throws DecodeErrorException {

        checkHandshakePacketType(version, (flags & 0x30) >> 4);

        int dstConnIdLength = buffer.get();
        // https://tools.ietf.org/html/draft-ietf-quic-transport-27#section-17.2
        // "In QUIC version 1, this value MUST NOT exceed 20.  Endpoints that receive a version 1 long header with a
        // value larger than 20 MUST drop the packet."
        if (dstConnIdLength < 0 || dstConnIdLength > 20) {
            throw new DecodeErrorException("invalid packet length");
        }
        if (buffer.remaining() < dstConnIdLength) {
            throw new DecodeErrorException("invalid packet length");
        }
        byte[] destinationConnectionId = new byte[dstConnIdLength];
        buffer.get(destinationConnectionId);

        int srcConnIdLength = buffer.get();
        if (srcConnIdLength < 0 || srcConnIdLength > 20) {
            throw new DecodeErrorException("invalid packet length");
        }
        if (buffer.remaining() < srcConnIdLength) {
            throw new DecodeErrorException("invalid packet length");
        }
        byte[] sourceConnectionId = new byte[srcConnIdLength];
        buffer.get(sourceConnectionId);

        return new PacketHeader(Level.Handshake, version, flags, posFlags,
                destinationConnectionId, sourceConnectionId, null);
    }

    static PacketReceived parseHandshake(PacketHeader packetHeader, ByteBuffer buffer,
                                         Keys keys, long largestPacketNumber,
                                         @Nullable TransportParameters transportParameters)
            throws DecodeErrorException, DecryptErrorAlert {


        // "The length of the remainder of the packet (that is, the Packet Number and Payload fields) in bytes"
        int length = VariableLengthInteger.parse(buffer);

        return parsePacketNumberAndPayload(packetHeader, buffer,
                length, keys, largestPacketNumber, transportParameters);

    }

    /**
     * @noinspection ExtractMethodRecommender
     */
    static PacketReceived parsePacketNumberAndPayload(
            PacketHeader packetHeader, ByteBuffer buffer, int remainingLength,
            Keys keys, long largestPacketNumber, @Nullable TransportParameters transportParameters)
            throws DecodeErrorException, DecryptErrorAlert {

        if (buffer.remaining() < remainingLength) {
            throw new DecodeErrorException("invalid packet length");
        }

        // https://tools.ietf.org/html/draft-ietf-quic-tls-17#section-5.3
        // "When removing packet protection, an endpoint
        //   first removes the header protection."

        int currentPosition = buffer.position();
        // https://tools.ietf.org/html/draft-ietf-quic-tls-17#section-5.4.2:
        // "The same number of bytes are always sampled, but an allowance needs
        //   to be made for the endpoint removing protection, which will not know
        //   the length of the Packet Number field.  In sampling the packet
        //   ciphertext, the Packet Number field is assumed to be 4 bytes long
        //   (its maximum possible encoded length)."
        if (buffer.remaining() < 4) {
            throw new DecodeErrorException("invalid packet length");
        }
        buffer.position(currentPosition + 4);
        // https://tools.ietf.org/html/draft-ietf-quic-tls-17#section-5.4.2:
        // "This algorithm samples 16 bytes from the packet ciphertext."
        if (buffer.remaining() < 16) {
            throw new DecodeErrorException("invalid packet length");
        }
        byte[] sample = new byte[16];
        buffer.get(sample);
        // https://tools.ietf.org/html/draft-ietf-quic-tls-17#section-5.4.1:


        // "Header protection is applied after packet protection is applied (see
        //   Section 5.3).  The ciphertext of the packet is sampled and used as
        //   input to an encryption algorithm."
        byte[] mask = createHeaderProtectionMask(sample, keys);


        // "The output of this algorithm is a 5 byte mask which is applied to the
        //   protected header fields using exclusive OR.  The least significant
        //   bits of the first byte of the packet are masked by the least
        //   significant bits of the first mask byte

        byte decryptedFlags;
        byte flags = packetHeader.flags();
        if ((flags & 0x80) == 0x80) {
            // Long header: 4 bits masked
            decryptedFlags = (byte) (flags ^ mask[0] & 0x0f);
        } else {
            // Short header: 5 bits masked
            decryptedFlags = (byte) (flags ^ mask[0] & 0x1f);
        }

        short keyPhaseBit = (short) ((decryptedFlags & 0x04) >> 2);

        Keys updated = null;
        if (packetHeader.level() == Level.App) {
            if (keys.checkKeyPhase(keyPhaseBit)) {
                keys = Keys.computeKeyUpdate(packetHeader.version(), keys);
                updated = keys;
            }
        }

        buffer.position(currentPosition);


        // https://tools.ietf.org/html/draft-ietf-quic-tls-17#section-5.4.1:
        // "pn_length = (packet[0] & 0x03) + 1"
        int protectedPackageNumberLength = (decryptedFlags & 0x03) + 1;
        byte[] protectedPackageNumber = new byte[protectedPackageNumberLength];
        buffer.get(protectedPackageNumber);

        byte[] unprotectedPacketNumber = new byte[protectedPackageNumberLength];
        for (int i = 0; i < protectedPackageNumberLength; i++) {
            // https://tools.ietf.org/html/draft-ietf-quic-tls-17#section-5.4.1:
            // " ...and the packet number is
            //   masked with the remaining bytes.  Any unused bytes of mask that might
            //   result from a shorter packet number encoding are unused."
            unprotectedPacketNumber[i] = (byte) (protectedPackageNumber[i] ^ mask[1 + i]);
        }
        long packetNumber = bytesToInt(unprotectedPacketNumber);

        packetNumber = decodePacketNumber(packetNumber, largestPacketNumber,
                protectedPackageNumberLength * 8);

        currentPosition = buffer.position();
        // https://tools.ietf.org/html/draft-ietf-quic-tls-17#section-5.3
        // "The associated data, A, for the AEAD is the contents of the QUIC
        //   header, starting from the flags byte in either the short or long
        //   header, up to and including the unprotected packet number."
        byte[] frameHeader = new byte[buffer.position() - packetHeader.posFlags()];
        buffer.position(packetHeader.posFlags());
        buffer.get(frameHeader);
        frameHeader[0] = decryptedFlags;
        buffer.position(currentPosition);

        // Copy unprotected (decrypted) packet number in frame header, before decrypting payload.
        System.arraycopy(unprotectedPacketNumber, 0, frameHeader, frameHeader.length -
                (protectedPackageNumberLength), protectedPackageNumberLength);

        // "The input plaintext, P, for the AEAD is the payload of the QUIC
        //   packet, as described in [QUIC-TRANSPORT]."
        // "The output ciphertext, C, of the AEAD is transmitted in place of P."
        int encryptedPayloadLength = remainingLength - protectedPackageNumberLength;
        if (encryptedPayloadLength < 1) {
            throw new DecodeErrorException("invalid packet length");
        }


        buffer.position(currentPosition + encryptedPayloadLength);
        byte[] frameBytes = decryptPayload(buffer.array(), currentPosition, encryptedPayloadLength,
                frameHeader, packetNumber, keys);

        FrameReceived[] frames = parseFrames(frameBytes, transportParameters);

        return switch (packetHeader.level()) {
            case Initial -> new PacketReceived.Initial(packetHeader,
                    frames, packetNumber, packetHeader.token());
            case App -> new PacketReceived.ShortHeader(packetHeader,
                    frames, packetNumber, updated);
            case Handshake -> new PacketReceived.Handshake(packetHeader,
                    frames, packetNumber);
        };
    }

    static byte[] createHeaderProtectionMask(byte[] sample, Keys secrets) {
        return PacketService.createHeaderProtectionMask(sample, 4, secrets);
    }

    static boolean isInitial(ByteBuffer data) {
        data.mark();
        int flags = data.get();
        data.rewind();
        return (flags & 0xf0) == 0b1100_0000;
    }

    static int bytesToInt(byte[] data) {
        int value = 0;
        for (byte datum : data) {
            value = (value << 8) | (datum & 0xff);

        }
        return value;
    }

    static long decodePacketNumber(long truncatedPacketNumber, long largestPacketNumber, int bits) {
        long expectedPacketNumber = largestPacketNumber + 1;
        long pnWindow = 1L << bits;
        long pnHalfWindow = pnWindow / 2;
        long pnMask = -pnWindow;

        long candidatePn = (expectedPacketNumber & pnMask) | truncatedPacketNumber;
        int shiftValue = 30; // note this was 62 before (not sure what it means)
        if (candidatePn <= expectedPacketNumber - pnHalfWindow && candidatePn < (1 << shiftValue) - pnWindow) {
            return candidatePn + pnWindow;
        }
        if (candidatePn > expectedPacketNumber + pnHalfWindow && candidatePn >= pnWindow) {
            return candidatePn - pnWindow;
        }

        return candidatePn;
    }

    static byte[] decryptPayload(byte[] input, int inputOffset, int inputLength,
                                 byte[] associatedData, long packetNumber, Keys secrets) throws DecryptErrorAlert {
        ByteBuffer nonceInput = ByteBuffer.allocate(12);
        nonceInput.putInt(0);
        nonceInput.putLong(packetNumber);

        byte[] writeIV = secrets.getWriteIV();
        byte[] nonce = new byte[12];
        int i = 0;
        for (byte b : nonceInput.array())
            nonce[i] = (byte) (b ^ writeIV[i++]);

        return secrets.aeadDecrypt(associatedData, nonce, input, inputOffset, inputLength);
    }

    static FrameReceived[] parseFrames(byte[] frameBytes,
                                       @Nullable TransportParameters transportParameters)
            throws DecodeErrorException {
        ByteBuffer buffer = ByteBuffer.wrap(frameBytes);
        List<FrameReceived> frames = new ArrayList<>();
        byte frameType;
        try {
            while (buffer.remaining() > 0) {
                // https://tools.ietf.org/html/draft-ietf-quic-transport-16#section-12.4
                // "Each frame begins with a Frame Type, indicating its payloadType, followed by additional payloadType-dependent fields"
                frameType = buffer.get();
                switch (frameType) {
                    case 0x00 -> frames.add(FrameReceived.parsePaddingFrame(buffer));
                    case 0x01 -> frames.add(FrameReceived.parsePingFrame());
                    case 0x02, 0x03 -> frames.add(FrameReceived.parseAckFrame(
                            frameType, buffer, transportParameters));
                    case 0x04 -> frames.add(FrameReceived.parseResetStreamFrame(buffer));
                    case 0x05 -> frames.add(FrameReceived.parseStopSendingFrame(buffer));
                    case 0x06 -> frames.add(FrameReceived.parseCryptoFrame(buffer));
                    case 0x07 -> frames.add(FrameReceived.parseNewTokenFrame(buffer));
                    case 0x10 -> frames.add(FrameReceived.parseMaxDataFrame(buffer));
                    case 0x011 -> frames.add(FrameReceived.parseMaxStreamDataFrame(buffer));
                    case 0x12, 0x13 ->
                            frames.add(FrameReceived.parseMaxStreamsFrame(frameType, buffer));
                    case 0x14 -> frames.add(FrameReceived.parseDataBlockedFrame(buffer));
                    case 0x15 -> frames.add(FrameReceived.parsestreamDataBlockedFrame(buffer));
                    case 0x16, 0x17 ->
                            frames.add(FrameReceived.parseStreamsBlockedFrame(frameType, buffer));
                    case 0x18 -> frames.add(FrameReceived.parseNewConnectionIdFrame(buffer));
                    case 0x19 -> frames.add(FrameReceived.parseRetireConnectionIdFrame(buffer));
                    case 0x1a -> frames.add(FrameReceived.parsePathChallengeFrame(buffer));
                    case 0x1b -> frames.add(FrameReceived.parsePathResponseFrame(buffer));
                    case 0x1c, 0x1d ->
                            frames.add(FrameReceived.parseConnectionCloseFrame(frameType, buffer));
                    case 0x1e -> frames.add(FrameReceived.parseHandshakeDoneFrame());
                    case 0x30, 0x31 ->
                            frames.add(FrameReceived.parseDatagramFrame(frameType, buffer));
                    default -> {
                        if ((frameType >= 0x08) && (frameType <= 0x0f)) {
                            frames.add(FrameReceived.parseStreamFrame(frameType, buffer));
                        } else {
                            // https://tools.ietf.org/html/draft-ietf-quic-transport-24#section-12.4
                            // "An endpoint MUST treat the receipt of a frame of unknown payloadType as a connection error of payloadType FRAME_ENCODING_ERROR."
                            throw new DecodeErrorException("Receipt a frame of unknown payloadType");
                        }
                    }
                }
            }
        } catch (IllegalArgumentException e) {
            // Could happen when a frame contains a large int (> 2^32-1) where an int value is expected (see VariableLengthInteger.parse()).
            // Strictly speaking, this would not be an invalid packet, but Kwik cannot handle it.
            throw new DecodeErrorException(e.getMessage());
        } catch (BufferUnderflowException e) {
            throw new DecodeErrorException(e.getMessage());
        }
        FrameReceived[] result = new FrameReceived[frames.size()];
        return frames.toArray(result);
    }


}

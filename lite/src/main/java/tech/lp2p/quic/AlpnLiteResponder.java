package tech.lp2p.quic;

import org.jetbrains.annotations.NotNull;

import java.net.UnknownServiceException;
import java.util.Objects;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.Protocol;
import tech.lp2p.core.Responder;
import tech.lp2p.lite.LiteErrorCode;
import tech.lp2p.proto.Lite;
import tech.lp2p.utils.Utils;


public record AlpnLiteResponder(Responder responder, StreamState streamState)
        implements StreamHandler {

    public static AlpnLiteResponder create(@NotNull Responder responder) {
        return new AlpnLiteResponder(responder, new LiteState(responder));
    }

    @Override
    public void terminated(Stream stream) {
        streamState.reset();
        // responder.closed(stream, state.protocol());
    }

    @Override
    public void fin(Stream stream) {
        streamState.reset();
        // responder.closed(stream, state.protocol());
    }

    @Override
    public void throwable(Throwable throwable) {
        streamState.reset();
        Utils.error("Exception ignored " + throwable.getMessage());
    }


    static class LiteState extends StreamState {
        private final Responder responder;

        LiteState(Responder responder) {
            this.responder = responder;
        }

        public void accept(Stream stream, byte[] frame) {
            new Thread(() -> {
                try {
                    Lite.ProtoSelect protoSelect = Lite.ProtoSelect.parseFrom(frame);
                    byte[] data = protoSelect.getData().toByteArray();
                    Objects.requireNonNull(data);
                    Protocol protocol = Protocol.codec(ALPN.lite, protoSelect.getProtocol().getNumber());
                    Objects.requireNonNull(protocol);

                    responder.data(stream, protocol, data);
                } catch (UnknownServiceException unknownServiceException) {
                    Utils.error("Exception ignored " + unknownServiceException.getMessage());
                    stream.resetStream(LiteErrorCode.PROTOCOL_NEGOTIATION_FAILED);
                    reset();
                } catch (Throwable throwable) {
                    Utils.error("Exception ignored " + throwable);
                    stream.resetStream(LiteErrorCode.INTERNAL_ERROR);
                    reset();
                }
            }).start();
        }
    }
}

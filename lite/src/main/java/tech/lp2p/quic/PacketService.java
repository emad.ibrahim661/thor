package tech.lp2p.quic;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;

import tech.lp2p.utils.Utils;

interface PacketService {


    static int estimatedFramesSize(Frame[] frames) {
        int size = 0;
        for (Frame frame : frames) {
            size += frame.frameLength();
        }
        return size;
    }

    static Packet.InitialPacket createInitial(
            Version version, Frame[] frames, long packetNumber,
            byte[] sourceConnectionId, byte[] destinationConnectionId) {
        return new Packet.InitialPacket(version, destinationConnectionId, sourceConnectionId,
                frames, packetNumber, null);
    }

    /**
     * Constructs a short header packet for sending (client role).
     */
    static Packet.ShortHeaderPacket createShortHeader(Version version,
                                                      Frame[] frames,
                                                      long packetNumber,
                                                      byte[] destinationConnectionId) {
        return new Packet.ShortHeaderPacket(version, destinationConnectionId,
                frames, packetNumber);
    }

    static Packet.HandshakePacket createHandshake(Version version,
                                                  Frame[] frames,
                                                  long packetNumber,
                                                  byte[] sourceConnectionId,
                                                  byte[] destinationConnectionId) {

        return new Packet.HandshakePacket(version, destinationConnectionId,
                sourceConnectionId, frames, packetNumber);

    }


    static byte[] protectPacketNumberAndPayload(byte[] additionalData,
                                                int packetNumberSize, byte[] payload,
                                                Keys clientSecrets, long packetNumber) {

        int packetNumberPosition = additionalData.length - packetNumberSize;

        // From https://tools.ietf.org/html/draft-ietf-quic-tls-16#section-5.3:
        // "The associated data, A, for the AEAD is the contents of the QUIC
        //   header, starting from the flags octet in either the short or long
        //   header, up to and including the unprotected packet number."

        byte[] encryptedPayload = PacketService.encryptPayload(
                payload, 0, payload.length,
                additionalData, packetNumber, clientSecrets);


        byte[] encodedPacketNumber = PacketService.encodePacketNumber(packetNumber);
        byte[] mask = createHeaderProtectionMask(encryptedPayload,
                encodedPacketNumber.length, clientSecrets);


        for (int i = 0; i < encodedPacketNumber.length; i++) {
            additionalData[i + packetNumberPosition] =
                    (byte) (encodedPacketNumber[i] ^ mask[1 + i]);
        }

        byte flags = additionalData[0];
        if ((flags & 0x80) == 0x80) {
            // Long header: 4 bits masked
            flags ^= (byte) (mask[0] & 0x0f);
        } else {
            // Short header: 5 bits masked
            flags ^= (byte) (mask[0] & 0x1f);
        }
        additionalData[0] = flags;

        return Utils.concat(additionalData, encryptedPayload);

    }

    static byte[] encodePacketNumber(long packetNumber) {
        if (packetNumber <= 0xff) {
            return new byte[]{(byte) packetNumber};
        } else if (packetNumber <= 0xffff) {
            return new byte[]{(byte) (packetNumber >> 8), (byte) (packetNumber & 0x00ff)};
        } else if (packetNumber <= 0xffffff) {
            return new byte[]{(byte) (packetNumber >> 16), (byte) (packetNumber >> 8), (byte) (packetNumber & 0x00ff)};
        } else if (packetNumber <= 0xffffffffL) {
            return new byte[]{(byte) (packetNumber >> 24), (byte) (packetNumber >> 16), (byte) (packetNumber >> 8), (byte) (packetNumber & 0x00ff)};
        } else {
            throw new IllegalStateException(" not yet implemented cannot encode pn > 4 bytes");
        }
    }


    static byte[] createHeaderProtectionMask(byte[] ciphertext, int encodedPacketNumberLength, Keys secrets) {
        // https://tools.ietf.org/html/draft-ietf-quic-tls-17#section-5.4
        // "The same number of bytes are always sampled, but an allowance needs
        //   to be made for the endpoint removing protection, which will not know
        //   the length of the Packet Number field.  In sampling the packet
        //   ciphertext, the Packet Number field is assumed to be 4 bytes long
        //   (its maximum possible encoded length)."
        int sampleOffset = 4 - encodedPacketNumberLength;
        byte[] sample = new byte[16];
        System.arraycopy(ciphertext, sampleOffset, sample, 0, 16);

        return secrets.createHeaderProtectionMask(sample);
    }

    static byte[] encryptPayload(byte[] input, int inputOffset, int inputLength,
                                 byte[] associatedData, long packetNumber, Keys secrets) {

        // From https://tools.ietf.org/html/draft-ietf-quic-tls-16#section-5.3:
        // "The nonce, N, is formed by combining the packet
        //   protection IV with the packet number.  The 64 bits of the
        //   reconstructed QUIC packet number in network byte order are left-
        //   padded with zeros to the size of the IV.  The exclusive OR of the
        //   padded packet number and the IV forms the AEAD nonce"
        byte[] writeIV = secrets.getWriteIV();
        ByteBuffer nonceInput = ByteBuffer.allocate(writeIV.length);
        for (int i = 0; i < nonceInput.capacity() - 8; i++)
            nonceInput.put((byte) 0x00);
        nonceInput.putLong(packetNumber);

        byte[] nonce = new byte[12];
        int i = 0;
        for (byte b : nonceInput.array())
            nonce[i] = (byte) (b ^ writeIV[i++]);

        return secrets.aeadEncrypt(associatedData, nonce, input, inputOffset, inputLength);
    }

    /**
     * Updates the given flags byte to encode the packet number length that is used for encoding the given packet number.
     */
    static byte encodePacketNumberLength(byte flags, long packetNumber) {
        if (packetNumber <= 0xff) {
            return flags;
        } else if (packetNumber <= 0xffff) {
            return (byte) (flags | 0x01);
        } else if (packetNumber <= 0xffffff) {
            return (byte) (flags | 0x02);
        } else if (packetNumber <= 0xffffffffL) {
            return (byte) (flags | 0x03);
        } else {
            throw new IllegalStateException("not yet implemented cannot encode pn > 4 bytes");
        }
    }

    static byte[] generatePayloadBytes(Frame[] frames, int encodedPacketNumberLength) {

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream(
                Settings.MAX_PACKET_SIZE)) {

            for (Frame frame : frames) {
                outputStream.write(frame.frameBytes());
            }

            int serializeFramesLength = outputStream.size();
            // https://tools.ietf.org/html/draft-ietf-quic-tls-27#section-5.4.2
            // "To ensure that sufficient data is available for sampling, packets are
            //   padded so that the combined lengths of the encoded packet number and
            //   protected payload is at least 4 bytes longer than the sample required
            //   for header protection."

            // "To ensure that sufficient data is available for sampling, packets are padded so that the combined lengths
            //   of the encoded packet number and protected payload is at least 4 bytes longer than the sample required
            //   for header protection. (...). This results in needing at least 3 bytes of frames in the unprotected payload
            //   if the packet number is encoded on a single byte, or 2 bytes of frames for a 2-byte packet number encoding."
            if (encodedPacketNumberLength + serializeFramesLength < 4) {
                Frame paddingFrame = Frame.createPaddingFrame(4 - encodedPacketNumberLength - serializeFramesLength);
                //frames.add(paddingFrame); // this might be necessary, but so far no failure detected
                outputStream.write(paddingFrame.frameBytes());
            }

            return outputStream.toByteArray();
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable.getMessage());
        }

    }


}

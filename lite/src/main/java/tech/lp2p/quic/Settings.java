package tech.lp2p.quic;

import java.util.function.Consumer;

public interface Settings {

    Consumer<Packet> EMPTY_PACKET_CALLBACK = p -> {
    };

    Consumer<Frame> EMPTY_FRAME_CALLBACK = f -> {
    };


    int NOT_DEFINED = -1;
    Frame[] FRAMES_EMPTY = new Frame[0];


    // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-17.2
    // "In QUIC version 1, this value MUST NOT exceed 20 bytes"
    int DEFAULT_CID_LENGTH = 8;

    // https://datatracker.ietf.org/doc/html/rfc9002#name-variables-of-interest-2
    // The sender's current maximum payload size. This does not include UDP or IP overhead.
    // The max datagram size is used for congestion window computations. An endpoint sets the
    // value of this variable based on its Path Maximum Transmission Unit (PMTU; see Section
    // 14.2 of [QUIC-TRANSPORT]), with a minimum value of 1200 bytes.
    int MAX_DATAGRAM_SIZE = 1200;


    // https://datatracker.ietf.org/doc/html/rfc9002#initial-cwnd
    // QUIC begins every connection in slow start with the congestion window set to an
    // initial value. Endpoints SHOULD use an initial congestion window of ten times the
    // maximum datagram size (max_datagram_size), while limiting the window to the larger
    // of 14,720 bytes or twice the maximum datagram size. This follows the analysis
    // and recommendations in [RFC6928], increasing the byte limit to account for the smaller
    // 8-byte overhead of UDP compared to the 20-byte overhead for TCP.
    //
    // If the maximum datagram size changes during the connection, the initial congestion
    // window SHOULD be recalculated with the new size. If the maximum datagram size is
    // decreased in order to complete the handshake, the congestion window SHOULD be set
    // to the new initial congestion window.
    //
    // Prior to validating the client's address, the server can be further limited by
    // the anti-amplification limit as specified in Section 8.1 of [QUIC-TRANSPORT]. T
    // hough the anti-amplification limit can prevent the congestion window from being fully
    // utilized and therefore slow down the increase in congestion window, it does not
    // directly affect the congestion window.
    //
    // The minimum congestion window is the smallest value the congestion window can attain
    // in response to loss, an increase in the peer-reported ECN-CE count, or persistent
    // congestion. The RECOMMENDED value is 2 * max_datagram_size.
    // Endpoints SHOULD use an initial congestion window of ten times the maximum datagram
    // size (max_datagram_size), while limiting the window to the larger of 14,720
    // bytes or twice the maximum datagram size.
    int INITIAL_CONGESTION_WINDOW = 10 * MAX_DATAGRAM_SIZE;

    int MINIMUM_CONGESTION_WINDOW = 2 * MAX_DATAGRAM_SIZE;


    // https://tools.ietf.org/html/draft-ietf-quic-recovery-23#appendix-B.1
    // "Reduction in congestion window when a new loss event is detected.  The RECOMMENDED value is 0.5."
    int CONGESTION_LOSS_REDUCTION_FACTOR = 2; // note how it is used

    // https://tools.ietf.org/html/draft-ietf-quic-transport-17#section-14.1:
    // "An endpoint SHOULD use Datagram Packetization Layer PMTU Discovery
    //   ([DPLPMTUD]) or implement Path MTU Discovery (PMTUD) [RFC1191]
    //   [RFC8201] ..."
    // "In the absence of these mechanisms, QUIC endpoints SHOULD NOT send IP
    //   packets larger than 1280 bytes.  Assuming the minimum IP header size,
    //   this results in a QUIC maximum packet size of 1232 bytes for IPv6 and
    //   1252 bytes for IPv4."
    // As it is not know (yet) whether running over IP4 or IP6, take the smallest of the two:
    int MAX_PACKAGE_SIZE = 1232;

    // https://tools.ietf.org/html/draft-ietf-quic-recovery-20#section-6.2
    // "If no previous RTT is available, or if the network
    //   changes, the initial RTT SHOULD be set to 500ms"
    int INITIAL_RTT = 500;
    int SERVER_INITIAL_RTT = 100; // this value was given previously

    boolean LEN_BIT_SET = true;

    int DEFAULT_ACTIVE_CONNECTION_ID_LIMIT = 2;  // default init value
    int DEFAULT_MAX_ACK_DELAY = 25;  // default init value
    int DEFAULT_ACK_DELAY_EXPONENT = 3; // default init value
    int DEFAULT_MAX_UDP_PAYLOAD_SIZE = 65527; // default init value

    boolean DISABLE_MIGRATION = true; // all other remote libp2p clients have same value
    int ACTIVE_CONNECTION_ID_LIMIT = 4;  // all other remote libp2p clients have same value
    int MAX_ACK_DELAY = 26;  // all other remote libp2p clients have same value
    int ACK_DELAY_EXPONENT = 3; // all other remote libp2p clients have same value
    int MAX_UDP_PAYLOAD_SIZE = 1452; // all other remote libp2p clients have same value
    int MAX_STREAMS_BIDI = 256; // all other remote libp2p clients have same value
    int MAX_STREAMS_UNI = 5; // all other remote libp2p clients have same value
    int INITIAL_MAX_DATA = 786432; // all other remote libp2p clients have same value
    int INITIAL_MAX_STREAM_DATA = 524288; // all other remote libp2p clients have same value
    int MAX_DATAGRAM_FRAME_SIZE = 1200;  // all other remote libp2p clients have same value
    int MAX_IDLE_TIMEOUT = 30000; // all other remote libp2p clients have same value

    int STREAM_TIMEOUT = 1; // 1 sec stream timeout creation

    // https://www.rfc-editor.org/rfc/rfc9000.html#name-handshake-packet
    // "A Handshake packet uses long headers with a payloadType value of 0x02, ..."
    int HANDSHAKE_V1_type = 2;
    // https://www.ietf.org/archive/id/draft-ietf-quic-v2-01.html#name-long-header-packet-types
    // "Handshake packets use a packet payloadType field of 0b11."
    int HANDSHAKE_V2_type = 3;

    // https://www.rfc-editor.org/rfc/rfc9000.html#name-initial-packet
    // "An Initial packet uses long headers with a payloadType value of 0x00."
    int INITIAL_V1_type = 0;
    // https://www.ietf.org/archive/id/draft-ietf-quic-v2-01.html#name-long-header-packet-types
    // Initial packets use a packet payloadType field of 0b01.
    int INITIAL_V2_type = 1;

    int MAX_PACKET_SIZE = 1500;


    // NOTE: this is the default value for the ack scale
    // ((int) Math.pow(2, Settings.ACK_DELAY_EXPONENT)) and it is valid as long
    // Settings.ACK_DELAY_EXPONENT is used as default for client and server and
    // do not change by a user defined value
    int ACK_DELAY_SCALE = 8;

    // used in receiver
    int MAX_DATAGRAM_PACKET_SIZE = 1500;

    long PING_INTERVAL = 15000; // MAX_IDLE_TIMEOUT / 2

    long SHUTDOWN_TIME = INITIAL_RTT;
}

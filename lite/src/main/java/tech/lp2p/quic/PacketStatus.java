package tech.lp2p.quic;


import java.util.function.Consumer;


final class PacketStatus {

    private final long timeSent;
    private final int size;
    private final Packet packet;
    private final Consumer<Packet> lostPacketCallback;
    private final Consumer<Packet> receivedPacketCallback;
    private volatile boolean lost;
    private volatile boolean acked;

    PacketStatus(Packet packet, long timeSent, int size, Consumer<Packet> lostPacketCallback
            , Consumer<Packet> receivedPacketCallback) {
        this.timeSent = timeSent;
        this.size = size;
        this.packet = packet;
        this.lostPacketCallback = lostPacketCallback;
        this.receivedPacketCallback = receivedPacketCallback;
    }

    public int size() {
        return size;
    }

    boolean acked() {
        return acked;
    }

    boolean setAcked() {
        if (!acked && !lost) {
            acked = true;
            return true;
        } else {
            return false;
        }
    }

    boolean inFlight() {
        return !acked && !lost;
    }

    boolean setLost() {
        if (!acked && !lost) {
            lost = true;
            return true;
        } else {
            return false;
        }
    }


    public long timeSent() {
        return timeSent;
    }

    public Packet packet() {
        return packet;
    }

    Consumer<Packet> lostPacketCallback() {
        return lostPacketCallback;
    }

    Consumer<Packet> receivedPacketCallback() {
        return receivedPacketCallback;
    }

}


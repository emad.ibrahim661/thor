package tech.lp2p.quic;


import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import tech.lp2p.utils.Utils;

/**
 * Base class for all classes that represent a QUIC frame.
 * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#frames">...</a>
 */
public record Frame(FrameType frameType, byte[] frameBytes) {

    /**
     * <a href="https://tools.ietf.org/html/draft-ietf-quic-transport-25#section-19.20">...</a>
     */
    public static final Frame HANDSHAKE_DONE = createHandshakeDoneFrame();

    // https://www.rfc-editor.org/rfc/rfc9000.html#name-terms-and-definitions
    // https://tools.ietf.org/html/draft-ietf-quic-recovery-33#section-2
    // "All frames other than ACK, PADDING, and CONNECTION_CLOSE are considered ack-eliciting."
    /**
     * Represents a ping frame.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-ping-frames">...</a>
     */
    public static final Frame PING = createPingFrame();
    private static final Random random = new Random();

    /**
     * <a href="https://datatracker.ietf.org/doc/html/rfc9221">...</a>
     * DATAGRAM frames are used to transmit application data in an unreliable manner.
     * The Type field in the DATAGRAM frame takes the form 0b0011000X (or the values 0x30 and 0x31).
     * The least significant bit of the Type field in the DATAGRAM frame is the LEN bit (0x01),
     * which indicates whether there is a Length field present: if this bit is set to 0, the
     * Length field is absent and the Datagram Data field extends to the end of the packet;
     * if this bit is set to 1, the Length field is present.
     */
    public static Frame createDatagramFrame(byte[] datagram) throws TransportError {
        if (datagram.length > Settings.MAX_DATAGRAM_FRAME_SIZE) {
            throw new TransportError(TransportError.Code.PROTOCOL_VIOLATION,
                    "Datagram length is invalid");
        }


        int sizeEnc = 0;
        boolean lenBitSet = Settings.LEN_BIT_SET;
        if (lenBitSet) {
            sizeEnc = VariableLengthInteger.bytesNeeded(datagram.length);
        }
        int capacity = 1 + sizeEnc + datagram.length; // 1 is the payloadType
        ByteBuffer buffer = ByteBuffer.allocate(capacity);
        if (lenBitSet) {
            buffer.put((byte) 0x31); // payloadType
            VariableLengthInteger.encode(datagram.length, buffer); // length is now encoded
        } else {
            buffer.put((byte) 0x30); // payloadType
        }
        buffer.put(datagram);

        return new Frame(FrameType.DatagramFrame, buffer.array());
    }

    /**
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-ack-frames">...</a>
     * Creates an AckFrame given a (sorted, non-adjacent) list of ranges and an ack delay.
     * <p>
     * ackDelay is value in milliseconds
     * <p>
     */
    public static Frame createAckFrame(Long[] packets, int ackDelay) {

        List<byte[]> ackRanges = new ArrayList<>();
        int ackRangesBytesSize = 0;
        Long to;
        int ackRangesSize = 0;

        long largestAcknowledgedTo = 0;
        long largestAcknowledgedFrom = 0;
        long smallest = 0;
        for (int i = packets.length - 1; i >= 0; i--) {
            to = packets[i];
            // now get the to
            long from = to;
            boolean check = true;
            while (check) {
                int index = i - 1;
                if (index >= 0) {
                    Long value = packets[index];
                    if (value == from - 1) {
                        i--;
                        from = value;
                    } else {
                        check = false;
                    }
                } else {
                    check = false;
                }
            }

            if (ackRangesSize == 0) {
                largestAcknowledgedTo = to;
                largestAcknowledgedFrom = from;
            } else {
                // https://www.rfc-editor.org/rfc/rfc9000.html#name-ack-frames
                // "Gap: A variable-length integer indicating the number of contiguous unacknowledged packets preceding the
                //  packet number one lower than the smallest in the preceding ACK Range."
                int gap = (int) (smallest - to - 2);  // e.g. 9..9, 5..4 => un-acked: 8, 7, 6; gap: 2
                // "ACK Range Length: A variable-length integer indicating the number of contiguous acknowledged packets
                //  preceding the largest packet number, as determined by the preceding Gap."
                //     ACK Range (..) ...,
                long distance = to - from;

                int capacity =
                        VariableLengthInteger.bytesNeeded(gap) +
                                VariableLengthInteger.bytesNeeded(distance);
                ByteBuffer bufferAckRange = ByteBuffer.allocate(capacity);
                VariableLengthInteger.encode(gap, bufferAckRange);
                VariableLengthInteger.encode(distance, bufferAckRange);
                byte[] arrayAckRange = bufferAckRange.array();
                ackRangesBytesSize += arrayAckRange.length;
                ackRanges.add(arrayAckRange);
            }
            smallest = from;
            ackRangesSize++;

        }


        // ackDelay : A variable-length integer encoding the acknowledgment
        // delay in microseconds; see Section 13.2.5.  It is decoded by
        // multiplying the value in the field by 2 to the power of the
        // ack_delay_exponent transport parameter sent by the sender of the
        // ACK frame; see Section 18.2.  Compared to simply expressing the
        // delay as an integer, this encoding allows for a larger range of
        // values within the same number of bytes, at the cost of lower
        // resolution.
        long encodedAckDelay = ackDelay * 1000L / Settings.ACK_DELAY_SCALE;


        // ACK frames are formatted as shown in Figure 25.
        //
        //   ACK Frame {
        //     Type (i) = 0x02..0x03,
        //     Largest Acknowledged (i),
        //     ACK Delay (i),
        //     ACK Range Count (i),
        //     First ACK Range (i),
        //     ACK Range (..) ...,
        //     [ECN Counts (..)],
        //   }


        long firstAckRange = largestAcknowledgedTo - largestAcknowledgedFrom;
        int bufferSize = 1 +
                VariableLengthInteger.bytesNeeded(largestAcknowledgedTo) +
                VariableLengthInteger.bytesNeeded(encodedAckDelay) +
                VariableLengthInteger.bytesNeeded(ackRanges.size()) +
                VariableLengthInteger.bytesNeeded(firstAckRange) + ackRangesBytesSize;

        ByteBuffer buffer = ByteBuffer.allocate(bufferSize);
        buffer.put((byte) 0x02); // only AckFrame of payloadType 0x02 is supported
        VariableLengthInteger.encode(largestAcknowledgedTo, buffer);  // Largest Acknowledged (i),
        VariableLengthInteger.encode(encodedAckDelay, buffer);  //  ACK Delay (i),
        VariableLengthInteger.encode(ackRanges.size(), buffer); // ACK Range Count (i)
        VariableLengthInteger.encode(firstAckRange, buffer);  // First ACK Range (i)
        for (byte[] ackRange : ackRanges) {
            buffer.put(ackRange); //     ACK Range (..) ...,
        }


        return new Frame(FrameType.AckFrame, buffer.array());
    }

    /**
     * Represents a crypto frame.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-crypto-frames">...</a>
     */
    public static Frame createCryptoFrame(long offset, byte[] payload) {


        ByteBuffer buffer = ByteBuffer.allocate(1
                + VariableLengthInteger.bytesNeeded(offset)
                + VariableLengthInteger.bytesNeeded(payload.length)
                + payload.length);
        buffer.put((byte) 0x06);
        VariableLengthInteger.encode(offset, buffer);
        VariableLengthInteger.encode(payload.length, buffer);
        buffer.put(payload);


        return new Frame(FrameType.CryptoFrame, buffer.array());
    }

    public static int frameLength(int streamId, long offset, int length) {
        return 1  // frame payloadType
                + VariableLengthInteger.bytesNeeded(streamId)
                + VariableLengthInteger.bytesNeeded(offset)
                + VariableLengthInteger.bytesNeeded(length)
                + length;
    }

    public static Frame createStreamFrame(
            int streamId, long offset, byte[] applicationData, boolean fin) {

        int frameLength = frameLength(streamId, offset, applicationData.length);

        ByteBuffer buffer = ByteBuffer.allocate(frameLength);
        byte baseType = (byte) 0x08;
        byte frameType = (byte) (baseType | 0x04 | 0x02);  // OFF-bit, LEN-bit, (no) FIN-bit
        if (fin) {
            frameType |= 0x01;
        }
        buffer.put(frameType);
        VariableLengthInteger.encode(streamId, buffer);
        VariableLengthInteger.encode(offset, buffer);
        VariableLengthInteger.encode(applicationData.length, buffer);
        buffer.put(applicationData);

        return new Frame(FrameType.StreamFrame, buffer.array());
    }

    /**
     * Creates a connection close frame for a normal connection close without errors
     */
    public static Frame createConnectionCloseFrame() {

        // https://tools.ietf.org/html/draft-ietf-quic-transport-24#section-20
        // "NO_ERROR (0x0):  An endpoint uses this with CONNECTION_CLOSE to
        //      signal that the connection is being closed abruptly in the absence
        //      of any error."
        return createConnectionCloseFrame(
                new TransportError(TransportError.Code.NO_ERROR, ""));
    }

    /**
     * Represents a connection close frame.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-connection_close-frames">...</a>
     */
    public static Frame createConnectionCloseFrame(TransportError transportError) {
        int frameType = 0x1c;
        long errorCode = transportError.errorCode();

        byte[] reasonPhrase = Utils.BYTES_EMPTY;
        String reason = transportError.transportErrorMessage();
        if (reason != null && !reason.isBlank()) {
            reasonPhrase = reason.getBytes(StandardCharsets.UTF_8);
        }

        ByteBuffer buffer = ByteBuffer.allocate(1
                + VariableLengthInteger.bytesNeeded(errorCode)
                + VariableLengthInteger.bytesNeeded(0)
                + VariableLengthInteger.bytesNeeded(reasonPhrase.length)
                + reasonPhrase.length);
        buffer.put((byte) frameType);
        VariableLengthInteger.encode(errorCode, buffer);
        VariableLengthInteger.encode(0, buffer);
        VariableLengthInteger.encode(reasonPhrase.length, buffer);
        buffer.put(reasonPhrase);

        return new Frame(FrameType.ConnectionCloseFrame, buffer.array());
    }

    /**
     * Represents a data blocked frame.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-data_blocked-frames">...</a>
     */
    public static Frame createDataBlockedFrame(long streamDataLimit) {
        ByteBuffer buffer = ByteBuffer.allocate(1 + VariableLengthInteger.bytesNeeded(streamDataLimit));
        buffer.put((byte) 0x14);
        VariableLengthInteger.encode(streamDataLimit, buffer);
        return new Frame(FrameType.DataBlockedFrame, buffer.array());
    }

    /**
     * Represents a stream data blocked frame.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-stream_data_blocked-frames">...</a>
     */
    public static Frame createStreamDataBlockedFrame(int streamId, long streamDataLimit) {
        ByteBuffer buffer = ByteBuffer.allocate(1 + VariableLengthInteger.bytesNeeded(streamId)
                + VariableLengthInteger.bytesNeeded(streamDataLimit));
        buffer.put((byte) 0x15);
        VariableLengthInteger.encode(streamId, buffer);
        VariableLengthInteger.encode(streamDataLimit, buffer);
        return new Frame(FrameType.StreamDataBlockedFrame, buffer.array());
    }

    static Frame createHandshakeDoneFrame() {
        ByteBuffer buffer = ByteBuffer.allocate(1);
        buffer.put((byte) 0x1e);
        return new Frame(FrameType.HandshakeDoneFrame, buffer.array());
    }

    public static Frame createMaxDataFrame(long maxData) {
        ByteBuffer buffer = ByteBuffer.allocate(1 + VariableLengthInteger.bytesNeeded(maxData));
        buffer.put((byte) 0x10);
        VariableLengthInteger.encode(maxData, buffer);

        return new Frame(FrameType.MaxDataFrame, buffer.array());
    }

    static Frame createPingFrame() {
        ByteBuffer buffer = ByteBuffer.allocate(1);
        buffer.put((byte) 0x01);
        return new Frame(FrameType.PingFrame, buffer.array());
    }

    /**
     * Represents a number of consecutive padding frames.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-padding-frames">...</a>
     * <p>
     * Usually, padding will consist of multiple padding frames, each being exactly one (zero) byte. This class can
     * represent an arbitrary number of consecutive padding frames, by recording padding length.
     */
    public static Frame createPaddingFrame(int length) {
        return new Frame(FrameType.PaddingFrame, new byte[length]);
    }

    /**
     * Represents a new token frame.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-new_token-frames">...</a>
     */
    @SuppressWarnings("unused")
    static Frame createNewTokenFrame(byte[] token) {
        ByteBuffer buffer = ByteBuffer.allocate(1 +
                VariableLengthInteger.bytesNeeded(token.length) + token.length);
        buffer.put((byte) 0x07);
        VariableLengthInteger.encode(token.length, buffer);
        buffer.put(token);
        return new Frame(FrameType.NewTokenFrame, buffer.array());
    }

    /**
     * Represents a reset stream frame.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-reset_stream-frames">...</a>
     */
    public static Frame createResetStreamFrame(int streamId, long errorCode,
                                               long finalSize) {
        ByteBuffer buffer = ByteBuffer.allocate(1
                + VariableLengthInteger.bytesNeeded(streamId)
                + VariableLengthInteger.bytesNeeded(errorCode)
                + VariableLengthInteger.bytesNeeded(finalSize));
        buffer.put((byte) 0x04);
        VariableLengthInteger.encode(streamId, buffer);
        VariableLengthInteger.encode(errorCode, buffer);
        VariableLengthInteger.encode(finalSize, buffer);
        return new Frame(FrameType.ResetStreamFrame, buffer.array());
    }

    /**
     * Represents a max stream data frame.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-max_stream_data-frames">...</a>
     */
    public static Frame createMaxStreamDataFrame(int streamId, long maxData) {
        ByteBuffer buffer = ByteBuffer.allocate(1 +
                VariableLengthInteger.bytesNeeded(streamId) +
                VariableLengthInteger.bytesNeeded(maxData));
        // https://www.rfc-editor.org/rfc/rfc9000.html#name-max_stream_data-frames
        // "The MAX_STREAM_DATA frame (payloadType=0x11)..."
        buffer.put((byte) 0x11);
        VariableLengthInteger.encode(streamId, buffer);
        VariableLengthInteger.encode(maxData, buffer);
        return new Frame(FrameType.MaxStreamDataFrame, buffer.array());

    }

    /**
     * Represents a max streams frame.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-max_streams-frames">...</a>
     */
    public static Frame createMaxStreamsFrame(long maxStreams,
                                              boolean appliesToBidirectional) {
        ByteBuffer buffer = ByteBuffer.allocate(1 +
                VariableLengthInteger.bytesNeeded(maxStreams));
        buffer.put((byte) (appliesToBidirectional ? 0x12 : 0x13));
        VariableLengthInteger.encode(maxStreams, buffer);
        return new Frame(FrameType.MaxStreamsFrame, buffer.array());
    }

    /**
     * Represents a new connection id frame.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-new_connection_id-frames">...</a>
     */
    public static Frame createNewConnectionIdFrame(int sequenceNr, int retirePriorTo, byte[] connectionId) {
        byte[] statelessResetToken = new byte[16];
        random.nextBytes(statelessResetToken);

        ByteBuffer buffer = ByteBuffer.allocate(1 + VariableLengthInteger.bytesNeeded(sequenceNr)
                + VariableLengthInteger.bytesNeeded(retirePriorTo)
                + 1 + connectionId.length + 16);

        buffer.put((byte) 0x18);
        VariableLengthInteger.encode(sequenceNr, buffer);
        VariableLengthInteger.encode(retirePriorTo, buffer);
        buffer.put((byte) connectionId.length);
        buffer.put(connectionId);
        buffer.put(statelessResetToken);

        return new Frame(FrameType.NewConnectionIdFrame, buffer.array());
    }

    /**
     * Represents a path challenge frame.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-path_challenge-frames">...</a>
     */
    @SuppressWarnings("unused")
    static Frame createPathChallengeFrame(byte[] data) {
        ByteBuffer buffer = ByteBuffer.allocate(1 + 8);
        buffer.put((byte) 0x1a);
        buffer.put(data);
        return new Frame(FrameType.PathChallengeFrame, buffer.array());
    }

    /**
     * Represents a path response frame.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-path_response-frames">...</a>
     */
    public static Frame createPathResponseFrame(byte[] data) {
        ByteBuffer buffer = ByteBuffer.allocate(1 + 8);
        buffer.put((byte) 0x1b);
        buffer.put(data);
        return new Frame(FrameType.PathResponseFrame, buffer.array());
    }

    /**
     * Represents a retire connection id frame.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-retire_connection_id-frames">...</a>
     */
    public static Frame createRetireConnectionsIdFrame(int sequenceNumber) {
        ByteBuffer buffer = ByteBuffer.allocate(1 +
                VariableLengthInteger.bytesNeeded(sequenceNumber));
        buffer.put((byte) 0x19);
        VariableLengthInteger.encode(sequenceNumber, buffer);
        return new Frame(FrameType.RetireConnectionIdFrame, buffer.array());
    }

    /**
     * Represents a stop sending frame.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-stop_sending-frames">...</a>
     */
    public static Frame createStopSendingFrame(int streamId, long errorCode) {
        ByteBuffer buffer = ByteBuffer.allocate(1
                + VariableLengthInteger.bytesNeeded(streamId)
                + VariableLengthInteger.bytesNeeded(errorCode));
        buffer.put((byte) 0x05);
        VariableLengthInteger.encode(streamId, buffer);
        VariableLengthInteger.encode(errorCode, buffer);
        return new Frame(FrameType.StopSendingFrame, buffer.array());
    }

    /**
     * Represents a streams blocked frame.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-streams_blocked-frames">...</a>
     */
    @SuppressWarnings("unused")
    static Frame createStreamsBlockedFrame(boolean bidirectional, int streamLimit) {
        ByteBuffer buffer = ByteBuffer.allocate(1 +
                VariableLengthInteger.bytesNeeded(streamLimit));
        // https://www.rfc-editor.org/rfc/rfc9000.html#name-streams_blocked-frames
        // "A STREAMS_BLOCKED frame of payloadType 0x16 is used to indicate reaching the bidirectional stream limit, and a
        // STREAMS_BLOCKED frame of payloadType 0x17 is used to indicate reaching the unidirectional stream limit."
        buffer.put(bidirectional ? (byte) 0x16 : (byte) 0x17);
        VariableLengthInteger.encode(streamLimit, buffer);
        return new Frame(FrameType.StreamsBlockedFrame, buffer.array());
    }

    public static int getStreamDataBlockedFrameMaxSize(int streamId) {
        return 1 + VariableLengthInteger.bytesNeeded(streamId) + 8;
    }

    /**
     * Returns an upper bound for the size of a frame with the given parameters. A frame created with these parameters
     * will never have a size larger than this upper bound.
     */
    public static int getMaximumResetStreamFrameSize(int streamId, long errorCode) {
        int maxFinalSizeLength = 8;
        return 1 + VariableLengthInteger.bytesNeeded(streamId) + VariableLengthInteger.bytesNeeded(errorCode) + maxFinalSizeLength;
    }

    /**
     * Returns whether the frame is ack eliciting
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-terms-and-definitions">...</a>
     * "Ack-eliciting packet: A QUIC packet that contains frames other than ACK, PADDING, and CONNECTION_CLOSE."
     *
     * @return true when the frame is ack-eliciting
     */
    public static boolean isAckEliciting(Frame frame) {
        return switch (frame.frameType()) {
            case AckFrame, PaddingFrame, ConnectionCloseFrame -> false;
            default -> true;
        };
    }

    public boolean isStreamFrameFin() {
        if (frameType == FrameType.StreamFrame) {
            if (frameBytes.length > 0) {
                byte type = frameBytes[0];
                return ((type & 0x01) == 0x01);
            }
        }
        return false;
    }

    public int frameLength() {
        return frameBytes().length;
    }

}

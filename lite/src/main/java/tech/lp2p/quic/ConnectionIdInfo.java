package tech.lp2p.quic;


final class ConnectionIdInfo implements Comparable<ConnectionIdInfo> {

    private final int sequenceNumber;
    private volatile byte[] connectionId;
    private volatile byte[] statelessResetToken;
    private volatile ConnectionIdStatus connectionIdStatus;


    ConnectionIdInfo(int sequenceNumber, byte[] connectionId,
                     byte[] statelessResetToken, ConnectionIdStatus status) {
        this.sequenceNumber = sequenceNumber;
        this.connectionId = connectionId;
        this.connectionIdStatus = status;
        this.statelessResetToken = statelessResetToken;
    }

    void statelessResetToken(byte[] statelessResetToken) {
        this.statelessResetToken = statelessResetToken;
    }

    int getSequenceNumber() {
        return sequenceNumber;
    }

    public byte[] getConnectionId() {
        return connectionId;
    }

    ConnectionIdStatus getConnectionIdStatus() {
        return connectionIdStatus;
    }

    byte[] getStatelessResetToken() {
        return statelessResetToken;
    }

    void setStatus(ConnectionIdStatus newStatus) {
        connectionIdStatus = newStatus;
    }

    @Override
    public int compareTo(ConnectionIdInfo o) {
        return Integer.compare(sequenceNumber, o.sequenceNumber);
    }

    public void connectionId(byte[] connectionId) {
        this.connectionId = connectionId;
    }
}


package tech.lp2p.quic;


import org.jetbrains.annotations.NotNull;

import java.util.concurrent.TimeoutException;

import tech.lp2p.core.ALPN;

public interface Requester {
    String PEER = "PEER";
    String RELAYED = "RELAYED";
    String STREAM = "STREAM";
    String ADDRS = "ADDRS";
    String TIMER = "TIMER";
    String INITIALIZED = "INITIALIZED";
    String RESERVATION = "RESERVATION";

    @NotNull
    static Stream createStream(@NotNull Connection connection, long delimiter,
                               @NotNull Requester requester)
            throws InterruptedException, TimeoutException {
        if (connection.alpn() == ALPN.libp2p) {
            return connection.createStream(AlpnLibp2pRequester.create(requester), delimiter);
        }
        throw new InterruptedException("not supported alpn");
    }

    @NotNull
    static Stream createStream(@NotNull Connection connection, long delimiter)
            throws InterruptedException, TimeoutException {
        return connection.createStream(delimiter);
    }

    void throwable(Throwable throwable);

    void data(Stream stream, byte[] data) throws Exception;

    void terminated(Stream stream);

    void fin(Stream stream);

}

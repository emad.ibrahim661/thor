package tech.lp2p.quic;

import java.nio.ByteBuffer;

import tech.lp2p.tls.DecodeErrorException;


// https://tools.ietf.org/html/draft-ietf-quic-transport-20#section-16
interface VariableLengthInteger {

    /**
     * Parses a variable length integer and returns the value as in int. Throws an exception when the actual value is
     * larger than <code>Integer.MAX_VALUE</code>, so only use it in cases where a large value can be considered an
     * error, e.g. when the QUIC specification defines a smaller range for a specific integer.
     * Note that smaller values (needlessly) encoded in eight bytes, are parsed correctly.
     */
    static int parse(ByteBuffer buffer) throws DecodeErrorException {
        long value = parseLong(buffer);
        if (value <= Integer.MAX_VALUE) {
            return (int) value;
        } else {
            // If value can be larger than int, parseLong should have called.
            throw new DecodeErrorException("value to large for Java int");
        }
    }

    static long parseLong(ByteBuffer buffer) throws DecodeErrorException {
        if (buffer.remaining() < 1) {
            throw new DecodeErrorException("Invalid size encoding");
        }

        long value;
        byte firstLengthByte = buffer.get();
        switch ((firstLengthByte & 0xc0) >> 6) {
            case 0 -> value = firstLengthByte;
            case 1 -> {
                if (buffer.remaining() < 1) {
                    throw new DecodeErrorException("Invalid size encoding");
                }
                buffer.position(buffer.position() - 1);
                value = buffer.getShort() & 0x3fff;
            }
            case 2 -> {
                if (buffer.remaining() < 3) {
                    throw new DecodeErrorException("Invalid size encoding");
                }
                buffer.position(buffer.position() - 1);
                value = buffer.getInt() & 0x3fffffff;
            }
            case 3 -> {
                if (buffer.remaining() < 7) {
                    throw new DecodeErrorException("Invalid size encoding");
                }
                buffer.position(buffer.position() - 1);
                value = buffer.getLong() & 0x3fffffffffffffffL;
            }
            default ->
                // Impossible, just to satisfy the compiler
                    throw new DecodeErrorException("Not handled size encoding");
        }
        return value;
    }


    static int bytesNeeded(long value) {
        if (value <= 63) {
            return 1;
        } else if (value <= 16383) {
            return 2;
        } else if (value <= 1073741823) {
            return 4;
        } else {
            return 8;
        }
    }

    static int encode(int value, ByteBuffer buffer) {
        // https://tools.ietf.org/html/draft-ietf-quic-transport-20#section-16
        // | 2Bit | Length | Usable Bits | Range                 |
        // +------+--------+-------------+-----------------------+
        // | 00   | 1      | 6           | 0-63                  |
        // | 01   | 2      | 14          | 0-16383               |
        // | 10   | 4      | 30          | 0-1073741823          |
        if (value <= 63) {
            buffer.put((byte) value);
            return 1;
        } else if (value <= 16383) {
            buffer.put((byte) ((value / 256) | 0x40));
            buffer.put((byte) (value % 256));
            return 2;
        } else if (value <= 1073741823) {
            int initialPosition = buffer.position();
            buffer.putInt(value);
            buffer.put(initialPosition, (byte) (buffer.get(initialPosition) | (byte) 0x80));
            return 4;
        } else {
            int initialPosition = buffer.position();
            buffer.putLong(value);
            buffer.put(initialPosition, (byte) (buffer.get(initialPosition) | (byte) 0xc0));
            return 8;
        }
    }

    static int encode(long value, ByteBuffer buffer) {
        if (value <= Integer.MAX_VALUE) {
            return encode((int) value, buffer);
        }
        // https://tools.ietf.org/html/draft-ietf-quic-transport-20#section-16
        // | 2Bit | Length | Usable Bits | Range                 |
        // +------+--------+-------------+-----------------------+
        // | 11   | 8      | 62          | 0-4611686018427387903 |
        else if (value <= 4611686018427387903L) {
            int initialPosition = buffer.position();
            buffer.putLong(value);
            buffer.put(initialPosition, (byte) (buffer.get(initialPosition) | (byte) 0xc0));
            return 8;
        } else {
            throw new IllegalArgumentException("value cannot be encoded in variable-length integer");
        }
    }

}

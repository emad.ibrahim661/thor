package tech.lp2p.quic;

import java.util.concurrent.atomic.AtomicInteger;

final class RateLimiter {
    private static final int FACTOR = 2;
    private final AtomicInteger nextOccasion = new AtomicInteger(1);
    private final AtomicInteger attempts = new AtomicInteger(0);


    public void execute(Runnable runnable) {
        if (attempts.incrementAndGet() == nextOccasion.get()) {
            runnable.run();
            nextOccasion.updateAndGet(i -> i * FACTOR);
        }
    }

}

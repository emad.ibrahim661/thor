package tech.lp2p.quic;

import org.jetbrains.annotations.NotNull;

import java.nio.ByteBuffer;
import java.util.List;


public sealed interface Packet permits Packet.HandshakePacket,
        Packet.InitialPacket, Packet.ShortHeaderPacket {

    byte[] UNDEFINED_TOKEN = new byte[]{(byte) 0x00};

    static boolean matchAll(Packet packet, List<FrameType> frameTypes) {
        for (Frame frame : packet.frames()) {
            if (!frameTypes.contains(frame.frameType())) {
                return false;
            }
        }
        return true;
    }

    // https://tools.ietf.org/html/draft-ietf-quic-recovery-33#section-2
    // "Packets that contain ack-eliciting frames elicit an ACK from the receiver (...)
    // and are called ack-eliciting packets."
    static boolean isAckEliciting(Packet packet) {
        for (Frame frame : packet.frames()) {
            if (Frame.isAckEliciting(frame)) {
                return true;
            }
        }
        return false;
    }

    static boolean isInflightPacket(Packet packet) {

        for (Frame frame : packet.frames()) {
            if (Frame.isAckEliciting(frame) || frame.frameType() == FrameType.PaddingFrame) {
                return true;
            }
        }
        return false;
    }

    long packetNumber();


    Level level();

    @NotNull
    Frame[] frames();

    /**
     * Estimates what the length of this packet will be after it has been encrypted.
     * The returned length must be less then or equal the actual length after encryption.
     * Length estimates are used when preparing packets for sending, where certain limits must
     * be met (e.g. congestion control, max datagram size, ...).
     */
    int estimateLength();

    default int framesLength() {
        int sum = 0;
        for (Frame frame : frames()) {
            sum += frame.frameLength();
        }
        return sum;
    }

    byte[] generatePacketBytes(Keys keys);

    // https://tools.ietf.org/html/draft-ietf-quic-recovery-33#section-2
    // "In-flight:  Packets are considered in-flight when they are ack-eliciting or contain a PADDING frame, and they
    //  have been sent but are not acknowledged, declared lost, or abandoned along with old keys."
    // This method covers only the first part, which can be derived from the packet.

    // https://tools.ietf.org/html/draft-ietf-quic-recovery-20#section-2
    // "ACK-only:  Any packet containing only one or more ACK frame(s)."
    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    default boolean isAckOnly() {
        for (Frame frame : frames()) {
            if (frame.frameType() != FrameType.AckFrame) {
                return false;
            }
        }
        return true;
    }

    record HandshakePacket(Version version,
                           byte[] destinationConnectionId,
                           byte[] sourceConnectionId,
                           Frame[] frames,
                           long packetNumber) implements Packet {


        byte getPacketType() {
            if (version().isV2()) {
                return (byte) Settings.HANDSHAKE_V2_type;
            } else {
                return (byte) Settings.HANDSHAKE_V1_type;
            }
        }


        @Override
        public byte[] generatePacketBytes(Keys keys) {

            byte[] frameHeader = generateFrameHeaderInvariant();
            byte[] encodedPacketNumber = PacketService.encodePacketNumber(packetNumber);
            byte[] frameBytes = PacketService.generatePayloadBytes(frames, encodedPacketNumber.length);


            int packetLength = frameBytes.length + 16 + encodedPacketNumber.length; // 16 is what
            // encryption adds, note that final length is larger due to adding packet length


            int encPacketLength = VariableLengthInteger.bytesNeeded(packetLength);

            int capacity = frameHeader.length + encodedPacketNumber.length + encPacketLength;
            ByteBuffer additionalData = ByteBuffer.allocate(capacity);
            additionalData.put(frameHeader);
            VariableLengthInteger.encode(packetLength, additionalData);
            additionalData.put(encodedPacketNumber);

            return PacketService.protectPacketNumberAndPayload(additionalData.array(),
                    encodedPacketNumber.length,
                    frameBytes, keys, packetNumber);
        }

        @Override
        public Level level() {
            return Level.Handshake;
        }

        @Override
        public int estimateLength() {
            int payloadLength = framesLength();
            return 1
                    + 4
                    + 1 + destinationConnectionId.length
                    + 1 + sourceConnectionId.length
                    + (payloadLength + 1 > 63 ? 2 : 1)
                    + 1  // packet number length: will usually be just 1, actual value cannot be
                    // computed until packet number is known
                    + payloadLength
                    // https://tools.ietf.org/html/draft-ietf-quic-tls-27#section-5.4.2
                    // "The ciphersuites defined in [TLS13] - (...) - have 16-byte expansions..."
                    + 16;
        }

        private byte[] generateFrameHeaderInvariant() {
            // https://www.rfc-editor.org/rfc/rfc9000.html#name-long-header-packets
            // "Long Header Packet {
            //    Header Form (1) = 1,
            //    Fixed Bit (1) = 1,
            //    Long Packet Type (2),
            //    Type-Specific Bits (4),"
            //    Version (32),
            //    Destination Connection ID Length (8),
            //    Destination Connection ID (0..160),
            //    Source Connection ID Length (8),
            //    Source Connection ID (0..160),
            //    Type-Specific Payload (..),
            //  }

            // Packet payloadType and packet number length
            byte flags = PacketService.encodePacketNumberLength((byte) (0b1100_0000 |
                    (getPacketType() << 4)), packetNumber);

            byte[] version = version().getBytes();
            int capacity = 1 + version.length + 1 + destinationConnectionId.length +
                    1 + sourceConnectionId.length;
            ByteBuffer buffer = ByteBuffer.allocate(capacity);
            buffer.put(flags);
            // Version
            buffer.put(version);
            // DCID Len
            buffer.put((byte) destinationConnectionId.length);
            // Destination connection id
            buffer.put(destinationConnectionId);
            // SCID Len
            buffer.put((byte) sourceConnectionId.length);
            // Source connection id
            buffer.put(sourceConnectionId);
            return buffer.array();
        }

    }


    record InitialPacket(Version version,
                         byte[] destinationConnectionId,
                         byte[] sourceConnectionId,
                         Frame[] frames,
                         long packetNumber,
                         byte[] token) implements Packet {


        byte getPacketType() {
            if (version().isV2()) {
                return (byte) Settings.INITIAL_V2_type;
            } else {
                return (byte) Settings.INITIAL_V1_type;
            }
        }

        byte[] generateAdditionalFields() {
            // Token length (variable-length integer)
            if (token != null) {
                int length = VariableLengthInteger.bytesNeeded(token.length);
                ByteBuffer buffer = ByteBuffer.allocate(length + token.length);
                VariableLengthInteger.encode(token.length, buffer);
                buffer.put(token);
                return buffer.array();
            } else {
                return Packet.UNDEFINED_TOKEN;
            }
        }

        int estimateAdditionalFieldsLength() {
            return token == null ? 1 : 1 + token.length;
        }

        @Override
        public byte[] generatePacketBytes(Keys keys) {


            byte[] frameHeader = generateFrameHeaderInvariant();
            byte[] addFields = generateAdditionalFields();
            byte[] encodedPacketNumber = PacketService.encodePacketNumber(packetNumber);
            byte[] frameBytes = PacketService.generatePayloadBytes(frames, encodedPacketNumber.length);

            // 16 is what encryption adds, note that final length is larger due to adding packet length
            int packetLength = frameBytes.length + 16 + encodedPacketNumber.length;

            int encPacketLength = VariableLengthInteger.bytesNeeded(packetLength);

            int capacity = frameHeader.length + addFields.length +
                    encPacketLength + encodedPacketNumber.length;
            ByteBuffer additionalData = ByteBuffer.allocate(capacity);
            additionalData.put(frameHeader);
            additionalData.put(addFields);
            VariableLengthInteger.encode(packetLength, additionalData);
            additionalData.put(encodedPacketNumber);


            return PacketService.protectPacketNumberAndPayload(additionalData.array(),
                    encodedPacketNumber.length,
                    frameBytes, keys, packetNumber);

        }

        @Override
        public Level level() {
            return Level.Initial;
        }

        @Override
        public int estimateLength() {
            int payloadLength = framesLength();
            return 1
                    + 4
                    + 1 + destinationConnectionId.length
                    + 1 + sourceConnectionId.length
                    + estimateAdditionalFieldsLength()
                    + (payloadLength + 1 > 63 ? 2 : 1)
                    + 1  // packet number length: will usually be just 1, actual value cannot be computed until packet number is known
                    + payloadLength
                    // https://tools.ietf.org/html/draft-ietf-quic-tls-27#section-5.4.2
                    // "The ciphersuites defined in [TLS13] - (...) - have 16-byte expansions..."
                    + 16;
        }

        private byte[] generateFrameHeaderInvariant() {
            // https://www.rfc-editor.org/rfc/rfc9000.html#name-long-header-packets
            // "Long Header Packet {
            //    Header Form (1) = 1,
            //    Fixed Bit (1) = 1,
            //    Long Packet Type (2),
            //    Type-Specific Bits (4),"
            //    Version (32),
            //    Destination Connection ID Length (8),
            //    Destination Connection ID (0..160),
            //    Source Connection ID Length (8),
            //    Source Connection ID (0..160),
            //    Type-Specific Payload (..),
            //  }

            // Packet payloadType and packet number length
            byte flags = PacketService.encodePacketNumberLength((byte) (0b1100_0000 |
                    (getPacketType() << 4)), packetNumber);


            byte[] version = version().getBytes();
            int capacity = 1 + version.length + 1 + destinationConnectionId.length +
                    1 + sourceConnectionId.length;
            ByteBuffer buffer = ByteBuffer.allocate(capacity);

            buffer.put(flags);
            // Version
            buffer.put(version().getBytes());
            // DCID Len
            buffer.put((byte) destinationConnectionId.length);
            // Destination connection id
            buffer.put(destinationConnectionId);
            // SCID Len
            buffer.put((byte) sourceConnectionId.length);
            // Source connection id
            buffer.put(sourceConnectionId);
            return buffer.array();
        }


    }


    record ShortHeaderPacket(Version version,
                             byte[] destinationConnectionId,
                             Frame[] frames,
                             long packetNumber) implements Packet {


        @Override
        public Level level() {
            return Level.App;
        }

        @Override
        public int estimateLength() {
            int payloadLength = framesLength();
            return 1
                    + destinationConnectionId.length
                    + 1  // packet number length: will usually be just 1, actual value cannot be
                    // computed until packet number is known
                    + payloadLength
                    // https://tools.ietf.org/html/draft-ietf-quic-tls-27#section-5.4.2
                    // "The ciphersuites defined in [TLS13] - (...) - have 16-byte expansions..."
                    + 16;
        }

        @Override
        public byte[] generatePacketBytes(Keys keys) {


            byte flags = getFlags(keys);


            byte[] encodedPacketNumber = PacketService.encodePacketNumber(packetNumber);


            int capacity = 1 + destinationConnectionId.length + encodedPacketNumber.length;
            ByteBuffer additionalData = ByteBuffer.allocate(capacity);
            additionalData.put(flags);
            additionalData.put(destinationConnectionId);
            additionalData.put(encodedPacketNumber);

            byte[] frameBytes = PacketService.generatePayloadBytes(frames, encodedPacketNumber.length);


            return PacketService.protectPacketNumberAndPayload(additionalData.array(),
                    encodedPacketNumber.length, frameBytes,
                    keys, packetNumber);
        }

        private byte getFlags(Keys keys) {
            byte flags;
            // https://tools.ietf.org/html/draft-ietf-quic-transport-17#section-17.3
            // "|0|1|S|R|R|K|P P|"
            // "Spin Bit (S):  The sixth bit (0x20) of byte 0 is the Latency Spin
            //      Bit, set as described in [SPIN]."
            // "Reserved Bits (R):  The next two bits (those with a mask of 0x18) of
            //      byte 0 are reserved. (...) The value included prior to protection MUST be set to 0. "
            flags = 0x40;  // 0100 0000
            short keyPhaseBit = keys.getKeyPhase();
            flags = (byte) (flags | (keyPhaseBit << 2));
            flags = PacketService.encodePacketNumberLength(flags, packetNumber);
            return flags;
        }

    }

}

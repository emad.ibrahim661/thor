package tech.lp2p.quic;

import org.jetbrains.annotations.NotNull;

public interface StreamHandler {
    @NotNull
    StreamState streamState();

    void terminated(Stream stream);

    void fin(Stream stream);

    void throwable(Throwable throwable);
}

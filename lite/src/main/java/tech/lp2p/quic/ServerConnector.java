package tech.lp2p.quic;


import static tech.lp2p.lite.LiteErrorCode.SHUTDOWN;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.net.ssl.X509TrustManager;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.Host;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.tls.TlsServerEngineFactory;
import tech.lp2p.utils.Utils;


/**
 * Listens for QUIC connections on a given port. Requires server certificate and corresponding private key.
 */
public final class ServerConnector implements ServerConnectionRegistry {

    private static final int MINIMUM_LONG_HEADER_LENGTH = 1 + 4 + 1 + 1;
    private final TlsServerEngineFactory tlsServerEngineFactory;
    private final Receiver receiver;
    private final List<Integer> supportedVersionIds;
    private final DatagramSocket serverSocket;
    private final X509TrustManager trustManager;
    private final Map<ConnectionSource, ConnectionProxy> connections = new ConcurrentHashMap<>();
    private final ApplicationProtocolRegistry applicationProtocolRegistry =
            new ApplicationProtocolRegistry();
    private final Map<ALPN, Function<Stream, StreamHandler>> alpnFunctionMap;

    private final Host host;
    private final Set<SocketAddress> expectation = ConcurrentHashMap.newKeySet();
    @Nullable
    private Consumer<Peeraddr> closedConsumer;

    public ServerConnector(Host host, DatagramSocket socket, X509TrustManager trustManager,
                           byte[] certificateFile, byte[] certificateKeyFile,
                           List<Version> supportedVersions,
                           Map<ALPN, Function<Stream, StreamHandler>> alpnFunctionMap) {
        this.host = host;
        this.serverSocket = socket;
        this.trustManager = trustManager;
        this.tlsServerEngineFactory = TlsServerEngineFactory.createTlsServerEngineFactory(
                certificateFile, certificateKeyFile);
        this.supportedVersionIds = supportedVersions.stream().map(
                Version::versionId).collect(Collectors.toList());
        this.alpnFunctionMap = alpnFunctionMap;
        this.receiver = new Receiver(socket, new DatagramPacketConsumer(), new ThrowableConsumer());
    }

    public int numConnections() {
        return connections.size();
    }

    @NotNull
    public Host host() {
        return host;
    }

    public void setClosedConsumer(@Nullable Consumer<Peeraddr> closedConsumer) {
        this.closedConsumer = closedConsumer;
    }

    public void punching(InetAddress address, int port, long expireTimeStamp) {

        // Upon expiry of the timer, B starts to send UDP packets filled with random bytes to A's
        // address. Packets should be sent repeatedly in random intervals between 10 and 200 ms.

        InetSocketAddress inetSocketAddress = new InetSocketAddress(address, port);
        expectation.add(inetSocketAddress);
        try {
            punch(inetSocketAddress, expireTimeStamp);
        } finally {
            expectation.remove(inetSocketAddress);
        }
    }

    @NotNull
    public Collection<Connection> serverConnections(PeerId peerId) {
        Set<Connection> connectionSet = new HashSet<>();
        for (Connection connection : serverConnections()) {
            if (Objects.equals(connection.remotePeerId(), peerId)) {
                connectionSet.add(connection);
            }
        }
        return connectionSet;
    }

    @NotNull
    public Collection<Connection> serverConnections() {
        Set<Connection> connectionSet = new HashSet<>();
        for (ConnectionProxy conn : connections.values()) {
            if (conn instanceof ServerConnection serverConnection) {
                if (serverConnection.isConnected()) {
                    connectionSet.add(serverConnection);
                }
            }
        }
        return connectionSet;
    }

    public Collection<Connection> connections() {
        Set<Connection> connectionSet = new HashSet<>();
        for (ConnectionProxy proxy : connections.values()) {
            if (proxy instanceof Connection connection) {
                if (connection.isConnected()) {
                    connectionSet.add(connection);
                }
            }
        }
        return connectionSet;
    }

    @NotNull
    public Collection<Connection> clientConnections() {
        Set<Connection> connectionSet = new HashSet<>();
        for (ConnectionProxy conn : connections.values()) {
            if (conn instanceof ClientConnection clientConnection) {
                if (clientConnection.isConnected()) {
                    connectionSet.add(clientConnection);
                }
            }
        }
        return connectionSet;
    }

    public void punch(InetSocketAddress inetSocketAddress, long expireTimeStamp) {

        // check expireTimeStamp
        if (System.currentTimeMillis() > expireTimeStamp) {
            return;
        }
        if (!expectation.contains(inetSocketAddress)) {
            return;
        }
        // check if there is a
        try {
            int length = 64;
            byte[] datagramData = new byte[length];
            Random rd = new Random();
            rd.nextBytes(datagramData);

            DatagramPacket datagram = new DatagramPacket(datagramData, length,
                    inetSocketAddress.getAddress(), inetSocketAddress.getPort());

            serverSocket.send(datagram);

            Thread.sleep(new Random().nextInt(190) + 10); // sleep for random value between 10 and 200

            if (!Thread.currentThread().isInterrupted()) {
                punch(inetSocketAddress, expireTimeStamp);
            }
            // interrupt exception should occur, but it simply ends the loop
        } catch (Throwable ignore) {
        }
    }

    public void shutdown() {
        try {
            connections().forEach(connection -> connection.close(SHUTDOWN));
            ScheduledExecutorService scheduledExecutorService =
                    Executors.newSingleThreadScheduledExecutor();
            scheduledExecutorService.schedule(this::terminate, Settings.SHUTDOWN_TIME,
                    TimeUnit.MILLISECONDS);
            scheduledExecutorService.shutdown();
            boolean result = scheduledExecutorService.awaitTermination(
                    Integer.MAX_VALUE, TimeUnit.SECONDS);
            if (!result) {
                scheduledExecutorService.shutdownNow();
            }
        } catch (Throwable throwable) {
            Utils.error(throwable); // should not occur
        }
    }

    private void terminate() {
        try {
            connections.values().forEach(ConnectionProxy::terminate);
            receiver.shutdown();
            serverSocket.close();
        } catch (Throwable throwable) {
            Utils.error(throwable); // should not occur
        } finally {
            connections.clear();
        }
    }

    public void registerApplicationProtocol(String protocol, ApplicationProtocolConnectionFactory protocolConnectionFactory) {
        applicationProtocolRegistry.registerApplicationProtocol(protocol, protocolConnectionFactory);
    }

    public void start() {
        receiver.start();
    }


    private void process(long timeReceived, DatagramPacket datagramPacket) {
        expectation.remove(datagramPacket.getSocketAddress());
        ByteBuffer data = ByteBuffer.wrap(datagramPacket.getData(), 0, datagramPacket.getLength());
        int flags = data.get();
        data.rewind();
        if ((flags & 0b1100_0000) == 0b1100_0000) {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-17.2
            // "Header Form:  The most significant bit (0x80) of byte 0 (the first byte) is set to 1 for long headers."
            processLongHeaderPacket((InetSocketAddress) datagramPacket.getSocketAddress(), data, timeReceived);
        } else if ((flags & 0b1100_0000) == 0b0100_0000) {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-17.3
            // "Header Form:  The most significant bit (0x80) of byte 0 is set to 0 for the short header.
            processShortHeaderPacket((InetSocketAddress) datagramPacket.getSocketAddress(), data, timeReceived);
        } else {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-17.2
            // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-17.3
            // "The next bit (0x40) of byte 0 is set to 1. Packets containing a zero value for
            // this bit are not valid packets in this version and MUST be discarded."
            Utils.error(String.format(" Invalid Quic packet (flags: %02x) is discarded", flags));
        }
    }

    private void processLongHeaderPacket(InetSocketAddress remoteAddress,
                                         ByteBuffer data, long received) {
        if (data.remaining() >= MINIMUM_LONG_HEADER_LENGTH) {
            data.position(1);
            int version = data.getInt();


            data.position(5);
            int dcidLength = data.get() & 0xff;
            // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-17.2
            // "In QUIC version 1, this value MUST NOT exceed 20. Endpoints that receive a version 1 long header with a
            //  value larger than 20 MUST drop the packet. In order to properly form a Version Negotiation packet,
            //  servers SHOULD be able to read longer connection IDs from other QUIC versions."
            if (dcidLength > 20) {
                if (initialWithUnspportedVersion(data, version)) {
                    // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-6
                    // "A server sends a Version Negotiation packet in response to each packet
                    // that might initiate a new connection;"
                    Utils.error("initialWithUnspportedVersion not supported");
                }
                Utils.error("Ignore connection from  " + remoteAddress);

                return;
            }
            if (data.remaining() >= dcidLength + 1) {  // after dcid at least one byte scid length
                byte[] dcid = new byte[dcidLength];
                data.get(dcid);

                int scidLength = data.get() & 0xff;
                if (data.remaining() >= scidLength) {
                    byte[] scid = new byte[scidLength];
                    data.get(scid);
                    data.rewind();


                    ConnectionProxy connection = isExistingConnection(dcid);
                    if (connection == null) {
                        if (mightStartNewConnection(version, dcid) &&
                                isExistingConnection(dcid) == null) {
                            connection = createNewConnection(version, remoteAddress, dcid);
                        } else if (initialWithUnspportedVersion(data, version)) {
                            // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-6
                            // "A server sends a Version Negotiation packet in response to each packet that might initiate a new connection;"
                            // NOTE NOT SUPPORTED
                            Utils.error("initialWithUnspportedVersion not supported");
                        }
                    }
                    if (connection != null) {
                        connection.parsePackets(received, data);
                    }
                }
            }
        }
    }

    private void processShortHeaderPacket(InetSocketAddress clientAddress, ByteBuffer data, long received) {
        byte[] dcid = new byte[Settings.DEFAULT_CID_LENGTH];
        data.position(1);
        data.get(dcid);
        data.rewind();

        ConnectionProxy connection = isExistingConnection(dcid);
        if (connection != null) {
            connection.parsePackets(received, data);
        } else {
            Utils.error("Discarding short header " +
                    " packet addressing non existent connection " + clientAddress.getPort());
        }
    }

    private boolean mightStartNewConnection(int version, byte[] dcid) {
        // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-7.2
        // "This Destination Connection ID MUST be at least 8 bytes in length."
        if (dcid.length >= 8) {
            return supportedVersionIds.contains(version);
        } else {
            return false;
        }
    }

    private boolean initialWithUnspportedVersion(ByteBuffer packetBytes, int version) {
        packetBytes.rewind();
        int type = (packetBytes.get() & 0x30) >> 4;
        if (PacketParser.isInitial(type, Version.parse(version))) {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-14.1
            // "A server MUST discard an Initial packet that is carried in a UDP
            //   datagram with a payload that is smaller than the smallest allowed
            //   maximum datagram size of 1200 bytes. "
            if (packetBytes.limit() >= 1200) {
                return !supportedVersionIds.contains(version);
            }
        }
        return false;
    }

    private ConnectionProxy createNewConnection(int versionValue, InetSocketAddress remoteAddress,
                                                byte[] dcid) {
        Version version = Version.parse(versionValue);
        ConnectionProxy connectionCandidate = new ServerConnectionCandidate(
                this, version, remoteAddress, dcid);
        // Register new connection now with the original connection id, as retransmitted initial packets with the
        // same original dcid might be received, which should _not_ lead to another connection candidate)

        connections.put(new ConnectionSource(dcid), connectionCandidate);
        return connectionCandidate;
    }

    void removeConnection(ServerConnection connection) {
        for (ConnectionIdInfo info : connection.sourceConnectionIds()) {
            Objects.requireNonNull(info);
            connections.remove(new ConnectionSource(info.getConnectionId()));
        }
        connections.remove(new ConnectionSource(connection.originalDestinationCid()));

        if (!connection.isClosed()) {
            Utils.error("Removed connection " + connection + " that is not closed...");
        }

        if (closedConsumer != null) {
            try {
                if (connection.hasRemotePeerId()) {
                    closedConsumer.accept(connection.remotePeeraddr());
                }
            } catch (Throwable throwable) {
                Utils.error(throwable); // should not occur
            }
        }
    }

    @Nullable
    private ConnectionProxy isExistingConnection(byte[] dcid) {
        return getConnections(dcid);
    }


    @Override
    public void registerConnection(ConnectionProxy connection, byte[] connectionId) {
        connections.put(new ConnectionSource(connectionId), connection);
    }

    @Override
    public void deregisterConnection(ConnectionProxy connection, byte[] connectionId) {
        boolean removed = removeConnection(connection, connectionId);

        if (!removed && containsKey(connectionId)) {
            Utils.error("Connection " + connection.remoteAddress() + " not removed, because "
                    + getConnections(connectionId) + " is still registered");
        }
    }

    private boolean containsKey(byte[] connectionId) {
        return connections.containsKey(new ConnectionSource(connectionId));
    }

    private boolean removeConnection(ConnectionProxy connection, byte[] connectionId) {
        return connections.remove(new ConnectionSource(connectionId), connection);
    }

    @Nullable
    private ConnectionProxy getConnections(byte[] connectionId) {
        return connections.get(new ConnectionSource(connectionId));
    }

    @Override
    public void registerAdditionalConnectionId(byte[] currentConnectionId, byte[] newConnectionId) {
        ConnectionProxy connection = getConnections(currentConnectionId);
        if (connection != null) {
            registerConnection(connection, newConnectionId);
        } else {
            Utils.error("Cannot add additional cid to non-existing connection");
        }
    }

    @Override
    public void deregisterConnectionId(byte[] connectionId) {
        connections.remove(new ConnectionSource(connectionId));
    }

    Receiver receiver() {
        return receiver;
    }

    DatagramSocket datagramSocket() {
        return serverSocket;
    }

    Map<ALPN, Function<Stream, StreamHandler>> alpnFunctionMap() {
        return alpnFunctionMap;
    }

    ApplicationProtocolRegistry applicationProtocolRegistry() {
        return applicationProtocolRegistry;
    }

    public X509TrustManager trustManager() {
        return trustManager;
    }

    public TlsServerEngineFactory tlsServerEngineFactory() {
        return tlsServerEngineFactory;
    }

    private static final class ThrowableConsumer implements Consumer<Throwable> {

        @Override
        public void accept(Throwable throwable) {
            Utils.error(throwable);
        }
    }

    private record ConnectionSource(byte[] dcid) {

        @Override
        public int hashCode() {
            return Arrays.hashCode(dcid); // ok, checked, maybe opt
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof ConnectionSource) {
                return Arrays.equals(this.dcid, ((ConnectionSource) obj).dcid);
            } else {
                return false;
            }
        }
    }

    private final class DatagramPacketConsumer implements Consumer<DatagramPacket> {

        @Override
        public void accept(DatagramPacket datagramPacket) {
            try {
                process(System.currentTimeMillis(), datagramPacket);
            } catch (Throwable throwable) {
                Utils.error("Invalid packet " + throwable.getClass().getSimpleName());
            }
        }
    }
}

package tech.lp2p.quic;

import java.util.Arrays;


final class ConnectionSourceIdRegistry extends ConnectionIdRegistry {

    ConnectionSourceIdRegistry() {
        super(new ConnectionIdInfo(0,
                ConnectionIdRegistry.generateCid(Settings.DEFAULT_CID_LENGTH),
                null, ConnectionIdStatus.IN_USE));
    }

    ConnectionIdInfo generateNew(int cidLength) {
        int sequenceNr = maxSequenceNr() + 1;
        ConnectionIdInfo newCid = new ConnectionIdInfo(sequenceNr,
                ConnectionIdRegistry.generateCid(cidLength),
                null, ConnectionIdStatus.NEW);
        connectionIds().add(newCid);
        return newCid;
    }

    /**
     * Registers a connection id for being used.
     *
     * @return true is the connection id is new (newly used), false otherwise.
     */
    boolean registerUsedConnectionId(byte[] connectionId) {
        if (Arrays.equals(getInitial(), connectionId)) {
            return false;
        } else {
            // LogUtils.error(SourceConnectionIdRegistry.class.getSimpleName(), bytesToHex(connectionId));
            // Register previous connection id as used
            connectionIds().stream()
                    .filter(cid -> Arrays.equals(cid.getConnectionId(), getInitial()))
                    .forEach(cid -> cid.setStatus(ConnectionIdStatus.USED));

            // Check if new connection id is newly used
            boolean wasNew = connectionIds().stream()
                    .filter(cid -> Arrays.equals(cid.getConnectionId(), connectionId))
                    .anyMatch(cid -> cid.getConnectionIdStatus() == ConnectionIdStatus.NEW);
            // Register current connection id as current
            connectionIds().stream()
                    .filter(cid -> Arrays.equals(cid.getConnectionId(), connectionId))
                    .forEach(cid -> cid.setStatus(ConnectionIdStatus.IN_USE));

            return wasNew;
        }
    }


}



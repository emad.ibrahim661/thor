package tech.lp2p.quic;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import tech.lp2p.tls.DecodeErrorException;
import tech.lp2p.utils.Utils;

public sealed interface FrameReceived permits FrameReceived.AckFrame, FrameReceived.StreamFrame,
        FrameReceived.ConnectionCloseFrame, FrameReceived.CryptoFrame,
        FrameReceived.DataBlockedFrame,
        FrameReceived.DatagramFrame, FrameReceived.HandshakeDoneFrame, FrameReceived.MaxDataFrame,
        FrameReceived.MaxStreamDataFrame, FrameReceived.MaxStreamsFrame,
        FrameReceived.NewConnectionIdFrame,
        FrameReceived.NewTokenFrame, FrameReceived.PaddingFrame, FrameReceived.PathChallengeFrame,
        FrameReceived.PathResponseFrame, FrameReceived.PingFrame, FrameReceived.StreamsBlockedFrame,
        FrameReceived.RetireConnectionIdFrame, FrameReceived.ResetStreamFrame,
        FrameReceived.StopSendingFrame, FrameReceived.StreamDataBlockedFrame {


    static StreamsBlockedFrame parseStreamsBlockedFrame(byte type, ByteBuffer buffer) throws DecodeErrorException {

        return new StreamsBlockedFrame(FrameType.StreamsBlockedFrame, type == 0x16,
                (int) VariableLengthInteger.parseLong(buffer));
    }

    /**
     * <a href="https://datatracker.ietf.org/doc/html/rfc9221">...</a>
     * DATAGRAM frames are used to transmit application data in an unreliable manner.
     * The Type field in the DATAGRAM frame takes the form 0b0011000X (or the values 0x30 and 0x31).
     * The least significant bit of the Type field in the DATAGRAM frame is the LEN bit (0x01),
     * which indicates whether there is a Length field present: if this bit is set to 0, the
     * Length field is absent and the Datagram Data field extends to the end of the packet;
     * if this bit is set to 1, the Length field is present.
     */
    static DatagramFrame parseDatagramFrame(byte type, ByteBuffer buffer) throws DecodeErrorException {
        int datagramLength = buffer.capacity();
        boolean lenBitSet = type == 0x31;
        if (lenBitSet) { // 49 (0x31), the least significant bit is 1
            datagramLength = VariableLengthInteger.parse(buffer);
        }

        byte[] datagram = new byte[datagramLength];
        buffer.get(datagram);

        return new DatagramFrame(lenBitSet, datagram);
    }

    static ConnectionCloseFrame parseConnectionCloseFrame(byte type, ByteBuffer buffer) throws DecodeErrorException {

        int triggeringFrameType = 0;
        long errorCode = VariableLengthInteger.parseLong(buffer);
        if (type == 0x1c) {
            triggeringFrameType = VariableLengthInteger.parse(buffer);
        }
        byte[] reasonPhrase = Utils.BYTES_EMPTY;
        int reasonPhraseLength = VariableLengthInteger.parse(buffer);
        if (reasonPhraseLength > 0) {
            reasonPhrase = new byte[reasonPhraseLength];
            buffer.get(reasonPhrase);
        }
        int tlsError = -1;
        if (type == 0x1c && errorCode >= 0x0100 && errorCode < 0x0200) {
            tlsError = (int) (errorCode - 256);
        }

        return new ConnectionCloseFrame(type, triggeringFrameType, reasonPhrase,
                errorCode, tlsError);
    }

    static AckFrame parseAckFrame(byte type, ByteBuffer buffer, @Nullable TransportParameters transportParameters)
            throws DecodeErrorException {
        // default value, as long no transport parameters are defined
        int delayScale = Settings.ACK_DELAY_SCALE;
        if (transportParameters != null) {
            delayScale = transportParameters.ackDelayScale();
        }


        //  If the frame payloadType is 0x03, ACK frames also contain the cumulative count
        //  of QUIC packets with associated ECN marks received on the connection
        //  up until this point.
        if (type == 0x03) {
            Utils.error("AckFrame of payloadType 0x03 is not yet fully supported");
        }

        // ACK frames are formatted as shown in Figure 25.
        //
        //   ACK Frame {
        //     Type (i) = 0x02..0x03,
        //     Largest Acknowledged (i),
        //     ACK Delay (i),
        //     ACK Range Count (i),
        //     First ACK Range (i),
        //     ACK Range (..) ...,
        //     [ECN Counts (..)],
        //   }


        // A variable length integer is an encoding of 64-bit unsigned
        // integers into between 1 and 9 bytes.

        // A variable-length integer representing the
        // largest packet number the peer is acknowledging; this is usually
        // the largest packet number that the peer has received prior to
        // generating the ACK frame.  Unlike the packet number in the QUIC
        //long or short header, the value in an ACK frame is not truncated.
        long largestAcknowledged = VariableLengthInteger.parseLong(buffer);

        // Parse as long to protect to against buggy peers. Convert to int as MAX_INT is large enough to hold the
        // largest ack delay that makes sense (even with an delay exponent of 0, MAX_INT is approx 2147 seconds, approx. half an hour).
        int ackDelay = ((int) VariableLengthInteger.parseLong(buffer) * delayScale) / 1000;

        int ackBlockCount = (int) VariableLengthInteger.parseLong(buffer);

        long[] acknowledgedRanges = new long[(ackBlockCount + 1) * 2];

        long currentSmallest = largestAcknowledged;
        // The smallest of the first block is the largest - (rangeSize - 1).
        int rangeSize = 1 + VariableLengthInteger.parse(buffer);
        int acknowledgedRangesIndex = 0;
        acknowledgedRanges[acknowledgedRangesIndex] = largestAcknowledged;
        acknowledgedRangesIndex++;
        acknowledgedRanges[acknowledgedRangesIndex] = largestAcknowledged - rangeSize - 1;
        currentSmallest -= rangeSize - 1;

        for (int i = 0; i < ackBlockCount; i++) {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-17#section-19.3.1:
            // "Each Gap indicates a range of packets that are not being
            //   acknowledged.  The number of packets in the gap is one higher than
            //   the encoded value of the Gap Field."
            int gapSize = VariableLengthInteger.parse(buffer) + 1;
            // https://tools.ietf.org/html/draft-ietf-quic-transport-17#section-19.3.1:
            // "Each ACK Block acknowledges a contiguous range of packets by
            //   indicating the number of acknowledged packets that precede the
            //   largest packet number in that block.  A value of zero indicates that
            //   only the largest packet number is acknowledged."
            int contiguousPacketsPreceding = VariableLengthInteger.parse(buffer) + 1;
            // The largest of the next range is the current smallest - (gap size + 1), because the gap size counts the
            // ones not being present, and we need the first (below) being present.
            // The new current smallest is largest of the next range - (range size - 1)
            //                             == current smallest - (gap size + 1) - (range size - 1)
            //                             == current smallest - gap size - range size
            long largestOfRange = currentSmallest - gapSize - 1;

            acknowledgedRangesIndex++;
            acknowledgedRanges[acknowledgedRangesIndex] = largestOfRange;

            acknowledgedRangesIndex++;
            acknowledgedRanges[acknowledgedRangesIndex] = largestOfRange - contiguousPacketsPreceding + 1;

            currentSmallest -= (gapSize + contiguousPacketsPreceding);
        }

        // ECN Counts {
        //     ECT0 Count (i),
        //     ECT1 Count (i),
        //     ECN-CE Count (i),
        //   }
        //
        //                        Figure 27: ECN Count Format
        //
        //   The three ECN Counts are:
        //
        //   ECT0 Count:  A variable-length integer representing the total number
        //      of packets received with the ECT(0) codepoint in the packet number
        //      space of the ACK frame.
        //
        //   ECT1 Count:  A variable-length integer representing the total number
        //      of packets received with the ECT(1) codepoint in the packet number
        //      space of the ACK frame.
        //
        //   CE Count:  A variable-length integer representing the total number of
        //      packets received with the CE codepoint in the packet number space
        //      of the ACK frame.
        //
        //   ECN counts are maintained separately for each packet number space.

        return new AckFrame(acknowledgedRanges, largestAcknowledged, ackDelay);
    }

    static CryptoFrame parseCryptoFrame(ByteBuffer buffer) throws DecodeErrorException {

        long offset = VariableLengthInteger.parseLong(buffer);
        int length = VariableLengthInteger.parse(buffer);

        byte[] cryptoData = new byte[length];
        buffer.get(cryptoData);

        return new CryptoFrame(offset, cryptoData, length);
    }

    static DataBlockedFrame parseDataBlockedFrame(ByteBuffer buffer) throws DecodeErrorException {
        return new DataBlockedFrame(VariableLengthInteger.parseLong(buffer));
    }

    static MaxDataFrame parseMaxDataFrame(ByteBuffer buffer) throws DecodeErrorException {

        return new MaxDataFrame(VariableLengthInteger.parseLong(buffer));
    }

    static MaxStreamDataFrame parseMaxStreamDataFrame(ByteBuffer buffer) throws DecodeErrorException {

        return new MaxStreamDataFrame(
                (int) VariableLengthInteger.parseLong(buffer),
                VariableLengthInteger.parseLong(buffer));
    }

    static MaxStreamsFrame parseMaxStreamsFrame(byte type, ByteBuffer buffer) throws DecodeErrorException {

        return new MaxStreamsFrame(VariableLengthInteger.parseLong(buffer),
                type == 0x12);
    }

    static NewConnectionIdFrame parseNewConnectionIdFrame(ByteBuffer buffer) throws DecodeErrorException {

        int sequenceNr = VariableLengthInteger.parse(buffer);
        int retirePriorTo = VariableLengthInteger.parse(buffer);
        int connectionIdLength = buffer.get();
        byte[] connectionId = new byte[connectionIdLength];
        buffer.get(connectionId);

        byte[] statelessResetToken = new byte[128 / 8];
        buffer.get(statelessResetToken);

        return new NewConnectionIdFrame(sequenceNr, retirePriorTo,
                connectionId, statelessResetToken);
    }

    static NewTokenFrame parseNewTokenFrame(ByteBuffer buffer) throws DecodeErrorException {


        int tokenLength = VariableLengthInteger.parse(buffer);
        byte[] newToken = new byte[tokenLength];
        buffer.get(newToken);

        return new NewTokenFrame(newToken);
    }

    /**
     * Strictly speaking, a padding frame consists of one single byte. For convenience, here all subsequent padding
     * bytes are collected in one padding object.
     */
    static PaddingFrame parsePaddingFrame(ByteBuffer buffer) {
        int length = 0;
        byte lastByte = 0;
        while (buffer.position() < buffer.limit() && (lastByte = buffer.get()) == 0)
            length++;

        if (lastByte != 0) {
            // Set back one position
            buffer.position(buffer.position() - 1);
        }

        return new PaddingFrame(length);
    }

    static PathChallengeFrame parsePathChallengeFrame(ByteBuffer buffer) {
        byte[] data = new byte[8];
        buffer.get(data);
        return new PathChallengeFrame(data);
    }

    static PathResponseFrame parsePathResponseFrame(ByteBuffer buffer) {
        byte[] data = new byte[8];
        buffer.get(data);
        return new PathResponseFrame(data);
    }

    static PingFrame parsePingFrame() {
        return new PingFrame();
    }

    static ResetStreamFrame parseResetStreamFrame(ByteBuffer buffer) throws DecodeErrorException {

        int streamId = VariableLengthInteger.parse(buffer);
        long errorCode = VariableLengthInteger.parseLong(buffer);
        long finalSize = VariableLengthInteger.parseLong(buffer);
        return new ResetStreamFrame(streamId, errorCode, finalSize);
    }

    static RetireConnectionIdFrame parseRetireConnectionIdFrame(ByteBuffer buffer) throws DecodeErrorException {

        int sequenceNr = VariableLengthInteger.parse(buffer);
        return new RetireConnectionIdFrame(sequenceNr);
    }

    static StopSendingFrame parseStopSendingFrame(ByteBuffer buffer) throws DecodeErrorException {

        int streamId = VariableLengthInteger.parse(buffer);
        long errorCode = VariableLengthInteger.parseLong(buffer);

        return new StopSendingFrame(streamId, errorCode);
    }

    static StreamDataBlockedFrame parsestreamDataBlockedFrame(ByteBuffer buffer) throws DecodeErrorException {

        int streamId = VariableLengthInteger.parse(buffer);
        long streamDataLimit = VariableLengthInteger.parseLong(buffer);

        return new StreamDataBlockedFrame(streamId, streamDataLimit);
    }

    static StreamFrame parseStreamFrame(byte type, ByteBuffer buffer) throws DecodeErrorException {

        boolean withOffset = ((type & 0x04) == 0x04);
        boolean withLength = ((type & 0x02) == 0x02);
        boolean isFinal = ((type & 0x01) == 0x01);

        int streamId = VariableLengthInteger.parse(buffer);

        long offset = 0;
        if (withOffset) {
            offset = VariableLengthInteger.parseLong(buffer);
        }
        int length;
        if (withLength) {
            length = VariableLengthInteger.parse(buffer);
        } else {
            length = buffer.limit() - buffer.position();
        }

        byte[] streamData = new byte[length];
        buffer.get(streamData);

        return new StreamFrame(streamId, isFinal, offset, length, streamData);
    }

    static HandshakeDoneFrame parseHandshakeDoneFrame() {
        return new HandshakeDoneFrame();
    }

    /**
     * Returns whether the frame is ack eliciting
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-terms-and-definitions">...</a>
     * "Ack-eliciting packet: A QUIC packet that contains frames other than ACK, PADDING, and CONNECTION_CLOSE."
     *
     * @return true when the frame is ack-eliciting
     */
    static boolean isAckEliciting(FrameReceived frameReceived) {
        return switch (frameReceived.type()) {
            case AckFrame, PaddingFrame, ConnectionCloseFrame -> false;
            default -> true;
        };
    }

    FrameType type();


    /**
     * <a href="https://datatracker.ietf.org/doc/html/rfc9221">...</a>
     * DATAGRAM frames are used to transmit application data in an unreliable manner.
     * The Type field in the DATAGRAM frame takes the form 0b0011000X (or the values 0x30 and 0x31).
     * The least significant bit of the Type field in the DATAGRAM frame is the LEN bit (0x01),
     * which indicates whether there is a Length field present: if this bit is set to 0, the
     * Length field is absent and the Datagram Data field extends to the end of the packet;
     * if this bit is set to 1, the Length field is present.
     */
    record DatagramFrame(boolean lenBitSet,
                         byte[] datagram) implements FrameReceived {
        private static final FrameType TYPE = FrameType.DatagramFrame;

        public FrameType type() {
            return TYPE;
        }

    }

    /**
     * Represents a connection close frame.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-connection_close-frames">...</a>
     */
    record ConnectionCloseFrame(int frameType, int triggeringFrameType,
                                byte[] reasonPhrase,
                                long errorCode, int tlsError) implements FrameReceived {
        private static final FrameType TYPE = FrameType.ConnectionCloseFrame;

        public FrameType type() {
            return TYPE;
        }

        public boolean hasTransportError() {
            return frameType == 0x1c && errorCode != 0;
        }

        public boolean hasTlsError() {
            return tlsError != -1;
        }

        public long getTlsError() {
            if (hasTlsError()) {
                return tlsError;
            } else {
                throw new IllegalStateException("Close does not have a TLS error");
            }
        }

        public long getErrorCode() {
            return errorCode;
        }

        public boolean hasReasonPhrase() {
            return reasonPhrase != null;
        }

        public String getReasonPhrase() {
            return new String(reasonPhrase, StandardCharsets.UTF_8);
        }

        public boolean hasApplicationProtocolError() {
            return frameType == 0x1d && errorCode != 0;
        }

        public boolean hasError() {
            return hasTransportError() || hasApplicationProtocolError();
        }

        @NotNull
        @Override
        public String toString() {
            return "ConnectionCloseFrame["
                    + (hasTlsError() ? "TLS " + tlsError : errorCode) + "|"
                    + triggeringFrameType + "|"
                    + (reasonPhrase != null ? new String(reasonPhrase) : "-") + "]";
        }

    }

    // https://www.rfc-editor.org/rfc/rfc9000.html#name-transport-parameter-definit
    // "...  a default value of 3 is assumed (indicating a multiplier of 8)."
    record AckFrame(long[] acknowledgedRanges, long largestAcknowledged,
                    int ackDelay) implements FrameReceived {
        private static final FrameType TYPE = FrameType.AckFrame;

        public FrameType type() {
            return TYPE;
        }
    }

    /**
     * Represents a crypto frame.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-crypto-frames">...</a>
     */
    record CryptoFrame(long offset, byte[] payload, int length)
            implements FrameReceived, Comparable<FrameReceived.CryptoFrame> {

        private static final FrameType TYPE = FrameType.CryptoFrame;

        public FrameType type() {
            return TYPE;
        }

        public long getUpToOffset() {
            return offset + length;
        }

        @Override
        public int compareTo(CryptoFrame other) {
            if (this.offset == other.offset()) {
                return Long.compare(this.length, other.length());
            } else {
                return Long.compare(this.offset, other.offset());
            }
        }

    }

    /**
     * Represents a data blocked frame.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-data_blocked-frames">...</a>
     */
    record DataBlockedFrame(long streamDataLimit) implements FrameReceived {
        private static final FrameType TYPE = FrameType.DataBlockedFrame;

        public FrameType type() {
            return TYPE;
        }
    }

    /**
     * <a href="https://tools.ietf.org/html/draft-ietf-quic-transport-25#section-19.20">...</a>
     */
    record HandshakeDoneFrame() implements FrameReceived {
        private static final FrameType TYPE = FrameType.HandshakeDoneFrame;

        public FrameType type() {
            return TYPE;
        }
    }

    // https://tools.ietf.org/html/draft-ietf-quic-transport-20#section-19.9
    record MaxDataFrame(long maxData) implements FrameReceived {
        private static final FrameType TYPE = FrameType.MaxDataFrame;

        public FrameType type() {
            return TYPE;
        }
    }

    /**
     * Represents a max stream data frame.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-max_stream_data-frames">...</a>
     */
    record MaxStreamDataFrame(int streamId, long maxData) implements FrameReceived {
        private static final FrameType TYPE = FrameType.MaxStreamDataFrame;

        public FrameType type() {
            return TYPE;
        }
    }

    /**
     * Represents a max streams frame.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-max_streams-frames">...</a>
     */
    record MaxStreamsFrame(long maxStreams,
                           boolean appliesToBidirectional) implements FrameReceived {
        private static final FrameType TYPE = FrameType.MaxStreamsFrame;

        public FrameType type() {
            return TYPE;
        }
    }

    /**
     * Represents a new connection id frame.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-new_connection_id-frames">...</a>
     */
    record NewConnectionIdFrame(int sequenceNr, int retirePriorTo, byte[] connectionId,
                                byte[] statelessResetToken) implements FrameReceived {
        private static final FrameType TYPE = FrameType.NewConnectionIdFrame;

        public FrameType type() {
            return TYPE;
        }
    }

    /**
     * Represents a new token frame.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-new_token-frames">...</a>
     */
    record NewTokenFrame(byte[] token) implements FrameReceived {
        private static final FrameType TYPE = FrameType.NewTokenFrame;

        public FrameType type() {
            return TYPE;
        }
    }

    /**
     * Represents a number of consecutive padding frames.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-padding-frames">...</a>
     * <p>
     * Usually, padding will consist of multiple padding frames, each being exactly one (zero) byte. This class can
     * represent an arbitrary number of consecutive padding frames, by recording padding length.
     */
    record PaddingFrame(int length) implements FrameReceived {
        private static final FrameType TYPE = FrameType.PaddingFrame;

        public FrameType type() {
            return TYPE;
        }
    }

    /**
     * Represents a path challenge frame.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-path_challenge-frames">...</a>
     */
    record PathChallengeFrame(byte[] data) implements FrameReceived {
        private static final FrameType TYPE = FrameType.PathChallengeFrame;

        public FrameType type() {
            return TYPE;
        }
    }

    /**
     * Represents a path response frame.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-path_response-frames">...</a>
     */
    record PathResponseFrame(byte[] data) implements FrameReceived {
        private static final FrameType TYPE = FrameType.PathResponseFrame;

        public FrameType type() {
            return TYPE;
        }
    }

    /**
     * Represents a ping frame.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-ping-frames">...</a>
     */
    record PingFrame() implements FrameReceived {
        private static final FrameType TYPE = FrameType.PingFrame;

        public FrameType type() {
            return TYPE;
        }
    }

    /**
     * Represents a reset stream frame.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-reset_stream-frames">...</a>
     */
    record ResetStreamFrame(int streamId, long errorCode, long finalSize) implements FrameReceived {
        private static final FrameType TYPE = FrameType.ResetStreamFrame;

        public FrameType type() {
            return TYPE;
        }
    }

    /**
     * Represents a retire connection id frame.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-retire_connection_id-frames">...</a>
     */
    record RetireConnectionIdFrame(int sequenceNumber) implements FrameReceived {
        private static final FrameType TYPE = FrameType.RetireConnectionIdFrame;

        public FrameType type() {
            return TYPE;
        }
    }

    /**
     * Represents a stop sending frame.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-stop_sending-frames">...</a>
     */
    record StopSendingFrame(int streamId, long errorCode) implements FrameReceived {
        private static final FrameType TYPE = FrameType.StopSendingFrame;

        public FrameType type() {
            return TYPE;
        }
    }

    /**
     * Represents a stream data blocked frame.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-stream_data_blocked-frames">...</a>
     */
    record StreamDataBlockedFrame(int streamId, long streamDataLimit) implements FrameReceived {
        private static final FrameType TYPE = FrameType.StreamDataBlockedFrame;

        public FrameType type() {
            return TYPE;
        }
    }

    record StreamFrame(int streamId, boolean isFinal, long offset, int length, byte[] streamData)
            implements FrameReceived, Comparable<FrameReceived.StreamFrame> {

        private static final FrameType TYPE = FrameType.StreamFrame;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            StreamFrame that = (StreamFrame) o;
            return streamId == that.streamId && isFinal == that.isFinal
                    && offset == that.offset && length == that.length
                    && Arrays.equals(streamData, that.streamData);
        }

        public FrameType type() {
            return TYPE;
        }

        @Override
        public int compareTo(FrameReceived.StreamFrame other) {
            if (this.offset == other.offset()) {
                return Long.compare(this.length, other.length());
            } else {
                return Long.compare(this.offset, other.offset());
            }
        }


        public long offsetLength() {
            return offset + length;
        }


    }

    /**
     * Represents a streams blocked frame.
     * <a href="https://www.rfc-editor.org/rfc/rfc9000.html#name-streams_blocked-frames">...</a>
     */
    record StreamsBlockedFrame(FrameType type, boolean bidirectional,
                               int streamLimit) implements FrameReceived {
    }

}


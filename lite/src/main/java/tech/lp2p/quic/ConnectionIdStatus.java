package tech.lp2p.quic;

enum ConnectionIdStatus {
    NEW,
    IN_USE,
    USED,
    RETIRED;

    boolean active() {
        // https://www.rfc-editor.org/rfc/rfc9000.html#name-issuing-connection-ids
        // "Connection IDs that are issued and not retired are considered active;..."
        return this != RETIRED;
    }

    boolean notUnusedOrRetired() {
        return this != NEW && this != RETIRED;
    }
}


package tech.lp2p.quic;

import java.util.function.Consumer;
import java.util.function.Function;

record SendRequest(int estimatedSize,
                   Function<Integer, Frame> frameSupplier,
                   Consumer<Frame> lostCallback,
                   Consumer<Frame> receivedCallback) {

}


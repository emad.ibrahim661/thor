package tech.lp2p.quic;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.function.Consumer;

final class Receiver {
    private final Consumer<Throwable> abortCallback;
    private final Consumer<DatagramPacket> consumer;
    private final Thread receiverThread;
    private final DatagramSocket socket;
    private volatile boolean isClosing = false;


    Receiver(DatagramSocket socket, Consumer<DatagramPacket> consumer,
             Consumer<Throwable> abortCallback) {
        this.socket = socket;
        this.abortCallback = abortCallback;
        this.consumer = consumer;

        receiverThread = new Thread(this::run, "receiver-loop");
        receiverThread.setDaemon(true);
        receiverThread.setPriority(Thread.MAX_PRIORITY);
    }

    public void start() {
        receiverThread.start();
    }

    public void shutdown() {
        isClosing = true;
        receiverThread.interrupt();
    }


    private void run() {
        try {

            byte[] receiveBuffer = new byte[Settings.MAX_DATAGRAM_PACKET_SIZE];
            while (!isClosing) {
                DatagramPacket receivedPacket = new DatagramPacket(receiveBuffer, receiveBuffer.length);
                socket.receive(receivedPacket);
                consumer.accept(receivedPacket);
            }

        } catch (IOException ioException) {
            if (!isClosing) {
                // This is probably fatal
                abortCallback.accept(ioException);
            }
        } catch (Throwable fatal) {
            abortCallback.accept(fatal);
        }
    }
}

package tech.lp2p.quic;


import static tech.lp2p.quic.Role.Client;
import static tech.lp2p.quic.Role.Server;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.AEADBadTagException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import tech.lp2p.tls.BadRecordMacAlert;
import tech.lp2p.tls.DecryptErrorAlert;
import tech.lp2p.tls.TlsState;
import tech.lp2p.tls.TrafficSecrets;

public record Keys(SecretKeySpec writeSpec, byte[] writeIV,
                   Cipher hpCipher, Cipher aeadCipher,
                   byte[] trafficSecret, int keyPhaseCounter) {


    private static final short KEY_LENGTH = 16;

    public static Keys createInitialKeys(Version version, byte[] initialSecret, Role nodeRole) {

        try {
            Mac mac = Mac.getInstance("HmacSHA256");

            byte[] initialNodeSecret = hkdfExpandLabel(mac, initialSecret, nodeRole == Client ?
                    "client in" : "server in", (short) 32);

            return computeKeys(version, initialNodeSecret);
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }

    // See https://tools.ietf.org/html/rfc8446#section-7.1 for definition of HKDF-Expand-Label.
    private static byte[] hkdfExpandLabel(Mac mac, byte[] secret, String label, short length) throws BadRecordMacAlert {

        byte[] prefix;
        // https://tools.ietf.org/html/draft-ietf-quic-tls-17#section-5.1:
        // "The keys used for packet protection are computed from the TLS secrets using the KDF provided by TLS."
        prefix = "tls13 ".getBytes(StandardCharsets.ISO_8859_1);

        ByteBuffer hkdfLabel = ByteBuffer.allocate(2 + 1 + prefix.length + label.getBytes(StandardCharsets.ISO_8859_1).length + 1 + "".getBytes(StandardCharsets.ISO_8859_1).length);
        hkdfLabel.putShort(length);
        hkdfLabel.put((byte) (prefix.length + label.getBytes().length));
        hkdfLabel.put(prefix);
        hkdfLabel.put(label.getBytes(StandardCharsets.ISO_8859_1));
        hkdfLabel.put((byte) ("".getBytes(StandardCharsets.ISO_8859_1).length));
        hkdfLabel.put("".getBytes(StandardCharsets.ISO_8859_1));

        return TlsState.expandHmac(mac, secret, hkdfLabel.array(), length);
    }


    /**
     * Compute new keys. Note that depending on the role of this Keys object, computing new keys concerns updating
     * the write secrets (role that initiates the key update) or the read secrets (role that responds to the key update).
     */
    public static Keys computeKeyUpdate(Version version, Keys keys) {
        String prefix;
        // https://tools.ietf.org/html/draft-ietf-quic-tls-17#section-5.1
        // "The current encryption level secret and the label "quic key" are
        //   input to the KDF to produce the AEAD key; the label "quic iv" is used
        //   to derive the IV, see Section 5.3.  The header protection key uses
        //   the "quic hp" label, see Section 5.4).  Using these labels provides
        //   key separation between QUIC and TLS, see Section 9.4."
        prefix = "quic ";
        if (version.isV2()) {
            // https://www.ietf.org/archive/id/draft-ietf-quic-v2-01.html#name-long-header-packet-types
            // "The labels used in [QUIC-TLS] to derive packet protection keys (Section 5.1), header protection keys (Section 5.4),
            //  Retry Integrity Tag keys (Section 5.8), and key updates (Section 6.1) change from "quic key" to "quicv2 key",
            //  from "quic iv" to "quicv2 iv", from "quic hp" to "quicv2 hp", and from "quic ku" to "quicv2 ku", to meet
            //  the guidance for new versions in Section 9.6 of that document."
            prefix = "quicv2 ";
        }
        try {
            Mac mac = Mac.getInstance("HmacSHA256");
            byte[] trafficSecret = hkdfExpandLabel(mac, keys.trafficSecret(), prefix + "ku", (short) 32);

            byte[] writeKey = hkdfExpandLabel(mac, trafficSecret, prefix + "key", KEY_LENGTH);

            byte[] writeIV = hkdfExpandLabel(mac, trafficSecret, prefix + "iv", (short) 12);

            return new Keys(new SecretKeySpec(writeKey, "AES"),
                    writeIV, keys.hpCipher, keys.aeadCipher,
                    trafficSecret, (keys.keyPhaseCounter() + 1));
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }

    static Keys computeHandshakeKeys(Version version, Role nodeRole, TrafficSecrets secrets) {
        if (nodeRole == Client) {
            return computeKeys(version, secrets.getClientHandshakeTrafficSecret());
        }
        if (nodeRole == Server) {
            return computeKeys(version, secrets.getServerHandshakeTrafficSecret());
        }
        throw new RuntimeException("not handled role");
    }

    static Keys computeApplicationKeys(Version version, Role nodeRole, TrafficSecrets secrets) {
        if (nodeRole == Client) {
            return computeKeys(version, secrets.getClientApplicationTrafficSecret());
        }
        if (nodeRole == Server) {
            return computeKeys(version, secrets.getServerApplicationTrafficSecret());
        }
        throw new RuntimeException("not handled role");
    }

    /**
     * @noinspection ExtractMethodRecommender
     */
    private static Keys computeKeys(Version version, byte[] trafficSecret) {


        // https://tools.ietf.org/html/draft-ietf-quic-tls-17#section-5.1
        // "The current encryption level secret and the label "quic key" are
        //   input to the KDF to produce the AEAD key; the label "quic iv" is used
        //   to derive the IV, see Section 5.3.  The header protection key uses
        //   the "quic hp" label, see Section 5.4).  Using these labels provides
        //   key separation between QUIC and TLS, see Section 9.4."
        String prefix = "quic ";
        if (version.isV2()) {
            // https://www.ietf.org/archive/id/draft-ietf-quic-v2-01.html#name-long-header-packet-types
            // "The labels used in [QUIC-TLS] to derive packet protection keys (Section 5.1), header protection keys (Section 5.4),
            //  Retry Integrity Tag keys (Section 5.8), and key updates (Section 6.1) change from "quic key" to "quicv2 key",
            //  from "quic iv" to "quicv2 iv", from "quic hp" to "quicv2 hp", and from "quic ku" to "quicv2 ku", to meet
            //  the guidance for new versions in Section 9.6 of that document."
            prefix = "quicv2 ";
        }

        try {
            Mac mac = Mac.getInstance("HmacSHA256");
            // https://tools.ietf.org/html/rfc8446#section-7.3
            byte[] writeKey = hkdfExpandLabel(mac, trafficSecret, prefix + "key", KEY_LENGTH);

            byte[] writeIV = hkdfExpandLabel(mac, trafficSecret, prefix + "iv", (short) 12);

            // https://tools.ietf.org/html/draft-ietf-quic-tls-17#section-5.1
            // "The header protection key uses the "quic hp" label"
            byte[] hp = hkdfExpandLabel(mac, trafficSecret, prefix + "hp", KEY_LENGTH);

            return new Keys(new SecretKeySpec(writeKey, "AES"),
                    writeIV, getHeaderProtectionCipher(hp),
                    Cipher.getInstance("AES/GCM/NoPadding"),
                    trafficSecret, 0);
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }

    private static Cipher getHeaderProtectionCipher(byte[] hp) {
        try {
            // https://tools.ietf.org/html/draft-ietf-quic-tls-27#section-5.4.3
            // "AEAD_AES_128_GCM and AEAD_AES_128_CCM use 128-bit AES [AES] in electronic code-book (ECB) mode."
            Cipher hpCipher = Cipher.getInstance("AES/ECB/NoPadding");
            SecretKeySpec keySpec = new SecretKeySpec(hp, "AES");
            hpCipher.init(Cipher.ENCRYPT_MODE, keySpec);
            return hpCipher;
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException e) {
            throw new IllegalStateException(e);
        }
    }

    public short getKeyPhase() {
        return (short) (keyPhaseCounter % 2);
    }

    /**
     * Check whether the key phase carried by a received packet still matches the current key phase; if not, compute
     * new keys (to be used for decryption). Note that the changed key phase can also be caused by packet corruption,
     * so it is not yet sure whether a key update is really in progress (this will be sure when decryption of the packet
     * failed or succeeded). This function will return true, when update is required.
     */
    public boolean checkKeyPhase(short keyPhaseBit) {
        return (keyPhaseCounter % 2) != keyPhaseBit;
    }

    public byte[] getWriteIV() {
        return writeIV;
    }

    public byte[] aeadEncrypt(byte[] associatedData, byte[] nonce,
                              byte[] input, int inputOffset, int inputLength) {
        try {
            // From https://tools.ietf.org/html/draft-ietf-quic-tls-16#section-5.3:
            // "Prior to establishing a shared secret, packets are protected with AEAD_AES_128_GCM"
            GCMParameterSpec parameterSpec = new GCMParameterSpec(128, nonce);   // https://tools.ietf.org/html/rfc5116#section-5.3: "the tag length t is 16"
            aeadCipher.init(Cipher.ENCRYPT_MODE, writeSpec, parameterSpec);
            aeadCipher.updateAAD(associatedData);
            return aeadCipher.doFinal(input, inputOffset, inputLength);
        } catch (InvalidKeyException | InvalidAlgorithmParameterException |
                 IllegalBlockSizeException | BadPaddingException e) {
            throw new IllegalStateException();
        }
    }

    public byte[] aeadDecrypt(byte[] associatedData, byte[] nonce,
                              byte[] input, int inputOffset, int inputLength) throws DecryptErrorAlert {
        if (input.length <= 16) {
            // https://www.rfc-editor.org/rfc/rfc9001.html#name-aead-usage
            // "These cipher suites have a 16-byte authentication tag and produce an output 16 bytes larger than their input."
            throw new DecryptErrorAlert("ciphertext must be longer than 16 bytes");
        }
        try {
            // From https://tools.ietf.org/html/draft-ietf-quic-tls-16#section-5.3:
            // "Prior to establishing a shared secret, packets are protected with AEAD_AES_128_GCM"
            GCMParameterSpec parameterSpec = new GCMParameterSpec(128, nonce);   // https://tools.ietf.org/html/rfc5116#section-5.3: "the tag length t is 16"
            aeadCipher.init(Cipher.DECRYPT_MODE, writeSpec, parameterSpec);
            aeadCipher.updateAAD(associatedData);
            return aeadCipher.doFinal(input, inputOffset, inputLength);
        } catch (AEADBadTagException decryptError) {
            throw new DecryptErrorAlert(decryptError.toString());
        } catch (InvalidKeyException | InvalidAlgorithmParameterException |
                 IllegalBlockSizeException | BadPaddingException e) {
            throw new IllegalStateException(e);
        }
    }

    public byte[] createHeaderProtectionMask(byte[] sample) {
        byte[] mask;
        try {
            mask = hpCipher.doFinal(sample);
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            throw new IllegalStateException(e);
        }
        return mask;
    }

}

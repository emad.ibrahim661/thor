package tech.lp2p.quic;

import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;


/**
 * Assembles QUIC packets for a given encryption level, based on "send requests" that are previously queued.
 * These send requests either contain a frame, or can produce a frame to be sent.
 */
final class PacketAssembler {

    private final Version version;
    private final Level level;
    private final SendRequestQueue sendRequestQueue;
    private final AckGenerator ackGenerator;
    private long packetNumberGenerator = 0L; // no concurrency


    PacketAssembler(Version version, Level level, SendRequestQueue sendRequestQueue,
                    AckGenerator ackGenerator) {
        this.version = version;
        this.level = level;
        this.sendRequestQueue = sendRequestQueue;
        this.ackGenerator = ackGenerator;
    }

    private static Consumer<Packet> createPacketCallback(
            Packet packet, List<Consumer<Frame>> consumers) {
        if (packet.frames().length != consumers.size()) {
            throw new IllegalStateException();
        }
        return new PacketAssemblerCallback(consumers);

    }


    private static void addPadding(PacketSkeleton packet) {
        if (packet.isInitialPacket()) {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-34#section-14
            // "A client MUST expand the payload of all UDP datagrams carrying Initial packets to at least the smallest
            //  allowed maximum datagram size of 1200 bytes... "
            // "Similarly, a server MUST expand the payload of all UDP datagrams carrying ack-eliciting Initial packets
            //  to at least the smallest allowed maximum datagram size of 1200 bytes."

            int requiredPadding = 1200 - packet.estimateLength(0);
            if (requiredPadding > 0) {
                packet.addFrame(Frame.createPaddingFrame(requiredPadding),
                        Settings.EMPTY_FRAME_CALLBACK,
                        Settings.EMPTY_FRAME_CALLBACK);
            }
        }
        if (packet.hasPathChallengeOrResponse()) {

            // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-8.2.1
            // "An endpoint MUST expand datagrams that contain a PATH_CHALLENGE frame to at least the smallest allowed
            //  maximum datagram size of 1200 bytes."
            // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-8.2.2
            // "An endpoint MUST expand datagrams that contain a PATH_RESPONSE frame to at least the smallest allowed
            // maximum datagram size of 1200 bytes."


            int requiredPadding = 1200 - packet.estimateLength(0);
            if (requiredPadding > 0) {
                packet.addFrame(Frame.createPaddingFrame(requiredPadding),
                        Settings.EMPTY_FRAME_CALLBACK,
                        Settings.EMPTY_FRAME_CALLBACK);
            }

        }
    }

    static Packet createPacket(Version version, PacketSkeleton skeleton) {

        Frame[] frames = new Frame[skeleton.frames().size()];
        skeleton.frames().toArray(frames);
        return switch (skeleton.level()) {
            case App -> PacketService.createShortHeader(version,
                    frames, skeleton.packetNumber(), skeleton.destinationConnectionId());
            case Handshake -> PacketService.createHandshake(version, frames,
                    skeleton.packetNumber(), skeleton.sourceConnectionId(),
                    skeleton.destinationConnectionId());
            case Initial -> PacketService.createInitial(version, frames,
                    skeleton.packetNumber(), skeleton.sourceConnectionId(),
                    skeleton.destinationConnectionId());
        };

    }

    /**
     * Assembles a QUIC packet for the encryption level handled by this instance.
     *
     * @param sourceConnectionId can be null when encryption level is 1-rtt; but not for the other levels; can be empty array though
     */

    @Nullable
    PacketSend assemble(int remainingCwndSize, int availablePacketSize,
                        byte[] sourceConnectionId, byte[] destinationConnectionId) {

        if (level == Level.Initial && availablePacketSize < 1200) {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-34#section-14
            // "A client MUST expand the payload of all UDP datagrams carrying Initial packets to at least the smallest
            //  allowed maximum datagram size of 1200 bytes... "
            // "Similarly, a server MUST expand the payload of all UDP datagrams carrying ack-eliciting Initial packets
            //  to at least the smallest allowed maximum datagram size of 1200 bytes."
            // Note that in case of an initial packet, the availablePacketSize equals the maximum datagram size; even
            // when different packets are coalesced, the initial packet is always the first that is assembled.
            return null;
        }
        // Packet can be 3 bytes larger than estimated size because of unknown packet number length.
        final int available = Integer.min(remainingCwndSize, availablePacketSize - 3);

        PacketSkeleton skeleton = switch (level) {
            case Handshake, Initial -> new PacketSkeleton(level, new ArrayList<>(),
                    new ArrayList<>(), new ArrayList<>(), packetNumberGenerator++,
                    sourceConnectionId, destinationConnectionId);
            case App -> new PacketSkeleton(level, new ArrayList<>(), new ArrayList<>(),
                    new ArrayList<>(), packetNumberGenerator++, null,
                    destinationConnectionId);
        };

        // Check for an explicit ack, i.e. an ack on ack-eliciting packet that cannot be delayed (any longer)
        if (ackGenerator.mustSendAck(level)) {

            Frame ackFrame = ackGenerator.generateAck(skeleton.packetNumber(),
                    length -> skeleton.estimateLength(length) <= availablePacketSize);

            // https://tools.ietf.org/html/draft-ietf-quic-transport-29#section-13.2
            // "... packets containing only ACK frames are not congestion controlled ..."
            // So: only check if it fits within available packet space
            if (ackFrame != null) {
                skeleton.addFrame(ackFrame, Settings.EMPTY_FRAME_CALLBACK,
                        Settings.EMPTY_FRAME_CALLBACK);
            } else {
                // If not even a mandatory ack can be added, don't bother about other frames: theoretically there might be frames
                // that can be fit, but this is very unlikely to happen (because limit packet size is caused by coalescing packets
                // in one datagram, which will only happen during handshake, when acks are still small) and even then: there
                // will be a next packet in due time.
                // AckFrame does not fit in availablePacketSize, so just return;
                return null;
            }
        }


        //   Probe packets MUST NOT be blocked by the congestion controller  -> look remainingCwndSize
        if (sendRequestQueue.hasProbeWithData()) {
            Frame[] probeData = sendRequestQueue.getProbe();
            // Probe is not limited by congestion control, but it is limited by max packet size.
            int estimatedSize = skeleton.estimateLength(PacketService.estimatedFramesSize(probeData));
            if (estimatedSize > availablePacketSize) {
                Frame probeFrame = Frame.PING;
                if (skeleton.estimateLength(probeFrame.frameLength()) > availablePacketSize) {
                    return null;
                }
                probeData = new Frame[]{probeFrame};
            }
            for (Frame frame : probeData) {
                skeleton.addFrame(frame, Settings.EMPTY_FRAME_CALLBACK,
                        Settings.EMPTY_FRAME_CALLBACK);
            }
            addPadding(skeleton);
            Packet packet = createPacket(version, skeleton);
            return new PacketSend(packet, Settings.EMPTY_PACKET_CALLBACK,
                    Settings.EMPTY_PACKET_CALLBACK);
        }


        if (sendRequestQueue.hasRequests()) {
            // Must create packet here, to have an initial estimate of packet header overhead
            int estimatedSize = skeleton.estimateLength(1000) - 1000;  // Estimate length if large frame would have been added; this will give upper limit of packet overhead.

            while (estimatedSize < available) {
                // First try to find a frame that will leave space for optional frame (if any)
                int proposedSize = available - estimatedSize;
                SendRequest next = sendRequestQueue.next(proposedSize);

                if (next == null) {
                    // Nothing fits within available space
                    break;
                }
                Frame nextFrame = next.frameSupplier().apply(proposedSize);
                if (nextFrame != null) {
                    if (nextFrame.frameLength() > proposedSize) {
                        throw new IllegalStateException(
                                "supplier does not produce frame of right (max) size: " +
                                        nextFrame.frameLength() + " > " + (proposedSize)
                                        + " frame: " + nextFrame);
                    }

                    estimatedSize += nextFrame.frameLength();
                    skeleton.addFrame(nextFrame, next.lostCallback(), next.receivedCallback());

                }
            }
        }


        //   Probe packets MUST NOT be blocked by the congestion controller  -> look remainingCwndSize
        if (sendRequestQueue.hasProbe() && skeleton.frames().isEmpty()) {
            sendRequestQueue.getProbe();
            skeleton.addFrame(Frame.PING, Settings.EMPTY_FRAME_CALLBACK,
                    Settings.EMPTY_FRAME_CALLBACK);
        }

        PacketSend assembledItem;
        if (skeleton.frames().isEmpty()) {
            // Nothing could be added, discard packet and mark packet number as not used
            packetNumberGenerator--;
            assembledItem = null;
        } else {
            addPadding(skeleton);
            Packet packet = createPacket(version, skeleton);
            assembledItem = new PacketSend(packet,
                    createPacketCallback(packet, skeleton.lost()),
                    createPacketCallback(packet, skeleton.received()));
        }
        return assembledItem;

    }


    private record PacketAssemblerCallback(List<Consumer<Frame>> callbacks)
            implements Consumer<Packet> {

        @Override
        public void accept(Packet packet) {
            for (int i = 0; i < callbacks.size(); i++) {
                if (callbacks.get(i) != Settings.EMPTY_FRAME_CALLBACK) {
                    Frame frame = packet.frames()[i];
                    callbacks.get(i).accept(frame);
                }
            }
        }
    }

    private record PacketSkeleton(Level level, List<Frame> frames,
                                  List<Consumer<Frame>> lost,
                                  List<Consumer<Frame>> received,
                                  long packetNumber, byte[] sourceConnectionId,
                                  byte[] destinationConnectionId) {

        public boolean isInitialPacket() {
            return level() == Level.Initial;
        }

        int framesLength() {
            int sum = 0;
            for (Frame frame : frames()) {
                sum += frame.frameLength();
            }
            return sum;
        }

        public boolean hasPathChallengeOrResponse() {
            for (Frame frame : frames()) {
                if (frame.frameType() == FrameType.PathResponseFrame
                        || frame.frameType() == FrameType.PathChallengeFrame) {
                    return true;
                }
            }
            return false;
        }

        public void addFrame(Frame frame,
                             Consumer<Frame> lostCallback,
                             Consumer<Frame> receivedCallback) {
            frames.add(frame);
            lost.add(lostCallback);
            received.add(receivedCallback);
        }


        /**
         * Estimates what the length of this packet will be after it has been encrypted. The returned length must be
         * less then or equal the actual length after encryption. Length estimates are used when preparing packets for
         * sending, where certain limits must be met (e.g. congestion control, max datagram size, ...).
         *
         * @param additionalPayload when not 0, estimate the length if this amount of additional (frame) bytes were added.
         */
        public int estimateLength(int additionalPayload) {
            return switch (level()) {
                case App -> estimateShortHeaderPacketLength(additionalPayload);
                case Handshake -> estimateHandshakePacketLength(additionalPayload);
                case Initial -> estimateInitialPacketLength(additionalPayload);
            };
        }

        public int estimateShortHeaderPacketLength(int additionalPayload) {
            int payloadLength = framesLength() + additionalPayload;
            return 1
                    + destinationConnectionId.length
                    + 1  // packet number length: will usually be just 1, actual value cannot be computed until packet number is known
                    + payloadLength
                    // https://tools.ietf.org/html/draft-ietf-quic-tls-27#section-5.4.2
                    // "The ciphersuites defined in [TLS13] - (...) - have 16-byte expansions..."
                    + 16;
        }


        public int estimateHandshakePacketLength(int additionalPayload) {
            int payloadLength = framesLength() + additionalPayload;
            return 1
                    + 4
                    + 1 + destinationConnectionId.length
                    + 1 + sourceConnectionId.length
                    + (payloadLength + 1 > 63 ? 2 : 1)
                    + 1  // packet number length: will usually be just 1, actual value cannot be computed until packet number is known
                    + payloadLength
                    // https://tools.ietf.org/html/draft-ietf-quic-tls-27#section-5.4.2
                    // "The ciphersuites defined in [TLS13] - (...) - have 16-byte expansions..."
                    + 16;
        }


        public int estimateInitialPacketLength(int additionalPayload) {
            int payloadLength = framesLength() + additionalPayload;
            return 1
                    + 4
                    + 1 + destinationConnectionId.length
                    + 1 + sourceConnectionId.length
                    + 1 // token length, which is not supported
                    + (payloadLength + 1 > 63 ? 2 : 1)
                    + 1  // packet number length: will usually be just 1, actual value cannot be computed until packet number is known
                    + payloadLength
                    // https://tools.ietf.org/html/draft-ietf-quic-tls-27#section-5.4.2
                    // "The ciphersuites defined in [TLS13] - (...) - have 16-byte expansions..."
                    + 16;
        }
    }

}


package tech.lp2p.quic;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.net.ConnectException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeoutException;
import java.util.function.Function;

import javax.net.ssl.X509TrustManager;

import tech.lp2p.Lite;
import tech.lp2p.core.ALPN;
import tech.lp2p.core.Certificate;
import tech.lp2p.core.Host;
import tech.lp2p.core.Network;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Responder;
import tech.lp2p.lite.LiteTrustManager;
import tech.lp2p.tls.CipherSuite;
import tech.lp2p.utils.Utils;

public final class ConnectionBuilder {

    @NotNull
    public static Connection connect(@NotNull Host host,
                                     @NotNull Peeraddr address,
                                     @NotNull Parameters parameters,
                                     @NotNull Responder responder)
            throws ConnectException, InterruptedException, TimeoutException {


        DatagramSocket socket;
        try {
            socket = new DatagramSocket();
        } catch (SocketException socketException) {
            throw new ConnectException(socketException.getMessage());
        }

        return ConnectionBuilder.connection(host, address, parameters, responder,
                socket, null);

    }

    @NotNull
    public static Connection connect(@NotNull Host host,
                                     @NotNull Peeraddr address,
                                     @NotNull Parameters parameters,
                                     @NotNull Responder responder,
                                     @NotNull DatagramSocket datagramSocket)
            throws ConnectException, InterruptedException, TimeoutException {

        return ConnectionBuilder.connection(host, address, parameters, responder,
                datagramSocket, null);

    }

    @NotNull
    public static Connection connect(@NotNull Host host,
                                     @NotNull Peeraddr address,
                                     @NotNull Parameters parameters,
                                     @NotNull Responder responder,
                                     @NotNull ServerConnector serverConnector)
            throws ConnectException, InterruptedException, TimeoutException {

        return ConnectionBuilder.connection(host, address, parameters, responder,
                serverConnector.datagramSocket(), serverConnector);

    }

    @NotNull
    private static ClientConnection connection(@NotNull Host host,
                                               @NotNull Peeraddr address,
                                               @NotNull Parameters parameters,
                                               @NotNull Responder responder,
                                               @NotNull DatagramSocket datagramSocket,
                                               @Nullable ServerConnector serverConnector)
            throws ConnectException, InterruptedException, TimeoutException {

        InetAddress inetAddress;
        try {
            inetAddress = InetAddress.getByAddress(address.address());
        } catch (Throwable throwable) {
            throw new ConnectException("invalid address " + address.peerId());
        }
        Objects.requireNonNull(inetAddress);

        int initialRtt = Settings.INITIAL_RTT;
        if (Network.isLocalAddress(inetAddress)) {
            initialRtt = 100;
        }

        if (!responder.validAlpns(parameters.alpn())) {
            throw new ConnectException("invalid alpn configuration");
        }

        Function<Stream, StreamHandler> streamHandlerFunction = null;
        if (parameters.alpn() == ALPN.libp2p) {
            streamHandlerFunction = quicStream -> AlpnLibp2pResponder.create(responder);
        }
        if (parameters.alpn() == ALPN.lite) {
            streamHandlerFunction = quicStream -> AlpnLiteResponder.create(responder);
        }
        Objects.requireNonNull(streamHandlerFunction, "Alpn not handled");

        X509TrustManager trustManager;
        if (parameters.validatePeerId()) {
            trustManager = new LiteTrustManager(address.peerId());
        } else {
            trustManager = new LiteTrustManager();
        }
        Certificate certificate = host.certificate();
        Objects.requireNonNull(certificate);

        return ClientConnection.connect(host, parameters.alpn(), inetAddress.getHostName(),
                address.peerId(), new InetSocketAddress(inetAddress, address.port()),
                Version.QUIC_version_1, parameters.initialMaxData(), initialRtt,
                List.of(CipherSuite.TLS_AES_128_GCM_SHA256),
                trustManager, certificate.x509Certificate(), certificate.key(),
                datagram -> Utils.debug(Arrays.toString(datagram)),
                streamHandlerFunction,
                parameters.keepAlive(), Lite.CONNECT_TIMEOUT,
                datagramSocket, serverConnector);

    }
}

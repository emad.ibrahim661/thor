package tech.lp2p.quic;


import static tech.lp2p.quic.Level.Handshake;
import static tech.lp2p.quic.Level.Initial;
import static tech.lp2p.quic.TransportError.Code.PROTOCOL_VIOLATION;
import static tech.lp2p.quic.TransportError.Code.TRANSPORT_PARAMETER_ERROR;
import static tech.lp2p.quic.TransportError.Code.VERSION_NEGOTIATION_ERROR;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.net.ConnectException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;
import java.util.function.Function;

import javax.net.ssl.X509TrustManager;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.Host;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.tls.ApplicationLayerProtocolNegotiationExtension;
import tech.lp2p.tls.CertificateMessage;
import tech.lp2p.tls.CertificateVerifyMessage;
import tech.lp2p.tls.CertificateWithPrivateKey;
import tech.lp2p.tls.CipherSuite;
import tech.lp2p.tls.ClientHello;
import tech.lp2p.tls.ClientMessageSender;
import tech.lp2p.tls.EarlyDataExtension;
import tech.lp2p.tls.Extension;
import tech.lp2p.tls.FinishedMessage;
import tech.lp2p.tls.TlsClientEngine;
import tech.lp2p.tls.TlsStatusEventHandler;
import tech.lp2p.utils.Utils;

public final class ClientConnection extends Connection implements ConnectionProxy {


    private final TlsClientEngine tlsEngine;
    private final DatagramSocket datagramSocket;
    private final Receiver receiver;
    private final ConnectionIdManager connectionIdManager;
    private final CountDownLatch handshakeFinishedCondition = new CountDownLatch(1);
    private final TransportParameters transportParams;
    private final PeerId remotePeerId;
    private final ALPN alpn;
    private final Function<Stream, StreamHandler> streamHandlerFunction;
    @Nullable
    private final ServerConnector serverConnector;


    private ClientConnection(Host host, ALPN alpn, String serverName, PeerId remotePeerId,
                             InetSocketAddress remoteAddress, Version version,
                             int initialMaxData, int initialRtt,
                             List<CipherSuite> cipherSuites, X509TrustManager trustManager,
                             X509Certificate clientCertificate, PrivateKey clientCertificateKey,
                             Consumer<byte[]> datagramConsumer,
                             Function<Stream, StreamHandler> streamHandlerFunction,
                             @NotNull DatagramSocket datagramSocket,
                             @Nullable ServerConnector serverConnector) {

        super(host, version, Role.Client, initialMaxData, initialRtt, datagramConsumer,
                datagramSocket, remoteAddress);
        this.alpn = alpn;
        this.remotePeerId = remotePeerId;
        this.streamHandlerFunction = streamHandlerFunction;
        this.serverConnector = serverConnector;
        this.datagramSocket = datagramSocket;

        if (serverConnector == null) {
            this.receiver = new Receiver(datagramSocket,
                    new DatagramPacketConsumer(), new ThrowableConsumer());
        } else {
            this.receiver = serverConnector.receiver();
        }
        Objects.requireNonNull(datagramSocket, "Socket not defined");
        Objects.requireNonNull(receiver, "Receiver not defined");

        Consumer<TransportError> transportError = (error) ->
                immediateCloseWithError(Level.App, error);
        int activeConnectionIdLimit = Settings.ACTIVE_CONNECTION_ID_LIMIT;
        this.connectionIdManager = new ConnectionIdManager(
                activeConnectionIdLimit, getSendRequestQueue(Level.App),
                transportError);


        TransportParameters.VersionInformation versionInformation = null;
        if (this.version.isV2()) {
            Version[] otherVersions = {Version.QUIC_version_2, Version.QUIC_version_1};
            versionInformation = new TransportParameters.VersionInformation(
                    Version.QUIC_version_2, otherVersions);
        }

        this.transportParams = TransportParameters.createClient(
                connectionIdManager.initialSourceCid(), initialMaxData,
                activeConnectionIdLimit, versionInformation);

        Extension tpExtension = TransportParametersExtension.create(
                this.version, transportParams, Role.Client);
        Extension aplnExtension = ApplicationLayerProtocolNegotiationExtension.create(alpn.name());
        this.tlsEngine = new TlsClientEngine(serverName, trustManager, cipherSuites,
                List.of(tpExtension, aplnExtension),
                new CryptoMessageSender(), new StatusEventHandler());
        initializeCryptoStreams(tlsEngine);

        tlsEngine.setHostnameVerifier((hostname, serverCertificate) -> true);

        tlsEngine.setClientCertificateCallback(authorities ->
                new CertificateWithPrivateKey(clientCertificate, clientCertificateKey));
    }

    public static ClientConnection connect(Host host, ALPN alpn, String serverName,
                                           PeerId remotePeerId,
                                           InetSocketAddress remoteAddress, Version version,
                                           int initialMaxData,
                                           int initialRtt,
                                           List<CipherSuite> cipherSuites,
                                           X509TrustManager trustManager,
                                           X509Certificate clientCertificate,
                                           PrivateKey clientCertificateKey,
                                           Consumer<byte[]> datagramConsumer,
                                           Function<Stream, StreamHandler> streamHandlerFunction,
                                           boolean keepAlive, int timeout,
                                           DatagramSocket datagramSocket,
                                           @Nullable ServerConnector serverConnector) throws
            InterruptedException, ConnectException, TimeoutException {

        ClientConnection clientConnection = new ClientConnection(host, alpn,
                serverName, remotePeerId, remoteAddress, version, initialMaxData, initialRtt,
                cipherSuites, trustManager, clientCertificate,
                clientCertificateKey, datagramConsumer, streamHandlerFunction,
                datagramSocket, serverConnector);


        clientConnection.startHandshake();

        try {
            boolean handshakeFinished = clientConnection.handshakeFinishedCondition.await(
                    timeout, TimeUnit.SECONDS);
            if (!handshakeFinished) {
                clientConnection.abortHandshake();
                Utils.error("Timeout error " + remotePeerId.toBase58());
                throw new TimeoutException("Connection timed out after " + timeout + " s");
            } else if (clientConnection.connectionState.get() != Status.Connected) {
                clientConnection.abortHandshake();
                Utils.error("Handshake error " + remotePeerId.toBase58());
                throw new ConnectException("Handshake error");
            }
        } catch (InterruptedException interruptedException) {
            clientConnection.abortHandshake();
            throw new InterruptedException("Interrupt exception occur");  // Should not happen.
        }
        if (keepAlive) {
            clientConnection.enableKeepAlive();
        }
        Utils.error("Connection success " + remotePeerId.toBase58());
        return clientConnection;

    }

    private void startHandshake() throws ConnectException {
        generateInitialKeys();


        // start the services
        if (serverConnector == null) {
            receiver.start();
        } else {
            byte[] scid = connectionIdManager.initialSourceCid();
            byte[] shortScid = Arrays.copyOf(scid, 4);
            serverConnector.registerConnection(this, scid);
            serverConnector.registerConnection(this, shortScid);
        }
        startRequester();

        try {
            tlsEngine.startHandshake();
        } catch (Throwable throwable) {
            // Will not happen, as our ClientMessageSender implementation will not throw.
            throw new ConnectException("Handshake start failed " + throwable.getMessage());
        }
    }

    @NotNull
    @Override
    public ALPN alpn() {
        return alpn;
    }

    @NotNull
    public Peeraddr remotePeeraddr() {
        return Peeraddr.create(remotePeerId(), remoteAddress());
    }

    @NotNull
    public PeerId remotePeerId() {
        return remotePeerId;
    }

    private void abortHandshake() {
        connectionState.set(Status.Failed);
        clearRequests();
        terminate();
    }

    private void generateInitialKeys() {
        computeInitialKeys(connectionIdManager.initialDestinationCid());
    }


    @Override
    public void process(PacketReceived packetReceived, long time) {
        switch (packetReceived.level()) {
            case Handshake -> processHandshake(packetReceived, time);
            case Initial -> processInitial(packetReceived, time);
            case App -> processShortHeader(packetReceived, time);
        }
    }

    private void processInitial(PacketReceived packetReceived, long time) {
        if (!packetReceived.version().equals(version)) {
            throw new IllegalStateException("Versions are different " + packetReceived.version());
        }
        connectionIdManager.registerInitialRemoteCid(((PacketReceived.Initial) packetReceived).sourceConnectionId());
        processFrames(packetReceived, time);
    }


    private void processHandshake(PacketReceived packetReceived, long time) {
        processFrames(packetReceived, time);
    }


    private void processShortHeader(PacketReceived packetReceived, long time) {
        connectionIdManager.registerCidInUse(packetReceived.destinationConnectionId());
        processFrames(packetReceived, time);
    }


    @Override
    public void process(FrameReceived.HandshakeDoneFrame handshakeDoneFrame, PacketReceived packetReceived) {

        HandshakeState state = handshakeState.updateAndGet(handshakeState -> {
            if (handshakeState.transitionAllowed(HandshakeState.Confirmed)) {
                return HandshakeState.Confirmed;
            }
            return handshakeState;
        });

        if (state == HandshakeState.Confirmed) {
            handshakeStateChangedEvent(HandshakeState.Confirmed);
        } else {
            throw new IllegalStateException("Handshake state cannot be set to Confirmed");
        }

        discard(Level.Handshake);

        // https://tools.ietf.org/html/draft-ietf-quic-tls-32#section-4.9.2
        // "An endpoint MUST discard its handshake keys when the TLS handshake is confirmed"
        // 4.9.2. Discarding Handshake Keys
        // An endpoint MUST discard its handshake keys when the TLS handshake is confirmed
        // (Section 4.1.2).
        discardHandshakeKeys();

    }

    @Override
    void process(FrameReceived.NewConnectionIdFrame newConnectionIdFrame, PacketReceived packetReceived) {
        connectionIdManager.process(newConnectionIdFrame);
    }


    @Override
    void process(FrameReceived.RetireConnectionIdFrame retireConnectionIdFrame, PacketReceived packetReceived) {
        connectionIdManager.process(retireConnectionIdFrame, packetReceived.destinationConnectionId());
    }

    @Override
    public void immediateCloseWithError(Level level, TransportError transportError) {
        super.immediateCloseWithError(level, transportError);
    }

    @Override
    public void parsePackets(long timeReceived, ByteBuffer data) {
        parseAndProcessPackets(timeReceived, data);
    }

    @NotNull
    @Override
    Function<Stream, StreamHandler> getStreamHandler() {
        return streamHandlerFunction;
    }

    /**
     * Closes the connection by discarding all connection state. Do not call directly, should be called after
     * closing state or draining state ends.
     */
    @Override
    public void terminate() {
        super.terminate();
        handshakeFinishedCondition.countDown();

        if (serverConnector == null) {
            receiver.shutdown();
            datagramSocket.close();
        } else {
            byte[] scid = connectionIdManager.initialSourceCid();
            byte[] shortScid = Arrays.copyOf(scid, 4);
            serverConnector.deregisterConnection(this, scid);
            serverConnector.deregisterConnection(this, shortScid);
        }
    }

    private void validateAndProcess(TransportParameters remoteTransportParameters) {

        if (remoteTransportParameters.maxUdpPayloadSize() < 1200) {
            immediateCloseWithError(Handshake, new TransportError(TRANSPORT_PARAMETER_ERROR,
                    "maxUdpPayloadSize transport parameter is invalid"));
            return;
        }
        if (remoteTransportParameters.ackDelayExponent() > 20) {
            immediateCloseWithError(Handshake, new TransportError(TRANSPORT_PARAMETER_ERROR,
                    "ackDelayExponent transport parameter is invalid"));
            return;
        }
        if (remoteTransportParameters.maxAckDelay() > 16384) { // 16384 = 2^14 ()
            immediateCloseWithError(Handshake, new TransportError(TRANSPORT_PARAMETER_ERROR,
                    "maxAckDelay value of 2^14 or greater are invalid"));
            return;
        }
        if (remoteTransportParameters.activeConnectionIdLimit() < 2) {
            immediateCloseWithError(Handshake, new TransportError(TRANSPORT_PARAMETER_ERROR,
                    "activeConnectionIdLimit transport parameter is invalid"));
            return;
        }


        // https://tools.ietf.org/html/draft-ietf-quic-transport-29#section-7.3
        // "An endpoint MUST treat absence of the initial_source_connection_id
        //   transport parameter from either endpoint or absence of the
        //   original_destination_connection_id transport parameter from the
        //   server as a connection error of payloadType TRANSPORT_PARAMETER_ERROR."
        if (remoteTransportParameters.initialSourceConnectionId() == null ||
                remoteTransportParameters.originalDestinationConnectionId() == null) {

            if (remoteTransportParameters.initialSourceConnectionId() == null) {
                immediateCloseWithError(Handshake, new TransportError(TRANSPORT_PARAMETER_ERROR,
                        "missing initial_source_connection_id transport parameter"));
            } else {
                immediateCloseWithError(Handshake, new TransportError(TRANSPORT_PARAMETER_ERROR,
                        "missing original_destination_connection_id transport parameter"));
            }
            return;
        }

        // https://tools.ietf.org/html/draft-ietf-quic-transport-29#section-7.3
        // "An endpoint MUST treat the following as a connection error of payloadType TRANSPORT_PARAMETER_ERROR or PROTOCOL_VIOLATION:
        //   *  a mismatch between values received from a peer in these transport parameters and the value sent in the
        //      corresponding Destination or Source Connection ID fields of Initial packets."
        if (!Arrays.equals(connectionIdManager.initialDestinationCid(),
                remoteTransportParameters.initialSourceConnectionId())) {
            immediateCloseWithError(Handshake, new TransportError(PROTOCOL_VIOLATION,
                    "initial_source_connection_id transport parameter does not match"));
            return;
        }
        if (!Arrays.equals(connectionIdManager.originalDestinationCid(),
                remoteTransportParameters.originalDestinationConnectionId())) {
            immediateCloseWithError(Handshake, new TransportError(PROTOCOL_VIOLATION,
                    "original_destination_connection_id transport parameter does not match"));
            return;
        }


        TransportParameters.VersionInformation versionInformation = remoteTransportParameters.versionInformation();
        if (versionInformation != null) {
            if (!versionInformation.chosenVersion().equals(version)) {
                // https://www.ietf.org/archive/id/draft-ietf-quic-version-negotiation-08.html
                // "clients MUST validate that the server's Chosen Version is equal to the negotiated version; if they do not
                //  match, the client MUST close the connection with a version negotiation error. "

                immediateCloseWithError(Handshake,
                        new TransportError(VERSION_NEGOTIATION_ERROR,
                                "Chosen version does not match packet version"));
                return;
            }
        }

        this.remoteTransportParameters.set(remoteTransportParameters);


        init(remoteTransportParameters.initialMaxData(),
                remoteTransportParameters.initialMaxStreamDataBidiLocal(),
                remoteTransportParameters.initialMaxStreamDataBidiRemote(),
                remoteTransportParameters.initialMaxStreamDataUni()
        );


        setInitialMaxStreamsBidi(remoteTransportParameters.initialMaxStreamsBidi());
        setInitialMaxStreamsUni(remoteTransportParameters.initialMaxStreamsUni());

        remoteMaxAckDelay = remoteTransportParameters.maxAckDelay();
        connectionIdManager.remoteCidLimit(remoteTransportParameters.activeConnectionIdLimit());

        determineIdleTimeout(transportParams.maxIdleTimeout(), remoteTransportParameters.maxIdleTimeout());

        connectionIdManager.initialStatelessResetToken(remoteTransportParameters.statelessResetToken());


        if (remoteTransportParameters.retrySourceConnectionId() != null) {
            immediateCloseWithError(Handshake, new TransportError(TRANSPORT_PARAMETER_ERROR,
                    "unexpected retry_source_connection_id transport parameter"));
        }
    }


    /**
     * Abort connection due to a fatal error in this client. No message is sent to peer; just inform client it's all over.
     *
     * @param error the exception that caused the trouble
     */
    @Override
    void abortConnection(Throwable error) {

        connectionState.set(Status.Closing);

        Utils.error("Aborting client connection " +
                remoteAddress().toString() +
                " because of error " + error.getMessage());

        handshakeFinishedCondition.countDown();
        clearRequests();
        terminate();
    }


    @Override
    boolean checkForStatelessResetToken(ByteBuffer data) {
        byte[] tokenCandidate = new byte[16];
        data.position(data.limit() - 16);
        data.get(tokenCandidate);
        return connectionIdManager.isStatelessResetToken(tokenCandidate);
    }

    @Override
    int sourceCidLength() {
        return connectionIdManager.getCidLength();
    }


    @Override
    byte[] activeSourceCid() {
        return connectionIdManager.activeSourceCid();
    }


    @Override
    byte[] activeDestinationCid() {
        return connectionIdManager.activeDestinationCid();
    }

    private void validateALPN(String[] protocols) {
        for (String protocol : protocols) {
            if (Objects.equals(protocol, alpn.name())) {
                return; // done all good
            }
        }
        immediateCloseWithError(Handshake, new TransportError(TRANSPORT_PARAMETER_ERROR,
                "alpn protocol parameter is invalid"));
    }

    private class StatusEventHandler implements TlsStatusEventHandler {

        @Override
        public void handshakeSecretsKnown() {
            // Server Hello provides a new secret, so:
            computeHandshakeSecrets(tlsEngine, tlsEngine.getSelectedCipher());
            HandshakeState state = handshakeState.updateAndGet(handshakeState -> {
                if (handshakeState.transitionAllowed(HandshakeState.HasHandshakeKeys)) {
                    return HandshakeState.HasHandshakeKeys;
                }
                return handshakeState;
            });

            if (state == HandshakeState.HasHandshakeKeys) {
                handshakeStateChangedEvent(HandshakeState.HasHandshakeKeys);
            } else {
                throw new IllegalStateException("Handshake state cannot be set to HasHandshakeKeys");
            }
        }

        @Override
        public void handshakeFinished() {
            // note this is not 100% correct, it discards only when handshake is finished,
            // not when the first handshake message is written [but fine for now !!!]

            // https://tools.ietf.org/html/draft-ietf-quic-tls-29#section-4.11.1
            // "Thus, a client MUST discard Initial keys when it first sends a Handshake packet (...).
            // This results in abandoning loss recovery state for the Initial encryption level and
            // ignoring any outstanding Initial packets."
            discard(Level.Initial);

            // https://tools.ietf.org/html/draft-ietf-quic-tls-32#section-4.9.1
            // -> (Thus, a client MUST discard Initial keys when it first sends a Handshake)
            // 4.9.1. Discarding Initial Keys
            // Packets protected with Initial secrets (Section 5.2) are not authenticated,
            // meaning that an attacker could spoof packets with the intent to disrupt a connection.
            // To limit these attacks, Initial packet protection keys are discarded more aggressively
            // than other keys.
            //
            // The successful use of Handshake packets indicates that no more Initial packets need to
            // be exchanged, as these keys can only be produced after receiving all CRYPTO frames from
            // Initial packets. Thus, a client MUST discard Initial keys when it first sends a
            // Handshake packet and a server MUST discard Initial keys when it first successfully
            // processes a Handshake packet. Endpoints MUST NOT send Initial packets after this point.
            //
            // This results in abandoning loss recovery state for the Initial encryption level and
            // ignoring any outstanding Initial packets.
            discardInitialKeys();


            computeApplicationSecrets(tlsEngine, tlsEngine.getSelectedCipher());

            HandshakeState state = handshakeState.updateAndGet(handshakeState -> {
                if (handshakeState.transitionAllowed(HandshakeState.HasAppKeys)) {
                    return HandshakeState.HasAppKeys;
                }
                return handshakeState;
            });

            if (state == HandshakeState.HasAppKeys) {
                handshakeStateChangedEvent(HandshakeState.HasAppKeys);
            } else {
                throw new IllegalStateException("Handshake state cannot be set to HasAppKeys");
            }


            connectionState.set(Status.Connected);
            handshakeFinishedCondition.countDown();
        }


        @Override
        public void extensionsReceived(Extension[] extensions) {
            for (Extension ex : extensions) {
                if (ex instanceof EarlyDataExtension) {
                    Utils.error("Server has accepted early data. Should not happen");
                } else if (ex instanceof TransportParametersExtension transportParametersExtension) {
                    validateAndProcess(transportParametersExtension.getTransportParameters());
                } else if (ex instanceof ApplicationLayerProtocolNegotiationExtension alpnExtension) {
                    validateALPN(alpnExtension.protocols());
                } else {
                    Utils.error("not handled extension received " + ex.toString());
                }
            }
        }
    }

    private class CryptoMessageSender implements ClientMessageSender {
        @Override
        public void send(ClientHello clientHello) {
            CryptoStream cryptoStream = getCryptoStream(Initial);
            cryptoStream.write(clientHello);
            flush();
            connectionState.set(Status.Handshaking);
        }

        @Override
        public void send(FinishedMessage finished) {
            CryptoStream cryptoStream = getCryptoStream(Handshake);
            cryptoStream.write(finished);
            flush();
        }

        @Override
        public void send(CertificateMessage certificateMessage) {
            CryptoStream cryptoStream = getCryptoStream(Handshake);
            cryptoStream.write(certificateMessage);
            flush();
        }

        @Override
        public void send(CertificateVerifyMessage certificateVerifyMessage) {
            CryptoStream cryptoStream = getCryptoStream(Handshake);
            cryptoStream.write(certificateVerifyMessage);
            flush();
        }
    }

    private final class DatagramPacketConsumer implements Consumer<DatagramPacket> {

        @Override
        public void accept(DatagramPacket datagramPacket) {
            try {
                parsePackets(System.currentTimeMillis(), ByteBuffer.wrap(datagramPacket.getData(),
                        0, datagramPacket.getLength()));

            } catch (Exception error) {
                abortConnection(error);
            }
        }
    }

    private final class ThrowableConsumer implements Consumer<Throwable> {
        @Override
        public void accept(Throwable throwable) {
            abortConnection(throwable);
        }
    }

}
